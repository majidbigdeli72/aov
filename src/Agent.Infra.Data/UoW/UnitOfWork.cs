﻿using Agent.Domain.Interfaces;
using Agent.Infra.Data.Context;

namespace Agent.Infra.Data.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AloVoipContext _context;

        public UnitOfWork(AloVoipContext context)
        {
            _context = context;
        }

        public bool Commit()
        {
            return _context.SaveChanges() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
