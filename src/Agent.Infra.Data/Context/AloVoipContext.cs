﻿using Agent.Application.Models;
using Agent.Domain.Interfaces;
using Agent.Domain.Models;
using Agent.Domain.Models.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;

using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Data;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Agent.Infra.Data.Context
{
    public class AloVoipContext : IdentityDbContext<ApplicationUser, ApplicationRole, int, UserClaim, UserRole, UserLogin, RoleClaim, UserToken>, IAloVoipContext
    {

        public AloVoipContext()
        {
        }

        public DbSet<Customer> Customers { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // get the configuration from the app settings
            var config = new ConfigurationBuilder()
                .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            
            // define the database to use
            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
        }


        public virtual DbSet<Call> Calls { get; set; }
        public virtual DbSet<CallChannel> CallChannels { get; set; }
        public virtual DbSet<LookupSource> LookupSources { get; set; }
        public virtual DbSet<PayamGostarLookupSource> PayamGostarLookupSources { get; set; }
        public virtual DbSet<Module> Modules { get; set; }
        public virtual DbSet<ModuleCheckContract> ModuleCheckContracts { get; set; }
        public virtual DbSet<ModuleCardtableConnect> ModuleCardtableConnects { get; set; }
        public virtual DbSet<ModuleQueueOperatorAnnouncement> ModuleQueueOperatorAnnouncements { get; set; }
        public virtual DbSet<ModuleQueueOperatorVoting> ModuleQueueOperatorVotings { get; set; }
        //public virtual IDbSet<ModuleQueueOperatorVotingItem> ModuleQueueOperatorVotingItems { get; set; }
        public virtual DbSet<ModuleQueueOperatorVotingResult> ModuleQueueOperatorVotingResults { get; set; }
        public virtual DbSet<Operator> Operators { get; set; }
        public virtual DbSet<Peer> Peers { get; set; }
        public DbSet<ExtentionHistory> ExtentionHistories { get; set; }
        public virtual DbSet<Queue> Queues { get; set; }
        public DbSet<QueueMember> QueueMembers { get; set; }
        public virtual DbSet<QueueCaller> QueueCallers { get; set; }
        public virtual DbSet<Department> Departments { get; set; }
        public virtual DbSet<OperatorDepartmentPermission> OperatorDepartmentPermissions { get; set; }

       // public virtual IDbSet<TelephonySystem> TelephonySystems { get; set; }
        public virtual DbSet<TelephonySystemSetting> TelephonySystemSettings { get; set; }
        public virtual DbSet<ModuleReconnect> ModuleReconnects { get; set; }
        public virtual DbSet<ModuleCustomerClub> ModuleCustomerClubs { get; set; }
        public virtual DbSet<ModuleReconnectScopeItem> ModuleReconnectScopeItems { get; set; }
        public virtual DbSet<ModuleRoleConnect> ModuleRoleConnects { get; set; }
        public virtual DbSet<ModulePay> ModulePays { get; set; }
        public DbSet<TelephonySystemLookupSource> TelephonySystemLookupSources { get; set; }
        public DbSet<ModuleAdvanceFollowMe> ModuleAdvanceFollowMe { get; set; }
        public DbSet<ModuleAdvanceFollowMeResult> ModuleAdvanceFollowMeResult { get; set; }

        public virtual DbSet<ModuleVoting> ModuleVotings { get; set; }
        public virtual DbSet<ModuleVotingQuestion> ModuleVotingQuestions { get; set; }
        public virtual DbSet<ModuleVotingQuestionItem> ModuleVotingQuestionItems { get; set; }
        public virtual DbSet<ModuleVotingQuestionResult> ModuleVotingQuestionResults { get; set; }

        public virtual DbSet<TeleMarketing> TeleMarketings { get; set; }
        public virtual DbSet<TeleMarketingTarget> TeleMarketingTargets { get; set; }

        public virtual DbSet<GeneralProperty> GeneralPropertys { get; set; }
        public virtual DbSet<GeneralPropertyItem> GeneralPropertyItems { get; set; }

        public virtual DbSet<OperatorSos> OperatorSos { get; set; }

        public virtual DbSet<DepartmentTelephonySystemHelperExtension> DepartmentTelephonySystemHelperExtensions { get; set; }
        public virtual DbSet<TelephonySystem> TelephonySystems { get ; set ; }

        //  public virtual IDbSet<ApplicationUser> Users { get ; set ; }

        public DashboardStatisticsModel GetDashboardData(DateTime fromDate, int? queueId = null)
        {
            //var spParams = new SqlParameter[]
            //{
            //    new SqlParameter("FromDate", fromDate),
            //    new SqlParameter("QueueId", queueId ?? DBNull.Value).is
            //};
            var pFromDate = new SqlParameter("FromDate", fromDate);
            var pQueueId = new SqlParameter("QueueId", System.Data.SqlDbType.Int);
            pQueueId.IsNullable = true;
            pQueueId.Value = (object)queueId ?? DBNull.Value;

            return this.Set<DashboardStatisticsModel>().FromSqlRaw("spGetDashboardStatistics @FromDate, @QueueId", pFromDate, pQueueId).Single();
        }

        public List<MemberStatisticsModel> GetMemberStatistics(DateTime fromDate, int? queueId = null)
        {
            var pFromDate = new SqlParameter("FromDate", fromDate);
            var pQueueId = new SqlParameter("QueueId", System.Data.SqlDbType.Int);
            pQueueId.IsNullable = true;
            pQueueId.Value = (object)queueId ?? DBNull.Value;
           return this.Set<MemberStatisticsModel>().FromSqlRaw("spGetMemberStatistics @FromDate, @QueueId", pFromDate, pQueueId).ToList();
           // return this.Database.SqlQuery<MemberStatisticsModel>("spGetMemberStatistics @FromDate, @QueueId", pFromDate, pQueueId).ToList();
        }

        public List<CallReportModel> GetCallReport(int tsId,
                                                   int operatorId,
                                                   int? callDirectionIndex = null,
                                                   int? callTypeIndex = null,
                                                   int? callStatus = null,
                                                   DateTime? callStartDateFrom = null,
                                                   DateTime? callStartDateTo = null,
                                                   DateTime? callEndDateFrom = null,
                                                   DateTime? callEndDateTo = null,
                                                   string callDurationFrom = null,
                                                   string callDurationTo = null,
                                                   string callWaitingFrom = null,
                                                   string callWaitingTo = null,
                                                   string phoneNumber = null,
                                                   string callerIdNum = null,
                                                   string callerIdName = null,
                                                   string dialedExtension = null,
                                                   int? callChannelState = null,
                                                   int? queueId = null,
                                                   int? extensionPeerId = null,
                                                   bool isExtensionStarter = false,
                                                   int? extensionResponseIndex = null,
                                                   int? trunkPeerId = null,
                                                   bool isTrunkStarter = false,
                                                   int? trunkResponseIndex = null,
                                                   int pageNumber = 1,
                                                   int pageSize = 30,
                                                   bool isExcel = false)
        {
            var sql = "spCallReport @TsId, @OperatorId, @CallDirectionIndex, @CallTypeIndex, @CallStatus, @CallStartDateFrom, @CallStartDateTo, @CallEndDateFrom, @CallEndDateTo, @CallDurationFrom, @CallDurationTo, @CallWaitingFrom, @CallWaitingTo, @PhoneNumber, @CallerIdNum, @CallerIdName, @DialedExtension, @CallChannelState, @QueueId, @ExtensionPeerId, @IsExtensionStarter, @ExtensionResponseIndex, @TrunkPeerId, @IsTrunkStarter, @TrunkResponseIndex, @PageNumber, @PageSize, @IsExcel";

            var cmd = this.Database.GetDbConnection().CreateCommand();
            cmd.CommandText = sql;

            var pTsId = new SqlParameter("TsId", tsId);
            cmd.Parameters.Add(pTsId);

            var pOperatorId = new SqlParameter("OperatorId", System.Data.SqlDbType.Int)
            {
                IsNullable = false,
                Value = (object)operatorId
            };
            cmd.Parameters.Add(pOperatorId);

            var pCallDirectionIndex = new SqlParameter("CallDirectionIndex", System.Data.SqlDbType.Int)
            {
                IsNullable = true,
                Value = (object)callDirectionIndex ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallDirectionIndex);

            var pCallTypeIndex = new SqlParameter("CallTypeIndex", System.Data.SqlDbType.Int)
            {
                IsNullable = true,
                Value = (object)callTypeIndex ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallTypeIndex);

            var pCallStatus = new SqlParameter("CallStatus", System.Data.SqlDbType.Int)
            {
                IsNullable = true,
                Value = (object)callStatus ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallStatus);

            var pCallStartDateFrom = new SqlParameter("CallStartDateFrom", System.Data.SqlDbType.DateTime)
            {
                IsNullable = true,
                Value = (object)callStartDateFrom ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallStartDateFrom);

            var pCallStartDateTo = new SqlParameter("CallStartDateTo", System.Data.SqlDbType.DateTime)
            {
                IsNullable = true,
                Value = (object)callStartDateTo ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallStartDateTo);

            var pCallEndDateFrom = new SqlParameter("CallEndDateFrom", System.Data.SqlDbType.DateTime)
            {
                IsNullable = true,
                Value = (object)callEndDateFrom ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallEndDateFrom);

            var pCallEndDateTo = new SqlParameter("CallEndDateTo", System.Data.SqlDbType.DateTime)
            {
                IsNullable = true,
                Value = (object)callEndDateTo ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallEndDateTo);

            var pCallDurationFrom = new SqlParameter("CallDurationFrom", System.Data.SqlDbType.VarChar)
            {
                IsNullable = true,
                Value = (object)callDurationFrom ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallDurationFrom);

            var pCallDurationTo = new SqlParameter("CallDurationTo", System.Data.SqlDbType.VarChar)
            {
                IsNullable = true,
                Value = (object)callDurationTo ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallDurationTo);

            var pCallWaitingFrom = new SqlParameter("CallWaitingFrom", System.Data.SqlDbType.VarChar)
            {
                IsNullable = true,
                Value = (object)callWaitingFrom ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallWaitingFrom);

            var pCallWaitingTo = new SqlParameter("CallWaitingTo", System.Data.SqlDbType.VarChar)
            {
                IsNullable = true,
                Value = (object)callWaitingTo ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallWaitingTo);

            var pPhoneNumber = new SqlParameter("PhoneNumber", System.Data.SqlDbType.VarChar)
            {
                IsNullable = true,
                Value = (object)phoneNumber ?? DBNull.Value
            };
            cmd.Parameters.Add(pPhoneNumber);

            var pCallerIdNum = new SqlParameter("CallerIdNum", System.Data.SqlDbType.VarChar)
            {
                IsNullable = true,
                Value = (object)callerIdNum ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallerIdNum);

            var pCallerIdName = new SqlParameter("CallerIdName", System.Data.SqlDbType.VarChar)
            {
                IsNullable = true,
                Value = (object)callerIdName ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallerIdName);

            var pDialedExtension = new SqlParameter("DialedExtension", System.Data.SqlDbType.VarChar)
            {
                IsNullable = true,
                Value = (object)dialedExtension ?? DBNull.Value
            };
            cmd.Parameters.Add(pDialedExtension);

            var pCallChannelState = new SqlParameter("CallChannelState", System.Data.SqlDbType.Int)
            {
                IsNullable = true,
                Value = (object)callChannelState ?? DBNull.Value
            };
            cmd.Parameters.Add(pCallChannelState);

            var pQueueId = new SqlParameter("QueueId", System.Data.SqlDbType.Int)
            {
                IsNullable = true,
                Value = (object)queueId ?? DBNull.Value
            };
            cmd.Parameters.Add(pQueueId);

            var pExtensionPeerId = new SqlParameter("ExtensionPeerId", System.Data.SqlDbType.Int)
            {
                IsNullable = true,
                Value = (object)extensionPeerId ?? DBNull.Value
            };
            cmd.Parameters.Add(pExtensionPeerId);

            var pIsExtensionStarter = new SqlParameter("IsExtensionStarter", isExtensionStarter);
            cmd.Parameters.Add(pIsExtensionStarter);

            var pExtensionResponseIndex = new SqlParameter("ExtensionResponseIndex", System.Data.SqlDbType.Int)
            {
                IsNullable = true,
                Value = (object)extensionResponseIndex ?? DBNull.Value
            };
            cmd.Parameters.Add(pExtensionResponseIndex);

            var pTrunkPeerId = new SqlParameter("TrunkPeerId", System.Data.SqlDbType.Int)
            {
                IsNullable = true,
                Value = (object)trunkPeerId ?? DBNull.Value
            };
            cmd.Parameters.Add(pTrunkPeerId);

            var pIsTrunkStarter = new SqlParameter("IsTrunkStarter", isTrunkStarter);
            cmd.Parameters.Add(pIsTrunkStarter);

            var pTrunkResponseIndex = new SqlParameter("TrunkResponseIndex", System.Data.SqlDbType.Int)
            {
                IsNullable = true,
                Value = (object)trunkResponseIndex ?? DBNull.Value
            };
            cmd.Parameters.Add(pTrunkResponseIndex);

            var pPageNumber = new SqlParameter("PageNumber", pageNumber);
            cmd.Parameters.Add(pPageNumber);

            var pPageSize = new SqlParameter("PageSize", pageSize);
            cmd.Parameters.Add(pPageSize);

            var pIsExcel = new SqlParameter("IsExcel", isExcel);
            cmd.Parameters.Add(pIsExcel);

            try
            {
                this.Database.GetDbConnection().Open();
                var reader = cmd.ExecuteReader();
                var calls = new List<CallReportModel>();
                //var calls = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)this).ObjectContext.Translate<CallReportModel>(reader).ToList();
                //reader.NextResult();
                //var totalRows = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)this).ObjectContext.Translate<int>(reader).First();

                //if (calls != null && calls.Count > 0)
                //    calls[0].TotalRows = totalRows;

                return calls;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in AloVoIPContext GetCallReport.");
                throw;
            }
            finally
            {
                this.Database.GetDbConnection().Close();
            }
        }

        public List<AgentReportModel> GetAgentReport(int tsId, DateTime? StartDate, DateTime? EndDate, DataTable extentions, int deparmentId)
        {
            var sql = "ExtentionReport @StartDate, @EndDate, @TsId, @Extentions, @DepartmentId";


            try
            {
                var pStartDate = new SqlParameter("StartDate", System.Data.SqlDbType.DateTime)
                {
                    IsNullable = true,
                    Value = (object)StartDate ?? DBNull.Value
                };
                var pEndDate = new SqlParameter("EndDate", System.Data.SqlDbType.DateTime)
                {
                    IsNullable = true,
                    Value = (object)EndDate ?? DBNull.Value
                };
                var pTsId = new SqlParameter("TsId", tsId);

                var pExtentions = new SqlParameter("Extentions", System.Data.SqlDbType.Structured)
                {
                    Value = (object)extentions,
                    TypeName = "ExtentionList"
                };
                var pDepartmentId = new SqlParameter("DepartmentId", deparmentId);

                return this.Set<AgentReportModel>().FromSqlRaw(sql, pStartDate, pEndDate, pTsId , pExtentions, pDepartmentId).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in AloVoIPContext GetAgentReport");
                throw;
            }
        }

        public List<ExtentionModel> GetExtentionByDepartmentId(int tsId, int? departmentId)
        {
            var sql = "GetExtentionByDepartmentId @DepartmentId, @TsId";
            try
            {
                var pDepartmentId = new SqlParameter("DepartmentId", System.Data.SqlDbType.Int)
                {
                    IsNullable = true,
                    Value = (object)departmentId ?? DBNull.Value
                };
                var pTsId = new SqlParameter("TsId", tsId);
                return this.Set<ExtentionModel>().FromSqlRaw(sql, pDepartmentId, pTsId).ToList();               
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in AloVoIPContext GetExtentionByDepartmentId.");
                throw;
            }
        }

        public List<Department> GetDepartment()
        {
            var sql = "GetDeparment";      
            try
            {
                return this.Set<Department>().FromSqlRaw(sql).ToList();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in AloVoIPContext GetDepartment.");
                throw;
            }
        }


        //protected override void OnModelCreating(ModelBuilder modelBuilder)
        //{
        //    modelBuilder.ApplyConfiguration(new CustomerMap());


        //}



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Call>()
                .Property(e => e.PhoneNumber)
                .IsUnicode(false);

            modelBuilder.Entity<Call>()
                .HasMany(e => e.CallChannels)
                .WithOne(e => e.Call)
                .IsRequired()
                .HasForeignKey(e => e.CallId)
                .OnDelete(DeleteBehavior.Restrict); 

            modelBuilder.Entity<CallChannel>()
                .Property(e => e.UniqueId)
                .IsUnicode(false);

            modelBuilder.Entity<CallChannel>()
                .HasIndex(x => new { x.State, x.Response, x.CreateDate })
                .HasName("IX_CallChannel_State_Response_CreateDate")
                .IsUnique(false)
                .IsClustered(false);

            modelBuilder.Entity<CallChannel>()
                .HasIndex(x => x.CallId)
                .HasName("IX_CallId")
                .IsUnique(false)
                .IsClustered(false);

            modelBuilder.Entity<CallChannel>()
                .HasIndex(x => x.PeerId)
                .HasName("IX_PeerId")
                .IsUnique(false)
                .IsClustered(false);

            //modelBuilder.Entity<CallChannel>()
            //    .Property(e => e.PhoneNumber)
            //    .IsUnicode(false);

            modelBuilder.Entity<CallChannel>()
                .HasMany(e => e.Calls)
                .WithOne(e => e.InitByChannel)
                .HasForeignKey(e => e.InitByChannelId);

            modelBuilder.Entity<CallChannel>()
                .HasMany(e => e.QueueEntries)
                .WithOne(e => e.AnsweredByCallChannel)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Module>()
                .HasOne(x => x.LookupSource);

            modelBuilder.Entity<Module>()
                .Property(e => e.Code)
                .IsUnicode(false);

            /*modelBuilder.Entity<ModuleQueueOperatorVoting>()
                .HasMany(e => e.ModuleQueueOperatorVotingItems)
                .WithRequired(e => e.ModuleQueueOperatorVoting)
                .HasForeignKey(e => e.ModuleQueueOperatorVotingId)
                .WillCascadeOnDelete(false);*/

            modelBuilder.Entity<ModuleQueueOperatorVoting>()
                .HasMany(e => e.ModuleQueueOperatorVotingResults)
                .WithOne(e => e.ModuleQueueOperatorVoting)
                .IsRequired()
                .HasForeignKey(e => e.ModuleQueueOperatorVotingId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ModuleQueueOperatorVotingResult>()
                .Property(e => e.Input)
                .IsUnicode(false);

            modelBuilder.Entity<Operator>()
                .HasMany(e => e.Peers)
                .WithOne(e => e.Operator)
                .HasForeignKey(e => e.OwnerOperatorId);

            //modelBuilder.Entity<Operator>()
            //    .HasOne(e => e.User);
                            
               

            modelBuilder.Entity<Peer>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Peer>()
                .HasMany(e => e.Calls)
                .WithOne(e => e.InitByPeer)
                .HasForeignKey(e => e.InitByPeerId);

            modelBuilder.Entity<Peer>()
                .HasMany(e => e.CallChannels)
                .WithOne(e => e.Peer)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<TelephonySystemSetting>().HasKey(c => c.TelephonySystemId);

            modelBuilder.Entity<Peer>()
                .HasMany(e => e.QueueEntries)
                .WithOne(e => e.AnsweredByPeer)
                .HasForeignKey(e => e.AnsweredByPeerId);

            modelBuilder.Entity<Queue>()
                .Property(e => e.Code)
                .IsUnicode(false);

            modelBuilder.Entity<Queue>()
                .HasMany(e => e.QueueEntries)
                .WithOne(e => e.Queue)
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TelephonySystem>()
                .HasMany(e => e.Calls)
                .WithOne(e => e.TelephonySystem)
                .IsRequired()
                .HasForeignKey(e => e.TsId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<TelephonySystem>()
                .HasMany(e => e.Modules)
                .WithOne(e => e.TelephonySystem)
                .IsRequired()
                .HasForeignKey(e => e.TsId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TelephonySystem>()
                .HasMany(e => e.Peers)
                .WithOne(e => e.TelephonySystem)
                .IsRequired()
                .HasForeignKey(e => e.TsId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TelephonySystem>()
                .HasMany(e => e.Queues)
                .WithOne(e => e.TelephonySystem)
                .IsRequired()
                .HasForeignKey(e => e.TsId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ModuleReconnect>()
                .HasMany(e => e.ModuleReconnectScopeItems)
                .WithOne(e => e.ModuleReconnect)
                .IsRequired()
                .HasForeignKey(e => e.ModuleReconnectId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<ModuleAdvanceFollowMe>()
                .HasMany(e => e.ModuleAdvanceFollowMeResults)
                .WithOne(e => e.ModuleAdvancedFollowMe)
                .IsRequired()
                .HasForeignKey(e => e.ModuleAdvanceFollowMeId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<ModuleAdvanceFollowMeResult>();

            modelBuilder.Entity<Department>()
                .HasMany(x => x.Children)
                .WithOne(x => x.Parent)
                .HasForeignKey(x => x.ParentId);

            modelBuilder.Entity<TelephonySystem>()
                .HasMany(e => e.TeleMarketings)
                .WithOne(e => e.TelephonySystem)
                .IsRequired()
                .HasForeignKey(e => e.TsId)
                .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<TeleMarketing>()
                .HasMany(x => x.TeleMarketingTargets)
                .WithOne(x => x.TeleMarketing)
                .IsRequired()
                .HasForeignKey(x => x.TeleMarketingId);

            modelBuilder.Entity<TeleMarketing>()
                .Property(e => e.StartDate)
                .HasColumnType("datetime2");

            modelBuilder.Entity<TeleMarketingTarget>()
                .Property(e => e.NextRetryDate)
                .HasColumnType("datetime2");
                

            modelBuilder.Entity<TelephonySystem>()
                .HasOne(e => e.OperatorSos)
                .WithOne(e => e.TelephonySystem)
                .IsRequired()
                .HasForeignKey<OperatorSos>(c => c.TsId);

         //   modelBuilder.Query<MemberStatisticsModel>();

            modelBuilder.Entity<MemberStatisticsModel>().HasNoKey();

            modelBuilder.Entity<DepartmentTelephonySystemHelperExtension>().HasKey(x => new { x.DepartmentId, x.TsId });
            modelBuilder.Entity<TelephonySystemLookupSource>().HasKey(x => new { x.TelephonySystemId, x.LookupSourceId });

            //modelBuilder.Entity<ApplicationRole>().HasData(
            //    new ApplicationRole() { Id = (int)Domain.Enum.UserRole.Admin, Name = "Admin" },
            //    new ApplicationRole() { Id = (int)Domain.Enum.UserRole.Operator, Name = "Operator" },
            //    new ApplicationRole() { Id = (int)Domain.Enum.UserRole.VoipManager, Name = "VoipManager" }
            //    );
            //modelBuilder.Entity<ApplicationUser>().HasData(
            //     new ApplicationUser
            //     {
            //         Id = 1,
            //         AccessFailedCount = 4,
            //         EmailConfirmed = true,
            //         PasswordHash = new PasswordHasher<ApplicationUser>().HashPassword(new ApplicationUser() { Id = 1}, "aA123456"),
            //         PhoneNumber = "1234567890",
            //         PhoneNumberConfirmed = true,
            //         SecurityStamp = "@#$^&*",
            //         TwoFactorEnabled = false,
            //         UserName = "admin"
            //     }
            //    );

            //modelBuilder.Entity<Department>()
            //    .HasOptional(e => e..OperatorSos)
            //    .WithRequired(e => e.TelephonySystem);

            base.OnModelCreating(modelBuilder);

           // base.OnModelCreating(modelBuilder);
        }

        public override EntityEntry Entry(object entity)
        {
            return base.Entry(entity);
        }

        public override int SaveChanges()
        {
            var entities = from e in ChangeTracker.Entries()
                           where e.State == EntityState.Added
                               || e.State == EntityState.Modified
                           select e.Entity;
            foreach (var entity in entities)
            {
                var validationContext = new ValidationContext(entity);
                Validator.ValidateObject(entity, validationContext);
            }

            return base.SaveChanges();
        }



        //DbEntityEntry<TEntity> IAloVoipContext.Entry<TEntity>(TEntity entity)
        //{
        //    throw new NotImplementedException();
        //}

        //IDbSet<TEntity> Set<TEntity>()
        //{
        //    throw new NotImplementedException();
        //}
    }
}
