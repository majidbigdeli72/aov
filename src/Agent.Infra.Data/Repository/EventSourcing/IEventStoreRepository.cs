﻿using System;
using System.Collections.Generic;
using Agent.Domain.Core.Events;

namespace Agent.Infra.Data.Repository.EventSourcing
{
    public interface IEventStoreRepository : IDisposable
    {
        void Store(StoredEvent theEvent);
        IList<StoredEvent> All(Guid aggregateId);
    }
}