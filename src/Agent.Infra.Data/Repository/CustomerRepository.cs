﻿using System.Linq;
using Agent.Domain.Interfaces;
using Agent.Domain.Models;
using Agent.Infra.Data.Context;
using Microsoft.EntityFrameworkCore;

namespace Agent.Infra.Data.Repository
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        public CustomerRepository(AloVoipContext context)
            : base(context)
        {

        }

        public Customer GetByEmail(string email)
        {
            return DbSet.AsNoTracking().FirstOrDefault(c => c.Email == email);
        }
    }
}
