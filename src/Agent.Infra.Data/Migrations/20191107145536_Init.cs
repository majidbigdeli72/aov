﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Agent.Infra.Data.Migrations
{
    public partial class Init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AspNetRoles",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedName = table.Column<string>(maxLength: 256, nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUsers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedUserName = table.Column<string>(maxLength: 256, nullable: true),
                    Email = table.Column<string>(maxLength: 256, nullable: true),
                    NormalizedEmail = table.Column<string>(maxLength: 256, nullable: true),
                    EmailConfirmed = table.Column<bool>(nullable: false),
                    PasswordHash = table.Column<string>(nullable: true),
                    SecurityStamp = table.Column<string>(nullable: true),
                    ConcurrencyStamp = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true),
                    PhoneNumberConfirmed = table.Column<bool>(nullable: false),
                    TwoFactorEnabled = table.Column<bool>(nullable: false),
                    LockoutEnd = table.Column<DateTimeOffset>(nullable: true),
                    LockoutEnabled = table.Column<bool>(nullable: false),
                    AccessFailedCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUsers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    BirthDate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Department",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(maxLength: 200, nullable: false),
                    ParentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Department", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Department_Department_ParentId",
                        column: x => x.ParentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GeneralProperty",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(maxLength: 50, nullable: true),
                    Mame = table.Column<string>(maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralProperty", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "LookupSource",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 400, nullable: false),
                    LookupSourceTypeIndex = table.Column<int>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    Host = table.Column<string>(maxLength: 400, nullable: true),
                    Username = table.Column<string>(maxLength: 100, nullable: true),
                    Password = table.Column<string>(maxLength: 100, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_LookupSource", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "MemberStatisticsModel",
                columns: table => new
                {
                    OperatorId = table.Column<int>(nullable: true),
                    Ext = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    AnsweredCount = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "TelephonySystem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Key = table.Column<string>(maxLength: 50, nullable: false),
                    BrevityName = table.Column<string>(maxLength: 50, nullable: true),
                    UpdateSettingsIfNotExists = table.Column<bool>(nullable: false),
                    ServerAddress = table.Column<string>(maxLength: 400, nullable: false),
                    ServerPort = table.Column<int>(nullable: false),
                    AmiUsername = table.Column<string>(maxLength: 100, nullable: false),
                    AmiPassword = table.Column<string>(maxLength: 100, nullable: false),
                    LinuxUsername = table.Column<string>(maxLength: 100, nullable: false),
                    LinuxPassword = table.Column<string>(maxLength: 100, nullable: false),
                    OutgoingPrefix = table.Column<string>(maxLength: 10, nullable: true),
                    RecordingsRootPath = table.Column<string>(nullable: true),
                    CountryAreaCode = table.Column<string>(maxLength: 5, nullable: true),
                    CityAreaCode = table.Column<string>(maxLength: 5, nullable: true),
                    MySqlUsername = table.Column<string>(maxLength: 100, nullable: false),
                    MySqlPassword = table.Column<string>(maxLength: 100, nullable: false),
                    TelephonySystemTypeIndex = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TelephonySystem", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "AspNetRoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    RoleId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetRoleClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetRoleClaims_AspNetRoles_RoleId1",
                        column: x => x.RoleId1,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false),
                    ClaimType = table.Column<string>(nullable: true),
                    ClaimValue = table.Column<string>(nullable: true),
                    UserId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserClaims", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserClaims_AspNetUsers_UserId1",
                        column: x => x.UserId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(nullable: false),
                    ProviderKey = table.Column<string>(nullable: false),
                    ProviderDisplayName = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: false),
                    UserId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserLogins", x => new { x.LoginProvider, x.ProviderKey });
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserLogins_AspNetUsers_UserId1",
                        column: x => x.UserId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    RoleId = table.Column<int>(nullable: false),
                    UserId1 = table.Column<int>(nullable: true),
                    RoleId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId",
                        column: x => x.RoleId,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetRoles_RoleId1",
                        column: x => x.RoleId1,
                        principalTable: "AspNetRoles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserRoles_AspNetUsers_UserId1",
                        column: x => x.UserId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    LoginProvider = table.Column<string>(nullable: false),
                    Name = table.Column<string>(nullable: false),
                    Value = table.Column<string>(nullable: true),
                    UserId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserTokens", x => new { x.UserId, x.LoginProvider, x.Name });
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AspNetUserTokens_AspNetUsers_UserId1",
                        column: x => x.UserId1,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Operator",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GenderIndex = table.Column<int>(nullable: false),
                    FirstName = table.Column<string>(maxLength: 200, nullable: false),
                    LastName = table.Column<string>(maxLength: 200, nullable: false),
                    NameVoice = table.Column<byte[]>(nullable: true),
                    Avatar = table.Column<byte[]>(nullable: true),
                    UserId = table.Column<int>(nullable: true),
                    Mobile = table.Column<string>(maxLength: 11, nullable: true),
                    DepartmentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Operator", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Operator_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Operator_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "GeneralPropertyItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    GeneralPropertyId = table.Column<int>(nullable: false),
                    Index = table.Column<int>(nullable: false),
                    Key = table.Column<string>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    Attribute = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GeneralPropertyItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_GeneralPropertyItem_GeneralProperty_GeneralPropertyId",
                        column: x => x.GeneralPropertyId,
                        principalTable: "GeneralProperty",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OperatorSos",
                columns: table => new
                {
                    TsId = table.Column<int>(nullable: false),
                    RequestCode = table.Column<string>(maxLength: 20, nullable: false),
                    OperatorManagerLookupModeTypeIndex = table.Column<int>(nullable: false),
                    OperatorManagerLookupSourceId = table.Column<int>(nullable: true),
                    OperatorSosSentVoice = table.Column<byte[]>(nullable: true),
                    ManagerNotFoundVoice = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperatorSos", x => x.TsId);
                    table.ForeignKey(
                        name: "FK_OperatorSos_TelephonySystem_TsId",
                        column: x => x.TsId,
                        principalTable: "TelephonySystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Queue",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TsId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 200, nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    JoinPlayAlert = table.Column<bool>(nullable: false),
                    LeavePlayAlert = table.Column<bool>(nullable: false),
                    WaitTimeMinThreshold = table.Column<TimeSpan>(nullable: false),
                    WaitTimeCriticalThreshold = table.Column<TimeSpan>(nullable: false),
                    WaitTimeCriticalThresholdPlayAlert = table.Column<bool>(nullable: false),
                    WaitTimeMaxThreshold = table.Column<TimeSpan>(nullable: false),
                    WaitTimeMaxThresholdPlayAlert = table.Column<bool>(nullable: false),
                    RemoveFromAbandonedAfterCallback = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Queue", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Queue_TelephonySystem_TsId",
                        column: x => x.TsId,
                        principalTable: "TelephonySystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TeleMarketing",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TsId = table.Column<int>(nullable: false),
                    Name = table.Column<string>(maxLength: 300, nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    StartDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IntroVoice = table.Column<byte[]>(nullable: true),
                    MinimumSuccessDurationThreshold = table.Column<int>(nullable: false),
                    MaxConcurrentCall = table.Column<int>(nullable: false),
                    StartTime = table.Column<TimeSpan>(nullable: false),
                    EndTime = table.Column<TimeSpan>(nullable: false),
                    RequestResponeTypeIndex = table.Column<int>(nullable: false),
                    NextCode = table.Column<string>(maxLength: 50, nullable: true),
                    WaitBeforePlayIntroInSeconds = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeleMarketing", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TeleMarketing_TelephonySystem_TsId",
                        column: x => x.TsId,
                        principalTable: "TelephonySystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TelephonySystemLookupSource",
                columns: table => new
                {
                    TelephonySystemId = table.Column<int>(nullable: false),
                    LookupSourceId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TelephonySystemLookupSource", x => new { x.TelephonySystemId, x.LookupSourceId });
                    table.ForeignKey(
                        name: "FK_TelephonySystemLookupSource_LookupSource_LookupSourceId",
                        column: x => x.LookupSourceId,
                        principalTable: "LookupSource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TelephonySystemLookupSource_TelephonySystem_TelephonySystemId",
                        column: x => x.TelephonySystemId,
                        principalTable: "TelephonySystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TelephonySystemSetting",
                columns: table => new
                {
                    TelephonySystemId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Key = table.Column<string>(maxLength: 200, nullable: false),
                    Value = table.Column<string>(maxLength: 200, nullable: false),
                    TelephonySystemId1 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TelephonySystemSetting", x => x.TelephonySystemId);
                    table.UniqueConstraint("AK_TelephonySystemSetting_Key", x => x.Key);
                    table.ForeignKey(
                        name: "FK_TelephonySystemSetting_TelephonySystem_TelephonySystemId1",
                        column: x => x.TelephonySystemId1,
                        principalTable: "TelephonySystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "OperatorDepartmentPermission",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    OperatorId = table.Column<int>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: false),
                    CanListen = table.Column<bool>(nullable: false),
                    CanWhisper = table.Column<bool>(nullable: false),
                    CanConference = table.Column<bool>(nullable: false),
                    CanHangup = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperatorDepartmentPermission", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OperatorDepartmentPermission_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_OperatorDepartmentPermission_Operator_OperatorId",
                        column: x => x.OperatorId,
                        principalTable: "Operator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Peer",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TsId = table.Column<int>(nullable: false),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    PeerProtocolTypeIndex = table.Column<int>(nullable: false),
                    PeerTypeIndex = table.Column<int>(nullable: false),
                    OwnerOperatorId = table.Column<int>(nullable: true),
                    Enabled = table.Column<bool>(nullable: false),
                    DepartmentId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Peer", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Peer_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Peer_Operator_OwnerOperatorId",
                        column: x => x.OwnerOperatorId,
                        principalTable: "Operator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Peer_TelephonySystem_TsId",
                        column: x => x.TsId,
                        principalTable: "TelephonySystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "TeleMarketingTarget",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TeleMarketingId = table.Column<int>(nullable: false),
                    Number = table.Column<string>(maxLength: 50, nullable: true),
                    TeleMarketingStatusIndex = table.Column<int>(nullable: false),
                    Duration = table.Column<TimeSpan>(nullable: true),
                    RetryCount = table.Column<int>(nullable: false),
                    NextRetryDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    RequestResponeTypeIndex = table.Column<int>(nullable: false),
                    ResponsedDigit = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeleMarketingTarget", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TeleMarketingTarget_TeleMarketing_TeleMarketingId",
                        column: x => x.TeleMarketingId,
                        principalTable: "TeleMarketing",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DepartmentTelephonySystemHelperExtension",
                columns: table => new
                {
                    DepartmentId = table.Column<int>(nullable: false),
                    TsId = table.Column<int>(nullable: false),
                    HelperExtensionId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DepartmentTelephonySystemHelperExtension", x => new { x.DepartmentId, x.TsId });
                    table.ForeignKey(
                        name: "FK_DepartmentTelephonySystemHelperExtension_Department_DepartmentId",
                        column: x => x.DepartmentId,
                        principalTable: "Department",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DepartmentTelephonySystemHelperExtension_Peer_HelperExtensionId",
                        column: x => x.HelperExtensionId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_DepartmentTelephonySystemHelperExtension_TelephonySystem_TsId",
                        column: x => x.TsId,
                        principalTable: "TelephonySystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ExtentionHistory",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExtentionId = table.Column<long>(nullable: false),
                    QueueId = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false),
                    ExtentionActionTypeIndex = table.Column<int>(nullable: false),
                    ActionDescription = table.Column<string>(nullable: true),
                    ExtentionId1 = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExtentionHistory", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExtentionHistory_Peer_ExtentionId1",
                        column: x => x.ExtentionId1,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExtentionHistory_Queue_QueueId",
                        column: x => x.QueueId,
                        principalTable: "Queue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Module",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TsId = table.Column<int>(nullable: false),
                    LookupSourceId = table.Column<int>(nullable: true),
                    Code = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    Name = table.Column<string>(maxLength: 300, nullable: false),
                    ModuleTypeIndex = table.Column<int>(nullable: false),
                    ReInitOnStartup = table.Column<bool>(nullable: false),
                    Enabled = table.Column<bool>(nullable: false),
                    SilentMode = table.Column<bool>(nullable: false),
                    NextCode = table.Column<string>(maxLength: 50, nullable: true),
                    FailedCode = table.Column<string>(maxLength: 50, nullable: true),
                    LastSyncDate = table.Column<DateTime>(nullable: true),
                    ModifyDate = table.Column<DateTime>(nullable: false),
                    Discriminator = table.Column<string>(nullable: false),
                    IntroVoice = table.Column<byte[]>(nullable: true),
                    PreTurnVoice = table.Column<byte[]>(nullable: true),
                    PostTurnVoice = table.Column<byte[]>(nullable: true),
                    PostCallbackVoice = table.Column<byte[]>(nullable: true),
                    AutoDialOperator = table.Column<bool>(nullable: true),
                    OwnerPeerId = table.Column<int>(nullable: true),
                    WaitQueueId = table.Column<int>(nullable: true),
                    CrmObjectTypeKey = table.Column<string>(maxLength: 450, nullable: true),
                    ContractTypeKey = table.Column<string>(maxLength: 450, nullable: true),
                    ContractIsValidVoice = table.Column<byte[]>(nullable: true),
                    ContractIsNotValidVoice = table.Column<byte[]>(nullable: true),
                    CustomerNumberPrefix = table.Column<string>(nullable: true),
                    AuthenticationModeIndex = table.Column<int>(nullable: true),
                    ReAuthenticate = table.Column<bool>(nullable: true),
                    RecoverCredentialEnabled = table.Column<bool>(nullable: true),
                    RecoverCredentialCode = table.Column<string>(maxLength: 50, nullable: true),
                    ModuleCustomerClub_IntroVoice = table.Column<byte[]>(nullable: true),
                    EnterCustomerNoVoice = table.Column<byte[]>(nullable: true),
                    EnterPassVoice = table.Column<byte[]>(nullable: true),
                    WelcomeVoice = table.Column<byte[]>(nullable: true),
                    WrongCustomerNoVoice = table.Column<byte[]>(nullable: true),
                    WrongCustomerNoOrPassVoice = table.Column<byte[]>(nullable: true),
                    AuthenticationFailedVoice = table.Column<byte[]>(nullable: true),
                    RecoverCredentialVoice = table.Column<byte[]>(nullable: true),
                    MoneyAccountUserKey = table.Column<string>(nullable: true),
                    WorkInAnonymousMode = table.Column<bool>(nullable: true),
                    BillableObjectTypeKey = table.Column<string>(nullable: true),
                    LookupNumberFieldKey = table.Column<string>(nullable: true),
                    ValueFieldKey = table.Column<string>(nullable: true),
                    SendPaymentLink = table.Column<bool>(nullable: true),
                    ForceToGetNewMobile = table.Column<bool>(nullable: true),
                    EnterNumberVoice = table.Column<byte[]>(nullable: true),
                    WrongNumberVoice = table.Column<byte[]>(nullable: true),
                    PreSayBalanceVoice = table.Column<byte[]>(nullable: true),
                    AfterSayBalanceVoice = table.Column<byte[]>(nullable: true),
                    SelectSendLinkVoice = table.Column<byte[]>(nullable: true),
                    PreSayCurrentMobileVoice = table.Column<byte[]>(nullable: true),
                    AfterSayCurrentMobileVoice = table.Column<byte[]>(nullable: true),
                    SelectCurrentMobileVoice = table.Column<byte[]>(nullable: true),
                    EnterMobileVoice = table.Column<byte[]>(nullable: true),
                    WrongMobileVoice = table.Column<byte[]>(nullable: true),
                    PreSayNewMobileVoice = table.Column<byte[]>(nullable: true),
                    AfterSayNewMobileVoice = table.Column<byte[]>(nullable: true),
                    RecheckNewMobileVoice = table.Column<byte[]>(nullable: true),
                    SendAndThanksVoice = table.Column<byte[]>(nullable: true),
                    NoBillFoundToPay = table.Column<byte[]>(nullable: true),
                    FailedToSendLink = table.Column<byte[]>(nullable: true),
                    QueueId = table.Column<int>(nullable: true),
                    ForceToPlayOperatorExtention = table.Column<bool>(nullable: true),
                    PreAnnounceOperatorVoice = table.Column<byte[]>(nullable: true),
                    PostAnnounceOperatorVoice = table.Column<byte[]>(nullable: true),
                    ModuleQueueOperatorVoting_QueueId = table.Column<int>(nullable: true),
                    OperatorAnnouncement = table.Column<bool>(nullable: true),
                    PlayOperatorExtensionInsteadOfName = table.Column<bool>(nullable: true),
                    Voting = table.Column<bool>(nullable: true),
                    ModuleQueueOperatorVoting_PreAnnounceOperatorVoice = table.Column<byte[]>(nullable: true),
                    ModuleQueueOperatorVoting_PostAnnounceOperatorVoice = table.Column<byte[]>(nullable: true),
                    InvalidInputVoice = table.Column<byte[]>(nullable: true),
                    SucceedVoice = table.Column<byte[]>(nullable: true),
                    InputDigitFrom = table.Column<int>(nullable: true),
                    InputDigitTo = table.Column<int>(nullable: true),
                    Voice = table.Column<byte[]>(nullable: true),
                    TargetRoleTypeIndex = table.Column<int>(nullable: true),
                    SayOperatorName = table.Column<bool>(nullable: true),
                    ModuleRoleConnect_PreSayBalanceVoice = table.Column<byte[]>(nullable: true),
                    ModuleRoleConnect_AfterSayBalanceVoice = table.Column<byte[]>(nullable: true),
                    PreAnnounceVoice = table.Column<byte[]>(nullable: true),
                    PostAnnounceVoice = table.Column<byte[]>(nullable: true),
                    ModuleVoting_InvalidInputVoice = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Module", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Module_Peer_OwnerPeerId",
                        column: x => x.OwnerPeerId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Module_Queue_WaitQueueId",
                        column: x => x.WaitQueueId,
                        principalTable: "Queue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Module_Queue_ModuleQueueOperatorVoting_QueueId",
                        column: x => x.ModuleQueueOperatorVoting_QueueId,
                        principalTable: "Queue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Module_LookupSource_LookupSourceId",
                        column: x => x.LookupSourceId,
                        principalTable: "LookupSource",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Module_TelephonySystem_TsId",
                        column: x => x.TsId,
                        principalTable: "TelephonySystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QueueMember",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QueueId = table.Column<int>(nullable: false),
                    ExtensionId = table.Column<int>(nullable: false),
                    IsLogined = table.Column<bool>(nullable: false),
                    LastLoginDate = table.Column<DateTime>(nullable: false),
                    LastLogoffDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueueMember", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QueueMember_Peer_ExtensionId",
                        column: x => x.ExtensionId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_QueueMember_Queue_QueueId",
                        column: x => x.QueueId,
                        principalTable: "Queue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ModuleReconnectScopeItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModuleReconnectId = table.Column<int>(nullable: false),
                    PeerId = table.Column<int>(nullable: true),
                    QueueId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleReconnectScopeItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ModuleReconnectScopeItem_Module_ModuleReconnectId",
                        column: x => x.ModuleReconnectId,
                        principalTable: "Module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleReconnectScopeItem_Peer_PeerId",
                        column: x => x.PeerId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ModuleReconnectScopeItem_Queue_QueueId",
                        column: x => x.QueueId,
                        principalTable: "Queue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ModuleVotingQuestion",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModuleVotingId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    Weight = table.Column<decimal>(nullable: false),
                    QuestionVoice = table.Column<byte[]>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleVotingQuestion", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ModuleVotingQuestion_Module_ModuleVotingId",
                        column: x => x.ModuleVotingId,
                        principalTable: "Module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ModuleVotingQuestionItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModuleVotingQuestionId = table.Column<int>(nullable: false),
                    Title = table.Column<string>(nullable: true),
                    InputDigit = table.Column<int>(nullable: false),
                    Points = table.Column<decimal>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleVotingQuestionItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ModuleVotingQuestionItem_ModuleVotingQuestion_ModuleVotingQuestionId",
                        column: x => x.ModuleVotingQuestionId,
                        principalTable: "ModuleVotingQuestion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CallChannel",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CallId = table.Column<long>(nullable: false),
                    UniqueId = table.Column<string>(unicode: false, maxLength: 50, nullable: false),
                    PeerId = table.Column<int>(nullable: false),
                    State = table.Column<int>(nullable: false),
                    Response = table.Column<int>(nullable: false),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    CreateDatePersianInt = table.Column<int>(nullable: false),
                    ConnectDate = table.Column<DateTime>(nullable: true),
                    ConnectDatePersianInt = table.Column<int>(nullable: true),
                    HangupDate = table.Column<DateTime>(nullable: true),
                    HangupDatePersianInt = table.Column<int>(nullable: true),
                    DialedExtension = table.Column<string>(maxLength: 450, nullable: true),
                    CallerIdName = table.Column<string>(maxLength: 450, nullable: true),
                    CallerIdNum = table.Column<string>(maxLength: 450, nullable: true),
                    RecordFileName = table.Column<string>(maxLength: 450, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CallChannel", x => x.Id);
                    table.ForeignKey(
                        name: "FK_CallChannel_Peer_PeerId",
                        column: x => x.PeerId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Call",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TsId = table.Column<int>(nullable: false),
                    ProfileId = table.Column<string>(maxLength: 50, nullable: true),
                    ProfileName = table.Column<string>(maxLength: 400, nullable: true),
                    StartDate = table.Column<DateTime>(nullable: false),
                    StartDatePersianInt = table.Column<int>(nullable: false),
                    EndDate = table.Column<DateTime>(nullable: true),
                    EndDatePersianInt = table.Column<int>(nullable: true),
                    CallDirectionIndex = table.Column<int>(nullable: false),
                    CallTypeIndex = table.Column<int>(nullable: false),
                    Status = table.Column<int>(nullable: false),
                    CallDuration = table.Column<TimeSpan>(nullable: true),
                    RingDuration = table.Column<TimeSpan>(nullable: true),
                    PhoneNumber = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    InitByChannelId = table.Column<long>(nullable: true),
                    InitByPeerId = table.Column<int>(nullable: true),
                    CustomerId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Call", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Call_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Call_CallChannel_InitByChannelId",
                        column: x => x.InitByChannelId,
                        principalTable: "CallChannel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Call_Peer_InitByPeerId",
                        column: x => x.InitByPeerId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Call_TelephonySystem_TsId",
                        column: x => x.TsId,
                        principalTable: "TelephonySystem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ModuleAdvanceFollowMeResult",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ModuleAdvanceFollowMeId = table.Column<int>(nullable: false),
                    InputDate = table.Column<DateTime>(nullable: false),
                    InputDTMF = table.Column<string>(nullable: true),
                    ModuleAdvanceFollowMeInputTypeIndex = table.Column<int>(nullable: false),
                    ExtensionId = table.Column<int>(nullable: false),
                    CallChannelId = table.Column<long>(nullable: false),
                    CallBackStatusIndex = table.Column<int>(nullable: true),
                    CallBackRetryCount = table.Column<int>(nullable: false),
                    LastCallBackRetryDate = table.Column<DateTime>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleAdvanceFollowMeResult", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ModuleAdvanceFollowMeResult_CallChannel_CallChannelId",
                        column: x => x.CallChannelId,
                        principalTable: "CallChannel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleAdvanceFollowMeResult_Peer_ExtensionId",
                        column: x => x.ExtensionId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleAdvanceFollowMeResult_Module_ModuleAdvanceFollowMeId",
                        column: x => x.ModuleAdvanceFollowMeId,
                        principalTable: "Module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ModuleVotingQuestionResult",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModuleVotingQuestionItemId = table.Column<int>(nullable: false),
                    CallChannelId = table.Column<long>(nullable: false),
                    QueueId = table.Column<int>(nullable: true),
                    PeerId = table.Column<int>(nullable: true),
                    Input = table.Column<string>(nullable: true),
                    IsValid = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleVotingQuestionResult", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ModuleVotingQuestionResult_CallChannel_CallChannelId",
                        column: x => x.CallChannelId,
                        principalTable: "CallChannel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleVotingQuestionResult_ModuleVotingQuestionItem_ModuleVotingQuestionItemId",
                        column: x => x.ModuleVotingQuestionItemId,
                        principalTable: "ModuleVotingQuestionItem",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleVotingQuestionResult_Peer_PeerId",
                        column: x => x.PeerId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ModuleVotingQuestionResult_Queue_QueueId",
                        column: x => x.QueueId,
                        principalTable: "Queue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "QueueCaller",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    QueueId = table.Column<int>(nullable: false),
                    CallChannelId = table.Column<long>(nullable: false),
                    QueueCallerStatusIndex = table.Column<int>(nullable: false),
                    JoinDate = table.Column<DateTime>(nullable: false),
                    LeaveDate = table.Column<DateTime>(nullable: true),
                    JoinPosition = table.Column<int>(nullable: false),
                    AbandonPosition = table.Column<int>(nullable: true),
                    AnsweredByCallChannelId = table.Column<long>(nullable: false),
                    AnsweredByPeerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_QueueCaller", x => x.Id);
                    table.ForeignKey(
                        name: "FK_QueueCaller_CallChannel_AnsweredByCallChannelId",
                        column: x => x.AnsweredByCallChannelId,
                        principalTable: "CallChannel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QueueCaller_Peer_AnsweredByPeerId",
                        column: x => x.AnsweredByPeerId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_QueueCaller_Queue_QueueId",
                        column: x => x.QueueId,
                        principalTable: "Queue",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ModuleQueueOperatorVotingResult",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    ModuleQueueOperatorVotingId = table.Column<int>(nullable: false),
                    PeerId = table.Column<int>(nullable: false),
                    CallId = table.Column<long>(nullable: false),
                    CallChannelId = table.Column<long>(nullable: false),
                    Input = table.Column<string>(unicode: false, maxLength: 10, nullable: false),
                    IsValid = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ModuleQueueOperatorVotingResult", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ModuleQueueOperatorVotingResult_CallChannel_CallChannelId",
                        column: x => x.CallChannelId,
                        principalTable: "CallChannel",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleQueueOperatorVotingResult_Call_CallId",
                        column: x => x.CallId,
                        principalTable: "Call",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ModuleQueueOperatorVotingResult_Module_ModuleQueueOperatorVotingId",
                        column: x => x.ModuleQueueOperatorVotingId,
                        principalTable: "Module",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ModuleQueueOperatorVotingResult_Peer_PeerId",
                        column: x => x.PeerId,
                        principalTable: "Peer",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetRoleClaims_RoleId1",
                table: "AspNetRoleClaims",
                column: "RoleId1");

            migrationBuilder.CreateIndex(
                name: "RoleNameIndex",
                table: "AspNetRoles",
                column: "NormalizedName",
                unique: true,
                filter: "[NormalizedName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId",
                table: "AspNetUserClaims",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserClaims_UserId1",
                table: "AspNetUserClaims",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId",
                table: "AspNetUserLogins",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserLogins_UserId1",
                table: "AspNetUserLogins",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_RoleId1",
                table: "AspNetUserRoles",
                column: "RoleId1");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserRoles_UserId1",
                table: "AspNetUserRoles",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "EmailIndex",
                table: "AspNetUsers",
                column: "NormalizedEmail");

            migrationBuilder.CreateIndex(
                name: "UserNameIndex",
                table: "AspNetUsers",
                column: "NormalizedUserName",
                unique: true,
                filter: "[NormalizedUserName] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUserTokens_UserId1",
                table: "AspNetUserTokens",
                column: "UserId1");

            migrationBuilder.CreateIndex(
                name: "IX_Call_CustomerId",
                table: "Call",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_Call_InitByChannelId",
                table: "Call",
                column: "InitByChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_Call_InitByPeerId",
                table: "Call",
                column: "InitByPeerId");

            migrationBuilder.CreateIndex(
                name: "IX_Call_TsId",
                table: "Call",
                column: "TsId");

            migrationBuilder.CreateIndex(
                name: "IX_CallId",
                table: "CallChannel",
                column: "CallId")
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_PeerId",
                table: "CallChannel",
                column: "PeerId")
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_CallChannel_State_Response_CreateDate",
                table: "CallChannel",
                columns: new[] { "State", "Response", "CreateDate" })
                .Annotation("SqlServer:Clustered", false);

            migrationBuilder.CreateIndex(
                name: "IX_Department_ParentId",
                table: "Department",
                column: "ParentId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentTelephonySystemHelperExtension_HelperExtensionId",
                table: "DepartmentTelephonySystemHelperExtension",
                column: "HelperExtensionId");

            migrationBuilder.CreateIndex(
                name: "IX_DepartmentTelephonySystemHelperExtension_TsId",
                table: "DepartmentTelephonySystemHelperExtension",
                column: "TsId");

            migrationBuilder.CreateIndex(
                name: "IX_ExtentionHistory_ExtentionId1",
                table: "ExtentionHistory",
                column: "ExtentionId1");

            migrationBuilder.CreateIndex(
                name: "IX_ExtentionHistory_QueueId",
                table: "ExtentionHistory",
                column: "QueueId");

            migrationBuilder.CreateIndex(
                name: "IX_GeneralPropertyItem_GeneralPropertyId",
                table: "GeneralPropertyItem",
                column: "GeneralPropertyId");

            migrationBuilder.CreateIndex(
                name: "IX_Module_OwnerPeerId",
                table: "Module",
                column: "OwnerPeerId");

            migrationBuilder.CreateIndex(
                name: "IX_Module_WaitQueueId",
                table: "Module",
                column: "WaitQueueId");

            migrationBuilder.CreateIndex(
                name: "IX_Module_ModuleQueueOperatorVoting_QueueId",
                table: "Module",
                column: "ModuleQueueOperatorVoting_QueueId");

            migrationBuilder.CreateIndex(
                name: "IX_Module_LookupSourceId",
                table: "Module",
                column: "LookupSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_Module_TsId",
                table: "Module",
                column: "TsId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleAdvanceFollowMeResult_CallChannelId",
                table: "ModuleAdvanceFollowMeResult",
                column: "CallChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleAdvanceFollowMeResult_ExtensionId",
                table: "ModuleAdvanceFollowMeResult",
                column: "ExtensionId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleAdvanceFollowMeResult_ModuleAdvanceFollowMeId",
                table: "ModuleAdvanceFollowMeResult",
                column: "ModuleAdvanceFollowMeId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleQueueOperatorVotingResult_CallChannelId",
                table: "ModuleQueueOperatorVotingResult",
                column: "CallChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleQueueOperatorVotingResult_CallId",
                table: "ModuleQueueOperatorVotingResult",
                column: "CallId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleQueueOperatorVotingResult_ModuleQueueOperatorVotingId",
                table: "ModuleQueueOperatorVotingResult",
                column: "ModuleQueueOperatorVotingId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleQueueOperatorVotingResult_PeerId",
                table: "ModuleQueueOperatorVotingResult",
                column: "PeerId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleReconnectScopeItem_ModuleReconnectId",
                table: "ModuleReconnectScopeItem",
                column: "ModuleReconnectId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleReconnectScopeItem_PeerId",
                table: "ModuleReconnectScopeItem",
                column: "PeerId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleReconnectScopeItem_QueueId",
                table: "ModuleReconnectScopeItem",
                column: "QueueId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleVotingQuestion_ModuleVotingId",
                table: "ModuleVotingQuestion",
                column: "ModuleVotingId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleVotingQuestionItem_ModuleVotingQuestionId",
                table: "ModuleVotingQuestionItem",
                column: "ModuleVotingQuestionId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleVotingQuestionResult_CallChannelId",
                table: "ModuleVotingQuestionResult",
                column: "CallChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleVotingQuestionResult_ModuleVotingQuestionItemId",
                table: "ModuleVotingQuestionResult",
                column: "ModuleVotingQuestionItemId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleVotingQuestionResult_PeerId",
                table: "ModuleVotingQuestionResult",
                column: "PeerId");

            migrationBuilder.CreateIndex(
                name: "IX_ModuleVotingQuestionResult_QueueId",
                table: "ModuleVotingQuestionResult",
                column: "QueueId");

            migrationBuilder.CreateIndex(
                name: "IX_Operator_DepartmentId",
                table: "Operator",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Operator_UserId",
                table: "Operator",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_OperatorDepartmentPermission_DepartmentId",
                table: "OperatorDepartmentPermission",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_OperatorDepartmentPermission_OperatorId",
                table: "OperatorDepartmentPermission",
                column: "OperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Peer_DepartmentId",
                table: "Peer",
                column: "DepartmentId");

            migrationBuilder.CreateIndex(
                name: "IX_Peer_OwnerOperatorId",
                table: "Peer",
                column: "OwnerOperatorId");

            migrationBuilder.CreateIndex(
                name: "IX_Peer_TsId",
                table: "Peer",
                column: "TsId");

            migrationBuilder.CreateIndex(
                name: "IX_Queue_TsId",
                table: "Queue",
                column: "TsId");

            migrationBuilder.CreateIndex(
                name: "IX_QueueCaller_AnsweredByCallChannelId",
                table: "QueueCaller",
                column: "AnsweredByCallChannelId");

            migrationBuilder.CreateIndex(
                name: "IX_QueueCaller_AnsweredByPeerId",
                table: "QueueCaller",
                column: "AnsweredByPeerId");

            migrationBuilder.CreateIndex(
                name: "IX_QueueCaller_QueueId",
                table: "QueueCaller",
                column: "QueueId");

            migrationBuilder.CreateIndex(
                name: "IX_QueueMember_ExtensionId",
                table: "QueueMember",
                column: "ExtensionId");

            migrationBuilder.CreateIndex(
                name: "IX_QueueMember_QueueId",
                table: "QueueMember",
                column: "QueueId");

            migrationBuilder.CreateIndex(
                name: "IX_TeleMarketing_TsId",
                table: "TeleMarketing",
                column: "TsId");

            migrationBuilder.CreateIndex(
                name: "IX_TeleMarketingTarget_TeleMarketingId",
                table: "TeleMarketingTarget",
                column: "TeleMarketingId");

            migrationBuilder.CreateIndex(
                name: "IX_TelephonySystemLookupSource_LookupSourceId",
                table: "TelephonySystemLookupSource",
                column: "LookupSourceId");

            migrationBuilder.CreateIndex(
                name: "IX_TelephonySystemSetting_TelephonySystemId1",
                table: "TelephonySystemSetting",
                column: "TelephonySystemId1");

            migrationBuilder.AddForeignKey(
                name: "FK_CallChannel_Call_CallId",
                table: "CallChannel",
                column: "CallId",
                principalTable: "Call",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Operator_AspNetUsers_UserId",
                table: "Operator");

            migrationBuilder.DropForeignKey(
                name: "FK_Call_Customers_CustomerId",
                table: "Call");

            migrationBuilder.DropForeignKey(
                name: "FK_Call_CallChannel_InitByChannelId",
                table: "Call");

            migrationBuilder.DropTable(
                name: "AspNetRoleClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserClaims");

            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "DepartmentTelephonySystemHelperExtension");

            migrationBuilder.DropTable(
                name: "ExtentionHistory");

            migrationBuilder.DropTable(
                name: "GeneralPropertyItem");

            migrationBuilder.DropTable(
                name: "MemberStatisticsModel");

            migrationBuilder.DropTable(
                name: "ModuleAdvanceFollowMeResult");

            migrationBuilder.DropTable(
                name: "ModuleQueueOperatorVotingResult");

            migrationBuilder.DropTable(
                name: "ModuleReconnectScopeItem");

            migrationBuilder.DropTable(
                name: "ModuleVotingQuestionResult");

            migrationBuilder.DropTable(
                name: "OperatorDepartmentPermission");

            migrationBuilder.DropTable(
                name: "OperatorSos");

            migrationBuilder.DropTable(
                name: "QueueCaller");

            migrationBuilder.DropTable(
                name: "QueueMember");

            migrationBuilder.DropTable(
                name: "TeleMarketingTarget");

            migrationBuilder.DropTable(
                name: "TelephonySystemLookupSource");

            migrationBuilder.DropTable(
                name: "TelephonySystemSetting");

            migrationBuilder.DropTable(
                name: "AspNetRoles");

            migrationBuilder.DropTable(
                name: "GeneralProperty");

            migrationBuilder.DropTable(
                name: "ModuleVotingQuestionItem");

            migrationBuilder.DropTable(
                name: "TeleMarketing");

            migrationBuilder.DropTable(
                name: "ModuleVotingQuestion");

            migrationBuilder.DropTable(
                name: "Module");

            migrationBuilder.DropTable(
                name: "Queue");

            migrationBuilder.DropTable(
                name: "LookupSource");

            migrationBuilder.DropTable(
                name: "AspNetUsers");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "CallChannel");

            migrationBuilder.DropTable(
                name: "Call");

            migrationBuilder.DropTable(
                name: "Peer");

            migrationBuilder.DropTable(
                name: "Operator");

            migrationBuilder.DropTable(
                name: "TelephonySystem");

            migrationBuilder.DropTable(
                name: "Department");
        }
    }
}
