﻿IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'RoleNameIndex' AND t.name = 'AspNetRoles')
BEGIN
	DROP INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
END
GO
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_UserId' AND t.name = 'AspNetUserClaims')
BEGIN
	DROP INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
END
GO
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_UserId' AND t.name = 'AspNetUserLogins')
BEGIN
	DROP INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
END
GO
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_UserId' AND t.name = 'AspNetUserRoles')
BEGIN
	DROP INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
END
GO
CREATE NONCLUSTERED INDEX [IX_UserId] ON [dbo].[AspNetUserRoles]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_RoleId' AND t.name = 'AspNetUserRoles')
BEGIN
	DROP INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
END
GO
CREATE NONCLUSTERED INDEX [IX_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Operator_Id' AND t.name = 'AspNetUsers')
BEGIN
	DROP INDEX [IX_Operator_Id] ON [dbo].[AspNetUsers]
END
GO
CREATE NONCLUSTERED INDEX [IX_Operator_Id] ON [dbo].[AspNetUsers]
(
	[Operator_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'UserNameIndex' AND t.name = 'AspNetUsers')
BEGIN
	DROP INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
END
GO
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[UserName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO




IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_InitByChannelId' AND t.name = 'Call')
BEGIN
	DROP INDEX [IX_InitByChannelId] ON [dbo].[Call]
END
GO
CREATE NONCLUSTERED INDEX [IX_InitByChannelId] ON [dbo].[Call]
(
	[InitByChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_InitByPeerId' AND t.name = 'Call')
BEGIN
	DROP INDEX [IX_InitByPeerId] ON [dbo].[Call]
END
GO
CREATE NONCLUSTERED INDEX [IX_InitByPeerId] ON [dbo].[Call]
(
	[InitByPeerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_TsId' AND t.name = 'Call')
BEGIN
	DROP INDEX [IX_TsId] ON [dbo].[Call]
END
GO
CREATE NONCLUSTERED INDEX [IX_TsId] ON [dbo].[Call]
(
	[TsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_CallChannel_State_Response_CreateDate' AND t.name = 'CallChannel')
BEGIN
	DROP INDEX [IX_CallChannel_State_Response_CreateDate] ON [dbo].[CallChannel]
END
GO
CREATE NONCLUSTERED INDEX [IX_CallChannel_State_Response_CreateDate] ON [dbo].[CallChannel]
(
	[State] ASC,
	[Response] ASC,
	[CreateDate] ASC
)
INCLUDE ( 	[Id]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_CallId' AND t.name = 'CallChannel')
BEGIN
	DROP INDEX [IX_CallId] ON [dbo].[CallChannel]
END
GO
CREATE NONCLUSTERED INDEX [IX_CallId] ON [dbo].[CallChannel]
(
	[CallId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_PeerId' AND t.name = 'CallChannel')
BEGIN
	DROP INDEX [IX_PeerId] ON [dbo].[CallChannel]
END
GO
CREATE NONCLUSTERED INDEX [IX_PeerId] ON [dbo].[CallChannel]
(
	[PeerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_ParentId' AND t.name = 'Department')
BEGIN
	DROP INDEX [IX_ParentId] ON [dbo].[Department]
END
GO
CREATE NONCLUSTERED INDEX [IX_ParentId] ON [dbo].[Department]
(
	[ParentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_LookupSourceId' AND t.name = 'Module')
BEGIN
	DROP INDEX [IX_LookupSourceId] ON [dbo].[Module]
END
GO
CREATE NONCLUSTERED INDEX [IX_LookupSourceId] ON [dbo].[Module]
(
	[LookupSourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_TsId' AND t.name = 'Module')
BEGIN
	DROP INDEX [IX_TsId] ON [dbo].[Module]
END
GO
CREATE NONCLUSTERED INDEX [IX_TsId] ON [dbo].[Module]
(
	[TsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'ModuleAdvanceFollowMe')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[ModuleAdvanceFollowMe]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ModuleAdvanceFollowMe]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_OwnerPeerId' AND t.name = 'ModuleAdvanceFollowMe')
BEGIN
	DROP INDEX [IX_OwnerPeerId] ON [dbo].[ModuleAdvanceFollowMe]
END
GO
CREATE NONCLUSTERED INDEX [IX_OwnerPeerId] ON [dbo].[ModuleAdvanceFollowMe]
(
	[OwnerPeerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_WaitQueueId' AND t.name = 'ModuleAdvanceFollowMe')
BEGIN
	DROP INDEX [IX_WaitQueueId] ON [dbo].[ModuleAdvanceFollowMe]
END
GO
CREATE NONCLUSTERED INDEX [IX_WaitQueueId] ON [dbo].[ModuleAdvanceFollowMe]
(
	[WaitQueueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_CallChannelId' AND t.name = 'ModuleAdvanceFollowMeResults')
BEGIN
	DROP INDEX [IX_CallChannelId] ON [dbo].[ModuleAdvanceFollowMeResults]
END
GO
CREATE NONCLUSTERED INDEX [IX_CallChannelId] ON [dbo].[ModuleAdvanceFollowMeResults]
(
	[CallChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_ExtensionId' AND t.name = 'ModuleAdvanceFollowMeResults')
BEGIN
	DROP INDEX [IX_ExtensionId] ON [dbo].[ModuleAdvanceFollowMeResults]
END
GO
CREATE NONCLUSTERED INDEX [IX_ExtensionId] ON [dbo].[ModuleAdvanceFollowMeResults]
(
	[ExtensionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_ModuleAdvanceFollowMeId' AND t.name = 'ModuleAdvanceFollowMeResults')
BEGIN
	DROP INDEX [IX_ModuleAdvanceFollowMeId] ON [dbo].[ModuleAdvanceFollowMeResults]
END
GO
CREATE NONCLUSTERED INDEX [IX_ModuleAdvanceFollowMeId] ON [dbo].[ModuleAdvanceFollowMeResults]
(
	[ModuleAdvanceFollowMeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'ModuleCheckContract')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[ModuleCheckContract]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ModuleCheckContract]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'ModuleCustomerClub')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[ModuleCustomerClub]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ModuleCustomerClub]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'ModulePay')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[ModulePay]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ModulePay]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'ModuleQueueOperatorAnnouncement')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[ModuleQueueOperatorAnnouncement]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ModuleQueueOperatorAnnouncement]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'ModuleQueueOperatorVoting')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[ModuleQueueOperatorVoting]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ModuleQueueOperatorVoting]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_QueueId' AND t.name = 'ModuleQueueOperatorVoting')
BEGIN
	DROP INDEX [IX_QueueId] ON [dbo].[ModuleQueueOperatorVoting]
END
GO
CREATE NONCLUSTERED INDEX [IX_QueueId] ON [dbo].[ModuleQueueOperatorVoting]
(
	[QueueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_CallChannelId' AND t.name = 'ModuleQueueOperatorVotingResult')
BEGIN
	DROP INDEX [IX_CallChannelId] ON [dbo].[ModuleQueueOperatorVotingResult]
END
GO
CREATE NONCLUSTERED INDEX [IX_CallChannelId] ON [dbo].[ModuleQueueOperatorVotingResult]
(
	[CallChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_CallId' AND t.name = 'ModuleQueueOperatorVotingResult')
BEGIN
	DROP INDEX [IX_CallId] ON [dbo].[ModuleQueueOperatorVotingResult]
END
GO
CREATE NONCLUSTERED INDEX [IX_CallId] ON [dbo].[ModuleQueueOperatorVotingResult]
(
	[CallId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_ModuleQueueOperatorVotingId' AND t.name = 'ModuleQueueOperatorVotingResult')
BEGIN
	DROP INDEX [IX_ModuleQueueOperatorVotingId] ON [dbo].[ModuleQueueOperatorVotingResult]
END
GO
CREATE NONCLUSTERED INDEX [IX_ModuleQueueOperatorVotingId] ON [dbo].[ModuleQueueOperatorVotingResult]
(
	[ModuleQueueOperatorVotingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO




IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_PeerId' AND t.name = 'ModuleQueueOperatorVotingResult')
BEGIN
	DROP INDEX [IX_PeerId] ON [dbo].[ModuleQueueOperatorVotingResult]
END
GO
CREATE NONCLUSTERED INDEX [IX_PeerId] ON [dbo].[ModuleQueueOperatorVotingResult]
(
	[PeerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'ModuleReconnect')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[ModuleReconnect]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ModuleReconnect]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_ModuleReconnectId' AND t.name = 'ModuleReconnectScopeItem')
BEGIN
	DROP INDEX [IX_ModuleReconnectId] ON [dbo].[ModuleReconnectScopeItem]
END
GO
CREATE NONCLUSTERED INDEX [IX_ModuleReconnectId] ON [dbo].[ModuleReconnectScopeItem]
(
	[ModuleReconnectId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_PeerId' AND t.name = 'ModuleReconnectScopeItem')
BEGIN
	DROP INDEX [IX_PeerId] ON [dbo].[ModuleReconnectScopeItem]
END
GO
CREATE NONCLUSTERED INDEX [IX_PeerId] ON [dbo].[ModuleReconnectScopeItem]
(
	[PeerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO




IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_QueueId' AND t.name = 'ModuleReconnectScopeItem')
BEGIN
	DROP INDEX [IX_QueueId] ON [dbo].[ModuleReconnectScopeItem]
END
GO
CREATE NONCLUSTERED INDEX [IX_QueueId] ON [dbo].[ModuleReconnectScopeItem]
(
	[QueueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'ModuleRoleConnect')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[ModuleRoleConnect]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ModuleRoleConnect]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'ModuleVoting')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[ModuleVoting]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[ModuleVoting]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_ModuleVotingId' AND t.name = 'ModuleVotingQuestion')
BEGIN
	DROP INDEX [IX_ModuleVotingId] ON [dbo].[ModuleVotingQuestion]
END
GO
CREATE NONCLUSTERED INDEX [IX_ModuleVotingId] ON [dbo].[ModuleVotingQuestion]
(
	[ModuleVotingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_ModuleVotingQuestionId' AND t.name = 'ModuleVotingQuestionItem')
BEGIN
	DROP INDEX [IX_ModuleVotingQuestionId] ON [dbo].[ModuleVotingQuestionItem]
END
GO
CREATE NONCLUSTERED INDEX [IX_ModuleVotingQuestionId] ON [dbo].[ModuleVotingQuestionItem]
(
	[ModuleVotingQuestionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_CallChannelId' AND t.name = 'ModuleVotingQuestionResult')
BEGIN
	DROP INDEX [IX_CallChannelId] ON [dbo].[ModuleVotingQuestionResult]
END
GO
CREATE NONCLUSTERED INDEX [IX_CallChannelId] ON [dbo].[ModuleVotingQuestionResult]
(
	[CallChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_ModuleVotingQuestionItemId' AND t.name = 'ModuleVotingQuestionResult')
BEGIN
	DROP INDEX [IX_ModuleVotingQuestionItemId] ON [dbo].[ModuleVotingQuestionResult]
END
GO
CREATE NONCLUSTERED INDEX [IX_ModuleVotingQuestionItemId] ON [dbo].[ModuleVotingQuestionResult]
(
	[ModuleVotingQuestionItemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO


IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_PeerId' AND t.name = 'ModuleVotingQuestionResult')
BEGIN
	DROP INDEX [IX_PeerId] ON [dbo].[ModuleVotingQuestionResult]
END
GO
CREATE NONCLUSTERED INDEX [IX_PeerId] ON [dbo].[ModuleVotingQuestionResult]
(
	[PeerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_QueueId' AND t.name = 'ModuleVotingQuestionResult')
BEGIN
	DROP INDEX [IX_QueueId] ON [dbo].[ModuleVotingQuestionResult]
END
GO
CREATE NONCLUSTERED INDEX [IX_QueueId] ON [dbo].[ModuleVotingQuestionResult]
(
	[QueueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO




IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Department_Id' AND t.name = 'Operator')
BEGIN
	DROP INDEX [IX_Department_Id] ON [dbo].[Operator]
END
GO
CREATE NONCLUSTERED INDEX [IX_Department_Id] ON [dbo].[Operator]
(
	[Department_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_DepartmentId' AND t.name = 'OperatorDepartmentPermission')
BEGIN
	DROP INDEX [IX_DepartmentId] ON [dbo].[OperatorDepartmentPermission]
END
GO
CREATE NONCLUSTERED INDEX [IX_DepartmentId] ON [dbo].[OperatorDepartmentPermission]
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_OperatorId' AND t.name = 'OperatorDepartmentPermission')
BEGIN
	DROP INDEX [IX_OperatorId] ON [dbo].[OperatorDepartmentPermission]
END
GO
CREATE NONCLUSTERED INDEX [IX_OperatorId] ON [dbo].[OperatorDepartmentPermission]
(
	[OperatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_Id' AND t.name = 'PayamGostarLookupSource')
BEGIN
	DROP INDEX [IX_Id] ON [dbo].[PayamGostarLookupSource]
END
GO
CREATE NONCLUSTERED INDEX [IX_Id] ON [dbo].[PayamGostarLookupSource]
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_DepartmentId' AND t.name = 'Peer')
BEGIN
	DROP INDEX [IX_DepartmentId] ON [dbo].[Peer]
END
GO
CREATE NONCLUSTERED INDEX [IX_DepartmentId] ON [dbo].[Peer]
(
	[DepartmentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_OwnerOperatorId' AND t.name = 'Peer')
BEGIN
	DROP INDEX [IX_OwnerOperatorId] ON [dbo].[Peer]
END
GO
CREATE NONCLUSTERED INDEX [IX_OwnerOperatorId] ON [dbo].[Peer]
(
	[OwnerOperatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_TsId' AND t.name = 'Peer')
BEGIN
	DROP INDEX [IX_TsId] ON [dbo].[Peer]
END
GO
CREATE NONCLUSTERED INDEX [IX_TsId] ON [dbo].[Peer]
(
	[TsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_TsId' AND t.name = 'Queue')
BEGIN
	DROP INDEX [IX_TsId] ON [dbo].[Queue]
END
GO
CREATE NONCLUSTERED INDEX [IX_TsId] ON [dbo].[Queue]
(
	[TsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_AnsweredByCallChannelId' AND t.name = 'QueueCaller')
BEGIN
	DROP INDEX [IX_AnsweredByCallChannelId] ON [dbo].[QueueCaller]
END
GO
CREATE NONCLUSTERED INDEX [IX_AnsweredByCallChannelId] ON [dbo].[QueueCaller]
(
	[AnsweredByCallChannelId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_AnsweredByPeerId' AND t.name = 'QueueCaller')
BEGIN
	DROP INDEX [IX_AnsweredByPeerId] ON [dbo].[QueueCaller]
END
GO
CREATE NONCLUSTERED INDEX [IX_AnsweredByPeerId] ON [dbo].[QueueCaller]
(
	[AnsweredByPeerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO




IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_QueueCaller_CallChannelId' AND t.name = 'QueueCaller')
BEGIN
	DROP INDEX [IX_QueueCaller_CallChannelId] ON [dbo].[QueueCaller]
END
GO
CREATE NONCLUSTERED INDEX [IX_QueueCaller_CallChannelId] ON [dbo].[QueueCaller]
(
	[CallChannelId] ASC
)
INCLUDE ( 	[QueueId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_QueueCaller_JoinDate' AND t.name = 'QueueCaller')
BEGIN
	DROP INDEX [IX_QueueCaller_JoinDate] ON [dbo].[QueueCaller]
END
GO
CREATE NONCLUSTERED INDEX [IX_QueueCaller_JoinDate] ON [dbo].[QueueCaller]
(
	[JoinDate] ASC
)
INCLUDE ( 	[QueueId],
	[QueueCallerStatusIndex]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_QueueCaller_JoinDate_QueueCallerStatusIndex' AND t.name = 'QueueCaller')
BEGIN
	DROP INDEX [IX_QueueCaller_JoinDate_QueueCallerStatusIndex] ON [dbo].[QueueCaller]
END
GO
CREATE NONCLUSTERED INDEX [IX_QueueCaller_JoinDate_QueueCallerStatusIndex] ON [dbo].[QueueCaller]
(
	[JoinDate] ASC,
	[QueueCallerStatusIndex] ASC
)
INCLUDE ( 	[QueueId],
	[LeaveDate]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_QueueCaller_QueueCallerStatusIndex_JoinDate' AND t.name = 'QueueCaller')
BEGIN
	DROP INDEX [IX_QueueCaller_QueueCallerStatusIndex_JoinDate] ON [dbo].[QueueCaller]
END
GO
CREATE NONCLUSTERED INDEX [IX_QueueCaller_QueueCallerStatusIndex_JoinDate] ON [dbo].[QueueCaller]
(
	[QueueCallerStatusIndex] ASC,
	[JoinDate] ASC
)
INCLUDE ( 	[QueueId],
	[AnsweredByCallChannelId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_QueueCaller_QueueCallerStatusIndex_LeaveDate' AND t.name = 'QueueCaller')
BEGIN
	DROP INDEX [IX_QueueCaller_QueueCallerStatusIndex_LeaveDate] ON [dbo].[QueueCaller]
END
GO
CREATE NONCLUSTERED INDEX [IX_QueueCaller_QueueCallerStatusIndex_LeaveDate] ON [dbo].[QueueCaller]
(
	[QueueCallerStatusIndex] ASC,
	[LeaveDate] ASC
)
INCLUDE ( 	[CallChannelId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_QueueId' AND t.name = 'QueueCaller')
BEGIN
	DROP INDEX [IX_QueueId] ON [dbo].[QueueCaller]
END
GO
CREATE NONCLUSTERED INDEX [IX_QueueId] ON [dbo].[QueueCaller]
(
	[QueueId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_LookupSourceId' AND t.name = 'TelephonySystemLookupSource')
BEGIN
	DROP INDEX [IX_LookupSourceId] ON [dbo].[TelephonySystemLookupSource]
END
GO
CREATE NONCLUSTERED INDEX [IX_LookupSourceId] ON [dbo].[TelephonySystemLookupSource]
(
	[LookupSourceId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO



IF EXISTS (SELECT ind.name, t.name FROM sys.indexes ind INNER JOIN sys.tables t ON ind.object_id = t.object_id WHERE ind.name = 'IX_TelephonySystemId' AND t.name = 'TelephonySystemLookupSource')
BEGIN
	DROP INDEX [IX_TelephonySystemId] ON [dbo].[TelephonySystemLookupSource]
END
GO
CREATE NONCLUSTERED INDEX [IX_TelephonySystemId] ON [dbo].[TelephonySystemLookupSource]
(
	[TelephonySystemId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

