﻿SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
ALTER PROCEDURE [dbo].[spCallReport]
    @TsId INT,
    @CallDirectionIndex INT = NULL,
    @CallTypeIndex INT = NULL,
    @CallStatus INT = NULL,
    @CallStartDateFrom DATETIME = NULL,
    @CallStartDateTo DATETIME = NULL,
    @CallEndDateFrom DATETIME = NULL,
    @CallEndDateTo DATETIME = NULL,
    @CallDurationFrom VARCHAR(8) = NULL,
    @CallDurationTo VARCHAR(8) = NULL,
    @CallWaitingFrom VARCHAR(8) = NULL,
    @CallWaitingTo VARCHAR(8) = NULL,
    @PhoneNumber VARCHAR(50) = NULL,
    @CallerIdNum VARCHAR(50) = NULL,
    @CallerIdName NVARCHAR(50) = NULL,
    @DialedExtension NVARCHAR(50) = NULL,
    @CallChannelState INT = NULL,
    @QueueId INT = NULL,
    @ExtensionPeerId INT = NULL,
    @IsExtensionStarter BIT = 0,
    @ExtensionResponseIndex INT = NULL,
    @TrunkPeerId INT = NULL,
    @IsTrunkStarter BIT = 0,
    @TrunkResponseIndex INT = NULL,
    @PageNumber INT = 1,
    @PageSize INT = 30,
    @IsExcel BIT = 0
AS
BEGIN

    SELECT CallView.*,
           CC.Peers,
           CC1.CallerIdName AS [Caller]
    FROM
    (
        SELECT ROW_NUMBER() OVER (ORDER BY C.Id DESC) AS RowNum,
               C.Id,
               C.InitByChannelId,
               C.ProfileId,
               C.ProfileName,
               C.StartDate,
               C.EndDate,
               C.CallDirectionIndex,
               C.CallTypeIndex,
               C.[Status] AS CallStatusIndex,
               CAST(C.CallDuration AS TIME(0)) AS CallDuration,
               CAST(C.RingDuration AS TIME(0)) AS RingDuration,
               C.PhoneNumber
        FROM [Call] AS C WITH (NOLOCK)
            INNER JOIN dbo.fnCall(
                                     @TsId,
                                     @CallDirectionIndex,
                                     @CallTypeIndex,
                                     @CallStatus,
                                     @CallStartDateFrom,
                                     @CallStartDateTo,
                                     @CallEndDateFrom,
                                     @CallEndDateTo,
                                     @CallDurationFrom,
                                     @CallDurationTo,
                                     @CallWaitingFrom,
                                     @CallWaitingTo,
                                     @PhoneNumber,
                                     @CallerIdNum,
                                     @CallerIdName,
                                     @DialedExtension,
                                     @CallChannelState,
                                     @QueueId,
                                     @ExtensionPeerId,
                                     @IsExtensionStarter,
                                     @ExtensionResponseIndex,
                                     @TrunkPeerId,
                                     @IsTrunkStarter,
                                     @TrunkResponseIndex
                                 ) AS tmp
                ON tmp.Id = C.Id
    ) AS CallView
        LEFT JOIN
        (
            SELECT CallId,
                   dbo.Concatenate(P.Code) AS Peers
            FROM CallChannel CC WITH (NOLOCK)
                INNER JOIN Peer AS P WITH (NOLOCK)
                    ON CC.PeerId = P.Id
            WHERE P.PeerTypeIndex = 1
            GROUP BY CC.CallId
        ) AS CC
            ON CallView.Id = CC.CallId
        LEFT JOIN CallChannel AS CC1
            ON CC1.Id = CallView.InitByChannelId
    WHERE (CallView.RowNum
          BETWEEN (((@PageNumber - 1) * @PageSize) + 1) AND @PageNumber * @PageSize
          )
          OR @IsExcel = 1
    ORDER BY CallView.RowNum;

    SELECT COUNT(Id)
    FROM dbo.fnCall(
                       @TsId,
                       @CallDirectionIndex,
                       @CallTypeIndex,
                       @CallStatus,
                       @CallStartDateFrom,
                       @CallStartDateTo,
                       @CallEndDateFrom,
                       @CallEndDateTo,
                       @CallDurationFrom,
                       @CallDurationTo,
                       @CallWaitingFrom,
                       @CallWaitingTo,
                       @PhoneNumber,
                       @CallerIdNum,
                       @CallerIdName,
                       @DialedExtension,
                       @CallChannelState,
                       @QueueId,
                       @ExtensionPeerId,
                       @IsExtensionStarter,
                       @ExtensionResponseIndex,
                       @TrunkPeerId,
                       @IsTrunkStarter,
                       @TrunkResponseIndex
                   );

END;
GO