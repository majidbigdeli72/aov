﻿IF EXISTS (Select * From sys.objects Where object_id = OBJECT_ID(N'VwGeneralProperty') And type IN ( N'V ')) 
Begin
	DROP View [dbo].[VwGeneralProperty]
End
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VwGeneralProperty]
AS

SELECT
	dbo.GeneralPropertyItem.Id,
	dbo.GeneralPropertyItem.GeneralPropertyId,
	dbo.GeneralPropertyItem.[Index],
	dbo.GeneralPropertyItem.[Key],
	dbo.GeneralPropertyItem.Name,
	dbo.GeneralPropertyItem.Attribute,
	dbo.GeneralProperty.[Key] AS GpKey
FROM dbo.GeneralPropertyItem
Inner Join dbo.GeneralProperty ON dbo.GeneralPropertyItem.GeneralPropertyId = dbo.GeneralProperty.Id
GO


IF EXISTS (Select * From sys.objects Where object_id = OBJECT_ID(N'VwFactCall') And type IN ( N'V ')) 
Begin
	DROP View [dbo].[VwFactCall]
End
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VwFactCall]
AS
Select
	c.*,
	GP_CallDirection.[Key] As [CallDirection*],
	GP_CallType.[Key] As [CallType*],
	GP_CallStatus.[Key] As [CallStatus*]
From [Call] As c
Left Outer Join VwGeneralProperty AS GP_CallDirection on (GP_CallDirection.GpKey='CallDirection' and c.CallDirectionIndex=GP_CallDirection.[Index])
Left Outer Join VwGeneralProperty AS GP_CallType on (GP_CallType.GpKey='CallType' and c.CallTypeIndex=GP_CallType.[Index])
Left Outer Join VwGeneralProperty AS GP_CallStatus on (GP_CallStatus.GpKey='CallStatus' and c.[Status]=GP_CallStatus.[Index])
GO


IF EXISTS (Select * From sys.objects Where object_id = OBJECT_ID(N'VwFactCallChannel') And type IN ( N'V ')) 
Begin
	DROP View [dbo].[VwFactCallChannel]
End
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VwFactCallChannel]
AS
Select
	cc.*,
	GP_State.[Key] As [State*],
	GP_ChannelResponse.[Key] As [Response*]
From [CallChannel] As cc
Left Outer Join VwGeneralProperty AS GP_State on (GP_State.GpKey='ChannelState' and cc.[State]=GP_State.[Index])
Left Outer Join VwGeneralProperty AS GP_ChannelResponse on (GP_ChannelResponse.GpKey='ChannelResponse' and cc.Response=GP_ChannelResponse.[Index])
GO



IF EXISTS (Select * From sys.objects Where object_id = OBJECT_ID(N'VwFactPeer') And type IN ( N'V ')) 
Begin
	DROP View [dbo].[VwFactPeer]
End
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VwFactPeer]
AS
Select
	p.*,
	GP_PeerType.[Key] As [PeerType*],
	GP_PeerProtocolType.[Key] As [PeerProtocolType*]
From [Peer] As p
Left Outer Join VwGeneralProperty AS GP_PeerType on (GP_PeerType.GpKey='PeerType' and p.PeerTypeIndex=GP_PeerType.[Index])
Left Outer Join VwGeneralProperty AS GP_PeerProtocolType on (GP_PeerProtocolType.GpKey='PeerProtocolType' and p.PeerProtocolTypeIndex=GP_PeerProtocolType.[Index])
GO


IF EXISTS (Select * From sys.objects Where object_id = OBJECT_ID(N'VwFactQueueCaller') And type IN ( N'V ')) 
Begin
	DROP View [dbo].[VwFactQueueCaller]
End
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VwFactQueueCaller]
AS
Select
	qc.*,
	GP_QueueCallerStatus.[Key] As [QueueCallerStatus*]
From [QueueCaller] As qc
Left Outer Join VwGeneralProperty AS GP_QueueCallerStatus on (GP_QueueCallerStatus.GpKey='QueueCallerStatus' and qc.QueueCallerStatusIndex=GP_QueueCallerStatus.[Index])
GO


IF EXISTS (Select * From sys.objects Where object_id = OBJECT_ID(N'VwFactQueueMember') And type IN ( N'V ')) 
Begin
	DROP View [dbo].[VwFactQueueMember]
End
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[VwFactQueueMember]
AS
Select
	qm.*
From [QueueMember] As qm
GO