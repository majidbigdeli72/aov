﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[ExtentionReport]
    @StartDate AS DATETIME2,
    @EndDate AS DATETIME2,
    @TsId AS INT,
    @Extentions ExtentionList READONLY,
    @DepartmentId INT
AS
DECLARE @temp AS TABLE
(
    Code INT,
    OwnerOperatorId INT,
    InputCountCall INT,
    OutPutCountCall INT,
    MaxCallDuration TIME,
    MinCallDuration TIME,
    AvgCallDuration TIME
);

WITH InComingCall (peerId, IncomingCount)
AS (SELECT p.Id,
           COUNT(1)
    FROM dbo.Call D WITH (NOLOCK)
        INNER JOIN dbo.CallChannel cc WITH (NOLOCK)
            ON cc.CallId = D.Id
        INNER JOIN dbo.Peer p
            ON p.Id = cc.PeerId
    WHERE D.InitByPeerId <> p.Id
          AND cc.Response = 2 -- answerd
          AND
          (
              D.StartDate > @StartDate
              OR @StartDate IS NULL
          )
          AND
          (
              D.StartDate <= @EndDate
              OR @EndDate IS NULL
          )
    GROUP BY p.Id),
     OutComingCall (peerId, OutcomingCount)
AS (SELECT p.Id,
           COUNT(1)
    FROM dbo.Call D WITH (NOLOCK)
        INNER JOIN dbo.Peer p
            ON p.Id = D.InitByPeerId
    WHERE D.CallDirectionIndex = 2
          AND
          (
              D.StartDate > @StartDate
              OR @StartDate IS NULL
          )
          AND
          (
              D.StartDate <= @EndDate
              OR @EndDate IS NULL
          )
    GROUP BY p.Id)
INSERT INTO @temp
(
    Code,
    OwnerOperatorId,
    InputCountCall,
    OutPutCountCall,
    MaxCallDuration,
    MinCallDuration,
    AvgCallDuration
)
SELECT CAST(p.Code AS INT) AS Code,
       p.OwnerOperatorId,
       (
           SELECT ic.IncomingCount FROM InComingCall ic WHERE ic.peerId = p.Id
       ),
       (
           SELECT oc.OutcomingCount FROM OutComingCall oc WHERE oc.peerId = p.Id
       ) AS OutPutCountCall,
       MAX(c.CallDuration) MaxCallDuration,
       MIN(c.CallDuration) MinCallDuration,
       CAST(DATEADD(ss, AVG(DATEDIFF(ss, 0, c.CallDuration)), 0) AS TIME) AS AvgCallDuration
FROM dbo.Peer p WITH (NOLOCK)
    LEFT JOIN
    (
        SELECT *
        FROM dbo.Call f WITH (NOLOCK)
        WHERE (
                  (
                      f.StartDate >= @StartDate
                      OR @StartDate IS NULL
                  )
                  AND
                  (
                      f.StartDate <= @EndDate
                      OR @EndDate IS NULL
                  )
              )
    ) AS c
        ON c.InitByPeerId = p.Id
WHERE p.PeerTypeIndex = 1
      AND p.TsId = @TsId
      AND p.Enabled = 1
      AND
      (
          p.DepartmentId = @DepartmentId
          OR @DepartmentId = 0
      )
      AND
      (
          c.CallDuration IS NULL
          OR CONVERT(CHAR(10), c.CallDuration, 108) > '00:00:00'
      )
GROUP BY p.Code,
         p.Id,
         p.OwnerOperatorId
ORDER BY CAST(p.Code AS INT);


IF
(
    SELECT COUNT(1) FROM @Extentions
) > 0
BEGIN

    SELECT t.AvgCallDuration,
           t.Code,
           t.InputCountCall,
           t.MaxCallDuration,
           t.MinCallDuration,
           t.OutPutCountCall,
           t.OwnerOperatorId
    FROM @temp t
        INNER JOIN @Extentions e
            ON t.Code = e.Code;

    RETURN;
END;
ELSE
BEGIN

    SELECT AvgCallDuration,
           Code,
           InputCountCall,
           MaxCallDuration,
           MinCallDuration,
           OutPutCountCall,
           OwnerOperatorId
    FROM @temp;
    RETURN;
END;
GO