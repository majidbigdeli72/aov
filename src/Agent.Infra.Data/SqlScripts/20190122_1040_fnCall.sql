﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kamal Jalali
-- Create date: 2018-11-27 19:20:00
-- Description:	Function that returns call report
-- =============================================
ALTER FUNCTION [dbo].[fnCall]
(	
	@TsId Int,
	@CallDirectionIndex Int=null,
	@CallTypeIndex Int=null,
	@CallStatus Int=null,
	@CallStartDateFrom DateTime=null,
	@CallStartDateTo DateTime=null,
	@CallEndDateFrom DateTime=null,
	@CallEndDateTo DateTime=null,
	@CallDurationFrom varchar(8)=null,
	@CallDurationTo varchar(8)=null,
	@CallWaitingFrom varchar(8)=null,
	@CallWaitingTo varchar(8)=null,	
	@PhoneNumber varchar(50)=Null,
	@CallerIdNum varchar(50)=Null,
	@CallerIdName nvarchar(50)=Null,
	@DialedExtension nvarchar(50)=Null,
	@CallChannelState Int=Null,
	@QueueId Int=Null,
	@ExtensionPeerId Int=Null,
	@IsExtensionStarter Bit=0,
	@ExtensionResponseIndex Int=Null,
	@TrunkPeerId Int=Null,
	@IsTrunkStarter Bit=0,
	@TrunkResponseIndex Int=Null
)
RETURNS TABLE 
AS
RETURN 
(
	Select			
		C.Id			
		From [Call] As C  With (NoLock)
		Inner Join 
		(
			Select
				CC.CallId
			From CallChannel As CC With (NoLock)
			Where (@CallChannelState Is Null Or CC.[State]=@CallChannelState)
			And (@DialedExtension Is Null Or CC.DialedExtension Like @DialedExtension)
			/*And (@CallerIdNum Is Null Or (CC.CallerIdNum Like N'%'+@CallerIdNum+'%' Or CC.CallerIdName Like N'%'+@CallerIdName+'%'))*/
			And (@CallerIdName Is Null Or (CC.CallerIdName Like @CallerIdName Or CC.CallerIdNum Like @CallerIdNum))
			And (@CallStartDateFrom Is Null Or CC.CreateDate>=@CallStartDateFrom)
			And (@CallStartDateTo Is Null Or CC.CreateDate<=@CallStartDateTo)
			and (@ExtensionPeerId Is Null or (@IsExtensionStarter=1 OR (CC.PeerId=@ExtensionPeerId And (@ExtensionResponseIndex IS NULL OR CC.Response=@ExtensionResponseIndex))))
			Group By CC.CallId
		) As CCE On C.Id=CCE.CallId
		Inner Join 
		(
			Select
				CC.CallId
			From CallChannel As CC With (NoLock)
			Where (@CallChannelState Is Null Or CC.[State]=@CallChannelState)
			And (@DialedExtension Is Null Or CC.DialedExtension Like @DialedExtension)
			/*And (@CallerIdNum Is Null Or (CC.CallerIdNum Like N'%'+@CallerIdNum+'%' Or CC.CallerIdName Like N'%'+@CallerIdName+'%'))*/
			And (@CallerIdName Is Null Or (CC.CallerIdName Like @CallerIdName Or CC.CallerIdNum Like @CallerIdNum))
			And (@CallStartDateFrom Is Null Or CC.CreateDate>=@CallStartDateFrom)
			And (@CallStartDateTo Is Null Or CC.CreateDate<=@CallStartDateTo)
			And (@TrunkPeerId Is Null or (@IsTrunkStarter=1 OR (CC.PeerId=@TrunkPeerId And (@TrunkResponseIndex is null or CC.Response=@TrunkResponseIndex))))
			Group By CC.CallId
		) As CCT On C.Id=CCT.CallId
		Inner Join 
		(
			Select
				CC.CallId
			From CallChannel As CC With (NoLock)
			Left Join QueueCaller As QC With (NoLock) On CC.Id=QC.CallChannelId
			Where (@CallChannelState Is Null Or CC.[State]=@CallChannelState)
			And (@DialedExtension Is Null Or CC.DialedExtension Like @DialedExtension)
			/*And (@CallerIdNum Is Null Or (CC.CallerIdNum Like N'%'+@CallerIdNum+'%' Or CC.CallerIdName Like N'%'+@CallerIdName+'%'))*/
			And (@CallerIdName Is Null Or (CC.CallerIdName Like @CallerIdName Or CC.CallerIdNum Like @CallerIdNum))
			And (@QueueId Is Null Or QC.QueueId=@QueueId)			
			And (@CallStartDateFrom Is Null Or CC.CreateDate>=@CallStartDateFrom)
			And (@CallStartDateTo Is Null Or CC.CreateDate<=@CallStartDateTo)
			Group By CC.CallId
		) As CCQ On C.Id=CCQ.CallId
		Where C.TsId=@TsId
		And (@CallStatus Is Null Or C.[Status]=@CallStatus)
		And (@CallDirectionIndex Is Null Or C.CallDirectionIndex=@CallDirectionIndex)
		And (@CallTypeIndex Is Null Or C.CallTypeIndex=@CallTypeIndex)	
		And (@CallStartDateFrom Is Null Or C.StartDate>=@CallStartDateFrom) And (@CallStartDateTo Is Null Or C.StartDate<=@CallStartDateTo)
		And (@CallEndDateFrom Is Null Or C.EndDate>=@CallEndDateFrom) And (@CallEndDateTo Is Null Or C.EndDate<=@CallEndDateTo)
		And (@CallDurationFrom Is Null Or C.CallDuration>=@CallDurationFrom) And (@CallDurationTo Is Null Or C.CallDuration<=@CallDurationTo)
		And (@CallWaitingFrom Is Null Or C.RingDuration>=@CallWaitingFrom) And (@CallWaitingTo Is Null Or C.RingDuration<=@CallWaitingTo)
		And (@PhoneNumber Is Null Or C.PhoneNumber Like @PhoneNumber)
		And (@ExtensionPeerId Is null OR (@IsExtensionStarter=0 OR (C.InitByPeerId=@ExtensionPeerId)))
		And (@TrunkPeerId Is Null Or (@IsTrunkStarter=0 or (C.InitByPeerId=@TrunkPeerId)))
)
GO