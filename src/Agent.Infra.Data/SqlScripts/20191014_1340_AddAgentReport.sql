﻿CREATE TYPE [dbo].[ExtentionList] AS TABLE(
	[Code] [int] NULL
)
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[ExtentionReport]
    @StartDate AS DATETIME2,
    @EndDate AS DATETIME2,
    @TsId AS INT,
    @Extentions ExtentionList READONLY,
    @DepartmentId INT
AS
DECLARE @temp AS TABLE
(
    Code INT,
    OwnerOperatorId INT,
    InputCountCall INT,
    OutPutCountCall INT,
    MaxCallDuration TIME,
    MinCallDuration TIME,
    AvgCallDuration TIME
);

INSERT INTO @temp
(
    Code,
    OwnerOperatorId,
    InputCountCall,
    OutPutCountCall,
    MaxCallDuration,
    MinCallDuration,
    AvgCallDuration
)
SELECT CAST(p.Code AS INT) AS Code,
       p.OwnerOperatorId,
       (
           SELECT COUNT(1)
           FROM dbo.Call D
               INNER JOIN dbo.CallChannel cc
                   ON cc.CallId = D.Id
           WHERE --D.CallDirectionIndex = 1 --IncomingComingCall
                 --AND
				 cc.PeerId = p.Id
                 AND D.InitByPeerId <> p.Id
                 AND cc.Response = 2 -- answerd
                 AND
                 (
                     D.StartDate > @StartDate
                     OR @StartDate IS NULL
                 )
                 AND
                 (
                     D.StartDate < @EndDate
                     OR @EndDate IS NULL
                 )
       ) AS InputCountCall,
       (
           SELECT COUNT(1)
           FROM dbo.Call D
           WHERE D.CallDirectionIndex = 2
                 AND D.InitByPeerId = p.Id
                 AND
                 (
                     D.StartDate > @StartDate
                     OR @StartDate IS NULL
                 )
                 AND
                 (
                     D.StartDate <= @EndDate
                     OR @EndDate IS NULL
                 )
       ) AS OutPutCountCall,
       MAX(c.CallDuration) MaxCallDuration,
       MIN(c.CallDuration) MinCallDuration,
       CAST(DATEADD(ss, AVG(DATEDIFF(ss, 0, c.CallDuration)), 0) AS TIME) AS AvgCallDuration
FROM dbo.Peer p
    LEFT JOIN
    (
        SELECT *
        FROM dbo.Call f
        WHERE (
                  (
                      f.StartDate >= @StartDate
                      OR @StartDate IS NULL
                  )
                  AND
                  (
                      f.StartDate <= @EndDate
                      OR @EndDate IS NULL
                  )
              )
    ) AS c
        ON c.InitByPeerId = p.Id
WHERE p.PeerTypeIndex = 1
      AND p.TsId = @TsId
      AND p.Enabled = 1
      AND
      (
          p.DepartmentId = @DepartmentId
          OR @DepartmentId = 0
      )
GROUP BY p.Code,
         p.Id,
         p.OwnerOperatorId
ORDER BY CAST(p.Code AS INT);


IF
(
    SELECT COUNT(1) FROM @Extentions
) > 0
BEGIN

    SELECT t.AvgCallDuration,
           t.Code,
           t.InputCountCall,
           t.MaxCallDuration,
           t.MinCallDuration,
           t.OutPutCountCall,
           t.OwnerOperatorId
    FROM @temp t
        INNER JOIN @Extentions e
            ON t.Code = e.Code;

    RETURN;
END;
ELSE
BEGIN

    SELECT AvgCallDuration,
           Code,
           InputCountCall,
           MaxCallDuration,
           MinCallDuration,
           OutPutCountCall,
           OwnerOperatorId
    FROM @temp;
    RETURN;
END;
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetDeparment]
AS
    SELECT Id,Title,ParentId FROM dbo.Department
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetExtentionByDepartmentId]
    @DepartmentId AS INT,
    @TsId AS INT
AS
IF (@DepartmentId IS NULL)
BEGIN
    SELECT CAST(Code AS INT) AS Code
    FROM dbo.Peer
    WHERE DepartmentId IS NULL
          AND PeerTypeIndex = 1
          AND [Enabled] = 1
          AND TsId = @TsId
    ORDER BY CAST(Code AS INT);
    RETURN;
END;

SELECT CAST(Code AS INT) AS Code
FROM dbo.Peer
WHERE [Enabled] = 1
      AND TsId = @TsId
      AND PeerTypeIndex = 1
      AND
      (
          DepartmentId = @DepartmentId
          OR @DepartmentId = 0
      )
ORDER BY CAST(Code AS INT);
GO