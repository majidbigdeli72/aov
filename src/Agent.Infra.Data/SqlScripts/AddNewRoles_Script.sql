﻿If Not Exists(Select Top 1 [Name] From [dbo].[AspNetRoles] Where [Name]=N'VoipManager') And (Select Count(Id) From [dbo].[AspNetRoles]) > 0
Begin
	INSERT INTO [dbo].[AspNetRoles] ([Name]) VALUES (N'VoipManager')
End
GO

/* Adding user roles for admin */
Declare @UserId As Int
Set @UserId = (Select Id From [dbo].[AspNetUsers] Where [UserName]='admin')
IF @UserId Is Not NULL
Begin
	Delete From [dbo].[AspNetUserRoles] Where UserId=@UserId

	Insert Into [dbo].[AspNetUserRoles] (UserId, RoleId) Values(@UserId, (Select Top 1 Id From [dbo].[AspNetRoles] Where [Name]='Admin'))
	Insert Into [dbo].[AspNetUserRoles] (UserId, RoleId) Values(@UserId, (Select Top 1 Id From [dbo].[AspNetRoles] Where [Name]='Operator'))
	Insert Into [dbo].[AspNetUserRoles] (UserId, RoleId) Values(@UserId, (Select Top 1 Id From [dbo].[AspNetRoles] Where [Name]='VoipManager'))
End
Go