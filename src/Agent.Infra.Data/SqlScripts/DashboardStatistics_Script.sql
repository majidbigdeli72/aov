﻿IF EXISTS (Select *From sys.objects Where object_id = OBJECT_ID(N'spGetDashboardStatistics') And type IN ( N'P', N'PC' )) 
Begin
	DROP PROCEDURE [dbo].[spGetDashboardStatistics]
End
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kamal Jalali
-- Create date: 2018-04-11 16:43:46.023
-- Description:	Retrives dashboard data for guages and statistics
-- =============================================
CREATE PROCEDURE [dbo].[spGetDashboardStatistics]
	@FromDate DATETIME,
	@QueueId INT=Null
AS
BEGIN
	
    /*===================================
    WaitTime AVG & MAX
    ===================================*/
    DECLARE @AVGWaitTime VARCHAR(20), @LongestWaitTime VARCHAR(20)
    
    SELECT TOP 1
    	@AVGWaitTime=(CONVERT(VARCHAR(8), Cast(DateAdd(ms, AVG(CAST(DateDiff( ms, '00:00:00', cast((LeaveDate-JoinDate) as time)) AS BIGINT)), '00:00:00' ) as Time ))),
    	@LongestWaitTime=(CONVERT(VARCHAR(8), Cast(DateAdd(ms, MAX(CAST(DateDiff( ms, '00:00:00', cast((LeaveDate-JoinDate) as time)) AS BIGINT)), '00:00:00' ) as Time )))
    FROM QueueCaller
    WHERE JoinDate > CONVERT(datetime,@FromDate)
	AND QueueCallerStatusIndex <> 1
    AND (@QueueId IS NULL OR QueueId=@QueueId)

	    
    /*===================================
    Call TalkTime AVG & MAX
    ===================================*/
    DECLARE @AVGTalkTime VARCHAR(20), @LongestTalkTime VARCHAR(20)
    
    SELECT TOP 1
    	@AVGTalkTime=(CONVERT(VARCHAR(8), Cast(DateAdd(ms, AVG(CAST(DateDiff( ms, '00:00:00', cast((cc.HangupDate-cc.ConnectDate) as time)) AS BIGINT)),'00:00:00' ) as Time ))),
    	@LongestTalkTime=(CONVERT(VARCHAR(8), Cast(DateAdd(ms, MAX(CAST(DateDiff( ms, '00:00:00', cast((cc.HangupDate-cc.ConnectDate) as time)) AS BIGINT)), '00:00:00' ) as Time )))
    FROM dbo.QueueCaller qc
    LEFT JOIN dbo.CallChannel cc ON qc.AnsweredByCallChannelId=cc.Id
    WHERE qc.JoinDate > CONVERT(datetime,@FromDate)
	AND qc.QueueCallerStatusIndex=2
    AND (@QueueId IS NULL OR qc.QueueId=@QueueId)
    AND cc.[State]=11
    
    
    /*===================================
    CallerWaitTimes
    ===================================*/
    Create Table #tmpQueueCallerWaitTimes
    (
    	WaitTimeSeconds Int,
    	TotalCount Int
    )
    Insert Into #tmpQueueCallerWaitTimes
    Select
    	Coalesce(DateDiff(Second, JoinDate, LeaveDate),0) As WaitTimeSeconds,
    	Count(QueueId) As TotalCount
    From QueueCaller
    Where QueueCallerStatusIndex=2
    And JoinDate > Convert(Datetime,@FromDate)
	And Coalesce(DateDiff(Second, JoinDate, LeaveDate),0) > 0
	--AND (DATEDIFF(SECOND, JoinDate, LeaveDate) <= 60 And (DATEDIFF(SECOND, JoinDate, LeaveDate) > 0))
    And (@QueueId IS NULL OR QueueId=@QueueId)
    Group By Rollup (DateDiff(Second, JoinDate, LeaveDate)) 
    
    Declare @TotalWaitTimesCount Int
    Set @TotalWaitTimesCount=IsNull((Select Top 1 TotalCount From #tmpQueueCallerWaitTimes Where WaitTimeSeconds=0),1)


	/*===================================
    Select data
    ===================================*/
	DECLARE @WaitTimeMinThreshold Int
	Set @WaitTimeMinThreshold = (Case When @QueueId Is Null
									  Then (Select IsNull(dbo.fnGetSecondsFromTime(Min(WaitTimeMinThreshold)), 0) From [Queue])
									  Else (Select Top 1 IsNull(dbo.fnGetSecondsFromTime(WaitTimeMinThreshold), 0) From [Queue] Where Id=@QueueId)
								 End)

	DECLARE @WaitTimeCriticalThreshold Int
	Set @WaitTimeCriticalThreshold = (Case When @QueueId Is Null
									  Then (Select IsNull(dbo.fnGetSecondsFromTime(Max(WaitTimeCriticalThreshold)), 0) From [Queue])
									  Else (Select Top 1 IsNull(dbo.fnGetSecondsFromTime(WaitTimeCriticalThreshold), 0) From [Queue] Where Id=@QueueId)
								 End)


	DECLARE @RecalledAbandoneds INT
	SET @RecalledAbandoneds=(Isnull((Select Count(Distinct CC.CallerIdNum) From CallChannel As CC
										Left Join QueueCaller As QC ON CC.Id=QC.CallChannelId
										Where (@QueueId Is Null Or QC.QueueId=@QueueId)
										And QC.LeaveDate > Convert(Datetime,@FromDate)
										And QC.QueueCallerStatusIndex=2
										And CC.CallerIdNum In(
											Select CC2.CallerIdNum From CallChannel As CC2
											Left Join QueueCaller As QC2 ON CC2.Id=QC2.CallChannelId
											Where (@QueueId Is Null Or QC2.QueueId=@QueueId)
											And QC2.LeaveDate > Convert(Datetime,@FromDate)
											And QC2.QueueCallerStatusIndex=3
											And DATEDIFF(SECOND, QC2.JoinDate, QC2.LeaveDate) >= @WaitTimeMinThreshold
										)),0))
										

	
    Declare @Completeds Int
	Set @Completeds=(Select Count(Id) From QueueCaller As QC
					 Where QC.QueueCallerStatusIndex=2
					 And (@QueueId IS NULL OR QC.QueueId=@QueueId)
					 And QC.JoinDate > CONVERT(datetime,@FromDate))


	Declare @Abandoneds Int
	Set @Abandoneds=(Select Count(Id) From QueueCaller As QC
					 Where QC.QueueCallerStatusIndex=3
					 And (@QueueId IS NULL OR QC.QueueId=@QueueId)
					 And QC.JoinDate > CONVERT(datetime,@FromDate)
					 And DATEDIFF(SECOND, QC.JoinDate, QC.LeaveDate) >= @WaitTimeMinThreshold)


	Declare @Stat_1 Int
	Set @Stat_1 = ((ISNULL((SELECT SUM(TotalCount) FROM #tmpQueueCallerWaitTimes WHERE WaitTimeSeconds>0 AND WaitTimeSeconds<=@WaitTimeMinThreshold),0)*100)/@TotalWaitTimesCount)

	Declare @Stat_2 As Int
	Set @Stat_2 = ((ISNULL((SELECT SUM(TotalCount) FROM #tmpQueueCallerWaitTimes WHERE WaitTimeSeconds>0 AND WaitTimeSeconds<=(@WaitTimeCriticalThreshold/4)),0)*100)/@TotalWaitTimesCount)

	Declare @Stat_3 As Int
	Set @Stat_3 = ((ISNULL((SELECT SUM(TotalCount) FROM #tmpQueueCallerWaitTimes WHERE WaitTimeSeconds>0 AND WaitTimeSeconds<=(@WaitTimeCriticalThreshold/4)*2),0)*100)/@TotalWaitTimesCount)

	Declare @Stat_4 As Int
	Set @Stat_4 = ((ISNULL((SELECT SUM(TotalCount) FROM #tmpQueueCallerWaitTimes WHERE WaitTimeSeconds>0 AND WaitTimeSeconds<=(@WaitTimeCriticalThreshold/4)*3),0)*100)/@TotalWaitTimesCount)

	Declare @Stat_5 As Int
	Set @Stat_5 = ((ISNULL((SELECT SUM(TotalCount) FROM #tmpQueueCallerWaitTimes WHERE WaitTimeSeconds>0 AND WaitTimeSeconds<=@WaitTimeCriticalThreshold),0)*100)/@TotalWaitTimesCount)


    SELECT
		Completeds=(ISNULL(@Completeds,0)),
    	Abandoneds=(ISNULL(@Abandoneds,0)),
		RecalledAbandoneds=@RecalledAbandoneds,
    	AVGWaitTime=ISNULL(@AVGWaitTime, '00:00:00'),
    	LongestWaitTime=ISNULL(@LongestWaitTime, '00:00:00'),
    	AVGTalkTime=ISNULL(@AVGTalkTime, '00:00:00'),
    	LongestTalkTime=ISNULL(@LongestTalkTime, '00:00:00'),
		
		WaitTimeSeconds_1=@WaitTimeMinThreshold,
		WaitTimeSecondsVal_1=@Stat_1,
		
		WaitTimeSeconds_2=(@WaitTimeCriticalThreshold/4),
		WaitTimeSecondsVal_2=@Stat_2,

		WaitTimeSeconds_3=(@WaitTimeCriticalThreshold/4)*2,
		WaitTimeSecondsVal_3=@Stat_3,

		WaitTimeSeconds_4=(@WaitTimeCriticalThreshold/4)*3,
		WaitTimeSecondsVal_4=@Stat_4,

		WaitTimeSeconds_5=@WaitTimeCriticalThreshold,
		WaitTimeSecondsVal_5=@Stat_5    
	

    DROP TABLE #tmpQueueCallerWaitTimes
    

END
GO


Update [Queue] Set
	[WaitTimeMinThreshold]='00:00:30',
	[WaitTimeCriticalThreshold]='00:05:00',
	[WaitTimeMaxThreshold]='00:10:00'
Go