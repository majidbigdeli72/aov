﻿Declare @ConstName As Varchar(500)
Set @ConstName=(Select name FROM sys.default_constraints where parent_object_id = OBJECT_ID(N'Module', N'U') AND name like '%Modify%')

DECLARE @sql NVARCHAR(MAX)
Set @sql = 'ALTER TABLE dbo.Module DROP CONSTRAINT ' + @ConstName
EXEC (@sql)
GO

ALTER TABLE Module ADD CONSTRAINT DF_Module_ModifyDate DEFAULT GETDATE() FOR ModifyDate
GO