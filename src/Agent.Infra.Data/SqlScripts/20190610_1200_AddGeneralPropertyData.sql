﻿SET IDENTITY_INSERT [dbo].[GeneralProperty] ON 
GO
INSERT [dbo].[GeneralProperty] ([Id], [Key], [Mame]) VALUES (1, N'CallDirection', N'جهت تماس')
GO
INSERT [dbo].[GeneralProperty] ([Id], [Key], [Mame]) VALUES (2, N'CallType', N'نوع تماس')
GO
INSERT [dbo].[GeneralProperty] ([Id], [Key], [Mame]) VALUES (3, N'CallStatus', N'وضعیت تماس')
GO
INSERT [dbo].[GeneralProperty] ([Id], [Key], [Mame]) VALUES (4, N'ChannelState', N'وضعیت کانال')
GO
INSERT [dbo].[GeneralProperty] ([Id], [Key], [Mame]) VALUES (5, N'ChannelResponse', NULL)
GO
INSERT [dbo].[GeneralProperty] ([Id], [Key], [Mame]) VALUES (6, N'QueueCallerStatus', NULL)
GO
INSERT [dbo].[GeneralProperty] ([Id], [Key], [Mame]) VALUES (7, N'PeerType', NULL)
GO
INSERT [dbo].[GeneralProperty] ([Id], [Key], [Mame]) VALUES (8, N'PeerProtocolType', NULL)
GO
INSERT [dbo].[GeneralProperty] ([Id], [Key], [Mame]) VALUES (9, N'QueueCallerStatus', NULL)
GO
SET IDENTITY_INSERT [dbo].[GeneralProperty] OFF
GO

SET IDENTITY_INSERT [dbo].[GeneralPropertyItem] ON 
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (1, 1, 1, N'Incoming', N'تماس ورودی', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (2, 1, 2, N'Outgoing', N'تماس خروجی', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (3, 2, 1, N'Missed', N'تماس از دست رفته', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (4, 2, 2, N'Incoming', N'تماس ورودی', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (5, 2, 3, N'Outgoing', N'تماس خروجی', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (6, 2, 4, N'Internal', N'تماس داخلی', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (7, 3, 1, N'Live', N'تماس زنده', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (8, 3, 2, N'Ended', N'تماس پایان یافته', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (9, 4, 0, N'Down', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (10, 4, 1, N'Rsrvd', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (11, 4, 2, N'OffHook', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (12, 4, 3, N'Dialing', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (13, 4, 4, N'Ring', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (14, 4, 5, N'Ringing', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (15, 4, 6, N'Up', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (16, 4, 7, N'Busy', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (17, 4, 8, N'DialingOffhook', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (18, 4, 9, N'PreRing', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (19, 4, 10, N'Unknown', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (20, 4, 11, N'Hangedup', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (21, 5, 1, N'NotAnswered', N'بدون پاسخ', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (22, 5, 2, N'Answered', N'پاسخ داده شده', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (23, 5, 3, N'Busy', N'درحال مکالمه', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (24, 5, 4, N'Transfered', N'انتقال داده شده', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (25, 6, 1, N'Waiting', N'در حال انتظار', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (26, 6, 2, N'Joined', N'متصل شده', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (27, 6, 3, N'Abandoned', N'از دست رفته', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (28, 7, 1, N'Extension', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (29, 7, 2, N'Trunk', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (30, 8, 1, N'SIP', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (31, 8, 2, N'IAX2', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (32, 8, 3, N'DAHDI', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (33, 8, 4, N'PJSIP', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (34, 8, 5, N'LOCAL', NULL, NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (35, 9, 1, N'Waiting', N'در حال انتظار', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (36, 9, 2, N'Joined', N'متصل شده', NULL)
GO
INSERT [dbo].[GeneralPropertyItem] ([Id], [GeneralPropertyId], [Index], [Key], [Name], [Attribute]) VALUES (38, 9, 3, N'Abandoned', N'از دست رفته', NULL)
GO
SET IDENTITY_INSERT [dbo].[GeneralPropertyItem] OFF
GO