﻿IF EXISTS (Select *From sys.objects Where object_id = OBJECT_ID(N'spGetMemberStatistics') And type IN ( N'P', N'PC' )) 
Begin
	DROP PROCEDURE [dbo].[spGetMemberStatistics]
End
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Kamal Jalali
-- Create date: 2018-04-23 14:03:46.023
-- Description:	Retrives queue member statistics
-- =============================================
CREATE PROCEDURE [dbo].[spGetMemberStatistics]
	@FromDate DATETIME,
	@QueueId INT=NULL
AS
BEGIN
	
	SELECT Ext=cc.CallerIdNum, AnsweredCount=COUNT(cc.Id)
	FROM [QueueCaller] qc
	LEFT JOIN [CallChannel] cc ON qc.AnsweredByCallChannelId=cc.Id
	WHERE (@QueueId IS NULL OR qc.QueueId=@QueueId)
	AND qc.QueueCallerStatusIndex=2
	AND qc.JoinDate > CONVERT(datetime,@FromDate)
	GROUP BY cc.CallerIdNum
END
GO