﻿using Septa.TelephonyServer.Entries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer
{
    public interface ITelephonyServer : IDisposable
    {
        int Id { get; }
        string Key { get; }
        string Type { get; }
        string OutgoingPrefix { get; }

        bool CanDial { get; }
        bool Initialized { get; }


        string RecordingsRootPath { get; }
        string CountryAreaCode { get; }
        string CityAreaCode { get; }


        List<PeerEntry> Peers { get; }
        List<TrunkEntry> Trunks { get; }
        List<ExtensionEntry> Extensions { get; }
        List<QueueEntry> Queues { get; }
        List<CallEntry> Calls { get; }

        #region DestinationTypes
        List<AnnouncementEntry> Announcements { get; }

        List<CallFlowControlEntry> CallFlowControls { get; }

        List<CallRecordingEntry> CallRecordings { get; }

        List<CallBackEntry> CallBacks { get; }

        List<ConferenceEntry> Conferences { get; }

        List<CustomApplicationEntry> CustomApplications { get; }

        List<DisaEntry> Disas { get; }

        //List<ExtensionEntry> Extensions { get; }

        List<FeatureCodeAdminEntry> FeatureCodeAdmins { get; }

        List<IvrEntry> Ivrs { get; }

        List<LanguageEntry> Languages { get; }

        List<MiscDestinationEntry> MiscDestinations { get; }

        List<PagingAndIntercomEntry> PagingAndIntercoms { get; }

        List<PhonebookDirectoryEntry> PhonebookDirectories { get; }

        List<QueuePriorityEntry> QueuePriorities { get; }

        //List<QueueEntry> Queues { get; }

        List<RingGroupEntry> RingGroups { get; }

        List<TerminateCallEntry> TerminateCalls { get; }

        List<TimeConditionEntry> TimeConditions { get; }

        //List<TrunkEntry> Trunks { get; }

        List<VoicemailBlastingEntry> VoicemailBlastings { get; }
        #endregion
        
        void Run();
        void Stop();


        void Reload();

        void UpdateExtensionStatus(ExtensionEntry extension);
        OriginateResponseStatus Originate(string channel, string channelCID, string number, bool applyOutgoingRules, Dictionary<string, string> variables = null);
        OriginateResponseStatus OriginateToExtension(string exten, string extenCID, string number, bool applyOutgoingRules, Dictionary<string, string> variables = null);
        string GetStandardNumber(string number, bool applyOutgoingRules);
        byte[] GetRecordingFile(string keys);
        CallChannelEntry GetChannel(string uniqueId);
        bool ChannelSpy(string extn, string spiedExtn, SpyMode spyMode);
        bool ChannelHangup(string channelName);
        void CheckConnection();
    }
}
