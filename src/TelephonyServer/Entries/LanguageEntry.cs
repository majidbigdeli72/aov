﻿
namespace Septa.TelephonyServer.Entries
{
    public class LanguageEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"app-languages,{Id},1";
            }
            set
            {
            }
        }
    }
}
