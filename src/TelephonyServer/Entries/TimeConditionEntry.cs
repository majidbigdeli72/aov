﻿namespace Septa.TelephonyServer.Entries
{
    public class TimeConditionEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"timeconditions,{Id},1";
            }
            set
            {
            }
        }
    }
}
