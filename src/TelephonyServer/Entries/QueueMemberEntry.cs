﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Septa.TelephonyServer.Entries
{
    
    public class QueueMemberEntry
    {
        
        public long DbId { get; set; }

        
        public string Ext { get; set; }

        
        public string Name { get; set; }

        public QueueEntry Queue { get; set; }

        public string Location
        {
            set
            {
                var pattern = @"(Local/(?<lineNo>\d*)@)";
                var match = System.Text.RegularExpressions.Regex.Match(value, pattern);
                if (match.Success)
                {
                    Ext = match.Groups["lineNo"].Value;
                }
            }
        }
        
        public int CallsTaken { get; set; }
    }
}