﻿namespace Septa.TelephonyServer.Entries
{
    public class PagingAndIntercomEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"app-pagegroups,{Id},1";
            }
            set
            {
            }
        }
    }
}
