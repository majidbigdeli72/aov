﻿
namespace Septa.TelephonyServer.Entries
{
    public class RingGroupEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"ext-group,{Id},1";
            }
            set
            {
            }
        }
    }
}
