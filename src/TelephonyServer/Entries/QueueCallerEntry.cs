﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Septa.TelephonyServer.Entries
{
    
    public class QueueCallerEntry
    {
        
        public long? DbId { get; set; }
        
        public string ParentQueueNo { get; set; }
        public QueueEntry ParentQueue { get; set; }
        
        public string UniqueId { get; set; }
        public CallChannelEntry CallChannel { get; set; }
        
        public string Name { get; set; }
        
        public string Number { get; set; }
        
        public QueueCallerStatus Status { get; set; }
        
        public DateTime JoinDate { get; set; }
        
        public DateTime? LeaveDate { get; set; }
        public TimeSpan WaitTime
        {
            get
            {
                if (LeaveDate.HasValue)
                    return LeaveDate.Value - JoinDate;
                else
                    return DateTime.Now - JoinDate;
            }
        }
        public string WaitTimeSt
        {
            get
            {
                return WaitTime.ToString(@"hh\:mm\:ss");
            }
        }

        public bool ShouldBeWaitAlerted
        {
            get
            {
                return ParentQueue.WaitedCallerMaxThresholdPlayAlert && WaitTime > ParentQueue.WaitedCallerMaxThreshold;
            }
        }

        public bool ShouldBeJoinAlerted
        {
            get
            {
                return ParentQueue.JoinPlayAlert && JoinDate < DateTime.Now.AddSeconds(5);
            }
        }

        public bool ShouldBeLeaveAlerted
        {
            get
            {
                return ParentQueue.LeavePlayAlert && LeaveDate < DateTime.Now.AddSeconds(5);
            }
        }

        
        public int JoinPosition { get; set; }

        
        public int? AbandonPosition { get; set; }

        
        public CallChannelEntry AnsweredBy { get; set; }

        public QueueCallerEntry()
        {
            Status = QueueCallerStatus.Waiting;
        }
    }
}