﻿using Septa.TelephonyServer;
using System.Runtime.Serialization;

namespace Septa.TelephonyServer.Entries
{
    

    public abstract class PeerEntry : BaseDestinationEntry
    {
        
        public int DbId { get; set; }

        
        public PeerProtocolType Protocol { get; set; }

        
        public abstract PeerType Type { get; set; }

        public override string ToString()
        {
            return $"{Type} - {Protocol}/{Name}";
        }
    }
}
