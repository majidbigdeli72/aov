﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer.Entries
{
    
    public abstract class BaseDestinationEntry
    {
        
        public string Id { get; set; }

        
        public string Name { get; set; }

        
        public abstract string DestinationCode { get; set; }

        public static DestinationType GetDestinationType(string destCode)
        {
            if (string.IsNullOrEmpty(destCode))
                return DestinationType.Custom;

            if (destCode.StartsWith("app-announcement")) return DestinationType.Announcements;
            if (destCode.StartsWith("app-daynight")) return DestinationType.CallFlowControl;
            if (destCode.StartsWith("ext-callrecording")) return DestinationType.CallRecording;
            if (destCode.StartsWith("callback")) return DestinationType.Callback;
            if (destCode.StartsWith("ext-meetme")) return DestinationType.Conferences;
            //if (destCode.StartsWith("CustomApplication")) return DestinationType.CustomApplications;
            if (destCode.StartsWith("disa")) return DestinationType.DISA;
            if (destCode.StartsWith("from-did-direct")) return DestinationType.Extensions;
            if (destCode.StartsWith("ext-featurecodes")) return DestinationType.FeatureCodeAdmin;
            if (destCode.StartsWith("ivr")) return DestinationType.IVR;
            if (destCode.StartsWith("app-languages")) return DestinationType.Languages;
            if (destCode.StartsWith("ext-miscdests")) return DestinationType.MiscDestinations;
            if (destCode.StartsWith("app-pagegroups")) return DestinationType.PagingAndIntercom;
            if (destCode.StartsWith("app-pbdirectory")) return DestinationType.PhonebookDirectory;
            if (destCode.StartsWith("app-queueprio")) return DestinationType.QueuePriorities;
            if (destCode.StartsWith("ext-queues")) return DestinationType.Queues;
            if (destCode.StartsWith("ext-group")) return DestinationType.RingGroups;
            if (destCode.StartsWith("app-blackhole")) return DestinationType.TerminateCall;
            if (destCode.StartsWith("timeconditions")) return DestinationType.TimeConditions;
            if (destCode.StartsWith("ext-trunk")) return DestinationType.Trunks;
            if (destCode.StartsWith("app-vmblast")) return DestinationType.VoicemailBlasting;

            return DestinationType.Custom;
        }
    }
}
