﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Septa.TelephonyServer.Entries
{
    
    public class QueueEntry : BaseDestinationEntry
    {
        
        public int DbId { get; set; }

        
        public bool JoinPlayAlert { get; set; }

        
        public bool LeavePlayAlert { get; set; }

        
        public TimeSpan WaitTimeMinThreshold { get; set; }

        
        public TimeSpan WaitTimeMaxThreshold { get; set; }

        
        public bool WaitTimeMaxThresholdPlayAlert { get; set; }

        
        public TimeSpan WaitedCallerMaxThreshold { get; set; }

        
        public bool WaitedCallerMaxThresholdPlayAlert { get; set; }

        
        public bool RemoveFromAbandonedAfterCallback { get; set; }

        
        public int Completed { get; set; }

        
        public int Abandoned { get; set; }

        
        public DateTime CreateDate { get; set; }

        
        public List<QueueMemberEntry> Members
        {
            get;
            set;
        }

        
        public List<QueueCallerEntry> Callers
        {
            get;
            set;
        }

        
        public List<QueueCallerEntry> AbandonedCallers
        {
            get;
            set;
        }

        public override string DestinationCode
        {
            get
            {
                return $"ext-queues,{Id},1";
            }
            set { }
        }

        public QueueEntry()
        {
            CreateDate = DateTime.Now;
            Members = new List<Entries.QueueMemberEntry>();
            Callers = new List<Entries.QueueCallerEntry>();
            AbandonedCallers = new List<Entries.QueueCallerEntry>();
        }

        public void AddMember(QueueMemberEntry member)
        {
            var oldMember = Members.FirstOrDefault(p => p.Ext == member.Ext);
            if (oldMember == null)
                Members.Add(member);
            else
            {
                oldMember.CallsTaken = member.CallsTaken;
            }
        }

        public void RemoveMember(string ext)
        {
            if (Members.Any(p => p.Ext == ext))
                Members.Remove(Members.FirstOrDefault(p => p.Ext == ext));
        }

        public void AddCaller(QueueCallerEntry caller)
        {
            var oldCaller = Callers.FirstOrDefault(p => p.UniqueId == caller.UniqueId);
            if (oldCaller == null)
                Callers.Add(caller);
            else
            {
            }
        }

        public QueueCallerEntry GetCaller(string uniqueId)
        {
            var caller = Callers.FirstOrDefault(p => p.UniqueId == uniqueId);
            return caller == null ? null : caller;
        }

        public void RemoveCaller(string uniqueId, bool markAsAbandoned = false)
        {
            if (Callers.Any(p => p.UniqueId == uniqueId))
            {
                var caller = Callers.FirstOrDefault(p => p.UniqueId == uniqueId);
                caller.LeaveDate = DateTime.Now;
                Callers.Remove(caller);
                if (markAsAbandoned)
                {
                    if (caller.WaitTime > WaitTimeMinThreshold)
                        AbandonedCallers.Add(caller);
                }
                else
                {
                    if (RemoveFromAbandonedAfterCallback)
                    {
                        var toRemoveFromAbandoned = AbandonedCallers.FirstOrDefault(p => p.Number == caller.Number);
                        if (toRemoveFromAbandoned != null)
                            AbandonedCallers.Remove(toRemoveFromAbandoned);
                    }
                }
            }
        }
    }
}