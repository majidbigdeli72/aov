﻿namespace Septa.TelephonyServer.Entries
{
    public class QueuePriorityEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"app-queueprio,{Id},1";
            }
            set
            {
            }
        }
    }
}
