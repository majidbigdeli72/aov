﻿namespace Septa.TelephonyServer.Entries
{
    public class FeatureCodeAdminEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"ext-featurecodes,{Id},1";
            }
            set
            {
            }
        }
    }
}
