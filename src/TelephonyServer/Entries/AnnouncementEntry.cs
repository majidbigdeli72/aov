﻿namespace Septa.TelephonyServer.Entries
{
    public class AnnouncementEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"app-announcement-{Id},s,1";
            }
            set
            {

            }
        }
    }
}
