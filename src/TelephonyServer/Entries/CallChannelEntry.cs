﻿using System;
using System.Linq;
using System.Runtime.Serialization;

namespace Septa.TelephonyServer.Entries
{
    
    public class CallChannelEntry
    {
        
        public long? DbId { get; set; }

        
        public string UniqueId { get; set; }

        
        public string Channel { get; set; }

        public CallEntry ParentCall { get; set; }
        
        public ChannelResponse Response { get; set; }

        
        private ChannelState _state;
        public ChannelState State
        {
            get { return _state; }
            set
            {
                _state = value;
                switch (_state)
                {
                    case ChannelState.Up:
                        Response = ChannelResponse.Answered;
                        if (!ConnectDate.HasValue)
                            ConnectDate = DateTime.Now;
                        break;
                    case ChannelState.Busy:
                        Response = ChannelResponse.Busy;
                        break;
                    case ChannelState.Hangedup:
                        if (!HangupDate.HasValue)
                            HangupDate = DateTime.Now;
                        break;
                }
                if (ParentCall != null && ParentCall.Channels.All(p => p.State == ChannelState.Hangedup))
                    ParentCall.Status = CallStatus.Ended;
            }
        }

        public TimeSpan Duration
        {
            get
            {
                if (ConnectDate.HasValue)
                {
                    if (HangupDate.HasValue)
                        return HangupDate.Value - ConnectDate.Value;
                    else
                        return DateTime.Now - ConnectDate.Value;
                }
                else
                    return TimeSpan.Zero;
                //return (HangupDate.HasValue && ConnectDate.HasValue) ? (HangupDate.Value - ConnectDate.Value) : TimeSpan.Zero;
            }
        }

        public TimeSpan RingDuration
        {
            get
            {
                return (ConnectDate ?? CreateDate) - CreateDate;
            }
        }

        public string OwnerSpanNo
        {
            get
            {
                var pattern = @"((?<proto>\w*)/i(?<spanNo>\w*)/(?<cid>.*)-(?<no>\w*))";
                var match = System.Text.RegularExpressions.Regex.Match(Channel, pattern);
                if (match.Success)
                {
                    return match.Groups["spanNo"].Value;
                }

                return null;
            }
        }

        public string OwnerPeerName
        {
            get
            {
                var pattern = @"((?<proto>\w*)/i(?<spanNo>\w*)/(?<cid>.*)-(?<no>\w*))";
                var match = System.Text.RegularExpressions.Regex.Match(Channel, pattern);
                if (match.Success)
                {
                    return match.Groups["no"].Value;
                }

                pattern = @"((?<proto>\w*)/(?<lineNo>.*)-)";
                match = System.Text.RegularExpressions.Regex.Match(Channel, pattern);
                if (match.Success)
                {
                    return match.Groups["lineNo"].Value;
                }


                return null;
            }
        }
        public PeerProtocolType OwnerPeerProtocolType
        {
            get
            {
                var pattern = @"((?<proto>\w*)/i(?<spanNo>\w*)/(?<cid>.*)-(?<no>\w*))";
                var match = System.Text.RegularExpressions.Regex.Match(Channel, pattern);
                if (match.Success)
                {
                    return (PeerProtocolType)Enum.Parse(typeof(PeerProtocolType), match.Groups["proto"].Value, true);
                }

                pattern = @"((?<proto>\w*)/(?<lineNo>.*)-)";
                match = System.Text.RegularExpressions.Regex.Match(Channel, pattern);
                if (match.Success)
                {
                    return (PeerProtocolType)Enum.Parse(typeof(PeerProtocolType), match.Groups["proto"].Value, true);
                }
                throw new Exception("ProtocolType not supported in channel:" + Channel);
            }
        }

        public bool IsAsyncGoto
        {
            get { return this.Channel.StartsWith("AsyncGoto"); }
        }

        public bool IsLocal
        {
            get { return this.Channel.StartsWith("Local"); }
        }

        
        public PeerEntry OwnerPeer { get; set; }

        
        public DateTime CreateDate { get; set; }

        
        public DateTime? ConnectDate { get; set; }

        
        public DateTime? HangupDate { get; set; }

        
        public string DialedExtension { get; set; }
        
        public PeerEntry DialedExtensionPeer { get; set; }
        
        public string CallerIdName { get; set; }
        
        public string CallerIdNum { get; set; }

        
        public string RecordFileName { get; set; }

        public string Arg1 { get; set; }

        public CallChannelEntry()
        {
            CreateDate = DateTime.Now;
            Response = ChannelResponse.NotAnswered;
            Channel = string.Empty;
        }

        public override string ToString()
        {
            return $"Ch:{Channel} uid:{UniqueId}  Exten:{DialedExtension}";
        }
    }
}
