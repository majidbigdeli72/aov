﻿namespace Septa.TelephonyServer.Entries
{
    public class VoicemailBlastingEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"app-vmblast,{Id},1";
            }
            set
            {
            }
        }
    }
}
