﻿
namespace Septa.TelephonyServer.Entries
{
    public class ConferenceEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"ext-meetme,{Id},1";
            }
            set
            {
            }
        }
    }
}
