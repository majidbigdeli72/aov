﻿namespace Septa.TelephonyServer.Entries
{
    public class CallRecordingEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"ext-callrecording,{Id},1";
            }
            set
            {
            }
        }
    }
}
