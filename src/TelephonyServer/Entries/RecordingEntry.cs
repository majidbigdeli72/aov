﻿namespace Septa.TelephonyServer.Entries
{
    public class RecordingEntry : BaseDestinationEntry
    {
        public override string DestinationCode { get; set; }
    }
}
