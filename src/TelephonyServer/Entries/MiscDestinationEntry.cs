﻿namespace Septa.TelephonyServer.Entries
{
    public class MiscDestinationEntry : BaseDestinationEntry
    {
        public string Dial { get; set; }

        public override string DestinationCode
        {
            get
            {
                return $"ext-miscdests,{Id},1";
            }
            set
            {
            }
        }
    }
}
