﻿namespace Septa.TelephonyServer.Entries
{
    public class CallFlowControlEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"app-daynight,{Id},1";
            }
            set
            {
            }
        }
    }
}
