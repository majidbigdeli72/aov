﻿using Septa.TelephonyServer;
using System.Runtime.Serialization;
using System;

namespace Septa.TelephonyServer.Entries
{
    
    public class TrunkEntry : PeerEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"ext-trunk,{Id},1";
            }
            set { }
        }

        //ext-trunk,1,1

        
        public override PeerType Type
        {
            get { return PeerType.Trunk; }
            set { }
        }

        public int? SpanNo { get; set; }
    }
}
