﻿using Septa.TelephonyServer;
using System.Runtime.Serialization;
using System;

namespace Septa.TelephonyServer.Entries
{
    
    public class ExtensionEntry : PeerEntry
    {
        public ExtensionEntry() : base()
        {
            Status = ExtensionStatus.NotSet;
        }
        public override string DestinationCode
        {
            get
            {
                return $"from-did-direct,{Id},1";
            }
            set { }
        }

        
        public string Desc { get; set; }

        
        public override PeerType Type
        {
            get { return PeerType.Extension; }
            set { }
        }
        
        public ExtensionStatus Status
        {
            get;
            set;
        }
    }
}
