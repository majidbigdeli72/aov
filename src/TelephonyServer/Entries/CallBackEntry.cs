﻿namespace Septa.TelephonyServer.Entries
{
    public class CallBackEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"callback,{Id},1";
            }
            set
            {
            }
        }
    }
}
