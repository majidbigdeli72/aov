﻿using System.Runtime.Serialization;

namespace Septa.TelephonyServer.Entries
{
    public class DisaEntry : BaseDestinationEntry
    {
        public override string DestinationCode { get { return $"disa,{Id},1"; } set { } }
    }
}
