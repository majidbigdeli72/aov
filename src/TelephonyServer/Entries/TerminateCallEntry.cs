﻿namespace Septa.TelephonyServer.Entries
{
    public class TerminateCallEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"app-blackhole,{Id},1";
            }
            set
            {
            }
        }
    }
}
