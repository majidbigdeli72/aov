﻿namespace Septa.TelephonyServer.Entries
{
    public class IvrEntry : BaseDestinationEntry
    {
        public override string DestinationCode
        {
            get
            {
                return $"ivr-{Id},s,1";
            }
            set { }
        }
    }
}
