﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer.Entries
{
    
    public class CallEntry
    {
        
        public long? DbId { get; set; }

        
        public string InternalCallId { get; set; }

        
        public int TsId { get; set; }
        
        public DateTime StartDate { get; set; }
        
        public DateTime? EndDate { get; set; }
        public CallDirection CallDirection
        {
            get
            {
                return InitByChannel.OwnerPeer?.Type == PeerType.Trunk
                  ? CallDirection.Incoming
                  : CallDirection.Outgoing;
            }
        }


        public CallType CallType
        {
            get
            {
                if (CallDirection == CallDirection.Incoming)
                    return ChainChannels.Any(p => p.Response == ChannelResponse.Answered || p.Response == ChannelResponse.Transfered)
                        ? CallType.Incoming
                        : CallType.Missed;
                else
                    return Channels.Any(p => p.OwnerPeer?.Type == PeerType.Trunk)
                        ? CallType.Outgoing
                        : CallType.Internal;
            }
        }

        
        private CallStatus _status;
        public CallStatus Status
        {
            get { return _status; }
            set
            {
                _status = value;
                if (_status == CallStatus.Ended)
                {
                    if (!this.EndDate.HasValue)
                        this.EndDate = DateTime.Now;
                }
            }
        }
        public TimeSpan Duration
        {
            get
            {
                var toReturn = Status == CallStatus.Ended && EndDate.HasValue
                    ? EndDate.Value - StartDate
                    : DateTime.Now - StartDate;

                return toReturn > TimeSpan.Zero ? toReturn : TimeSpan.Zero;
            }
        }
        public TimeSpan RingDuration
        {
            get
            {
                var extChannel = Channels.FirstOrDefault(p => p.OwnerPeer?.Type == PeerType.Extension && p.ConnectDate.HasValue);

                var toReturn = extChannel != null
                    ? extChannel.ConnectDate.Value - StartDate
                    : DateTime.Now - StartDate;

                return toReturn > TimeSpan.Zero ? toReturn : TimeSpan.Zero;
            }
        }
        public TimeSpan CallDuration
        {
            get
            {
                var ringDuration = RingDuration;
                var toReturn = Duration - ringDuration;
                return toReturn > TimeSpan.Zero ? toReturn : TimeSpan.Zero;
            }
        }


        
        public string ProfileId { get; set; }
        
        public string ProfileName { get; set; }
        public string PhoneNumber
        {
            get
            {
                if (CallDirection == CallDirection.Incoming)
                    return InitByChannel.CallerIdNum;
                else
                {
                    if (!string.IsNullOrEmpty(InitByChannel.DialedExtension) && InitByChannel.DialedExtension != "s")
                        return InitByChannel.DialedExtension;
                    else
                    {
                        var item = this.Channels.FirstOrDefault(p => p.OwnerPeerName != p.CallerIdNum);
                        return item != null ? item.CallerIdNum : "";
                    }
                }
            }
        }
        
        public CallChannelEntry InitByChannel { get; set; }
        
        public List<CallChannelEntry> Channels { get; set; }
        public List<CallChannelEntry> ChainChannels
        {
            get
            {
                return Channels.Where(p => p != InitByChannel).ToList();
            }
        }

        public CallEntry()
        {
            Channels = new List<Entries.CallChannelEntry>();
            StartDate = DateTime.Now;
            Status = CallStatus.Live;
        }
    }
}
