﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer
{
    public class CallCreateResult
    {
        public string CallId { get; set; }
        public string InitCallChannelId { get; set; }
        public string ProfileId { get; set; }
        public string ProfileName { get; set; }
    }
}
