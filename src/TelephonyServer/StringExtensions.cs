﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer
{
    public static class StringExtensions
    {
        public static T? ConvertTo<T>(this string value) where T : struct
        {
            try
            {
                return (T?)TypeDescriptor.GetConverter(typeof(T)).ConvertFromInvariantString(value);
            }
            catch
            {
                return default(T?);
            }
        }
    }
}
