﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer
{

    public enum CallDirection
    {
        [DisplayName("تماس ورودی")]
        Incoming = 1,

        [DisplayName("تماس خروجی")]
        Outgoing = 2,
    }

    public enum CallType
    {
        [DisplayName("تماس از دست رفته")]
        Missed = 1,

        [DisplayName("تماس ورودی")]
        Incoming = 2,

        [DisplayName("تماس خروجی")]
        Outgoing = 3,

        [DisplayName("تماس داخلی")]
        Internal = 4
    }

    public enum CallStatus
    {
        [DisplayName("تماس زنده")]
        Live = 1,

        [DisplayName("تماس پایان یافته")]
        Ended = 2,
    }

    public enum ChannelState
    {
        Down,
        Rsrvd,
        OffHook,
        Dialing,
        Ring,
        Ringing,
        Up,
        Busy,
        DialingOffhook,
        PreRing,
        Unknown,
        Hangedup,
    }

    public enum ExtensionStatus
    {
        NotSet = -5,
        Removed = -2,
        HintRemoved = -1,
        Idle = 0,
        InUse = 1,
        Busy = 2,
        Unavailable = 4,
        Ringing = 8,
        InUseAndRinging = 9,
        Hold = 16,
        InUseAndHold = 17,
    }
    public enum OriginateResponseStatus
    {
        Failed = 0,
        Error = 1,
        Success = 2
    }
    public enum SpyMode
    {
        Spy = 1,
        Whisper = 2,
        Barge = 3
    }

    public enum ChannelResponse
    {
        [DisplayName("بدون پاسخ")]
        NotAnswered = 1,

        [DisplayName("پاسخ داده شده")]
        Answered = 2,

        [DisplayName("درحال مکالمه")]
        Busy = 3,

        [DisplayName("انتقال داده شده")]
        Transfered = 4,
    }
    public enum PeerType
    {
        Extension = 1,
        Trunk = 2
    }

    public enum QueueCallerStatus
    {
        [DisplayName("در حال انتظار")]
        Waiting = 1,

        [DisplayName("متصل شده")]
        Joined = 2,

        [DisplayName("از دست رفته")]
        Abandoned = 3,
    }

    public enum PeerProtocolType
    {
        SIP = 1,
        IAX2 = 2,
        DAHDI = 3,
        PJSIP = 4,
        LOCAL = 5,
    }

    public enum DestinationType
    {
        Custom = 0,
        Announcements = 1,
        CallFlowControl = 2,
        CallRecording = 3,
        Callback = 4,
        Conferences = 5,
        CustomApplications = 6,
        DISA = 7,
        Extensions = 8,
        FeatureCodeAdmin = 9,
        IVR = 10,
        Languages = 11,
        MiscDestinations = 12,
        PagingAndIntercom = 13,
        PhonebookDirectory = 14,
        QueuePriorities = 15,
        Queues = 16,
        RingGroups = 17,
        TerminateCall = 18,
        TimeConditions = 19,
        Trunks = 20,
        VoicemailBlasting = 21
    }

    public enum AsteriskMySqlEntityType
    {
        Ivr,
        Queue,
        QueuePriority,
        TimeCondition,
        TimeGroup,
        TimeGroupDetail,
        RingGroup,
        Extension
    }
}
