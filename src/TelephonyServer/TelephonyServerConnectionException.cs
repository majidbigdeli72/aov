﻿using System;

namespace Septa.TelephonyServer
{
    public class TelephonyServerConnectionException : Exception
    {
        public TelephonyServerConnectionException(string message)
            : base(message)
        {
        }
    }
}
