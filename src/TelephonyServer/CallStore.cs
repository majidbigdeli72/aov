﻿using Septa.TelephonyServer;
using System;
using System.Collections.Concurrent;

namespace Septa.TelephonyServer
{
    public class CallStore
    {
        ConcurrentDictionary<long, ActiveCall> _myCalls = new ConcurrentDictionary<long, ActiveCall>();

        public void AddCall(long callId, string lookupSourceCallId, CallType callType, string number)
        {
            _myCalls.TryAdd(callId, new ActiveCall
            {
                LookupSourceCallId = lookupSourceCallId,
                CallType = callType,
                Number = number
            });
        }
        public void AddCallChannel(long callId, long callChannelId, string lookupSourceCallChannelId, ChannelState state, ChannelResponse response, string recordedFileName)
        {
            var activeCall = FindCall(callId);
            if (activeCall != null)
            {
                activeCall.CallChannels.TryAdd(callChannelId, new ActiveCallChannel()
                {
                    LookupSourceCallChannelId = lookupSourceCallChannelId,
                    ChannelState = state,
                    ChannelResponse = response,
                    RecordedFileName = recordedFileName
                });
            }
        }
        public bool RemoveCall(long callId)
        {
            ActiveCall activeCall;
            return _myCalls.TryRemove(callId, out activeCall);
        }
        public ActiveCall FindCall(long callId)
        {
            ActiveCall activeCall = null;
            _myCalls.TryGetValue(callId, out activeCall);
            return activeCall;
        }
        public ActiveCallChannel FindCallChannel(long callId, long callChannelId)
        {
            var activeCall = FindCall(callId);
            if (activeCall != null)
            {
                ActiveCallChannel activeCallChannel;
                activeCall.CallChannels.TryGetValue(callChannelId, out activeCallChannel);
                return activeCallChannel;
            }

            return null;
        }
    }

    public class ActiveCall
    {
        public CallType CallType { get; set; }
        public string Number { get; set; }
        public string ProfileId { get; set; }
        public string LookupSourceCallId { get; set; }
        public ConcurrentDictionary<long, ActiveCallChannel> CallChannels = new ConcurrentDictionary<long, ActiveCallChannel>();
    }

    public class ActiveCallChannel
    {
        public DateTime? ConnectDate { get; set; }
        public DateTime? HangDate { get; set; }
        public ChannelState ChannelState { get; set; }
        public ChannelResponse ChannelResponse { get; set; }
        public string RecordedFileName { get; set; }
        public string LookupSourceCallChannelId { get; set; }
    }
}
