﻿using AsterNET.NetStandard.Manager.Action;
using AsterNET.NetStandard.Manager.Event;
using AsterNET.NetStandard.Manager.Response;
using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Septa.TelephonyServer.Asterisk
{
	public partial class AsteriskTelephonyServer
	{
		protected Dictionary<string, CallChannelEntry> _callChannels;

		public override List<CallEntry> Calls
		{
			get
			{
				return _callChannels.Values.Where(p => p.ParentCall != null).Select(p => p.ParentCall).Distinct().ToList();
			}
		}

		public void InitCalls()
		{
			_callChannels = new Dictionary<string, CallChannelEntry>();
		}

		private void AddChannel(CallChannelEntry callChannel)
		{
			//_callChannels.Add
		}
		public override CallChannelEntry GetChannel(string uniqueId)
		{
			CallChannelEntry toReturn;
			return _callChannels.TryGetValue(uniqueId, out toReturn) ? toReturn : null;
		}

		protected virtual void Do_NewChannel(AsterNET.NetStandard.Manager.Event.NewChannelEvent e)
		{
			if (!_callChannels.ContainsKey(e.UniqueId))
				_callChannels.Add(e.UniqueId, new CallChannelEntry() { UniqueId = e.UniqueId });

			//var callChannel = CreateChannel(e);
			//_callChannels.Add(callChannel.UniqueId, callChannel);
			//string linkedId;
			//if (e.Attributes.TryGetValue("linkedid", out linkedId) && e.UniqueId != linkedId)
			//{
			//    var firstChannel = GetChannel(e.Attributes["linkedid"]);
			//    if (firstChannel != null)
			//    {
			//        firstChannel.ParentCall.Channels.Add(callChannel);
			//        callChannel.ParentCall = firstChannel.ParentCall;
			//    }
			//}
			//else
			//{
			//    var call = new CallEntry() { InitByChannel = callChannel };
			//    callChannel.ParentCall = call;
			//    call.Channels.Add(callChannel);
			//}
		}

		private void Do_ChanStart(AsterNET.NetStandard.Manager.Event.UnknownEvent e)
		{
			var toUpdate = GetChannel(e.UniqueId);
			if (toUpdate != null)
			{
				_callChannels.Remove(e.UniqueId);
			}

			var callChannel = CreateChannel(toUpdate, e);
			if (callChannel == null)
				return;

			string linkedId;
			if (e.Attributes.TryGetValue("linkedid", out linkedId) && e.UniqueId != linkedId)
			{
				var firstChannel = GetChannel(e.Attributes["linkedid"]);
				if (firstChannel != null)
				{
					if (callChannel.IsAsyncGoto)
					{
						var relatedChannel = firstChannel.ParentCall.Channels.FirstOrDefault(p => p.OwnerPeerName == callChannel.OwnerPeerName);
						if (relatedChannel != null)
						{
							_callChannels.Remove(relatedChannel.UniqueId);
							relatedChannel.UniqueId = callChannel.UniqueId;
							_callChannels[callChannel.UniqueId] = relatedChannel;
							_callChannels.Add(callChannel.UniqueId, callChannel);
							OnCallChannelUpdated(relatedChannel);
						}
					}
					else
					{
						callChannel.ParentCall = firstChannel.ParentCall;
						firstChannel.ParentCall.Channels.Add(callChannel);
						_callChannels.Add(callChannel.UniqueId, callChannel);
						OnCallChannelUpdated(callChannel);
					}
				}
			}
			else
			{
				var call = new CallEntry() { InitByChannel = callChannel, TsId = this.Id };
				callChannel.ParentCall = call;
				call.Channels.Add(callChannel);
				_callChannels.Add(callChannel.UniqueId, callChannel);
				OnCallUpdated(call);
			}
		}


		private void Do_Hangup(AsterNET.NetStandard.Manager.Event.HangupEvent e)
		{
			var channel = GetChannel(e.UniqueId);
			if (channel != null)
			{
				channel.State = ChannelState.Hangedup;
				_callChannels.Remove(e.UniqueId);
				OnCallChannelUpdated(channel);
			}
		}

		protected virtual void Do_NewState(AsterNET.NetStandard.Manager.Event.NewStateEvent e)
		{
			var channel = GetChannel(e.UniqueId);
			if (channel != null)
			{
				int newState = 0;
				int.TryParse(e.ChannelState, out newState);
				channel.State = (ChannelState)newState;
				OnCallChannelUpdated(channel);
			}
		}

		private void Do_Cdr(AsterNET.NetStandard.Manager.Event.CdrEvent e)
		{

		}

		private void Do_Hold(AsterNET.NetStandard.Manager.Event.HoldEvent e)
		{

		}
		private void Do_HoldedCall(AsterNET.NetStandard.Manager.Event.HoldedCallEvent e)
		{

		}

		private void Do_Unhold(AsterNET.NetStandard.Manager.Event.UnholdEvent e)
		{

		}
		private void Do_Bridge(AsterNET.NetStandard.Manager.Event.BridgeEvent e)
		{

		}

		protected virtual void Do_NewCallerId(AsterNET.NetStandard.Manager.Event.NewCallerIdEvent e)
		{
			var channel = GetChannel(e.UniqueId);
			if (channel != null)
			{
				bool changed = false;
				if (string.IsNullOrEmpty(channel.CallerIdName) && !string.IsNullOrEmpty(e.CallerIdName))
				{
					channel.CallerIdName = e.CallerIdName;
					changed = true;
				}
				if (string.IsNullOrEmpty(channel.CallerIdNum) && !string.IsNullOrEmpty(e.CallerIdNum))
				{
					channel.CallerIdNum = e.CallerIdNum;
					changed = true;
				}
				if (changed)
					OnCallChannelUpdated(channel);
			}
		}

		private void Do_NewExten(AsterNET.NetStandard.Manager.Event.NewExtenEvent e)
		{
			if (e.Application.Trim().ToLower() == "mixmonitor" && (e.Context.Trim().ToLower() == "sub-record-check" || e.Context.Trim().ToLower() == "macro-record-enable"))
			{
				var match = Regex.Match(e.AppData, @"(.*)(\.wav|\.mp3|\.ogg|\.gsm)", RegexOptions.IgnoreCase);
				if (match.Success)
				{
					var channel = GetChannel(e.UniqueId);
					if (channel != null)
					{
						channel.RecordFileName = match.Value;
					}
				}
			}
		}

		private void Do_Unknown(AsterNET.NetStandard.Manager.Event.UnknownEvent e)
		{
			if (e != null && e.Attributes != null && e.Attributes.ContainsKey("eventname"))
			{
				string eventname = e.Attributes["eventname"];
				switch (eventname)
				{
					case "CHAN_START":
						Do_ChanStart(e);
						break;
					case "CHAN_END":
						break;
					case "HANGUP":
						break;
					case "LINKEDID_END":
						break;
					case "BLINDTRANSFER":
						break;
					case "ATTENDEDTRANSFER":
						Do_AttendedTransfer(e);
						break;
					case "BRIDGE_START":
						break;
					case "PICKUP":
						Do_Pickup(e);
						break;
					default:
						break;
				}
			}
		}

		private void Do_AttendedTransfer(AsterNET.NetStandard.Manager.Event.UnknownEvent e)
		{
			var channelMain = GetChannel(e.UniqueId);
			var channelToDestroy = GetChannel(e.Attributes["extra"]);
			if (channelMain != null && channelToDestroy != null)
			{
				var callMain = channelMain.ParentCall;
				var callToDestroy = channelToDestroy.ParentCall;

				var channelTarget = callToDestroy.Channels.FirstOrDefault(x => x != channelToDestroy && x.State != ChannelState.Hangedup);
				if (channelTarget != null)
				{
					OnCallDestroyed(callToDestroy, callMain);

					foreach (var ch in callToDestroy.Channels)
					{
						ch.ParentCall = callMain;
						callMain.Channels.Add(ch);
					}

					callToDestroy.InitByChannel = null;
					callToDestroy.Channels.Clear();

					UpdateAttendentTransferedChannel(channelMain, e, channelTarget);
				}
			}
		}

		private void Do_Pickup(AsterNET.NetStandard.Manager.Event.UnknownEvent e)
		{
			var channel = GetChannel(e.UniqueId);
			if (channel != null)
			{
				var calleridnum = e.Attributes["calleridnum"];
				var peerCode = GetOwnerPeerNameFromChannel(e.Attributes["peer"]);
				if (calleridnum != peerCode)
				{
					var dialedChannel = channel.ParentCall.Channels.FirstOrDefault(p => p.OwnerPeer.Name == calleridnum);
					var peerToSet = Peers.FirstOrDefault(p => p.Name == peerCode);
					if (peerToSet != null)
					{
						dialedChannel.CallerIdNum = peerCode;
						var oldPeer = dialedChannel.OwnerPeer;
						dialedChannel.OwnerPeer = peerToSet;
						dialedChannel.State = ChannelState.Up;
						OnCallChannelUpdated(dialedChannel, oldPeer: oldPeer);
					}
				}
			}
		}
		private string GetOwnerPeerNameFromChannel(string channel)
		{
			var pattern = @"((?<proto>\w*)/i(?<spanNo>\w*)/(?<cid>\w*)-(?<no>\w*))";
			var match = System.Text.RegularExpressions.Regex.Match(channel, pattern);
			if (match.Success)
			{
				return match.Groups["no"].Value;
			}

			pattern = @"((?<proto>\w*)/(?<lineNo>.*)-)";
			match = System.Text.RegularExpressions.Regex.Match(channel, pattern);
			if (match.Success)
			{
				return match.Groups["lineNo"].Value;
			}


			return null;
		}

		private void Do_SetVar(AsterNET.NetStandard.Manager.Event.VarSetEvent e)
		{
			if (e.Variable == "CustomerId")
			{
				var channel = GetChannel(e.UniqueId);
				if (channel != null && channel.ParentCall != null)
				{
					channel.ParentCall.ProfileId = e.Value;

					OnCallUpdated(channel.ParentCall);
				}
			}
		}

		protected CallChannelEntry FindFirstChannelByCallerID(string callerIdNum)
		{
			return _callChannels.Values.FirstOrDefault(p => p.CallerIdNum == callerIdNum && p.ParentCall.Channels.Count == 1);
		}


		protected CallChannelEntry CreateChannel(CallChannelEntry toUpdate, AsterNET.NetStandard.Manager.Event.ManagerEvent e)
		{
			string item;
			return CreateChannel(toUpdate,
								 e.Channel,
								 e.UniqueId,
								 ChannelState.Down,
								 e.Attributes.TryGetValue("exten", out item) ? item : string.Empty,
								 e.Attributes.TryGetValue("calleridname", out item) ? item : (e is NewChannelEvent ? (e as NewChannelEvent).CallerIdName : string.Empty),
								 e.Attributes.TryGetValue("calleridnum", out item) ? item : (e is NewChannelEvent ? (e as NewChannelEvent).CallerIdNum : string.Empty));
		}
		protected CallChannelEntry CreateChannel(CallChannelEntry toUpdate, string channel, string uniqueId, ChannelState state, string exten, string callerIdName, string callerIdNum)
		{
			CallChannelEntry toReturn;
			if (toUpdate != null)
			{
				toReturn = toUpdate;
				toReturn.Channel = channel;

				if (string.IsNullOrEmpty(toReturn.DialedExtension))
				{
					toReturn.DialedExtension = exten;
				}
				if (string.IsNullOrEmpty(toReturn.CallerIdName))
				{
					toReturn.CallerIdName = callerIdName;
				}
				if (string.IsNullOrEmpty(toReturn.CallerIdNum))
				{
					toReturn.CallerIdNum = callerIdNum;
				}

				toReturn.State = state;
			}
			else
			{
				toReturn = new CallChannelEntry()
				{
					UniqueId = uniqueId,
					Channel = channel,
					DialedExtension = exten,
					CallerIdName = callerIdName,
					CallerIdNum = callerIdNum,
					State = state,
				};
			}

			if (!toReturn.IsLocal)
			{
				toReturn.OwnerPeer = FindRelatedPeer(toReturn);
				toReturn.DialedExtensionPeer = FindRelatedDialedExtensionPeer(toReturn);
			}

			return toReturn;
		}

		private void ReBuildHierachyRuningCallChannels()
		{
			try
			{
				if (!_manager.IsConnected())
					_manager.Login();

				ManagerResponse response = SendAction(new CommandAction("core show channels"));
				var channelsResult = ((CommandResponse)response).Result;

				var channels = new List<string>();
				channels.AddRange(channelsResult);

				if (channels != null && channels.Count > 0)
				{
					List<Tuple<CallEntry, CallChannelEntry>> callsAndChnnels = new List<Tuple<CallEntry, CallChannelEntry>>();
					foreach (var channel in channels)
					{
						var log = channel;
						var channelName = log.Substring(0, channel.IndexOf(' ')).Trim();

						var ch = GetChannelInfo(channelName);
						if (ch != null)
						{
							callsAndChnnels.Add(ch);
						}
					}

					foreach (var callsAndChnnel in callsAndChnnels)
					{
						AddCallToHierachy(callsAndChnnel.Item1, callsAndChnnel.Item2);
					}

					Log.Debug("InitialConfigurator BuildCallChannels successfully");
				}
				else
					Log.Error("Error in InitialConfigurator BuildCallChannels response: " + response);
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Error in InitialConfigurator BuildCallChannels: ");
				throw;
			}
		}


		protected virtual Tuple<CallEntry, CallChannelEntry> GetChannelInfo(string channel)
		{
			var exten = string.Empty;
			var callerIdNum = string.Empty;
			var callerIdName = string.Empty;
			var state = string.Empty;
			var recordFound = false;
			var callIdentifer = string.Empty;
			var uniqueId = string.Empty;

			var channelInfos = RunCoreShowChannel(channel);

			if (channelInfos.ContainsKey("call identifer"))
			{
				callIdentifer = channelInfos["call identifer"];
				recordFound = true;
			}
			if (channelInfos.ContainsKey("uniqueid"))
			{
				uniqueId = channelInfos["uniqueid"];
				recordFound = true;
			}
			if (channelInfos.ContainsKey("caller id name"))
			{
				callerIdName = channelInfos["caller id name"];
				recordFound = true;
			}
			if (channelInfos.ContainsKey("caller id"))
			{
				callerIdNum = channelInfos["caller id"];
				recordFound = true;
			}
			if (channelInfos.ContainsKey("extension"))
			{
				exten = channelInfos["extension"];
				recordFound = true;
			}
			if (channelInfos.ContainsKey("state"))
			{
				state = channelInfos["state"].Substring(0, channelInfos["state"].IndexOf(" "));
				recordFound = true;
			}

			if (recordFound)
			{
				var callEntry = new CallEntry() { InternalCallId = callIdentifer };
				var channelEntry = CreateChannel(null, channel, uniqueId, (ChannelState)Enum.Parse(typeof(ChannelState), state, true), exten, callerIdName, callerIdNum);
				return new Tuple<CallEntry, CallChannelEntry>(callEntry, channelEntry);
			}
			return null;
		}

		protected Dictionary<string, string> RunCoreShowChannel(string channel)
		{
			var toReturn = new Dictionary<string, string>();

			var channelInfoRecords = ((CommandResponse)SendAction(new CommandAction("core show channel " + channel))).Result;
			foreach (var channelInfoRecord in channelInfoRecords)
			{
				if (channelInfoRecord.Contains("="))
				{
					var index = channelInfoRecord.IndexOf("=");
					var key = channelInfoRecord.Substring(0, index).Trim().ToLower();
					if (!toReturn.ContainsKey(key))
						toReturn.Add(key, channelInfoRecord.Substring(index + 1).Trim());
				}
				else if (channelInfoRecord.Contains(":"))
				{
					var index = channelInfoRecord.IndexOf(":");
					var key = channelInfoRecord.Substring(0, index).Trim().ToLower();
					if (!toReturn.ContainsKey(key))
						toReturn.Add(key, channelInfoRecord.Substring(index + 1).Trim());
				}
			}

			return toReturn;
		}


		private void AddCallToHierachy(CallEntry callEntry, CallChannelEntry callChannelEntry)
		{
			var channel = _callChannels.Values.FirstOrDefault(x => x.UniqueId == callChannelEntry.UniqueId);
			if (channel != null)
			{
				channel.ParentCall.InternalCallId = callEntry.InternalCallId;
				return;
			}

			var call = Calls.FirstOrDefault(p => p.InternalCallId == callEntry.InternalCallId);
			if (call == null)
			{
				callEntry.InitByChannel = callChannelEntry;
				callEntry.TsId = this.Id;
				callChannelEntry.ParentCall = callEntry;
				callEntry.Channels.Add(callChannelEntry);
				_callChannels.Add(callChannelEntry.UniqueId, callChannelEntry);
			}
			else
			{
				callChannelEntry.ParentCall = call;
				call.Channels.Add(callChannelEntry);
				_callChannels.Add(callChannelEntry.UniqueId, callChannelEntry);
			}
		}

		private PeerEntry FindRelatedPeer(CallChannelEntry channel)
		{
			try
			{
				var toReturn = string.IsNullOrEmpty(channel.OwnerSpanNo)
								? Peers.FirstOrDefault(p => p.Protocol == channel.OwnerPeerProtocolType && p.Name == channel.OwnerPeerName)
								: Trunks.Where(p => p.Protocol == channel.OwnerPeerProtocolType && p.SpanNo == Convert.ToInt32(channel.OwnerSpanNo)).First();
				if (toReturn == null)
					throw new Exception($"Owner peer not found for channel:{channel.Channel} UniqueId:{channel.UniqueId}");

				return toReturn;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		private PeerEntry FindRelatedDialedExtensionPeer(CallChannelEntry channel)
		{
			if (!string.IsNullOrEmpty(channel.DialedExtension))
			{
				var dialedExtensionPeer = Peers.FirstOrDefault(p => p.Name == channel.DialedExtension);
				if (dialedExtensionPeer != null)
					channel.DialedExtensionPeer = dialedExtensionPeer;
			}
			return null;
		}

		private void UpdateAttendentTransferedChannel(CallChannelEntry channelMain, AsterNET.NetStandard.Manager.Event.UnknownEvent e, CallChannelEntry channelTarget)
		{
			channelMain.Response = ChannelResponse.Transfered;
			channelMain.State = ChannelState.Hangedup;
			OnCallChannelUpdated(channelMain);

			channelMain.DbId = null;
			channelMain.Channel = channelTarget.Channel;

			channelMain.DialedExtension = channelTarget.DialedExtension;
			channelMain.CallerIdName = channelTarget.CallerIdName;
			channelMain.CallerIdNum = channelTarget.CallerIdNum;

			channelMain.OwnerPeer = FindRelatedPeer(channelMain);
			channelMain.DialedExtensionPeer = FindRelatedDialedExtensionPeer(channelMain);
			channelMain.CreateDate = DateTime.Now;
			channelMain.ConnectDate = DateTime.Now;
			channelMain.HangupDate = null;
			channelMain.State = ChannelState.Up;
			OnCallChannelUpdated(channelMain, false);

			// Call to propagate channel 'UP' status to other thirdparty systems
			OnCallChannelUpdated(channelMain, false);
		}

		public override OriginateResponseStatus Originate(string channel, string channelCID, string toDial, bool applyOutgoingRules, Dictionary<string, string> variables = null)
		{
			try
			{
				Log.Debug($"Sending OriginateAction. IsConnected:{_manager.IsConnected()}, {nameof(channel)}:{channel}, {nameof(channelCID)}:{channelCID}, {nameof(toDial)}:{toDial}, {nameof(applyOutgoingRules)}:{applyOutgoingRules}");
				ManagerResponse aManagerResponse = new ManagerResponse();
				if (!_manager.IsConnected())
					_manager.Login();
				OriginateAction aOriginateAction = new OriginateAction();
				aOriginateAction.Exten = GetStandardNumber(toDial, applyOutgoingRules);
				aOriginateAction.Channel = channel;
				if (!string.IsNullOrEmpty(channelCID))
					aOriginateAction.CallerId = channelCID;
				aOriginateAction.Context = this.OriginateContext;
				aOriginateAction.Timeout = 30000;
				aOriginateAction.Priority = "1";

				if (variables != null) aOriginateAction.SetVariables(variables);

				aManagerResponse = SendAction(aOriginateAction, 30000);
				Log.Debug("OriginateAction sent.");
				return (OriginateResponseStatus)Enum.Parse(typeof(OriginateResponseStatus), aManagerResponse.Response);

			}
			catch (Exception ex)
			{
				Log.Error(ex, "Error in Originate");
				throw;
			}
		}

		public override OriginateResponseStatus OriginateToExtension(string exten, string extenCID, string toDial, bool applyOutgoingRules, Dictionary<string, string> variables = null)
		{
			try
			{
				var ext = Extensions.FirstOrDefault(p => p.Id == exten);
				if (ext != null)
				{
					var channel = $"{ext.Protocol}/{exten}";
					return Originate(channel, extenCID, toDial, applyOutgoingRules, variables);
				}
				else
					return OriginateResponseStatus.Failed;
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Error in Originate: ");
				throw;
			}
		}

		public override bool ChannelSpy(string extn, string spiedExten, SpyMode spyMode)
		{
			Log.Debug($"ChannelSpy. {nameof(extn)}:{extn}, {nameof(spiedExten)}:{spiedExten}, {nameof(spyMode)}:{spyMode}");

			try
			{
				var ext = Extensions.FirstOrDefault(p => p.Id == extn);
				Log.Debug("Extension: {@ext}", ext);
				if (ext != null)
				{
					OriginateAction oa = new OriginateAction();
					oa.Channel = string.Format("{0}/{1}", ext.Protocol, extn);
					oa.Context = OriginateContext;

					switch (spyMode)
					{
						case SpyMode.Spy:
							oa.Exten = "*8651" + spiedExten;
							break;
						case SpyMode.Whisper:
							oa.Exten = "*8652" + spiedExten;
							break;
						case SpyMode.Barge:
							oa.Exten = "*8653" + spiedExten;
							break;
						default:
							break;
					}
					oa.CallerId = string.Format("{0}: {1}", spyMode.ToString(), spiedExten);
					oa.Priority = "1";
					oa.Timeout = 30000;

					Log.Debug($"Sending OriginateAction. IsConnected: {_manager.IsConnected()}");
					ManagerResponse aManagerResponse = new ManagerResponse();
					if (!_manager.IsConnected())
						_manager.Login();

					var response = SendAction(oa);
					var res = GetOriginateResponseMessage(response.Message);
					return res == OriginateResponseStatus.Success;
				}
				else
					return false;

			}
			catch (Exception exp)
			{
				Log.Error(exp, "Error in ChannelSpy");
			}

			return false;
		}
		private OriginateResponseStatus GetOriginateResponseMessage(string message)
		{
			Log.Debug($"GetOriginateResponseMessage. {nameof(message)}:{message}");

			if (message.ToLower().Contains("success"))
				return OriginateResponseStatus.Success;
			else
				return (OriginateResponseStatus)Enum.Parse(typeof(OriginateResponseStatus), message);
		}

		public override bool ChannelHangup(string channelName)
		{
			try
			{
				HangupAction ha = new HangupAction();
				ha.Channel = channelName;

				Log.Debug($"Sending HangupAction. IsConnected: {_manager.IsConnected()}");
				ManagerResponse aManagerResponse = new ManagerResponse();
				if (!_manager.IsConnected())
					_manager.Login();

				var response = SendAction(ha);
				return response.IsSuccess();

			}
			catch (Exception exp)
			{
				Log.Error(exp, "Error in ChannelHangup");
			}

			return false;
		}
	}
}
