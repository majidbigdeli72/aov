﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Serilog;
using Septa.TelephonyServer.Entries;
using AsterNET.NetStandard.Manager.Action;

namespace Septa.TelephonyServer.Asterisk
{
    public partial class AsteriskTelephonyServer
    {
        protected Dictionary<string, QueueEntry> _queues = new Dictionary<string, QueueEntry>();
        private bool _getQueueStatusCompleted;

        public override List<QueueEntry> Queues
        {
            get { return _queues.Values.ToList(); }
        }

        protected virtual void InitQueues()
        {
            ReloadQueueStatus();
        }

        private void ReloadQueueStatus()
        {
            try
            {
                _queues = new Dictionary<string, QueueEntry>();
                _getQueueStatusCompleted = false;
                
                var myManager = CreateManager();
                myManager.QueueEntry += QueuManager_QueueEntry;
                myManager.QueueMember += QueuManager_QueueMember;
                myManager.QueueParams += QueuManager_QueueParams;
                myManager.QueueStatusComplete += QueuManager_QueueStatusComplete;
                myManager.Login();
                try
                {
                    var action = new QueueStatusAction();
                    myManager.SendAction(action);

                    int i = 0;
                    while (!_getQueueStatusCompleted)
                    {
                        if (i < 120)
                        {
                            Thread.Sleep(1000);
                            i++;
                        }
                        else
                            throw new InvalidOperationException("QueueStatusAction did not complete after 120 seconds!!!");
                    }
                }
                finally
                {
                    myManager.Logoff();
                }
            }
            catch (InvalidOperationException)
            {
                Log.Error("InvalidOperationException Error in InitQueues for AsteriskTelephonyServer. Queues did not initialized after 120 secs");
                throw;
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in InitQueues for AsteriskTelephonyServer.");
                // Ignored
            }
        }

        private void Do_Join(AsterNET.NetStandard.Manager.Event.JoinEvent e)
        {
            if (!string.IsNullOrWhiteSpace(e.Queue))
            {
                var queue = GetQueue(e.Queue);
                var toAdd = new QueueCallerEntry()
                {
                    ParentQueue = queue,
                    ParentQueueNo = e.Queue,
                    JoinDate = DateTime.Now,
                    JoinPosition = e.Position,
                    Name = e.CallerIdName,
                    Number = e.CallerId,
                    Status = QueueCallerStatus.Waiting,
                    UniqueId = e.UniqueId,
                    CallChannel = GetChannel(e.UniqueId),
                };
                queue.AddCaller(toAdd);

                OnQueueCallerJoined(toAdd);
            }
        }
        private void Do_Leave(AsterNET.NetStandard.Manager.Event.LeaveEvent e)
        {
            if (!string.IsNullOrWhiteSpace(e.Queue))
            {
                var queue = GetQueue(e.Queue);

                var caller = queue.GetCaller(e.UniqueId);
                if (caller != null)
                {
                    caller.Status = QueueCallerStatus.Joined;
                    var channel = GetChannel(e.UniqueId);
                    if (channel != null)
                        caller.AnsweredBy = channel.ParentCall.Channels.FirstOrDefault(p => p.OwnerPeer?.Type == PeerType.Extension && p.UniqueId != e.UniqueId && p.State == ChannelState.Up);
                    queue.RemoveCaller(e.UniqueId);

                    OnQueueCallerLeaved(caller);
                }
            }
        }
        private void Do_QueueCallerAbandon(AsterNET.NetStandard.Manager.Event.QueueCallerAbandonEvent e)
        {
            if (!string.IsNullOrWhiteSpace(e.Queue))
            {
                var queue = GetQueue(e.Queue);
                var caller = queue.GetCaller(e.UniqueId);
                caller.Status = QueueCallerStatus.Abandoned;
                caller.AbandonPosition = e.Position;
                queue.RemoveCaller(e.UniqueId, true);

                OnQueueCallerAbandoned(caller);
            }
        }
        private void Do_QueueMemberAdded(AsterNET.NetStandard.Manager.Event.QueueMemberAddedEvent e)
        {
            if (!string.IsNullOrWhiteSpace(e.Queue))
            {
                var queue = GetQueue(e.Queue);
                var member = new QueueMemberEntry()
                {
                    Location = e.Location,
                    Name = e.MemberName,
                    CallsTaken = e.CallsTaken,
                    Queue = queue
                };
                queue.AddMember(member);

                OnQueueMemberAdded(member);
            }
        }
        private void Do_QueueMemberRemoved(AsterNET.NetStandard.Manager.Event.QueueMemberRemovedEvent e)
        {
            if (!string.IsNullOrWhiteSpace(e.Queue))
            {
                var member = new QueueMemberEntry
                {
                    Location = e.Location
                };
                var queue = GetQueue(e.Queue);
                queue.RemoveMember(member.Ext);

                OnQueueMemberRemoved(member);
            }
        }

        private void QueuManager_QueueEntry(object sender, AsterNET.NetStandard.Manager.Event.QueueEntryEvent e)
        {
            if (e.Queue == "default")
                return;

            if (!string.IsNullOrWhiteSpace(e.Queue))
            {
                var queue = GetQueue(e.Queue);
                queue.AddCaller(new QueueCallerEntry
                {
                    ParentQueue = queue,
                    ParentQueueNo = e.Queue,
                    JoinDate = DateTime.Now.AddSeconds(-e.Wait),
                    JoinPosition = e.Position,
                    Name = e.CallerIdName,
                    Number = e.CallerId,
                    Status = QueueCallerStatus.Waiting,
                    UniqueId = e.UniqueId,
                });
            }
        }
        private void QueuManager_QueueMember(object sender, AsterNET.NetStandard.Manager.Event.QueueMemberEvent e)
        {
            if (e.Queue == "default")
                return;

            if (!string.IsNullOrWhiteSpace(e.Queue))
            {
                var queue = GetQueue(e.Queue);
                queue.AddMember(new QueueMemberEntry
                {
                    Location = e.Location,
                    Name = e.Name,
                    CallsTaken = e.CallsTaken
                });
            }
        }
        private void QueuManager_QueueParams(object sender, AsterNET.NetStandard.Manager.Event.QueueParamsEvent e)
        {
            if (e.Queue == "default")
                return;

            if (!string.IsNullOrWhiteSpace(e.Queue))
            {
                var queue = GetQueue(e.Queue);
                queue.Completed = e.Completed;
                queue.Abandoned = e.Abandoned;
            }
        }
        private void QueuManager_QueueStatusComplete(object sender, AsterNET.NetStandard.Manager.Event.QueueStatusCompleteEvent e)
        {
            _getQueueStatusCompleted = true;

            QueuesPersist();
        }

        protected virtual void QueuesPersist()
        {
        }

        private QueueEntry GetQueue(string queueNo)
        {
            if (!_queues.ContainsKey(queueNo))
                _queues[queueNo] = new QueueEntry
                {
                    Id = queueNo,
                };

            return _queues[queueNo];
        }
    }
}
