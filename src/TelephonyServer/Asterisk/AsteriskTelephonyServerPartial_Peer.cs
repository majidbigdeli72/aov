﻿using AsterNET.NetStandard.Manager.Action;
using AsterNET.NetStandard.Manager.Event;
using AsterNET.NetStandard.Manager.Response;
using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Threading;

namespace Septa.TelephonyServer.Asterisk
{
    public partial class AsteriskTelephonyServer
    {
        //private bool _getPeerlistCompleted;
        private List<PeerEntry> _peers;

        public override List<TrunkEntry> Trunks
        {
            get { return _peers.OfType<TrunkEntry>().ToList(); }
        }

        public override List<ExtensionEntry> Extensions
        {
            get { return _peers.OfType<ExtensionEntry>().ToList(); }
        }

        public override List<PeerEntry> Peers
        {
            get { return _peers; }
        }

        public void InitExtentionsAndTrunks()
        {
            _peers = new List<PeerEntry>();

            #region Load SIP Peers

            var response = SendAction(new CommandAction("sip show users")) as CommandResponse;
            for (int j = 1; j < response.Result.Count; j++)
            {
                var line = response.Result[j];
                var id = line.Substring(0, line.IndexOf(' '));
                bool isTrunk = Regex.IsMatch(line, "from-trunk", RegexOptions.IgnoreCase);
                int t;

                Log.Debug($"InitExtentionsAndTrunks sip users: {nameof(line)}:{line}, {nameof(id)}:{id}, {nameof(isTrunk)}:{isTrunk}");

                if (!isTrunk && int.TryParse(id, out t))
                    _peers.Add(new ExtensionEntry { Id = id, Name = id, Protocol = PeerProtocolType.SIP });
                else
                    _peers.Add(new TrunkEntry() { Name = id, Protocol = PeerProtocolType.SIP });
            }


            response = SendAction(new CommandAction("sip show peers")) as CommandResponse;
            for (int j = 1; j < response.Result.Count - 1; j++)
            {
                var line = response.Result[j];
                var peerName = line.Substring(0, line.IndexOfAny(new char[] { ' ', '/' }));
                var peer = _peers.FirstOrDefault(p => p.Name == peerName);

                Log.Debug($"InitExtentionsAndTrunks sip peers: {nameof(line)}:{line}, {nameof(peerName)}:{peerName}");

                if (peer == null)
                {
                    _peers.Add(new TrunkEntry() { Name = peerName, Protocol = PeerProtocolType.SIP });
                }
            }

            #endregion

            #region Load IAX Peers

            response = SendAction(new CommandAction("iax2 show users")) as CommandResponse;
            for (int j = 1; j < response.Result.Count - 1; j++)
            {
                var line = response.Result[j];
                var id = line.Substring(0, line.IndexOf(' '));
                bool isTrunk = Regex.IsMatch(line, "from-trunk", RegexOptions.IgnoreCase);
                int t;
                if (!isTrunk && int.TryParse(id, out t))
                    _peers.Add(new ExtensionEntry { Id = id, Name = id, Protocol = PeerProtocolType.IAX2 });
                else
                    _peers.Add(new TrunkEntry() { Name = id, Protocol = PeerProtocolType.IAX2 });
            }

            response = SendAction(new CommandAction("iax2 show peers")) as CommandResponse;
            for (int j = 1; j < response.Result.Count - 2; j++)
            {
                var line = response.Result[j];
                var peerName = line.Substring(0, line.IndexOfAny(new char[] { ' ', '/' }));
                var peer = _peers.FirstOrDefault(p => p.Name == peerName);
                if (peer == null)
                {
                    _peers.Add(new TrunkEntry() { Name = peerName, Protocol = PeerProtocolType.IAX2 });
                }
            }

            #endregion

            #region Load Dahdi channels

            response = SendAction(new CommandAction("dahdi show channels")) as CommandResponse;
            var dahdiShowChannels = response.Result.ToArray();
            //res.Insert(2, "23                 from-analog                default                         In Service");
            for (int j = 2; j < dahdiShowChannels.Length - 1; j++)
            {
                var line = dahdiShowChannels[j];
                var dahdiLineNo = line.Substring(0, line.IndexOf(' '));
                var peer = _peers.FirstOrDefault(p => p.Protocol == PeerProtocolType.DAHDI && p.Name == dahdiLineNo);
                if (peer == null)
                {
                    var trunk = new TrunkEntry() { Name = dahdiLineNo, Protocol = PeerProtocolType.DAHDI };
                    try
                    {
                        var showChannelResponse = SendAction(new CommandAction("dahdi show channel " + dahdiLineNo)) as CommandResponse;
                        foreach (var item in showChannelResponse.Result)
                        {
                            var pattern = @"Span\: (?<lineNo>\d*)";
                            var match = System.Text.RegularExpressions.Regex.Match(item, pattern);
                            if (match.Success)
                            {
                                trunk.SpanNo = Convert.ToInt32(match.Groups["lineNo"].Value);
                                break;
                            }
                        }
                    }
                    catch { }

                    _peers.Add(trunk);
                }
            }

            #endregion

            PeersPersist();
        }

        protected virtual void PeersPersist()
        {
            
        }

        public override void UpdateExtensionStatus(ExtensionEntry extension)
        {
            try
            {
                ExtensionStateAction aExtensionStateAction = new ExtensionStateAction();
                aExtensionStateAction.Exten = extension.Name;
                var aExtensionStateResponse = (ExtensionStateResponse)SendAction(aExtensionStateAction);
                Log.Debug("ExtensionStateAction sent.");
                var result = (ExtensionStatus)aExtensionStateResponse.Status;
            }
            catch (Exception ex)
            {
                Log.Error("Error in GetExtensionStatus: " + ex.ToString());
                throw;
            }
        }

        private void UpdateAllExtensionStatus()
        {
            var response = SendAction(new CommandAction("core show hints")) as CommandResponse;
            var coreShowHints = response.Result.ToArray();
            for (int j = 2; j < coreShowHints.Length - 1; j++)
            {
                var line = coreShowHints[j];
                var extension = Extensions.FirstOrDefault(p => line.Contains(p.Protocol.ToString() + "/" + p.Name));
                if (extension != null && extension.Status == ExtensionStatus.NotSet)
                {
                    var pattern = @"State\:(?<extensionState>\S*)";
                    var match = System.Text.RegularExpressions.Regex.Match(line, pattern);
                    if (match.Success)
                    {
                        extension.Status = GetExtensionStatus(match.Groups["extensionState"].Value);
                    }
                }
            }
        }

        private ExtensionStatus GetExtensionStatus(string status)
        {
            if (status.Equals("InUse&Ringing", StringComparison.InvariantCultureIgnoreCase))
                return ExtensionStatus.InUseAndRinging;

            return (ExtensionStatus)Enum.Parse(typeof(ExtensionStatus), status);
        }


        private void Do_ExtensionStatus(ExtensionStatusEvent e)
        {
            var extension = Extensions.FirstOrDefault(p => p.Name == e.Exten);
            if (extension != null)
                extension.Status = (ExtensionStatus)e.Status;
        }
    }
}
