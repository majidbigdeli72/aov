﻿using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;

using System.Linq;
using System.Runtime.Serialization;


namespace Septa.TelephonyServer.Asterisk
{
    public partial class AsteriskTelephonyServer
    {
        public override List<AnnouncementEntry> Announcements { get; set; }
        public override List<CallFlowControlEntry> CallFlowControls { get; set; }
        public override List<CallRecordingEntry> CallRecordings { get; set; }
        public override List<CallBackEntry> CallBacks { get; set; }
        public override List<ConferenceEntry> Conferences { get; set; }
        public override List<CustomApplicationEntry> CustomApplications { get; set; }
        public override List<DisaEntry> Disas { get; set; }
        public override List<FeatureCodeAdminEntry> FeatureCodeAdmins { get; set; }
        public override List<IvrEntry> Ivrs { get; set; }
        public override List<LanguageEntry> Languages { get; set; }
        public override List<MiscDestinationEntry> MiscDestinations { get; set; }
        public override List<PagingAndIntercomEntry> PagingAndIntercoms { get; set; }
        public override List<PhonebookDirectoryEntry> PhonebookDirectories { get; set; }
        public override List<QueuePriorityEntry> QueuePriorities { get; set; }
        public override List<RingGroupEntry> RingGroups { get; set; }
        public override List<TerminateCallEntry> TerminateCalls { get; set; }
        public override List<TimeConditionEntry> TimeConditions { get; set; }
        public override List<VoicemailBlastingEntry> VoicemailBlastings { get; set; }

        protected virtual void InitFromMySQL()
        {
            
        }
        protected virtual void InitFromMySQLQueue()
        {
            
        }

        protected virtual bool CheckMySqlConnection()
        {
            return true;
        }
    }
}
