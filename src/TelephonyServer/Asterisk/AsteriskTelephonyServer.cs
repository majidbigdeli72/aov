﻿using AsterNET.NetStandard.Manager;
using AsterNET.NetStandard.Manager.Action;
using AsterNET.NetStandard.Manager.Event;
using AsterNET.NetStandard.Manager.Response;
using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using WinSCP;

namespace Septa.TelephonyServer.Asterisk
{
    public partial class AsteriskTelephonyServer : BaseTelephonyServer
    {
        protected const int DefaultRetryCount = 3;
        protected const int DefaultRetryWait = 3000;

        private System.Timers.Timer _queueTimer;
        private ConcurrentQueue<EventArgs> _eventQueue;
        protected ManagerConnection _manager;
        private object _sendActionLock = new object();
        private bool _stoped = false;
        private bool _runed = false;

        private System.Timers.Timer _lastEventRaisedCheckTimer;
        private DateTime _lastEventRaisedDate;

        protected string _serverAddress;
        protected int _amiPort;
        protected string _amiUsername;
        protected string _amiPassword;

        protected int _sshPort;
        protected int _ftpPort;
        protected string _linuxUserName;
        protected string _linuxPassword;

       // Thread _initThread;

        public int QueuedEventCount => _eventQueue.Count;

        public string OriginateContext { get; set; }
        public string WinscpPath { get; set; }


        public override string Type
        {
            get { return "Astrisk"; }
        }

        public override bool CanDial
        {
            get { return true; }
        }

        public virtual bool SupportCelEvents { get => true; }
        public virtual bool SupportAccessFileSystem { get => true; }
        public virtual bool SupportChangeDialPlan { get => true; }
        public virtual bool SupportSftp { get => true; }
        public virtual bool SupportCallIdentifier { get => true; }

        public AsteriskTelephonyServer(string tsKey, string serverAddress, int amiPort, string amiUsername, string amiPassword, int sshPort, int ftpPort, string linuxUsername, string linuxPassword,
                                       string countryAreaCode, string cityAreaCode, string outgoingPrefix, string orginateContext, string recordingsRootPath, string winscpPath)
            : base(tsKey, countryAreaCode, cityAreaCode, outgoingPrefix)
        {
            _serverAddress = serverAddress;
            _amiPort = amiPort;
            _amiUsername = amiUsername;
            _amiPassword = amiPassword;

            _ftpPort = ftpPort;
            _linuxUserName = linuxUsername;
            _linuxPassword = linuxPassword;

            OriginateContext = orginateContext;

            RecordingsRootPath = string.IsNullOrWhiteSpace(recordingsRootPath) ? "/var/spool/asterisk/monitor/" : recordingsRootPath;
            WinscpPath = winscpPath;

            _manager = CreateManager();
            _manager.UnhandledEvent += _manager_UnhandledEvent;
            _manager.FireAllEvents = true;

            _queueTimer = new System.Timers.Timer();
            _queueTimer.Elapsed += _queueTimer_Elapsed;
            _queueTimer.Interval = 10;

            _lastEventRaisedCheckTimer = new System.Timers.Timer();
            _lastEventRaisedCheckTimer.Elapsed += _lastEventRaisedCheckTimer_Elapsed;
            _lastEventRaisedCheckTimer.Interval = 60000;

            _lastEventRaisedDate = DateTime.Now;
        }

        private ManagerConnection CreateManager()
        {
            var toReturn = new ManagerConnection(_serverAddress, _amiPort, _amiUsername, _amiPassword);
            toReturn.DefaultResponseTimeout = 10000;
            toReturn.ReconnectRetryMax = 100;

            return toReturn;
        }

        public override void Run()
        {
            Log.Debug("Start running {tsKey} AsteriskTelephonyServer.", Key);
            _runed = true;

            _lastEventRaisedDate = DateTime.Now;
            _lastEventRaisedCheckTimer.Start();

            _eventQueue = new ConcurrentQueue<EventArgs>();
            _queueTimer.Start();

            if (SupportChangeDialPlan)
            {
                var myManager = CreateManager();
                myManager.Login();

                var doReload = false;
                if (SupportCelEvents)
                    doReload = DoCelConfig(myManager);

                doReload = DoChannelSpy_SurveyConfig(myManager) || doReload;

                if (doReload)
                    myManager.SendAction(new CommandAction("core reload"));

                myManager.Logoff();
            }

            _manager.Login();
            _lastEventRaisedDate = DateTime.Now;
            _stoped = false;

            Log.Debug("Running {tsKey} AsteriskTelephonyServer finished.", Key);
        }

        private void _lastEventRaisedCheckTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (_runed && _lastEventRaisedDate.AddSeconds(60) < DateTime.Now)
            {
                for (int i = 0; i < 3; i++)
                {
                    if (IsAsteriskServerRespond())
                        return;
                    Thread.Sleep(5000);
                }

                Log.Error("Restarting {tsKey} AsteriskTelephonyServer because of Not responding.", Key);

                try
                {
                    this.Stop();
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Restarting {tsKey} AsteriskTelephonyServer because of Not responding. Stoping server error", Key);
                }
                finally
                {
                    try
                    {
                        this.Run();
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Restarting {tsKey} AsteriskTelephonyServer because of Not responding. Running server error", Key);
                    }
                }
            }
        }

        public override void Stop()
        {
            Initialized = false;
            _stoped = true;
            _runed = false;

            _queueTimer.Stop();
            _lastEventRaisedCheckTimer.Stop();

            if (_manager.IsConnected())
                _manager.Logoff();
        }

        private void _queueTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _queueTimer.Stop();
            try
            {
                int actionCount = 0;
                while (_eventQueue.Count > 0 && actionCount < 1000)
                {
                    if (_stoped)
                        return;

                    EventArgs eventArgs = null;
                    if (_eventQueue.TryDequeue(out eventArgs))
                    {
                        if (eventArgs is ConnectEvent)
                        {
                            Init();
                        }
                        else if (Initialized)
                        {
                            if (eventArgs is UnknownEvent)
                                Do_Unknown(eventArgs as UnknownEvent);
                            else if (eventArgs is VarSetEvent)
                                Do_SetVar(eventArgs as VarSetEvent);
                            else if (eventArgs is NewChannelEvent)
                                Do_NewChannel(eventArgs as NewChannelEvent);
                            else if (eventArgs is HangupEvent)
                                Do_Hangup(eventArgs as HangupEvent);
                            else if (eventArgs is NewStateEvent)
                                Do_NewState(eventArgs as NewStateEvent);
                            else if (eventArgs is JoinEvent)
                                Do_Join(eventArgs as JoinEvent);
                            else if (eventArgs is LeaveEvent)
                                Do_Leave(eventArgs as LeaveEvent);
                            else if (eventArgs is QueueCallerAbandonEvent)
                                Do_QueueCallerAbandon(eventArgs as QueueCallerAbandonEvent);
                            else if (eventArgs is QueueMemberAddedEvent)
                                Do_QueueMemberAdded(eventArgs as QueueMemberAddedEvent);
                            else if (eventArgs is QueueMemberRemovedEvent)
                                Do_QueueMemberRemoved(eventArgs as QueueMemberRemovedEvent);
                            else if (eventArgs is HoldEvent)
                                Do_Hold(eventArgs as HoldEvent);
                            else if (eventArgs is HoldedCallEvent)
                                Do_HoldedCall(eventArgs as HoldedCallEvent);
                            else if (eventArgs is UnholdEvent)
                                Do_Unhold(eventArgs as UnholdEvent);
                            else if (eventArgs is BridgeEvent)
                                Do_Bridge(eventArgs as BridgeEvent);
                            else if (eventArgs is NewExtenEvent)
                                Do_NewExten(eventArgs as NewExtenEvent);
                            else if (eventArgs is NewCallerIdEvent)
                                Do_NewCallerId(eventArgs as NewCallerIdEvent);
                            else if (eventArgs is ExtensionStatusEvent)
                                Do_ExtensionStatus(eventArgs as ExtensionStatusEvent);
                            else if (eventArgs is CdrEvent)
                                Do_Cdr(eventArgs as CdrEvent);
                        }
                        
                        //else if (eventArgs is PeerEntryEvent)
                        //    Do_PeerEntry(eventArgs as PeerEntryEvent);
                        //else if (eventArgs is PeerlistCompleteEvent)
                        //    Do_PeerlistComplete(eventArgs as PeerlistCompleteEvent);
                        actionCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in queue worker of {tsKey} AsteriskTelephonyServer", Key);
            }
            _queueTimer.Start();
        }

        private object _initLock = new object();
        private void Init()
        {
            if (System.Threading.Monitor.TryEnter(_initLock))
            {
                try
                {
                    for (int i = 0; i < 20; i++)
                    {
                        try
                        {
                            Log.Information("Start Init {tsKey} AsteriskTelephonyServer.", Key);

                            Initialized = false;

                            InitCalls();
                            _lastEventRaisedDate = DateTime.Now;
                            InitExtentionsAndTrunks();
                            _lastEventRaisedDate = DateTime.Now;
                            InitFromMySQL();

                            Initialized = true;

                            InitQueues();
                            _lastEventRaisedDate = DateTime.Now;

                            UpdateAllExtensionStatus();
                            _lastEventRaisedDate = DateTime.Now;

                            if (SupportCallIdentifier)
                                ReBuildHierachyRuningCallChannels();

                            Log.Information("Init {tsKey} AsteriskTelephonyServer finished.", Key);
                            return;
                        }
                        catch (Exception e)
                        {
                            Log.Error(e, "Error in Initializing {tsKey} AsteriskTelephonyServer. try {index}", Key, i);

                            Initialized = false;

                            try
                            {
                                if (_manager.IsConnected())
                                    _manager.Logoff();

                                _manager.Login();
                            }
                            catch (Exception ex)
                            {
                                Log.Error(ex, "Error in Relogin To {tsKey} AsteriskTelephonyServer", Key);
                            }

                            Thread.Sleep(10000);
                        }
                    }

                    Log.Fatal("Killing AloVoIP process.");
                    Log.CloseAndFlush();
                    Process.GetCurrentProcess().Kill();
                }
                finally
                {
                    System.Threading.Monitor.Exit(_initLock);
                }
            }
            else
                Log.Information("Init rejected because of another thread exists.");
        }

        private void _manager_UnhandledEvent(object sender, ManagerEvent e)
        {
            //if (e is ConnectEvent)
            //{
            //    var t = Task.Run(() => Init());
            //}
            //else
            //{
            _lastEventRaisedDate = DateTime.Now;
            _eventQueue.Enqueue(e);
            //}
        }

        public override void Dispose()
        {
            _queueTimer.Dispose();
            _lastEventRaisedCheckTimer.Dispose();

            if (_manager != null)
            {
                _manager.Logoff();
                _manager = null;
            }
        }

        private bool DoCelConfig(ManagerConnection myManager)
        {
            try
            {
                ManagerResponse response = new ManagerResponse();
                UpdateConfigAction config = new UpdateConfigAction();
                bool HasGeneralContext = false;
                bool HasManagerContext = false;
                if (myManager.AsteriskVersion == AsteriskVersion.ASTERISK_1_8)
                {
                    response = myManager.SendAction(new GetConfigAction("cel.conf"));
                    config = new UpdateConfigAction("cel.conf", "cel.conf", true);
                }
                else if (myManager.AsteriskVersion > AsteriskVersion.ASTERISK_1_8)
                {
                    response = myManager.SendAction(new GetConfigAction("cel_general_custom.conf"));
                    config = new UpdateConfigAction("cel_general_custom.conf", "cel_general_custom.conf", true);
                }
                if (!response.IsSuccess() && response.Message == "Config file not found")
                {
                    response = myManager.SendAction(new CreateConfigAction("cel_general_custom.conf"));
                    response = myManager.SendAction(new GetConfigAction("cel_general_custom.conf"));
                }
                if (response.IsSuccess())
                {
                    GetConfigResponse responseConfig = (GetConfigResponse)response;
                    foreach (int key in responseConfig.Categories.Keys)
                    {
                        if (responseConfig.Categories[key] == "general")
                        {
                            if (!responseConfig.Lines(key).Any(z => z.Value.ToLower() == "enable=yes"))
                            {
                                config.AddCommand(UpdateConfigAction.ACTION_APPEND, "general", "enable", "yes");
                            }
                            if (!responseConfig.Lines(key).Any(z => z.Value.Contains("apps=")))
                            {
                                config.AddCommand(UpdateConfigAction.ACTION_UPDATE, "general", "apps", "dial,park");
                            }
                            if (!responseConfig.Lines(key).Any(z => z.Value.ToLower() == "events=chan_start,attendedtransfer,pickup"))
                            {
                                if (responseConfig.Lines(key).Any(z => z.Value.Contains("events=")))
                                {
                                    config.AddCommand(UpdateConfigAction.ACTION_UPDATE, "general", "events", "CHAN_START,ATTENDEDTRANSFER,PICKUP");
                                }
                                else
                                {
                                    config.AddCommand(UpdateConfigAction.ACTION_APPEND, "general", "events", "CHAN_START,ATTENDEDTRANSFER,PICKUP");
                                }
                            }
                            HasGeneralContext = true;
                            break;
                        }
                    }
                    if (!HasGeneralContext)
                    {
                        config.AddCommand(UpdateConfigAction.ACTION_NEWCAT, "general");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, "general", "enable", "yes");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, "general", "apps", "dial,park");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, "general", "events", "CHAN_START,ATTENDEDTRANSFER,PICKUP");
                    }
                    foreach (int key in responseConfig.Categories.Keys)
                    {
                        if (responseConfig.Categories[key] == "manager")
                        {
                            if (!responseConfig.Lines(key).Any(z => z.Value.ToLower() == "enabled=yes"))
                            {
                                config.AddCommand(UpdateConfigAction.ACTION_APPEND, "manager", "enabled", "yes");
                            }
                            HasManagerContext = true;
                            break;
                        }
                    }
                    if (!HasManagerContext)
                    {
                        config.AddCommand(UpdateConfigAction.ACTION_NEWCAT, "manager");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, "manager", "enabled", "yes");
                    }
                    if (config.Actions.Count > 0)
                    {
                        config.Reload = string.Empty;
                        myManager.SendAction(config);
                        Log.Debug("InitialConfigurator DoCelConfig successfully");
                        return true;
                    }
                }
                else
                    Log.Error("Error in InitialConfigurator DoCelConfig response: " + response);
            }
            catch (Exception ex)
            {
                Log.Error("Error in InitialConfigurator DoCelConfig: " + ex.ToString());
            }
            return false;
        }

        private bool DoChannelSpy_SurveyConfig(ManagerConnection myManager)
        {
            try
            {
                string spyCatName = "AloVoIP-ChannelSpy";

                ManagerResponse response = new ManagerResponse();
                UpdateConfigAction config = new UpdateConfigAction();
                response = myManager.SendAction(new GetConfigAction("extensions_custom.conf"));
                config = new UpdateConfigAction("extensions_custom.conf", "extensions_custom.conf", true);
                if (response.IsSuccess())
                {
                    bool fromInternalCustomFound = false;
                    GetConfigResponse responseConfig = (GetConfigResponse)response;
                    foreach (int key in responseConfig.Categories.Keys)
                    {
                        if (responseConfig.Categories[key] == "from-internal-custom")
                        {
                            fromInternalCustomFound = true;
                            if (!responseConfig.Lines(key).ContainsValue("include=" + spyCatName))
                            {
                                config.AddCommand(UpdateConfigAction.ACTION_APPEND, "from-internal-custom", "include", ">" + spyCatName);
                            }
                            break;
                        }
                    }
                    if (!fromInternalCustomFound)
                    {
                        config.AddCommand(UpdateConfigAction.ACTION_NEWCAT, "from-internal-custom");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, "from-internal-custom", "include", ">" + spyCatName);
                    }

                    if (!responseConfig.Categories.Values.Contains(spyCatName))
                    {
                        config.AddCommand(UpdateConfigAction.ACTION_NEWCAT, spyCatName);

                        //listen
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8651x.#,1,Macro(user-callerid,)");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8651x.#,n,Answer");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8651x.#,n,NoCDR");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8651x.#,n,Wait(1)");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8651x.#,n,ChanSpy(sip/${EXTEN:5},q)");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8651x.#,n,Hangup");

                        //whisper
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8652x.#,1,Macro(user-callerid,)");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8652x.#,n,Answer");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8652x.#,n,NoCDR");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8652x.#,n,Wait(1)");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8652x.#,n,ChanSpy(sip/${EXTEN:5},qw)");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8652x.#,n,Hangup");

                        //barge
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8653x.#,1,Macro(user-callerid,)");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8653x.#,n,Answer");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8653x.#,n,NoCDR");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8653x.#,n,Wait(1)");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8653x.#,n,ChanSpy(sip/${EXTEN:5},qB)");
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, spyCatName, "exten", ">_*8653x.#,n,Hangup");

                    }

                    if (config.Actions.Count > 0)
                    {
                        config.Reload = string.Empty;
                        myManager.SendAction(config);
                        Log.Debug("AsteriskTelephonyServer DoChannelSpy_SurveyConfig successfully");
                        return true;
                    }
                }
                else
                    Log.Error("Error in AsteriskTelephonyServer DoChannelSpy_SurveyConfig response: " + response);
            }
            catch (Exception ex)
            {
                Log.Error("Error in AsteriskTelephonyServer DoChannelSpy_SurveyConfig: ", ex);
            }
            return false;
        }


        public override void Reload()
        {
            Log.Debug($"Sending Reload commandAction. IsConnected: {_manager.IsConnected()}");

            if (!_manager.IsConnected())
                _manager.Login();

            SendAction(new CommandAction("core reload"));

            Log.Debug("Reload commandAction sent.");
        }


        public override byte[] GetRecordingFile(string keys)
        {
            try
            {
                List<string> recordedFileNamesInWindows = new List<string>();

                if (keys == null || keys.Length == 0)
                    return null;

                List<string> rfNames = keys.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = SupportSftp ? Protocol.Sftp : Protocol.Ftp,
                    HostName = _serverAddress,
                    PortNumber = _ftpPort,
                    UserName = _linuxUserName,
                    Password = _linuxPassword,
                    GiveUpSecurityAndAcceptAnySshHostKey = SupportSftp
                };



                using (Session session = new Session())
                {
                    session.ExecutablePath = WinscpPath;
                    // Connect
                    session.Open(sessionOptions);

                    // Upload files
                    TransferOptions transferOptions = new TransferOptions();
                    //transferOptions.TransferMode = TransferMode.Binary;

                    TransferOperationResult transferResult;

                    foreach (var item in rfNames)
                    {
                        string remotePath = Path.Combine(RecordingsRootPath, item);
                        if (session.FileExists(remotePath))
                        {
                            transferResult = session.GetFiles(remotePath, Path.GetTempPath(), false, transferOptions);

                            // Throw on any error
                            transferResult.Check();

                            // Print results
                            foreach (TransferEventArgs transfer in transferResult.Transfers)
                            {
                                Log.Debug("Download of recorded file " + transfer.FileName + " succeeded");
                                recordedFileNamesInWindows.Add(transfer.Destination);
                            }
                        }
                    }

                    var outputFile = Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString());

                    WaveIO wa = new WaveIO();
                    wa.Merge(recordedFileNamesInWindows.ToArray(), outputFile);

                    foreach (var fn in recordedFileNamesInWindows)
                    {
                        if (File.Exists(fn))
                        {
                            File.Delete(fn);
                        }
                    }

                    using (var fs = new FileStream(outputFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        var buff = new byte[fs.Length];
                        fs.Read(buff, 0, buff.Length);
                        return buff;
                    }
                }
            }
            catch (Exception exp)
            {
                Log.Error(exp, "Error in GetFile : " + keys);
                return null;
            }
        }

        public override void CheckConnection()
        {
            Log.Debug("Checking Asterisk connections");

            // Checking Ami connection
            Log.Debug("Checking Ami connection...");
            try
            {
                var amiManagerConnection = new ManagerConnection(_serverAddress, _amiPort, _amiUsername, _amiPassword);
                amiManagerConnection.DefaultResponseTimeout = 10000;
                amiManagerConnection.Login();
                amiManagerConnection.Logoff();

                Log.Debug("Ami connection is Ok.");
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;
                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                    errorMessage += ". " + ex.InnerException.Message;

                throw new TelephonyServerConnectionException("Error in ami connection: " + errorMessage);
            }

            // Checking linux connection
            if (SupportAccessFileSystem)
            {
                Log.Debug("Checking linux connection...");
                try
                {
                    SessionOptions sessionOptions = new SessionOptions
                    {
                        Protocol = SupportSftp ? Protocol.Sftp : Protocol.Ftp,
                        HostName = _serverAddress,
                        PortNumber = _ftpPort,
                        UserName = _linuxUserName,
                        Password = _linuxPassword,
                        GiveUpSecurityAndAcceptAnySshHostKey = SupportSftp,
                        TimeoutInMilliseconds = 30000
                    };

                    using (Session session = new Session())
                    {
                        session.ReconnectTimeInMilliseconds = 30000;
                        session.ExecutablePath = WinscpPath;

                        // Connect
                        session.Open(sessionOptions);
                    }

                    Log.Debug("Linux connection is Ok.");
                }
                catch (Exception ex)
                {
                    var errorMessage = ex.Message;
                    if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                        errorMessage += ". " + ex.InnerException.Message;

                    throw new TelephonyServerConnectionException("Error in linux connection: " + errorMessage);
                }
            }
            // Checking mysql connection
            Log.Debug("Checking mysql connection...");
            try
            {
                CheckMySqlConnection();

                Log.Debug("MySql connection is Ok.");
            }
            catch (Exception ex)
            {
                var errorMessage = ex.Message;
                if (ex.InnerException != null && !string.IsNullOrEmpty(ex.InnerException.Message))
                    errorMessage += ". " + ex.InnerException.Message;

                throw new TelephonyServerConnectionException("Error in mysql connection: " + errorMessage);
            }

            Log.Debug("Successfully connected to AsteriskTelephonyServer.");
        }


        private bool IsAsteriskServerRespond()
        {
            try
            {
                var response = SendAction(new CommandAction("core show channels")) as CommandResponse;
                if (response != null && response.Result != null && response.Result.Count > 0)
                    return true;
            }
            catch
            {
            }
            return false;
        }
        protected ManagerResponse SendAction(ManagerAction action)
        {
            lock (_sendActionLock)
                return _manager.SendAction(action);
        }
        protected ManagerResponse SendAction(ManagerAction action, int timeout)
        {
            lock (_sendActionLock)
                return _manager.SendAction(action, timeout);
        }
    }
}