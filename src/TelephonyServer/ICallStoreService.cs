﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer
{
    public interface ICallStoreService
    {
        string CallStoreId { get; }
        CallCreateResult CallCreated(string tsKey, string sourceCallId, string number, DateTime date, CallType callType, string sourceInitCallChannelId, string sourceInitCallChannelPeerName, PeerType sourceInitCallChannelPeerType);
        CallUpdateResult CallUpdated(string callId, string number, DateTime? date, CallType callType, string profileId);

        string CallChannelCreated(string callId, string peerName, PeerType peerType, string sourceCallChannelId, ChannelState channelState, DateTime createDate);
        void CallChannelUpdated(string channelId, ChannelState channelState, ChannelResponse channelResponse, DateTime? connectDate, DateTime? hangupDate, string RecordedFileName, string toChangePeerName = "", PeerType? toChangePeerType = null);

        void MergeCall(string tsKey, long sourceCallId, long destCallId);
    }
}
