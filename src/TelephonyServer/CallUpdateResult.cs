﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer
{
    public class CallUpdateResult
    {
        public string ProfileId { get; set; }
        public string ProfileName { get; set; }
    }
}
