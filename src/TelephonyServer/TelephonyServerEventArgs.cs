﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Septa.TelephonyServer
{
    public class CallCreatedEventArgs : EventArgs
    {
        public CallCreatedEventArgs(string tsKey, string callId, string number, DateTime date, CallType callType, string initCallChannelId, string initCallChannelPeerName, PeerType initCallChannelPeerType)
        {
            this.TsKey = tsKey;
            this.CallId = callId;
            this.Number = number;
            this.Date = date;
            this.CallType = callType;
            this.InitCallChannelId = initCallChannelId;
            this.InitCallChannelPeerName = initCallChannelPeerName;
            this.InitCallChannelPeerType = initCallChannelPeerType;
        }

        public string TsKey { get; set; }
        public string CallId { get; set; }
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public CallType CallType { get; set; }
        public string InitCallChannelId { get; set; }
        public string InitCallChannelPeerName { get; set; }
        public PeerType InitCallChannelPeerType { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }

    public class CallUpdatedEventArgs : EventArgs
    {
        public CallUpdatedEventArgs(string tsKey, string callId, string number, DateTime? endDate, CallType callType, string profileId)
        {
            this.TsKey = tsKey;
            this.CallId = callId;
            this.Number = number;
            this.EndDate = endDate;
            this.CallType = callType;
            this.ProfileId = profileId;
        }

        public string TsKey { get; set; }
        public string CallId { get; set; }
        public string Number { get; set; }
        public DateTime? EndDate { get; set; }
        public CallType CallType { get; set; }
        public string ProfileId { get; set; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }

    public class CallChannelCreatedEventArgs : EventArgs
    {
        public CallChannelCreatedEventArgs(string tsKey, string callId, string peerName, PeerType peerType, string channelId, ChannelState channelState, DateTime createDate)
        {
            this.TsKey = tsKey;
            this.CallId = callId;
            this.PeerName = peerName;
            this.PeerType = peerType;
            this.ChannelId = channelId;
            this.ChannelState = channelState;
            this.CreateDate = createDate;
        }

        public string TsKey { get; }
        public string CallId { get; }
        public string PeerName { get; }
        public PeerType PeerType { get; }
        public string ChannelId { get; }
        public ChannelState ChannelState { get; }
        public DateTime CreateDate { get; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }

    public class CallChannelUpdatedEventArgs : EventArgs
    {
        public CallChannelUpdatedEventArgs(string tsKey, string callId, string channelId, ChannelState channelState, ChannelResponse channelResponse, DateTime? connectDate, DateTime? hangupDate, string recordedFileName, string toChangePeerName = "", PeerType? toChangePeerType = null)
        {
            this.TsKey = tsKey;
            this.CallId = callId;
            this.ChannelId = channelId;
            this.ChannelState = channelState;
            this.ChannelResponse = channelResponse;
            this.ConnectDate = connectDate;
            this.HangupDate = hangupDate;
            this.RecordedFileName = recordedFileName;
            this.ToChangePeerName = toChangePeerName;
            this.ToChangePeerType = toChangePeerType;
        }

        public string TsKey { get; }
        public string CallId { get; }
        public string ChannelId { get; }
        public ChannelState ChannelState { get; }
        public ChannelResponse ChannelResponse { get; }
        public DateTime? ConnectDate { get; }
        public DateTime? HangupDate { get; }
        public string RecordedFileName { get; }
        public string ToChangePeerName { get; }
        public PeerType? ToChangePeerType { get; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }

    public class CallsMergedEventargs : EventArgs
    {
        public CallsMergedEventargs(string tsKey, long destroyedCallId, long mainCallId)
        {
            this.TsKey = tsKey;
            this.DestroyedCallId = destroyedCallId;
            this.MainCallId = mainCallId;
        }

        public string TsKey { get; }
        public long DestroyedCallId { get; }
        public long MainCallId { get; }

        public override string ToString()
        {
            return JsonSerializer.Serialize(this);
        }
    }

    public class NewProfileInfoFoundEventArgs : EventArgs
    {
        public NewProfileInfoFoundEventArgs(long callId, string newProfileId, string newProfileName)
        {
            CallId = callId;
            NewProfileId = newProfileId;
            NewProfileName = newProfileName;
        }

        public long CallId { get; set; }
        public string NewProfileId { get; set; }
        public string NewProfileName { get; set; }
    }
}
