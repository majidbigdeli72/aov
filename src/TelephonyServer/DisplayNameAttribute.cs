﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer
{
    public class DisplayNameAttribute : Attribute
    {
        public string Name { get; private set; }
        public DisplayNameAttribute(string name)
        {
            this.Name = name;
        }        
    }
}
