﻿using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Septa.TelephonyServer
{
    public class CallStoreServiceManager
    {
        private ConcurrentDictionary<string, CallStore> _callStores;
        private System.Timers.Timer _lookupSourceQueueTimer;
        private ConcurrentQueue<EventArgs> _eventLookupSourceQueue;
        private readonly Func<string, IList<ICallStoreService>> _callStoreServicesResolver;

        private long _callCreateCount;
        private double _callCreateApiAvg;

        private long _callUpdateCount;
        private long _callUpdateBypassedCount;
        private double _callUpdateApiAvg;

        private long _callChannelCreateCount;
        private double _callChannelCreateApiAvg;

        private long _callChannelUpdateCount;
        private long _callChannelUpdateBypassedCount;
        private double _callChannelUpdateApiAvg;

        private long _callMergeCount;
        private double _callMergeApiAvg;


        public event EventHandler<NewProfileInfoFoundEventArgs> NewProfileInfoFound;

        public CallStoreServiceManager(Func<string, IList<ICallStoreService>> callStoreServicesResolver)
        {
            _callStores = new ConcurrentDictionary<string, TelephonyServer.CallStore>();

            _eventLookupSourceQueue = new ConcurrentQueue<EventArgs>();
            _lookupSourceQueueTimer = new System.Timers.Timer();
            _lookupSourceQueueTimer.Interval = 100;
            _lookupSourceQueueTimer.Elapsed += _lookupSourceQueueTimer_Elapsed;

            _callStoreServicesResolver = callStoreServicesResolver;
        }

        public Dictionary<string,string> GetIndicators()
        {
            var toReturn = new Dictionary<string, string>();

            toReturn.Add("QueuedEventCount", $"Total:{_eventLookupSourceQueue.Count}");
            toReturn.Add("QueuedEventStats", $"CC:{_eventLookupSourceQueue.Count(p => p is CallCreatedEventArgs)} CU:{_eventLookupSourceQueue.Count(p => p is CallUpdatedEventArgs)} CCC:{_eventLookupSourceQueue.Count(p => p is CallChannelCreatedEventArgs)} CCU:{_eventLookupSourceQueue.Count(p => p is CallChannelUpdatedEventArgs)} CM:{_eventLookupSourceQueue.Count(p => p is CallsMergedEventargs)}");

            toReturn.Add("CallCreate", $"Count:{_callCreateCount} Avg: {_callCreateApiAvg}");
            toReturn.Add("CallUpdate", $"Count:{_callUpdateCount} Bypassed: {_callUpdateBypassedCount} Avg: {_callUpdateApiAvg}");
            toReturn.Add("CallChannelCreate", $"Count:{_callChannelCreateCount} Avg: {_callChannelCreateApiAvg}");
            toReturn.Add("CallChannelUpdate", $"Count:{_callChannelUpdateCount} Bypassed: {_callChannelUpdateBypassedCount} Avg: {_callChannelUpdateApiAvg}");
            toReturn.Add("CallMerge", $"Count:{_callMergeCount} Avg: {_callMergeApiAvg}");

            return toReturn;
        }

        public void Start()
        {
            _lookupSourceQueueTimer.Start();
        }

        public void Stop()
        {
            _lookupSourceQueueTimer.Stop();
        }

        private void _lookupSourceQueueTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            //if (File.Exists(@"c:\l.txt"))
            //{
            //    Log.Debug("LookupSourceQueueTimer Beated. Queue Count : {count}", _eventLookupSourceQueue.Count);
            //}

            _lookupSourceQueueTimer.Stop();


            try
            {
                int actionCount = 0;
                while (_eventLookupSourceQueue.Count > 0 && actionCount < 1000)
                {
                    //if (File.Exists(@"c:\l.txt"))
                    //{
                    //    Log.Debug("Before try Dequeue Count : {count}", _eventLookupSourceQueue.Count);
                    //}
                    EventArgs eventArgs = null;
                    if (_eventLookupSourceQueue.TryDequeue(out eventArgs))
                    {
                        if (eventArgs is CallCreatedEventArgs)
                            Do_CallCreate(eventArgs as CallCreatedEventArgs);
                        else if (eventArgs is CallUpdatedEventArgs)
                            Do_CallUpdate(eventArgs as CallUpdatedEventArgs);
                        else if (eventArgs is CallChannelCreatedEventArgs)
                            Do_CallChannelCreate(eventArgs as CallChannelCreatedEventArgs);
                        else if (eventArgs is CallChannelUpdatedEventArgs)
                            Do_CallChannelUpdate(eventArgs as CallChannelUpdatedEventArgs);
                        else if (eventArgs is CallsMergedEventargs)
                            Do_CallMerged(eventArgs as CallsMergedEventargs);

                        actionCount++;
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in lookupSource queue worker");
            }

            _lookupSourceQueueTimer.Start();
        }

        private CallStore GetMyStore(string lookupSourceId)
        {
            return _callStores.GetOrAdd(lookupSourceId, new CallStore());
        }

        public void OnCallCreated(object sender, CallCreatedEventArgs e)
        {
#if DEBUG
            Do_CallCreate(e);
#else
            _eventLookupSourceQueue.Enqueue(e);
#endif
        }
        private void Do_CallCreate(CallCreatedEventArgs e)
        {
            try
            {
                foreach (var lookupSourceService in _callStoreServicesResolver(e.TsKey))
                {
                    string msgTemplate = $"LookupSource CallCreate {nameof(e.TsKey)}:{e.TsKey}:";
                    try
                    {
                        Log.Debug(msgTemplate + "{@callInfo}", e);

                        _callCreateCount++;
                        var startTime = DateTime.Now;
                        var result = lookupSourceService.CallCreated(e.TsKey,
                                                                    e.CallId,
                                                                    e.Number,
                                                                    e.Date,
                                                                    e.CallType,
                                                                    e.InitCallChannelId,
                                                                    e.InitCallChannelPeerName,
                                                                    e.InitCallChannelPeerType);

                        _callCreateApiAvg = ((_callCreateApiAvg * (_callCreateCount - 1)) + (DateTime.Now - startTime).TotalMilliseconds) / _callCreateCount;

                        Log.Debug(msgTemplate + "{@result}", result);

                        long callId = long.Parse(e.CallId);
                        GetMyStore(lookupSourceService.CallStoreId).AddCall(callId, result.CallId, e.CallType, e.Number);
                        GetMyStore(lookupSourceService.CallStoreId).AddCallChannel(callId, long.Parse(e.InitCallChannelId), result.InitCallChannelId, ChannelState.Down, ChannelResponse.NotAnswered, string.Empty);

                        if (!string.IsNullOrEmpty(result.ProfileId))
                        {
                            NewProfileInfoFound?.Invoke(this, new NewProfileInfoFoundEventArgs(callId, result.ProfileId, result.ProfileName));
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, msgTemplate + "{@callInfo}", e);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Unknown Error in Do_CallCreate");
            }
        }

        public void OnCallUpdated(object sender, CallUpdatedEventArgs e)
        {
#if DEBUG
            Do_CallUpdate(e);
#else
            _eventLookupSourceQueue.Enqueue(e);
#endif
        }
        private void Do_CallUpdate(CallUpdatedEventArgs e)
        {
            try
            {
                foreach (var lookupSourceService in _callStoreServicesResolver(e.TsKey))
                {
                    string msgTemplate = $"LookupSource CallUpdate {nameof(e.TsKey)}:{e.TsKey}:";
                    try
                    {
                        var myStore = GetMyStore(lookupSourceService.CallStoreId);
                        var callId = long.Parse(e.CallId);
                        var activeCall = myStore.FindCall(callId);
                        if (activeCall != null)
                        {
                            if (activeCall.Number != e.Number ||
                                activeCall.CallType != e.CallType ||
                                activeCall.ProfileId != e.ProfileId ||
                                e.EndDate.HasValue)
                            {
                                Log.Debug(msgTemplate + "{@callInfo}", e);

                                _callUpdateCount++;
                                var startTime = DateTime.Now;
                                var result = lookupSourceService.CallUpdated(activeCall.LookupSourceCallId,
                                                                e.Number,
                                                                e.EndDate,
                                                                e.CallType,
                                                                e.ProfileId);

                                _callUpdateApiAvg = ((_callUpdateApiAvg * (_callUpdateCount - 1)) + (DateTime.Now - startTime).TotalMilliseconds) / _callUpdateCount;

                                activeCall.Number = e.Number;
                                activeCall.CallType = e.CallType;
                                activeCall.ProfileId = e.ProfileId;

                                if (!string.IsNullOrEmpty(result.ProfileId))
                                {
                                    activeCall.ProfileId = result.ProfileId;
                                    NewProfileInfoFound?.Invoke(this, new NewProfileInfoFoundEventArgs(callId, result.ProfileId, result.ProfileName));
                                }
                            }
                            else
                            {
                                _callUpdateBypassedCount++;
                                Log.Debug(msgTemplate + "{data}", $"call {activeCall.LookupSourceCallId} bypassed");
                            }
                        }
                        if (e.EndDate.HasValue)
                            myStore.RemoveCall(long.Parse(e.CallId));
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, msgTemplate + "{@callInfo}", e);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Unknown Error in Do_CallUpdate");
            }
        }

        public void OnCallChannelCreated(object sender, CallChannelCreatedEventArgs e)
        {
#if DEBUG
            Do_CallChannelCreate(e);
#else
            _eventLookupSourceQueue.Enqueue(e);
#endif
        }
        private void Do_CallChannelCreate(CallChannelCreatedEventArgs e)
        {
            try
            {
                foreach (var lookupSourceService in _callStoreServicesResolver(e.TsKey))
                {
                    string msgTemplate = $"LookupSource CallChannelCreate {nameof(e.TsKey)}:{e.TsKey}:";
                    try
                    {
                        var myStore = GetMyStore(lookupSourceService.CallStoreId);
                        var activeCall = myStore.FindCall(long.Parse(e.CallId));
                        if (activeCall != null)
                        {
                            Log.Debug(msgTemplate + "{@callChannelInfo}", e);

                            _callChannelCreateCount++;
                            var startTime = DateTime.Now;
                            var callChannelId = lookupSourceService.CallChannelCreated(activeCall.LookupSourceCallId,
                                                                                       e.PeerName,
                                                                                       e.PeerType,
                                                                                       e.ChannelId,
                                                                                       e.ChannelState,
                                                                                       e.CreateDate).ToString();
                            _callChannelCreateApiAvg = ((_callChannelCreateApiAvg * (_callChannelCreateCount - 1)) + (DateTime.Now - startTime).TotalMilliseconds) / _callChannelCreateCount;

                            Log.Debug(msgTemplate + "{data}", $"created with callchannelid:{callChannelId}");

                            myStore.AddCallChannel(long.Parse(e.CallId), long.Parse(e.ChannelId), callChannelId, e.ChannelState, ChannelResponse.NotAnswered, string.Empty);
                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, msgTemplate + "{@callChannelInfo}", e);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Unknown Error in Do_CallChannelCreate.");
            }
        }

        public void OnCallChannelUpdated(object sender, CallChannelUpdatedEventArgs e)
        {
#if DEBUG
            Do_CallChannelUpdate(e);
#else
            _eventLookupSourceQueue.Enqueue(e);
#endif
        }
        private void Do_CallChannelUpdate(CallChannelUpdatedEventArgs e)
        {
            try
            {
                foreach (var lookupSourceService in _callStoreServicesResolver(e.TsKey))
                {
                    string msgTemplate = $"LookupSource CallChannelUpdate {nameof(e.TsKey)}:{e.TsKey}:";
                    try
                    {
                        var myStore = GetMyStore(lookupSourceService.CallStoreId);
                        var activeCallChannel = myStore.FindCallChannel(long.Parse(e.CallId), long.Parse(e.ChannelId));
                        if (activeCallChannel != null)
                        {
                            if (activeCallChannel.ConnectDate != e.ConnectDate ||
                                activeCallChannel.HangDate != e.HangupDate ||
                                activeCallChannel.ChannelState != e.ChannelState ||
                                activeCallChannel.ChannelResponse != e.ChannelResponse ||
                                activeCallChannel.RecordedFileName != e.RecordedFileName ||
                                !string.IsNullOrEmpty(e.ToChangePeerName))
                            {
                                Log.Debug(msgTemplate + "LookupSourceCallChannelId:{LookupSourceCallChannelId} CallChannelUpdatedEventArgs:{@callChannelInfo}", activeCallChannel.LookupSourceCallChannelId, e);

                                _callChannelUpdateCount++;
                                var startTime = DateTime.Now;
                                lookupSourceService.CallChannelUpdated(activeCallChannel.LookupSourceCallChannelId,
                                                                       e.ChannelState,
                                                                       e.ChannelResponse,
                                                                       e.ConnectDate,
                                                                       e.HangupDate,
                                                                       e.RecordedFileName,
                                                                       e.ToChangePeerName,
                                                                       e.ToChangePeerType);
                                _callChannelUpdateApiAvg = ((_callChannelUpdateApiAvg * (_callChannelUpdateCount - 1)) + (DateTime.Now - startTime).TotalMilliseconds) / _callChannelUpdateCount;
                                Log.Debug(msgTemplate + "Successfully Updated. LookupSourceCallChannelId:{LookupSourceCallChannelId} CallChannelUpdatedEventArgs:{@callChannelInfo}", activeCallChannel.LookupSourceCallChannelId, e);


                                activeCallChannel.ConnectDate = e.ConnectDate;
                                activeCallChannel.HangDate = e.HangupDate;
                                activeCallChannel.ChannelState = e.ChannelState;
                                activeCallChannel.ChannelResponse = e.ChannelResponse;
                                activeCallChannel.RecordedFileName = e.RecordedFileName;
                            }
                            else
                            {
                                _callChannelUpdateBypassedCount++;
                                Log.Debug(msgTemplate + "{data}", $"callchannel {activeCallChannel.LookupSourceCallChannelId} bypassed");
                            }
                        }

                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, msgTemplate + "{@callChannelInfo}", e);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Unknown Error in Do_CallChannelUpdate.");
            }
        }

        public void OnCallMerged(object sender, CallsMergedEventargs e)
        {
#if DEBUG
            Do_CallMerged(e);
#else
            _eventLookupSourceQueue.Enqueue(e);
#endif
        }
        private void Do_CallMerged(CallsMergedEventargs e)
        {
            try
            {
                foreach (var lookupSourceService in _callStoreServicesResolver(e.TsKey))
                {
                    string msgTemplate = $"LookupSource CallMerged {nameof(e.TsKey)}:{e.TsKey}:";
                    try
                    {
                        Log.Debug(msgTemplate + "{@callMergeInfo}", e);

                        var myStore = GetMyStore(lookupSourceService.CallStoreId);
                        var sourceCallId = myStore.FindCall(e.DestroyedCallId).LookupSourceCallId;
                        var destCallId = myStore.FindCall(e.MainCallId).LookupSourceCallId;

                        _callMergeCount++;
                        var startTime = DateTime.Now;
                        lookupSourceService.MergeCall(e.TsKey,
                                                      long.Parse(sourceCallId),
                                                      long.Parse(destCallId));

                        _callMergeApiAvg = ((_callMergeApiAvg * (_callMergeCount - 1)) + (DateTime.Now - startTime).TotalMilliseconds) / _callMergeCount;
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, msgTemplate + "{@callMergeInfo}", e);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Unknown Error in Do_CallMerged.");
            }
        }
    }
}
