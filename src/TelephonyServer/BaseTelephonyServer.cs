﻿using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Generic;
using System.Text.Json;
using System.Threading;

namespace Septa.TelephonyServer
{
	public abstract partial class BaseTelephonyServer : ITelephonyServer
	{
		public int Id { get; set; }
		public string Key { get; set; }
		public abstract string Type { get; }
		public string OutgoingPrefix { get; set; }

		public abstract bool CanDial { get; }
		public bool Initialized { get; protected set; }


		public string RecordingsRootPath { get; set; }
		public string CountryAreaCode { get; set; }
		public string CityAreaCode { get; set; }

		public abstract List<PeerEntry> Peers { get; }
		public abstract List<TrunkEntry> Trunks { get; }
		public abstract List<ExtensionEntry> Extensions { get; }
		public abstract List<QueueEntry> Queues { get; }
		public abstract List<CallEntry> Calls { get; }
		public abstract List<AnnouncementEntry> Announcements { get; set; }
		public abstract List<CallFlowControlEntry> CallFlowControls { get; set; }
		public abstract List<CallRecordingEntry> CallRecordings { get; set; }
		public abstract List<CallBackEntry> CallBacks { get; set; }
		public abstract List<ConferenceEntry> Conferences { get; set; }
		public abstract List<CustomApplicationEntry> CustomApplications { get; set; }
		public abstract List<DisaEntry> Disas { get; set; }
		public abstract List<FeatureCodeAdminEntry> FeatureCodeAdmins { get; set; }
		public abstract List<IvrEntry> Ivrs { get; set; }
		public abstract List<LanguageEntry> Languages { get; set; }
		public abstract List<MiscDestinationEntry> MiscDestinations { get; set; }
		public abstract List<PagingAndIntercomEntry> PagingAndIntercoms { get; set; }
		public abstract List<PhonebookDirectoryEntry> PhonebookDirectories { get; set; }
		public abstract List<QueuePriorityEntry> QueuePriorities { get; set; }
		public abstract List<RingGroupEntry> RingGroups { get; set; }
		public abstract List<TerminateCallEntry> TerminateCalls { get; set; }
		public abstract List<TimeConditionEntry> TimeConditions { get; set; }
		public abstract List<VoicemailBlastingEntry> VoicemailBlastings { get; set; }

		public abstract void Dispose();
		public abstract void Reload();
		public abstract void Run();
		public abstract void Stop();
		public abstract void UpdateExtensionStatus(ExtensionEntry extension);
		public abstract OriginateResponseStatus Originate(string channel, string channelCID, string number, bool applyOutgoingRules, Dictionary<string, string> variables = null);
		public abstract OriginateResponseStatus OriginateToExtension(string exten, string extenCID, string number, bool applyOutgoingRules, Dictionary<string, string> variables = null);
		public abstract byte[] GetRecordingFile(string keys);
		public abstract CallChannelEntry GetChannel(string uniqueId);
		public abstract bool ChannelSpy(string extn, string spiedExtn, SpyMode spyMode);
		public abstract bool ChannelHangup(string channelName);
		public abstract void CheckConnection();

		public BaseTelephonyServer(string tsKey, string countryAreaCode, string cityAreaCode, string outgoingPrefix)
		{
			Key = tsKey;

			OutgoingPrefix = outgoingPrefix;

			CountryAreaCode = string.IsNullOrWhiteSpace(countryAreaCode) ? "98" : countryAreaCode;
			CityAreaCode = string.IsNullOrWhiteSpace(cityAreaCode) ? "21" : cityAreaCode;
		}


		public virtual string GetStandardNumber(string number, bool applyOutgoingRules)
		{
			string numberToDial = number;

			if (applyOutgoingRules)
			{
				if (numberToDial.StartsWith("00"))
					numberToDial = numberToDial.Remove(0, 2);

				if (numberToDial.StartsWith("+"))
					numberToDial = numberToDial.Remove(0, 1);

				if (numberToDial.StartsWith(CountryAreaCode) && numberToDial.Length > 8)
					numberToDial = numberToDial.Remove(0, numberToDial.IndexOf(CountryAreaCode) + CountryAreaCode.Length);

				if (numberToDial.StartsWith(CityAreaCode) && numberToDial.Length > 8)
					numberToDial = numberToDial.Remove(0, numberToDial.IndexOf(CityAreaCode) + CityAreaCode.Length);

				if (numberToDial.Length == 11 && !numberToDial.StartsWith("0"))
					numberToDial = "0" + numberToDial;
				else if (numberToDial.Length == 10)
					numberToDial = "0" + numberToDial;

				numberToDial = OutgoingPrefix + numberToDial;
			}

			return numberToDial;
		}

		protected string RemoveOutgoingPrefixFromNumber(CallType callType, string number)
		{
			switch (callType)
			{
				case CallType.Outgoing:
					return !string.IsNullOrEmpty(number) && !string.IsNullOrEmpty(OutgoingPrefix) && number.StartsWith(OutgoingPrefix)
														? number.Substring(OutgoingPrefix.Length)
														: number;
				case CallType.Internal:
				case CallType.Missed:
				case CallType.Incoming:
				default:
					return number;
			}
		}

		#region Events

		public event EventHandler<CallCreatedEventArgs> CallCreated;
		public event EventHandler<CallUpdatedEventArgs> CallUpdated;

		public event EventHandler<CallChannelCreatedEventArgs> CallChannelCreated;
		public event EventHandler<CallChannelUpdatedEventArgs> CallChannelUpdated;

		public event EventHandler<CallsMergedEventargs> CallsMerged;


		//protected void OnCallCreated(CallEntry e)
		//{
		//    if (!e.DbId.HasValue)
		//    {
		//        var callId = CallCreatedPersist(e);
		//        e.DbId = callId;

		//        Log.Debug("call created: " + JsonConvert.SerializeObject(e));
		//        if (CallCreated != null && !e.InitByChannel.IsLocal)
		//        {
		//            CallCreated(this, new CallCreatedEventArgs(
		//                this.Key,
		//                e.DbId.ToString(),
		//                RemoveOutgoingPrefixFromNumber(e.CallType, e.PhoneNumber),
		//                e.StartDate,
		//                e.CallType,
		//                e.InitByChannel.DbId.ToString(),
		//                e.InitByChannel.OwnerPeer.Name,
		//                e.InitByChannel.OwnerPeer.Type));
		//        }
		//    }
		//    else
		//    {

		//    }
		//}
		protected virtual void OnCallUpdated(CallEntry e)
		{
			if (e == null)
				return;

			if (!e.DbId.HasValue)
			{
				var callId = CallCreatedPersist(e);
				e.DbId = callId;

				//Log.Debug("call created: " + JsonSerializer.Serialize(e));
				if (!e.InitByChannel.IsLocal)
				{
					CallCreated?.Invoke(this, new CallCreatedEventArgs(
						this.Key,
						e.DbId.ToString(),
						RemoveOutgoingPrefixFromNumber(e.CallType, e.PhoneNumber),
						e.StartDate,
						e.CallType,
						e.InitByChannel.DbId.ToString(),
						e.InitByChannel.OwnerPeer.Name,
						e.InitByChannel.OwnerPeer.Type));

					if (e.InitByChannel.State == ChannelState.Up)
					{
						OnCallChannelUpdated(e.InitByChannel, false);
					}
				}

				foreach (var chainChannel in e.ChainChannels)
				{
					OnCallChannelUpdated(chainChannel, false);
				}
			}
			else
			{
				CallUpdatedPersist(e);

				//Log.Debug("OnCallUpdated: " + JsonConvert.SerializeObject(e));
				if (!e.InitByChannel.IsLocal)
				{
					CallUpdated?.Invoke(this, new CallUpdatedEventArgs(
						this.Key,
						e.DbId.ToString(),
						RemoveOutgoingPrefixFromNumber(e.CallType, e.PhoneNumber),
						e.EndDate,
						e.CallType,
						e.ProfileId));
				}
			}
		}

		//protected void OnCallChannelCreated(CallChannelEntry e, bool updateCall = true)
		//{
		//    if (e.ParentCall.DbId.HasValue)
		//    {
		//        Log.Debug("OnCallChannelCreated...");

		//        if (!e.IsLocal)
		//        {
		//            var callChannelId = CallChannelCreatedPersist(e);
		//            e.DbId = callChannelId;

		//            CallChannelCreated?.Invoke(this, new CallChannelCreatedEventArgs(
		//                this.Key,
		//                e.ParentCall.DbId.ToString(),
		//                e.OwnerPeer.Name,
		//                e.OwnerPeer.Type,
		//                e.DbId.ToString(),
		//                e.State,
		//                e.CreateDate));
		//        }
		//    }
		//    if (updateCall)
		//        OnCallUpdated(e.ParentCall);
		//}
		protected virtual void OnCallChannelUpdated(CallChannelEntry e, bool updateCall = true, PeerEntry oldPeer = null)
		{
			if (e.ParentCall == null)
				return;

			if (e.ParentCall.DbId.HasValue)
			{
				if (!e.IsLocal)
				{
					if (e.DbId.HasValue)
					{
						CallChannelUpdatedPersist(e, oldPeer);

						//Log.Debug("Call channel updated: " + JsonConvert.SerializeObject(e));

						var toChangePeerName = "";
						PeerType? toChangePeerType = null;
						if (oldPeer != null)
						{
							toChangePeerName = e.OwnerPeer.Name;
							toChangePeerType = e.OwnerPeer.Type;
						}

						CallChannelUpdated?.Invoke(this, new CallChannelUpdatedEventArgs(this.Key,
																						 e.ParentCall.DbId.ToString(),
																						 e.DbId.ToString(),
																						 e.State,
																						 e.Response,
																						 e.ConnectDate,
																						 e.HangupDate,
																						 e.RecordFileName,
																						 toChangePeerName,
																						 toChangePeerType));
					}
					else
					{
						var callChannelId = CallChannelCreatedPersist(e);
						e.DbId = callChannelId;

						CallChannelCreated?.Invoke(this, new CallChannelCreatedEventArgs(
							this.Key,
							e.ParentCall.DbId.ToString(),
							e.OwnerPeer.Name,
							e.OwnerPeer.Type,
							e.DbId.ToString(),
							e.State,
							e.CreateDate));
					}
				}
			}
			if (updateCall)
				OnCallUpdated(e.ParentCall);
		}

		protected void OnCallDestroyed(CallEntry toDestroy, CallEntry main)
		{
			try
			{
				CallDestroyedPersist(toDestroy, main);

				CallsMerged?.Invoke(this, new CallsMergedEventargs(this.Key,
																   toDestroy.DbId.Value,
																   main.DbId.Value));
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Error in OnCallDestroyed:");
			}
		}

		protected void OnQueueCallerJoined(QueueCallerEntry e)
		{
			var callerId = QueueCallerJoinedPersist(e);
			e.DbId = callerId;
		}
		protected void OnQueueCallerLeaved(QueueCallerEntry e)
		{
			QueueCallerLeavedPersist(e);

			//UpdateQueueDashboardStatsChanges();
		}
		protected void OnQueueCallerAbandoned(QueueCallerEntry e)
		{
			QueueCallerAbandonedPersist(e);

			//UpdateQueueDashboardStatsChanges();
		}

		protected void OnQueueMemberAdded(QueueMemberEntry e)
		{
			QueueMemberAddedPersist(e);

			//UpdateQueueDashboardStatsChanges();
		}

		protected void OnQueueMemberRemoved(QueueMemberEntry e)
		{
			QueueMemberRemovedPersist(e);

			//UpdateQueueDashboardStatsChanges();
		}

		#endregion

		long _callId = 0;
		protected virtual long CallCreatedPersist(CallEntry e)
		{
			Interlocked.Increment(ref _callId);
			e.DbId = _callId;

			if (e.InitByChannel != null)
			{
				Interlocked.Increment(ref _callChannelId);
				e.InitByChannel.DbId = _callChannelId;
			}
			return e.DbId.Value;
		}
		protected virtual void CallUpdatedPersist(CallEntry e)
		{

		}
		long _callChannelId = 0;
		protected virtual long CallChannelCreatedPersist(CallChannelEntry e)
		{
			Interlocked.Increment(ref _callChannelId);
			e.DbId = _callChannelId;
			return e.DbId.Value;
		}
		protected virtual void CallChannelUpdatedPersist(CallChannelEntry e, PeerEntry oldPeer)
		{

		}
		protected virtual void CallDestroyedPersist(CallEntry toDestroy, CallEntry main)
		{

		}
		protected virtual long QueueCallerJoinedPersist(QueueCallerEntry e)
		{
			return 0;
		}
		protected virtual void QueueCallerLeavedPersist(QueueCallerEntry e)
		{

		}
		protected virtual void QueueCallerAbandonedPersist(QueueCallerEntry e)
		{

		}

		protected virtual void QueueMemberAddedPersist(QueueMemberEntry e)
		{

		}

		protected virtual void QueueMemberRemovedPersist(QueueMemberEntry e)
		{

		}
	}
}
