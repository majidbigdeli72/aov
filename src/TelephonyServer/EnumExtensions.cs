﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.TelephonyServer
{
    public static class EnumExtensions
    {
        public static string GetDisplayName(this Enum enumValue)
        {
            try
            {
                var res = enumValue.GetType().GetMember(enumValue.ToString()).FirstOrDefault();
                if (res != null)
                {
                    var customAttributes = res.GetCustomAttributes(typeof(DisplayNameAttribute), false);
                    if (customAttributes != null && customAttributes.Count() > 0)
                    {
                        var displayNameAttribute = (DisplayNameAttribute)customAttributes[0];
                        if (displayNameAttribute != null)
                            return displayNameAttribute.Name;
                    }
                }
            }
            catch
            {
            }
            return string.Empty;
        }
    }
}
