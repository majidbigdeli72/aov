﻿using Agent.Application.Interfaces;
using Agent.Application.Services;
using Agent.Domain.CommandHandlers;
using Agent.Domain.Commands;
using Agent.Domain.Core.Bus;
using Agent.Domain.Core.Events;
using Agent.Domain.Core.Notifications;
using Agent.Domain.EventHandlers;
using Agent.Domain.Events;
using Agent.Domain.Interfaces;
using Agent.Domain.Models.Identity;
using Agent.Infra.CrossCutting.Bus;
using Agent.Infra.CrossCutting.Identity.Authorization;
using Agent.Infra.CrossCutting.Identity.Services;
using Agent.Infra.Data.Context;
using Agent.Infra.Data.EventSourcing;
using Agent.Infra.Data.Repository;
using Agent.Infra.Data.Repository.EventSourcing;
using Agent.Infra.Data.UoW;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Agent.Infra.CrossCutting.IoC
{
    public class NativeInjectorBootStrapper
    {
        public static void RegisterServices(IServiceCollection services)
        {
            // ASP.NET HttpContext dependency
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Domain Bus (Mediator)
            services.AddScoped<IMediatorHandler, InMemoryBus>();

            // ASP.NET Authorization Polices
            services.AddSingleton<IAuthorizationHandler, ClaimsRequirementHandler>();

            // Application
            services.AddScoped<ICustomerAppService, CustomerAppService>();
            services.AddScoped<IAloVoIPInfoService, AloVoIPInfoService>();
           // services.AddScoped<IAloVoipTelephonyService, AloVoipTelephonyService>();
            //services.AddScoped<ISettingService, SettingService>();

            // Domain - Events
            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
            services.AddScoped<INotificationHandler<CustomerRegisteredEvent>, CustomerEventHandler>();
            services.AddScoped<INotificationHandler<CustomerUpdatedEvent>, CustomerEventHandler>();
            services.AddScoped<INotificationHandler<CustomerRemovedEvent>, CustomerEventHandler>();

            // Domain - Commands
            services.AddScoped<IRequestHandler<RegisterNewCustomerCommand, bool>, CustomerCommandHandler>();
            services.AddScoped<IRequestHandler<UpdateCustomerCommand, bool>, CustomerCommandHandler>();
            services.AddScoped<IRequestHandler<RemoveCustomerCommand, bool>, CustomerCommandHandler>();

            // Infra - Data
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddDbContext<AgentContext>();

            // Infra - Data EventSourcing
            services.AddScoped<IEventStoreRepository, EventStoreSQLRepository>();
            services.AddScoped<IEventStore, SqlEventStore>();
            services.AddDbContext<EventStoreSQLContext>();

            // Infra - Identity Services
            services.AddTransient<IEmailSender, AuthEmailMessageSender>();
            services.AddTransient<ISmsSender, AuthSMSMessageSender>();

            // Infra - Identity
            services.AddScoped<IUser, AspNetUser>();
        }
    }
}