﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.AloVoip.Agent.Gateway
{
    public interface IGatewayRequest
    {
        string CallerId { get; }
        string UniqueId { get; }

        string DestinationId { get; }
        string Extension { get; }
        string Context { get; }
        string Channel { get; }
        string Script { get; }
    }
}
