﻿using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.PgIdentityService;
using Serilog;
using System;

namespace Septa.AloVoip.Agent.Gateway
{
    public class GatewayCustomerHelper : IGatewayCustomerHelper
    {
        private readonly IGateway _gateway;

        private const string CUSTOMERID = "CustomerId";
        private const string CUSTOMERNUMBER = "CustomerNumber";

        public GatewayCustomerHelper(IGateway gateway)
        {
            if (gateway == null)
                throw new ArgumentNullException(nameof(gateway));

            _gateway = gateway;
        }

        public void SetCustomer(IdentityInfo customer)
        {
            Log.Debug($"SetCustomer for gateway. CustomerNumber:{customer.CustomerNumber},CustomerId:{customer.CrmId?.ToString() ?? ""}");

            _gateway.SetVar(CUSTOMERNUMBER, customer.CustomerNumber);
            _gateway.SetVar(CUSTOMERID, customer.CrmId?.ToString() ?? "");
        }

        public CustomerInfo GetCustomerInfo()
        {
            return new CustomerInfo
            {
                CustomerId = _gateway.GetVar(CUSTOMERID),
                CustomerNo = _gateway.GetVar(CUSTOMERNUMBER)
            };
        }

        public bool IsCustomerPresent()
        {
            return !string.IsNullOrEmpty(_gateway.GetVar(CUSTOMERID)) ||
                   !string.IsNullOrEmpty(_gateway.GetVar(CUSTOMERNUMBER));
        }

    }
}