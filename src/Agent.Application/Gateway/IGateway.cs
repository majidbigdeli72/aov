﻿using Agent.Domain.Enum;

namespace Septa.AloVoip.Agent.Gateway
{
    public interface IGateway
    {
        IGatewayCustomerHelper CustomerHelper { get; }
        void GoTo(string destination);
        void GoToExtension(string destination);
        void StreamVoice(string filename);
        void SetVar(string name, string value);
        string GetVar(string name);
        void PlayHoldMusic();
        string GetInput(string file, int timeout, int? maxDigits = null);
        void PlayNumber(string number);
        void PlayDigits(string digits);
        int Transfer(string destination);
        int VoiceMail(string destination);
        void Hangup();
        ChannelStatus GetChannelStatus();
        int Execute(string application, string options);
    }
}
