﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.AloVoip.Agent.Gateway
{
    public interface IGatewayReply
    {
        int ResultCode { get; }
    }
}
