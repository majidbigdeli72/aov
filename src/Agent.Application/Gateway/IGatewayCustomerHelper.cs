﻿using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.PgIdentityService;

namespace Septa.AloVoip.Agent.Gateway
{
    public interface IGatewayCustomerHelper
    {
        void SetCustomer(IdentityInfo customer);
        bool IsCustomerPresent();
        CustomerInfo GetCustomerInfo();
    }
}