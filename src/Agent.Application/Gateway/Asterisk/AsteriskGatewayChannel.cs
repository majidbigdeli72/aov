﻿using AsterNET.NetStandard.FastAGI;

namespace Septa.AloVoip.Agent.Gateway.Asterisk
{
    public class AsteriskGatewayChannel : IGatewayChannel
    {
        AGIChannel _channel;
        public AsteriskGatewayChannel(AGIChannel channel)
        {
            _channel = channel;
        }
    }
}
