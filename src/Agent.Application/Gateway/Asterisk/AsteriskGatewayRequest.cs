﻿using AsterNET.NetStandard.FastAGI;

namespace Septa.AloVoip.Agent.Gateway.Asterisk
{
    public class AsteriskGatewayRequest : IGatewayRequest
    {
        AGIRequest _request;
        public AsteriskGatewayRequest(AGIRequest request)
        {
            _request = request;
        }

        public string CallerId
        {
            get
            {
                return _request.CallerId;
            }
        }

        public string UniqueId
        {
            get
            {
                return _request.UniqueId;
            }
        }
        public string DestinationId
        {
            get
            {
                return _request.Dnid;
            }
        }
        public string Extension
        {
            get
            {
                return _request.Extension;
            }
        }
        public string Context
        {
            get
            {
                return _request.Context;
            }
        }
        public string Channel
        {
            get
            {
                return _request.Channel;
            }
        }

        public string Script
        {
            get
            {
                return _request.Script;
            }
        }
    }
}