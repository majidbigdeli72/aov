﻿using Agent.Application.Models;
using Agent.Domain.Classes;
using Agent.Domain.Enum;
using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using AsterNET.NetStandard.FastAGI;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.Gateway.Asterisk;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.Module;
using Septa.AloVoip.Agent.Module.AuthenticatorHelper;
using Septa.AloVoip.Agent.Module.PaymentHelper;
using Serilog;
using Serilog.Context;
using System;
using System.Configuration;
using System.Linq;

namespace Agent.Domain.Gateway.Asterisk
{
    public class AsteriskGateway : AGIScript, IGateway
    {
        private const int DefaultDataDefaultTimeout = 20000;
        protected const int DefaultRetryCount = 3;
        public static int BindPort
        {
            get
            {
                var val = ConfigurationManager.AppSettings["asterisk:agiserver.bindport"];
                if (!string.IsNullOrEmpty(val))
                    return Convert.ToInt32(val);
                return 4574;
            }
        }
        public const string VOICE_FILES_PATH = "/var/lib/asterisk/sounds/custom/";

        public IGatewayCustomerHelper CustomerHelper
        {
            get { return new GatewayCustomerHelper(this); }
        }

        public override void Service(AGIRequest request, AGIChannel channel)
        {
            var gatewayRequest = new AsteriskGatewayRequest(request);
            var gatewayChannel = new AsteriskGatewayChannel(channel);

            using (LogContext.PushProperty("RequestUniqueId", request.UniqueId))
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                Log.Debug("AsteriskGateway Service. request: {@request}, channel: {@channel}", request, channel);

                Answer();

                try
                {
                    if (request.Script.StartsWith(Constants.Modules.SendFax.AgiPrefix))
                    {

                    }
                    else if (request.Script.StartsWith(Constants.Modules.TeleMarketingPlayVoice.AgiPrefix))
                    {
                        ProcessTeleMarketingModule(gatewayRequest, gatewayChannel, context);
                    }
                    else if (request.Script.StartsWith(Constants.Modules.OperatorSos.AgiPrefix))
                    {
                        ProcessOperatorSosModule(gatewayRequest, gatewayChannel, context);
                    }
                    else
                        ProcessModule(gatewayRequest, gatewayChannel, context);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "AsteriskGateway service unhandled error");
                    this.Hangup();
                }

            }
        }
        private void ProcessTeleMarketingModule(IGatewayRequest gatewayRequest, IGatewayChannel gatewayChannel, IAloVoipContext context)
        {
            var tsId = Convert.ToInt32(gatewayRequest.Script.Split('-')[1]);
            var telephonyServer = AloVoipAgent.Booter.TelephonyServers[tsId];
            var m = new Septa.AloVoip.Agent.Module.Embeded.TeleMarketing(context, this, gatewayRequest, gatewayChannel, telephonyServer);
            m.Process();
        }

        private void ProcessOperatorSosModule(IGatewayRequest gatewayRequest, IGatewayChannel gatewayChannel, IAloVoipContext context)
        {
            var tsId = Convert.ToInt32(gatewayRequest.Script.Split('-')[1]);
            var telephonyServer = AloVoipAgent.Booter.TelephonyServers[tsId];
            var m = new Septa.AloVoip.Agent.Module.Embeded.OperatorSos(context, this, gatewayRequest, gatewayChannel, telephonyServer);
            m.Process();
        }

        private void ProcessModule(IGatewayRequest gatewayRequest, IGatewayChannel gatewayChannel, IAloVoipContext context)
        {
            int moduleId;
            string moduleParams = string.Empty;
            if (gatewayRequest.Script.Contains("-"))
            {
                var t = gatewayRequest.Script.Split('-');
                moduleId = Convert.ToInt32(t[0]);
                moduleParams = t[1];
            }
            else
                moduleId = Convert.ToInt32(gatewayRequest.Script);

            ILookupSourceService loopupSourceService = null;
            IAuthenticatorFactory authenticatorFactory = null;
            IPaymentFactory paymentFactory = null;

            var module = context.Modules.FirstOrDefault(p => p.Id == moduleId);
            if (module.LookupSourceId.HasValue)
            {
                loopupSourceService = new LookupSourceServiceFactory(context).Create(module.LookupSourceId);
                authenticatorFactory = new AuthenticatorFactory(loopupSourceService, gatewayRequest, this);
                paymentFactory = new PaymentFactory(loopupSourceService, this);
            }

            var telephonyServer = AloVoipAgent.Booter.TelephonyServers[module.TsId];
            var moduleManager = CreateModuleManager(module,
                                                    moduleParams,
                                                    context,
                                                    this,
                                                    gatewayRequest,
                                                    gatewayChannel,
                                                    loopupSourceService,
                                                    authenticatorFactory,
                                                    paymentFactory,
                                                    telephonyServer);
            moduleManager.Process();
        }

        private IModuleManager CreateModuleManager(Models.Module module, string moduleParams, IAloVoipContext dbContext, IGateway gateway, IGatewayRequest request, IGatewayChannel channel, ILookupSourceService lookupSourceService, IAuthenticatorFactory authenticationFactory, IPaymentFactory paymentFactory, IAloVoipTelephonyServer telephonyServer)
        {
            switch (module.ModuleType)
            {
                //case ModuleType.QueueOperatorAnnouncement:
                //    break;
                case ModuleType.QueueOperatorVoting:
                    return new QueueOperatorVoting(module.Id, moduleParams, dbContext, gateway, request, channel, lookupSourceService, telephonyServer);
                case ModuleType.SmartReconnect:
                    return new Reconnect(module.Id, dbContext, gateway, request, channel, lookupSourceService, telephonyServer);
                case ModuleType.SendMessage:
                    break;
                case ModuleType.HelpRequest:
                    break;
                case ModuleType.NumberMatch:
                    break;
                case ModuleType.CustomerClub:
                    return new CustomerClub(module.Id, dbContext, gateway, request, channel, lookupSourceService, telephonyServer, authenticationFactory);
                case ModuleType.CheckContract:
                    return new CheckContract(module.Id, dbContext, gateway, request, channel, lookupSourceService, telephonyServer);
                case ModuleType.Pay:
                    return new Pay(module.Id, dbContext, gateway, request, channel, lookupSourceService, telephonyServer, paymentFactory);
                case ModuleType.RoleConnect:
                    return new RoleConnect(module.Id, dbContext, gateway, request, channel, lookupSourceService, telephonyServer);
                case ModuleType.CardtableConnect:
                    return new CardtableConnect(module.Id, dbContext, gateway, request, channel, lookupSourceService, telephonyServer);
                case ModuleType.SayProcessState:
                    break;
                case ModuleType.CallbackRequest:
                    break;
                case ModuleType.IdentityInfoDecision:
                    break;
                case ModuleType.AdvanceFollowMe:
                    return new AdvanceFollowMe(module.Id, dbContext, gateway, request, channel, lookupSourceService, telephonyServer);
                case ModuleType.Voting:
                    return new Voting(module.Id, dbContext, gateway, request, channel, lookupSourceService, telephonyServer);
                default:
                    break;
            }
            return null;
        }

        public void GoTo(string destination)
        {
            Log.Debug($"exec  goTo {destination}");

            this.Exec("Goto", destination);
        }

        public void GoToExtension(string destination)
        {
            this.Exec("Goto", $"from-did-direct,{destination},1");
        }

        public void StreamVoice(string filename)
        {
            this.StreamFile($"{VOICE_FILES_PATH}{filename}");
        }

        public void SetVar(string name, string value)
        {
            this.SetVariable(name, value);
        }

        public string GetVar(string name)
        {
            return this.GetVariable(name);
        }

        public void PlayHoldMusic()
        {
            this.PlayMusicOnHold();
        }

        public string GetInput(string file, int timeout, int? maxDigits = null)
        {
            if (maxDigits.HasValue)
                return GetData($"{VOICE_FILES_PATH}{file}", timeout, maxDigits.Value);

            return GetData($"{VOICE_FILES_PATH}{file}", timeout);
        }

        public void PlayNumber(string number)
        {
            this.SayNumber(number);
        }

        public void PlayDigits(string digits)
        {
            this.SayDigits(digits);
        }

        public int Transfer(string destination)
        {
            return this.Exec("Transfer", destination);
        }
        public int VoiceMail(string destination)
        {
            return this.Exec("Voicemail", destination);
        }
        public int Execute(string application, string options)
        {
            return this.Exec(application, options);
        }

        void IGateway.Hangup()
        {
            this.Hangup();
        }
        ChannelStatus IGateway.GetChannelStatus()
        {
            return (ChannelStatus)this.GetChannelStatus();
        }
    }
}