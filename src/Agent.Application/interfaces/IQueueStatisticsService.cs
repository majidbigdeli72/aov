﻿using Agent.Application.Models;
using System;

namespace Agent.Application.Interfaces
{
    public interface IQueueStatisticsService : IDisposable
    {
        QueueStatisticsModel GetQueueStatistics(QueueStatisticsFilterModel filterModel);
    }
}
