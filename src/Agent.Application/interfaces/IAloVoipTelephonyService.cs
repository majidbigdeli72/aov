﻿using Agent.Application.ViewModels;
using System.Collections.Generic;

namespace Agent.Application.Interfaces
{
    public interface IAloVoipTelephonyService : ITelephonyService
    {
        TelephonyServerInfoResult GetTelephonyInfoById(int tsId);
        void InitModule(int moduleId);
        void DestroyModule(int deletedModuleId);
        void InitOperatorSos(int tsId);
        Dictionary<string, string> GetBillableObjectTypes(int lookupSourceId);
        Dictionary<string, string> GetBillableObjectTypeProps(int lookupSourceId, string billableObjectTypeKey);
        Dictionary<string, string> GetMoneyAccounts(int lookupSourceId);
        void RegisterVoiceOnTelephonyServer(string voiceName, byte[] voice);
        void UnRegisterVoiceFromTelephonyServer(string voiceName);
        GeneralResult CheckConnection(int telephonySystemTypeIndex,
                                      string serverAddress,
                                      int serverPort,
                                      string amiUserName,
                                      string amiPassword,
                                      string linuxUserName,
                                      string linuxPassword,
                                      string mysqlUserName,
                                      string mysqlPassword);
    }
}
