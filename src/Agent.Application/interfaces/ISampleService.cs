﻿using Agent.Application.Models;

namespace Agent.Application.Interfaces
{
    public interface ISampleService
    {
        Call GetFirstCallInDb();
    }
}
