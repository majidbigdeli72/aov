﻿using Agent.Application.ViewModels;
using System.Collections.Generic;
using System.ServiceModel;

namespace Agent.Application.Interfaces
{
    public interface ITelephonyService
    {
        TelephonyServerInfoResult GetTelephonyInfoByKey(string tsKey);
        byte[] GetRecordedFile(string tsKey, string fileName);
        bool Dial(string tsKey, string extn, string dialNo, bool applyOutgoingRules);
        bool ChannelSpy(string tsKey, string extn, string spiedExtn, int spyMode);
        bool CanDial(string tsKey);
        bool ChannelHangup(string tsKey, string extn);
        void ReBoot();
        Dictionary<string, string> GetIndicators();
    }

}
