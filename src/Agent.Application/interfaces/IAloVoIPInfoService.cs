﻿using Agent.Application.ViewModels;
using System.Text;

namespace Agent.Application.Interfaces
{
    public interface IAloVoIPInfoService
    {
        InfoViewModel IsFeatureAvailable(byte[] nm);
        public string GetSerial();
        public bool IsTrial();
    }
}
