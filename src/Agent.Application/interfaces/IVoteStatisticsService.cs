﻿using Agent.Application.Models;
using System;

namespace Agent.Application.Interfaces
{
    public interface IVoteStatisticsService : IDisposable
    {
        VoteStatisticsModel GetVoteStatistics(VoteStatisticsFilterModel filterModel);
    }
}
