﻿using Agent.Application.Models;
using Agent.Domain.Helper;
using Agent.Domain.Manager;
using Serilog;
using System;
using System.Text;
using System.Timers;

namespace Agent.Domain.Classes
{
    public partial class AloVoipAgent
    {


        private readonly Timer _initializerTimer = new Timer(300 /*interval in milliseconds*/);
        private static SystemManager _sm = null;

        public static SystemManager Booter
        {
            get { return _sm; }
        }

        public AloVoipAgent()
        {
            _initializerTimer.Elapsed += _initializerTimer_Elapsed;
        }

        private void _initializerTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            _initializerTimer.Stop();

            Init();

            _initializerTimer.Elapsed -= _initializerTimer_Elapsed;
            _initializerTimer.Dispose();

        }

        public void Init()
        {
            Log.Debug("AloVoipAgent init started.");

            //Job

            using (ApplicationConfigurator.ServiceProvider.Scoped<SystemManager>(out var _sm))
            {
                _sm.Boot(false);

            }
        }

        //Todo : Job
        //void CallBackJobInit()
        //{
        //    _callBackScheduler = ApplicationConfigurator.Container.Resolve<IScheduler>();
        //    _callBackScheduler.JobFactory = new AutofacJobFactory(ApplicationConfigurator.Container);
        //    IJobDetail job = JobBuilder.Create<CallbackJob>()
        //        .WithIdentity("CallbackJob", "CallbackGroup")
        //        .Build();

        //    ITrigger trigger = TriggerBuilder.Create()
        //        .WithIdentity("CallbackTrigger", "CallbackGroup")
        //        .WithSimpleSchedule(x => x
        //            .WithIntervalInMinutes(1)
        //            .RepeatForever()
        //            .WithMisfireHandlingInstructionIgnoreMisfires())
        //        .Build();

        //    var cts = new System.Threading.CancellationTokenSource();
        //    _callBackScheduler.ScheduleJob(job, trigger).GetAwaiter().GetResult();
        //    _callBackScheduler.Start().GetAwaiter().GetResult();
        //}

        //void TeleMarketingJobJobInit()
        //{
        //    _teleMarkteingJob = ApplicationConfigurator.Container.Resolve<IScheduler>();
        //    _teleMarkteingJob.JobFactory = new AutofacJobFactory(ApplicationConfigurator.Container);
        //    IJobDetail job = JobBuilder.Create<TeleMarketingJob>()
        //        .WithIdentity("TeleMarketingJob", "TeleMarketingGroup")
        //        .Build();

        //    ITrigger trigger = TriggerBuilder.Create()
        //        .WithIdentity("TeleMarketingTrigger", "TeleMarketingGroup")
        //        .WithSimpleSchedule(x => x
        //            .WithIntervalInMinutes(1)
        //            .RepeatForever()
        //            .WithMisfireHandlingInstructionIgnoreMisfires())
        //        .Build();

        //    var cts = new System.Threading.CancellationTokenSource();
        //    _teleMarkteingJob.ScheduleJob(job, trigger).GetAwaiter().GetResult();
        //    _teleMarkteingJob.Start().GetAwaiter().GetResult();
        //}
    }


}
