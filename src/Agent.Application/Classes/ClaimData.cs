﻿using Agent.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agent.Domain.Classes
{
    public static class ClaimData
    {
        public static Dictionary<UserRole, List<AloVoIPClaims>> Claims
        {
            get
            {
                return new Dictionary<UserRole, List<AloVoIPClaims>>
                {
                    {
                        UserRole.Admin,
                        new List<AloVoIPClaims>
                        {
                            AloVoIPClaims.Manage_TelephonySystems,
                            AloVoIPClaims.Manage_LookupSources,
                            AloVoIPClaims.Manage_Queues,
                            AloVoIPClaims.Manage_Operators,
                            AloVoIPClaims.Manage_Extensions,
                            AloVoIPClaims.Manage_Departments,
                        }
                    },
                    {
                        UserRole.VoipManager,
                        new List<AloVoIPClaims>
                        {
                            AloVoIPClaims.Show_Dashboard_Queue,
                            AloVoIPClaims.Show_Dashboard_CallCenter,
                            AloVoIPClaims.Show_Dashboard_CallbackRequest,
                            AloVoIPClaims.Show_Dashboard_CallReport,
                            AloVoIPClaims.Show_Dashboard_AgentReport,
                            AloVoIPClaims.Manage_Modules,
                            AloVoIPClaims.Create_Call,
                            AloVoIPClaims.Manage_ServerSettings
                        }
                    },
                    {
                        UserRole.Operator,
                        new List<AloVoIPClaims>
                        {
                            AloVoIPClaims.Account_ChangePassword,
                            AloVoIPClaims.Show_Dashboard_CallbackRequest
                        }
                    }
                };
            }
        }
    }

}
