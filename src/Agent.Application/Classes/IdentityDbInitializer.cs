﻿using Agent.Domain.Interfaces;
using Agent.Domain.Models.Identity;
using Agent.Infra.Data.Context;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;

namespace Agent.Domain.Classes
{
    public class IdentityDbInitializer : IIdentityDbInitializer
    {
        private readonly IServiceScopeFactory _scopeFactory;

        public IdentityDbInitializer(
            IServiceScopeFactory scopeFactory
            )
        {
            _scopeFactory = scopeFactory;
        }

        /// <summary>
        /// Applies any pending migrations for the context to the database.
        /// Will create the database if it does not already exist.
        /// </summary>
        public void Initialize()
        {
            using (var serviceScope = _scopeFactory.CreateScope())
            {
                using (var context = serviceScope.ServiceProvider.GetService<AloVoipContext>())
                {
                    // context.Database.EnsureCreated();
                    context.Database.Migrate();

                }
            }
        }

        /// <summary>
        /// Adds some default values to the IdentityDb
        /// </summary>
        public void SeedData()
        {
            using (var serviceScope = _scopeFactory.CreateScope())
            {
                var identityDbSeedData = serviceScope.ServiceProvider.GetService<IIdentityDbInitializer>();

                // How to add initial data to the DB directly
                using (var context = serviceScope.ServiceProvider.GetService<AloVoipContext>())
                {
                    var adminRole = new ApplicationRole() { Name = "Admin" };
                    var operatorRole = new ApplicationRole() { Name = "Operator" };
                    var voipManagerRole = new ApplicationRole() { Name = "VoipManager" };
                    var adminUser = new ApplicationUser
                    {
                        AccessFailedCount = 4,
                        EmailConfirmed = true,
                        PhoneNumber = "1234567890",
                        PhoneNumberConfirmed = true,
                        SecurityStamp = "@#$^&*",
                        TwoFactorEnabled = false,
                        UserName = "admin"
                    };

                    if (!context.Roles.Any())
                    {
                        context.Roles.Add(adminRole);
                        context.Roles.Add(operatorRole);
                        context.Roles.Add(voipManagerRole);
                        context.SaveChanges();
                    }

                    if (!context.Users.Any())
                    {
                        context.Users.Add(adminUser);
                        context.SaveChanges();
                        adminUser.PasswordHash = new PasswordHasher<ApplicationUser>().HashPassword(adminUser, "aA123456");

                        context.Users.Update(adminUser);

                        context.UserRoles.Add(new UserRole
                        {
                            UserId = adminUser.Id,
                            RoleId = adminRole.Id
                        });
                        //context.Users.Update(adminUser);

                        context.UserRoles.Add(new UserRole
                        {
                            UserId = adminUser.Id,
                            RoleId = operatorRole.Id
                        });
                        context.UserRoles.Add(new UserRole
                        {
                            UserId = adminUser.Id,
                            RoleId = voipManagerRole.Id
                        });

                    }





#if Debug


                    var testTelephony = new TelephonySystem
                    {
                        Name = "پیش فرض",
                        ServerAddress = "192.168.2.93",
                        AmiUsername = "admin",
                        AmiPassword = "aA1234567@",
                        LinuxUsername = "admin",
                        LinuxPassword = "aA1234567@",
                        Key = "ts1",
                        ServerPort = 5038,
                        BrevityName = "default",
                        UpdateSettingsIfNotExists = false,
                        TelephonySystemTypeIndex = 1,
                        MySqlPassword = "alovoip",
                        MySqlUsername = "aA1234567@"
                    };

                    var testTelephony2 = new TelephonySystem
                    {
                        Name = "TS2",
                        AmiUsername = "admin",
                        AmiPassword = "123456",
                        Key = "ts2",
                        LinuxUsername = "admin",
                        LinuxPassword = "123456",
                        ServerAddress = "192.168.1.1",
                        ServerPort = 5038,
                        BrevityName = "ts2",
                        UpdateSettingsIfNotExists = false,
                        TelephonySystemTypeIndex = 1,
                        MySqlPassword = "alovoip",
                        MySqlUsername = "aA1234567@"
                    };

                    if (context.TelephonySystems.Any())
                    {
                        context.TelephonySystems.Add(testTelephony);
                        context.TelephonySystems.Add(testTelephony);
                    }

                    var queue1 = new Queue
                    {
                        Name = "queue1",
                        Code = "1234",
                        Enabled = false,
                        JoinPlayAlert = false,
                        LeavePlayAlert = false,
                        RemoveFromAbandonedAfterCallback = false,
                        TelephonySystem = testTelephony,
                        WaitTimeCriticalThreshold = new TimeSpan(0, 5, 0),
                        WaitTimeCriticalThresholdPlayAlert = false,
                        WaitTimeMaxThreshold = TimeSpan.FromMinutes(1),
                        WaitTimeMinThreshold = TimeSpan.FromMinutes(1),
                        WaitTimeMaxThresholdPlayAlert = false
                    };

                    var queue2 = new Queue
                    {
                        Name = "queue2",
                        Code = "9876",
                        Enabled = false,
                        JoinPlayAlert = true,
                        LeavePlayAlert = true,
                        RemoveFromAbandonedAfterCallback = false,
                        TelephonySystem = testTelephony,
                        WaitTimeCriticalThreshold = new TimeSpan(0, 5, 0),
                        WaitTimeCriticalThresholdPlayAlert = false,
                        WaitTimeMaxThreshold = TimeSpan.FromMinutes(2),
                        WaitTimeMinThreshold = TimeSpan.FromMinutes(2),
                        WaitTimeMaxThresholdPlayAlert = false
                    };

                    if (context.Queues.Any())
                    {
                        context.Queues.Add(queue1);
                        context.Queues.Add(queue2);
                    }

                    var testOperator = new Operator
                    {
                        FirstName = "کاربر",
                        LastName = "اپراتور",
                        NameVoice = new ASCIIEncoding().GetBytes("MyBytes"),
                    };

                    if (context.Operators.Any())
                    {
                        context.Operators.Add(testOperator);
                    }

                    var peer1 = new Peer
                    {
                        TelephonySystem = testTelephony,
                        Operator = testOperator,
                        Code = "1234",
                        Enabled = false,
                        PeerProtocolTypeIndex = 1,
                        PeerTypeIndex = 1
                    };

                    var peer2 = new Peer
                    {
                        TelephonySystem = testTelephony,
                        Operator = testOperator,
                        Code = "9832",
                        Enabled = false,
                        PeerProtocolTypeIndex = 1,
                        PeerTypeIndex = 1
                    };

                    if (context.Peers.Any())
                    {
                        context.Peers.Add(peer1);
                        context.Peers.Add(peer2);
                    }

                    var pgLookup1 = new PayamGostarLookupSource
                    {
                        Host = "http://192.168.1.1",
                        Name = "پیش فرض",
                        LookupSourceTypeIndex = 1,
                        Username = "admin",
                        Password = "123456"
                    };

                    var pgLookup2 = new PayamGostarLookupSource
                    {
                        Host = "http://192.168.1.1",
                        Name = "pg2",
                        LookupSourceTypeIndex = 1,
                        Username = "admin",
                        Password = "1234568"
                    };

                    if (context.PayamGostarLookupSources.Any())
                    {
                        context.PayamGostarLookupSources.Add(pgLookup1);
                        context.PayamGostarLookupSources.Add(pgLookup2);
                    }
#endif
                    context.SaveChanges();


                }

            }
        }

    }


}
