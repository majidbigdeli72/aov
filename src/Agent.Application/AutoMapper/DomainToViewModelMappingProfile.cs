﻿using AutoMapper;
using Agent.Application.ViewModels;
using Agent.Domain.Models;

namespace Agent.Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Customer, CustomerViewModel>();
        }
    }
}
