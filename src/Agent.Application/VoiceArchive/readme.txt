﻿Sound files must create by these configs:

Extension: .wav
Bit resolution: 16 Bit
Sampling rate: 8000 Hz
Audio channels: mono
Online Tools: https://audio.online-convert.com/convert-to-wav