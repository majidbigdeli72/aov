﻿using Agent.Application.Interfaces;
using Agent.Application.Models;
using Agent.Domain.Enum;
using Agent.Domain.Gateway.Asterisk;
using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using Agent.Domain.Models;
using Agent.Infra.Data.Context;
using AsterNET.NetStandard.FastAGI;
using AsterNET.NetStandard.FastAGI.MappingStrategies;
using Microsoft.Extensions.DependencyInjection;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.TelephonyServer;
using Septa.TelephonyServer;
using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Agent.Domain.Manager
{
    public partial class SystemManager
    {
        //private IAloVoipContext _context;
        private ISettingService _settingService;
        private Dictionary<int, IAloVoipTelephonyServer> _telephonyServers = null;
        private AsteriskFastAGI _agiServer = new AsteriskFastAGI();
        private List<ScriptMapping> _scripts = null;
        private Task _currentAgiServerTask = null;
        private string _scriptClass = "Septa.AloVoip.Agent.Gateway.Asterisk.AsteriskGateway";
        private bool _hasBeenBoot;
        private CallStoreServiceManager _callStoreServiceManager;
        private ConcurrentDictionary<string, IList<ICallStoreService>> _telephonySystemCallStores;

        public SystemManager(ISettingService settingService)
        {
            
            //this._context = context;
            this._settingService = settingService;

            _agiServer.BindPort = AsteriskGateway.BindPort;
            _callStoreServiceManager = new CallStoreServiceManager(GetLookupSourcesForTelephonySystem);
            _callStoreServiceManager.NewProfileInfoFound += _callStoreServiceManager_NewProfileInfoFound;

            _telephonySystemCallStores = new ConcurrentDictionary<string, IList<ICallStoreService>>();
        }


        public Dictionary<int, IAloVoipTelephonyServer> TelephonyServers
        {
            get { return _telephonyServers; }
        }

        public CallStoreServiceManager CallStoreServiceManager => _callStoreServiceManager;

        public bool HasBeenBoot
        {
            get { return _hasBeenBoot; }
            set { _hasBeenBoot = value; }
        }
        public void Stop()
        {
            if (_telephonyServers != null)
            {
                foreach (var ts in _telephonyServers.Values)
                {
                    ts.Stop();

                    BaseTelephonyServer server = ts as BaseTelephonyServer;
                    server.CallCreated -= _callStoreServiceManager.OnCallCreated;
                    server.CallUpdated -= _callStoreServiceManager.OnCallUpdated;
                    server.CallChannelCreated -= _callStoreServiceManager.OnCallChannelCreated;
                    server.CallChannelUpdated -= _callStoreServiceManager.OnCallChannelUpdated;
                    server.CallsMerged -= _callStoreServiceManager.OnCallMerged;
                }

                _callStoreServiceManager.Stop();

                try
                {
                    if (_currentAgiServerTask != null)
                    {
                        Task.Run(() => _agiServer.Stop());
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error in stoping agi server!!!");
                    throw;
                }
            }
        }

        public void Boot(bool reboot)
        {
            try
            {
                HasBeenBoot = false;
                using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
                {

                    //if (!reboot)
                    //{
                    //    UpdateDatabase(context);
                    //}

                    Log.Debug("SystemManager boot started.");

                    if (_telephonyServers != null)
                        foreach (var ts in _telephonyServers.Values)
                            try
                            {
                                ts.Stop();
                            }
                            catch { }

                    _telephonyServers = new Dictionary<int, IAloVoipTelephonyServer>();

                    Log.Debug(String.Format("_context.TelephonySystems count: {0}", context.TelephonySystems == null ? 0 : context.TelephonySystems.Count()));

                    foreach (var ts in context.TelephonySystems.ToList())
                        try
                        {
                            RegisterTelephonyServer(ts);
                        }
                        catch (TelephonyServerConnectionException)
                        {
                        }
                        catch (Exception)
                        {
                            throw;
                        }

                    RegisterTelephonyServerModules(context, true);


                    _callStoreServiceManager.Start();

                    Log.Debug("SystemManager boot finished.");

                    HasBeenBoot = true;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in Boot");
                throw;
            }
        }


        public void RegisterTelephonyServerModules(IAloVoipContext context, bool reinitModules)
        {
            Log.Debug($"Registering server modules started");
            _scripts = new List<ScriptMapping>();
            foreach (var ts in context.TelephonySystems.ToList())
            {
                try
                {
                    var server = _telephonyServers[ts.Id];
                    RegisterTelephonyServerModules(ts, server, context, reinitModules);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error in Register Modules:{tsKey}", ts.Key);
                }
            }

            Log.Debug($"_scripts Count: {_scripts.Count}");
            if (_scripts.Count > 0)
            {

                try
                {
                    if (_currentAgiServerTask != null)
                    {
                        Task.Run(() => _agiServer.Stop());
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error in stoping agi server!!!");
                    throw;
                }

                try
                {
                    if (_currentAgiServerTask != null)
                    {
                        while (_currentAgiServerTask.Status != TaskStatus.RanToCompletion)
                            Thread.Sleep(1000);

                        _currentAgiServerTask.Dispose();
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error in disposing CurrentAgiServerTask:");
                    throw;
                }

                _agiServer.MappingStrategy = new GeneralMappingStrategy(_scripts);
                Log.Debug("GeneralMappingStrategy set.");

                _currentAgiServerTask = Task.Run(() =>
                {
                    Log.Information($"Starting AGI server on port {AsteriskGateway.BindPort}");
                    try
                    {
                        _agiServer.Start();
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Start AGI Error:");
                    }
                });
            }
        }

        private void RegisterTelephonyServerModules(TelephonySystem ts, IAloVoipTelephonyServer server, IAloVoipContext context, bool reinitModules)
        {
            Log.Debug($"Registering server modules Name: {ts.Name}, Type:{server.Type}");
            switch (server.Type)
            {
                case nameof(TelephonySystemType.Astrisk):
                    RegisterAsteriskModuleServer(ts, server, context, reinitModules);
                    break;
                default:
                    break;
            }
        }

        public void RegisterTelephonyServer(TelephonySystem ts)
        {
            Log.Debug($"Creating TelephonyServer started. Name: {ts.Name}");
            BaseTelephonyServer server = TelephonyServerFactory.Create(ts) as BaseTelephonyServer;
            server.CallCreated += _callStoreServiceManager.OnCallCreated;
            server.CallUpdated += _callStoreServiceManager.OnCallUpdated;
            server.CallChannelCreated += _callStoreServiceManager.OnCallChannelCreated;
            server.CallChannelUpdated += _callStoreServiceManager.OnCallChannelUpdated;
            server.CallsMerged += _callStoreServiceManager.OnCallMerged;
            server.CheckConnection();
            server.Run();

            //Log.Debug($"TelephonyServer is running. Name: {ts.Name}, Type:{server.Type}");

            _telephonyServers[ts.Id] = server as IAloVoipTelephonyServer;
        }

        //private void UpdateDatabase(IAloVoipContext context)
        //{
        //    new AloVoipDBInitializer().ApplyMigrations(context as AloVoipContext);
        //}

        private void RegisterAsteriskModuleServer(TelephonySystem ts, IAloVoipTelephonyServer server, IAloVoipContext context, bool reinitModules)
        {
            Log.Debug(string.Format("Registering Asterisk modules started. Modules count: {0}", ts.Modules == null ? 0 : ts.Modules.Count));

            // Register Silent voice
            server.RegisterVoice("silent", Models.Module.GetSilentVoice(_settingService));

            var activeModules = ts.Modules.ToList().Where(x => x.Enabled);
            foreach (var module in activeModules)
            {
                if (reinitModules && module.ReInitOnStartup)
                {
                    if (module.ModuleType != ModuleType.QueueOperatorVoting)
                    {
                        module.Init(server, _settingService, false);

                        module.LastSyncDate = DateTime.Now;
                        context.SaveChanges();
                    }
                }

                RegisterModuleScript(module, context);
            }

            RegisterSendFax(ts, server);
            RegisterTeleMarketingPlayVoice(context, ts, server);
            RegisterOperatorSos(ts, server);

            if (reinitModules)
            {
                Log.Debug("Reloading Asterisk modules started.");
                server.ReloadModules();
            }
        }

        private void RegisterModuleScript(Domain.Models.Module module, IAloVoipContext context)
        {
            if (module.ModuleType == ModuleType.QueueOperatorVoting)
            {
                var moduleQueueOperatorVoting = context.ModuleQueueOperatorVotings.FirstOrDefault(x => x.Id == module.Id);
                if (moduleQueueOperatorVoting != null)
                {
                    if (moduleQueueOperatorVoting.OperatorAnnouncement)
                    {
                        _scripts.Add(new ScriptMapping()
                        {
                            ScriptName = string.Format("{0}-1", module.Id.ToString()),
                            ScriptClass = _scriptClass
                        });
                    }
                    if (moduleQueueOperatorVoting.Voting)
                    {
                        _scripts.Add(new ScriptMapping()
                        {
                            ScriptName = string.Format("{0}-2", module.Id.ToString()),
                            ScriptClass = _scriptClass
                        });
                    }
                }
            }
            else
            {
                _scripts.Add(new ScriptMapping()
                {
                    ScriptName = module.Id.ToString(),
                    ScriptClass = _scriptClass
                });
            }
        }

        private void RegisterSendFax(TelephonySystem ts, IAloVoipTelephonyServer server)
        {
            server.RegisterSendFaxModule(_settingService.GetSystemIp().ToString());

            _scripts.Add(new ScriptMapping()
            {
                ScriptName = string.Format("SendFax-{0}", ts.Id.ToString()),
                ScriptClass = _scriptClass
            });
        }

        private void RegisterOperatorSos(TelephonySystem ts, IAloVoipTelephonyServer server)
        {
            if (ts.OperatorSos != null)
                ts.OperatorSos.Init(server);

            var agiCode = Constants.Modules.OperatorSos.AgiCode;
            if (ts.OperatorSos != null && !string.IsNullOrEmpty(ts.OperatorSos.RequestCode))
                agiCode = ts.OperatorSos.RequestCode;

            server.RegisterOperatorSosModule(agiCode, _settingService.GetSystemIp().ToString());

            _scripts.Add(new ScriptMapping()
            {
                ScriptName = string.Format("OperatorSos-{0}", ts.Id.ToString()),
                ScriptClass = _scriptClass
            });
        }

        private void RegisterTeleMarketingPlayVoice(IAloVoipContext context, TelephonySystem ts, IAloVoipTelephonyServer server)
        {
            server.RegisterTeleMarketingPlayVoiceModule(_settingService.GetSystemIp().ToString());

            _scripts.Add(new ScriptMapping()
            {
                ScriptName = string.Format("TeleMarketingPlayVoice-{0}", ts.Id.ToString()),
                ScriptClass = _scriptClass
            });

        }


        private IList<ICallStoreService> GetLookupSourcesForTelephonySystem(string tsKey)
        {
            return _telephonySystemCallStores.GetOrAdd(tsKey, CreateLookupSourcesForTelephonySystem);
        }

        private IList<ICallStoreService> CreateLookupSourcesForTelephonySystem(string tsKey)
        {
            var tsId = _telephonyServers.FirstOrDefault(x => x.Value.Key == tsKey).Key;
            var result = new List<ICallStoreService>();

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var telephonySystemLookupSources = context.TelephonySystemLookupSources.Where(x => x.TelephonySystemId == tsId);
                foreach (var telephonySystemLookupSource in telephonySystemLookupSources)
                {
                    result.Add(new LookupSourceServiceFactory(context).Create(telephonySystemLookupSource.LookupSourceId));
                }
            }

            return result;
        }

        private void _callStoreServiceManager_NewProfileInfoFound(object sender, NewProfileInfoFoundEventArgs e)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var call = context.Calls.FirstOrDefault(x => x.Id == e.CallId);
                call.ProfileId = e.NewProfileId;
                call.ProfileName = e.NewProfileName;
                context.SaveChanges();
            }
        }
    }
}
