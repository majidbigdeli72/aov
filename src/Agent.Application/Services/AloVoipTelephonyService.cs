﻿using Agent.Application.Interfaces;
using Agent.Application.Models;
using Agent.Application.ViewModels;
using Agent.Domain.Classes;
using Agent.Domain.Enum;
using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.TelephonyServer.Asterisk;
using Septa.TelephonyServer;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Agent.Application.Services
{
    public class AloVoipTelephonyService : IAloVoipTelephonyService
    {


        public AloVoipTelephonyService()
        {
           
        }

        public TelephonyServerInfoResult GetTelephonyInfoById(int tsId)
        {
            try
            {
                var its = AloVoipAgent.Booter.TelephonyServers[tsId];
                return new TelephonyServerInfoResult(its);
            }
            catch (Exception ex)
            {
                return new TelephonyServerInfoResult(ex.Message);
            }
        }

        public TelephonyServerInfoResult GetTelephonyInfoByKey(string tsKey)
        {
            try
            {
                var its = GetTelephonyServerByKey(tsKey);
                //try
                //{
                //    DataContractSerializer dcs = new DataContractSerializer(typeof(List<CallEntry>));
                //    MemoryStream stream = new MemoryStream();
                //    dcs.WriteObject(stream, its.Calls);
                //}
                //catch (Exception ex)
                //{
                //    Console.WriteLine("An exception occured: " + ex.Message);
                //}

                if (its != null)
                    return new TelephonyServerInfoResult(its);
                return new TelephonyServerInfoResult("Telephony server did not found!!!");
            }
            catch (Exception ex)
            {
                return new TelephonyServerInfoResult(ex.Message);
            }
        }

        public byte[] GetRecordedFile(string tsKey, string fileName)
        {
            var its = GetTelephonyServerByKey(tsKey);
            if (its != null)
                return its.GetRecordingFile(fileName);
            return null;
        }

        public bool Dial(string tsKey, string extn, string dialNo, bool applyOutgoingRules)
        {
            var its = GetTelephonyServerByKey(tsKey);
            if (its != null)
            {
                string cid = "D:" + dialNo;
                var res = its.OriginateToExtension(extn, cid, dialNo, applyOutgoingRules);
                return res == OriginateResponseStatus.Success;
            }
            return false;
        }

        public bool ChannelSpy(string tsKey, string extn, string spiedExtn, int spyMode)
        {
            var its = GetTelephonyServerByKey(tsKey);
            if (its != null)
            {
                return its.ChannelSpy(extn, spiedExtn, (SpyMode)spyMode);
            }
            return false;
        }

        public bool CanDial(string tsKey)
        {
            var its = GetTelephonyServerByKey(tsKey);
            if (its != null)
                return its.CanDial;

            return false;
        }

        public bool ChannelHangup(string tsKey, string channelName)
        {
            var its = GetTelephonyServerByKey(tsKey);
            if (its != null)
            {
                return its.ChannelHangup(channelName);
            }
            return false;
        }

        private IAloVoipTelephonyServer GetTelephonyServerByKey(string tsKey)
        {
            return AloVoipAgent.Booter.TelephonyServers.Values.FirstOrDefault(p => p.Key == tsKey);
        }

        public void ReBoot()
        {
            try
            {
                AloVoipAgent.Booter.Boot(true);
                return;


//                string appDir = AppDomain.CurrentDomain.BaseDirectory;
//                string appFileName = AppDomain.CurrentDomain.FriendlyName;
//                Process currentProcess = Process.GetCurrentProcess();
//                Process cmd = new Process();
//                ProcessStartInfo processStartInfo = new ProcessStartInfo();
//                processStartInfo.FileName = @"cmd.exe";
//#if DEBUG
//                processStartInfo.Arguments = "/user:Administrator \"cmd /K " + @"@taskkill /F /IM " + appFileName + ">nul && @timeout /t 3 /nobreak >nul && " + "@start " + appDir + appFileName + " >nul && exit";
//#else
//                processStartInfo.Arguments = "net stop AloVoipAgent && timeout /t 20 && net start AloVoipAgent";
//#endif
//                processStartInfo.LoadUserProfile = false;
//                processStartInfo.UseShellExecute = false;
//                processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
//                cmd.StartInfo = processStartInfo;
//                cmd.Start();
//                Console.WriteLine("Agent Restarted Succefully.");
            }
            catch (Exception ex)
            {
                Console.WriteLine("An error occurred!!!: " + ex.Message);
                return;
            }
        }

        public void InitModule(int moduleId)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<ISettingService>(out var settingService))
            using (ApplicationConfigurator.ServiceProvider.Scoped<IModuleService>(out var moduleService))
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var module = moduleService.GetInheritedModule(moduleId);

                var telephonyServer = AloVoipAgent.Booter.TelephonyServers[module.TsId];
                if (telephonyServer != null)
                {
                    if (telephonyServer.Type == TelephonySystemType.Astrisk.ToString() && module.ModuleType == ModuleType.QueueOperatorVoting)
                    {
                        (telephonyServer as AloVoipAsteriskTelephonyServer).RegisterQueueVariables();
                    }
                    else
                    {
                        module.Init(telephonyServer, settingService, true);
                        AloVoipAgent.Booter.RegisterTelephonyServerModules(context, false);
                    }
                }
                else
                    throw new Exception($"TelephonyServer with Id:{module.TsId} not found!!!");
            }
        }

        public void InitOperatorSos(int tsId)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var telephonyServer = AloVoipAgent.Booter.TelephonyServers[tsId];
                if (telephonyServer != null)
                {
                    if (telephonyServer.Type == TelephonySystemType.Astrisk.ToString())
                    {
                        var operatorSos = context.OperatorSos.FirstOrDefault(x => x.TsId == tsId);
                        if (operatorSos != null)
                            operatorSos.Init(telephonyServer);
                    }
                }
                else
                    throw new Exception($"TelephonyServer with Id:{tsId} not found!!!");
            }
        }

        public void DestroyModule(int deletedModuleId)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<ISettingService>(out var settingService))
            using (ApplicationConfigurator.ServiceProvider.Scoped<IModuleService>(out var moduleService))
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var module = moduleService.GetInheritedModule(deletedModuleId);

                var telephonyServer = AloVoipAgent.Booter.TelephonyServers[module.TsId];
                if (telephonyServer != null)
                {
                    telephonyServer.UnRegisterModule(deletedModuleId, module.ModuleType);
                    AloVoipAgent.Booter.RegisterTelephonyServerModules(context, false);
                }
                else
                    throw new Exception($"TelephonyServer with Id:{module.TsId} not found!!!");
            }
        }


        public Dictionary<string, string> GetBillableObjectTypeProps(int lookupSourceId, string billableObjectTypeKey)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                try
                {
                    var loopupSourceService = new LookupSourceServiceFactory(context).Create(lookupSourceId);
                    return loopupSourceService.GetBillableObjectTypeProps(billableObjectTypeKey);
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error in GetBillableObjectTypeProps. lookupSourceId:{lookupSourceId}, billableObjectTypeKey:{billableObjectTypeKey}");
                }
            }

            return null;
        }
        public Dictionary<string, string> GetBillableObjectTypes(int lookupSourceId)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                try
                {
                    var loopupSourceService = new LookupSourceServiceFactory(context).Create(lookupSourceId);
                    return loopupSourceService.GetBillableObjectTypes();
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error in GetBillableObjectTypes. lookupSourceId:{lookupSourceId}");
                }
            }

            return null;
        }
        public Dictionary<string, string> GetMoneyAccounts(int lookupSourceId)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                try
                {
                    var loopupSourceService = new LookupSourceServiceFactory(context).Create(lookupSourceId);
                    return loopupSourceService.GetMoneyAccounts();
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error in GetMoneyAccounts. lookupSourceId:{lookupSourceId}");
                }
            }

            return null;
        }

        public void RegisterVoiceOnTelephonyServer(string voiceName, byte[] voice)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                try
                {
                    foreach (var telephonySystem in context.TelephonySystems)
                    {
                        var telephonyServer = AloVoipAgent.Booter.TelephonyServers[telephonySystem.Id];
                        telephonyServer.RegisterVoice(voiceName, voice);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error in RegisterVoiceOnTelephonyServer. {nameof(voiceName)}:{voiceName}");
                }
            }
        }
        public void UnRegisterVoiceFromTelephonyServer(string voiceName)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                try
                {
                    foreach (var telephonySystem in context.TelephonySystems)
                    {
                        var telephonyServer = AloVoipAgent.Booter.TelephonyServers[telephonySystem.Id];
                        telephonyServer.UnRegisterVoice(voiceName);
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error in UnRegisterVoiceFromTelephonyServer. {nameof(voiceName)}:{voiceName}");
                }
            }
        }

        public GeneralResult CheckConnection(int telephonySystemTypeIndex,
                                             string serverAddress,
                                             int serverPort,
                                             string amiUserName,
                                             string amiPassword,
                                             string linuxUserName,
                                             string linuxPassword,
                                             string mysqlUserName,
                                             string mysqlPassword)
        {
            try
            {
                if ((TelephonySystemType)telephonySystemTypeIndex == TelephonySystemType.Astrisk)
                {
                    using (ApplicationConfigurator.ServiceProvider.Scoped<ISettingService>(out var settingService))
                    {
                        var agentPath = settingService.AgentPath;
                        var winscpPath = Path.Combine(agentPath, "WinSCP.exe");

                        var ts = new TelephonySystem()
                        {
                            ServerAddress = serverAddress,
                            ServerPort = serverPort,
                            AmiUsername = amiUserName,
                            AmiPassword = amiPassword,
                            LinuxUsername = linuxUserName,
                            LinuxPassword = linuxPassword,
                            MySqlUsername = mysqlUserName,
                            MySqlPassword = mysqlPassword
                        };
                        new AloVoipAsteriskTelephonyServer(ts, winscpPath).CheckConnection();
                    }
                }

                return new GeneralResult() { Success = true, Message = "OK" };
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error in CheckConnection. {nameof(telephonySystemTypeIndex)}:{(TelephonySystemType)telephonySystemTypeIndex}");

                return new GeneralResult()
                {
                    Success = false,
                    Message = "Error in CheckConnection:" + ex.Message,
                };
            }
        }

        public Dictionary<string, string> GetIndicators()
        {
            var toReturn = new Dictionary<string, string>();
            foreach (var ts in AloVoipAgent.Booter.TelephonyServers)
                if (ts.Value is AloVoipAsteriskTelephonyServer)
                {
                    toReturn.Add("Ts_" + ts.Key, (ts.Value as AloVoipAsteriskTelephonyServer).QueuedEventCount.ToString());
                }

            foreach (var item in AloVoipAgent.Booter.CallStoreServiceManager.GetIndicators())
            {
                toReturn.Add(item.Key, item.Value);
            }

            return toReturn;
        }
    }
}
