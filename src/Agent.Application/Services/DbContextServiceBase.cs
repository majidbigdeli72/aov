﻿using Agent.Application.Interfaces;
using Agent.Application.Models;
using Agent.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Septa.AloVoip.Domain.Services
{
    //public static class DbContextHelper
    //{
    //	public static object GetPropertyValue(this object obj, string propertyName)
    //	{
    //		return obj.GetType().GetProperties()
    //			.Single(pi => pi.Name.Equals(propertyName, StringComparison.OrdinalIgnoreCase))
    //			.GetValue(obj, null);
    //	}
    //}

    public class DbContextServiceBase<T> : ISampleService where T : class
    {
        private IAloVoipContext _context;
       // private IDbSet<T> _relatedDbSetInContext;
        private DbSet<T> _relatedDbSetInContext;


        public DbContextServiceBase(IAloVoipContext context, DbSet<T> relatedDbSetInContext)
        {
            _context = context;
            _relatedDbSetInContext = relatedDbSetInContext;
        }



        public virtual IEnumerable<T> List(Expression<Func<T,bool>> predicate)
        {
            return _relatedDbSetInContext.Where(predicate).ToList();
        }
        public virtual IEnumerable<T> List()
        {
            return _relatedDbSetInContext.ToList();
        }
        public virtual T Find(Expression<Func<T, bool>> expression)
        {
            return _relatedDbSetInContext.SingleOrDefault(expression);
        }

		public async virtual Task<T> FindAsync(Expression<Func<T, bool>> expression)
		{
			try
			{
				return await _relatedDbSetInContext.FirstOrDefaultAsync(expression);			

			}
			catch (Exception e)
			{

				throw;
			}
		}


		public virtual T Find(params object[] keys)
        {
            return _relatedDbSetInContext.Find(keys);
        }


        private static void PrintToDebug(ValidationException e)
        {
       
                Debug.WriteLine(e.Message);
        }

        public virtual void Add(T model)
        {
            try
            {
                _relatedDbSetInContext.Add(model);
                _context.SaveChanges();
            }
            catch (ValidationException e)
            {
                PrintToDebug(e);
                throw;
            }
        }

        public virtual void Update(T model, params object[] keys)
        {
            try
            {
                var oldModel = _relatedDbSetInContext.Find(keys);
                _context.Entry(oldModel).CurrentValues.SetValues(model);
                _context.Entry(oldModel).State = Microsoft.EntityFrameworkCore.EntityState.Modified;

                _context.SaveChanges();
            }
            catch (ValidationException e)
            {
                PrintToDebug(e);
                throw;
            }
        }

        public virtual void Attach(T model)
        {
            _relatedDbSetInContext.Add(model);
            _context.Entry(model).State = Microsoft.EntityFrameworkCore.EntityState.Added;
        }

        public virtual void Remove(T model)
        {
            _relatedDbSetInContext.Remove(model);
            _context.Entry(model).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
        }

        public virtual void Modify(T model, params object[] keys)
        {
            var oldModel = _relatedDbSetInContext.Find(keys);
            _context.Entry(oldModel).CurrentValues.SetValues(model);
            _context.Entry(oldModel).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
        }

        //public virtual void Update<TEntity>(IEnumerable<TEntity> items, params object[] keys) where TEntity : class
        //{
        //    try
        //    {
        //        var dbset = _context.Set<TEntity>();

        //        _context.SaveChanges();
        //    }
        //    catch (DbEntityValidationException e)
        //    {
        //        PrintToDebug(e);
        //        throw;
        //    }
        //}

        public virtual void AddOrUpdate(T model)
        {
            try
            {
                _relatedDbSetInContext.Upsert(model);
                _context.SaveChanges();
            }
            catch (ValidationException e)
            {
                PrintToDebug(e);
                throw;
            }
        }
        
        public virtual void Delete(int pk)
        {
            try
            {
                var entity = _relatedDbSetInContext.Find(pk);
                if (entity == null) return;
                _relatedDbSetInContext.Remove(entity);

                _context.SaveChanges();
            }
            catch (ValidationException e)
            {
                PrintToDebug(e);
                throw;
            }
        }

        public virtual Call GetFirstCallInDb()
        {
            throw new NotImplementedException();
        }
    }
}