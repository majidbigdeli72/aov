﻿using Agent.Application.Interfaces;
using Agent.Application.Models;
using Agent.Application.ViewModels;
using Agent.Domain.Enum;
using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using Agent.Domain.Models.Identity;
using Septa.AloVoip.Domain.Entries;
using Septa.TelephonyServer;
using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Septa.AloVoip.Domain.Services
{
    public class DashboardService : IDashboardService
    {
        private readonly IAloVoipContext _context;
        private readonly ISettingService _settingService;
        private readonly IAloVoipTelephonyService _aloVoipTelephonyService;
        private readonly DbContextServiceBase<Operator> _operatorService;
        private readonly DbContextServiceBase<Peer> _peerService;
        private readonly DbContextServiceBase<Department> _departmentService;
        private readonly DbContextServiceBase<OperatorDepartmentPermission> _operatorDepartmentPermissionService;
        private readonly DbContextServiceBase<Queue> _queueService;
        private readonly DbContextServiceBase<TelephonySystem> _telephonySystemService;
        protected readonly DbContextServiceBase<ApplicationUser> _applicationUserService;


        public DashboardService(IAloVoipContext context, ISettingService settingService, IAloVoipTelephonyService aloVoipTelephonyService)
        {
            _context = context;
            _settingService = settingService;
            _aloVoipTelephonyService = aloVoipTelephonyService;
            _operatorService = new DbContextServiceBase<Operator>(context, context.Operators);
            _peerService = new DbContextServiceBase<Peer>(context, context.Peers);
            _departmentService = new DbContextServiceBase<Department>(context, context.Departments);
            _operatorDepartmentPermissionService = new DbContextServiceBase<OperatorDepartmentPermission>(context, context.OperatorDepartmentPermissions);
            _telephonySystemService = new DbContextServiceBase<TelephonySystem>(context, context.TelephonySystems);
            _queueService = new DbContextServiceBase<Queue>(context, context.Queues);
            _applicationUserService = new DbContextServiceBase<ApplicationUser>(context, context.Users);
        }

        public object GetTelephonySystems()
        {
            var telephonySystems = _context.TelephonySystems;
            if (telephonySystems == null)
                return new List<string>();

            return telephonySystems.Select(x => new
            {
                tsKey = x.Key,
                tsName = x.Name
            });
        }

        /* Queue Dashboard */
        public DashboardStatisticsModel GetQueueStatistics(string tsKey, int? queueId = null, int? datePeriodInDay = 0)
        {
            try
            {
                DateTime fromDate = DateTime.Today;
                if (datePeriodInDay.HasValue && datePeriodInDay.Value > 0)
                    fromDate = DateTime.Today.AddDays(-datePeriodInDay.Value);

                var actives = 0;
                var waitings = 0;

                    var telephonyServerInfo = _aloVoipTelephonyService.GetTelephonyInfoByKey(tsKey);
                    if (telephonyServerInfo.Success)
                    {
                        IEnumerable<QueueEntry> queues = null;

                        if (queueId.HasValue && queueId.Value > 0)
                            queues = telephonyServerInfo.Queues.Where(x => x.DbId == queueId.Value);
                        else
                            queues = telephonyServerInfo.Queues;

                        if (queues != null && queues.Any())
                        {
                            waitings = queues.SelectMany(x => x.Callers).Where(z => z.JoinDate > fromDate && z.Status == QueueCallerStatus.Waiting).ToList().Count;

                            var queueMemberExtensions = queues.SelectMany(x => x.Members).Select(x => x.Ext);
                            actives = telephonyServerInfo.Channels.Count(x => x.ConnectDate > fromDate && queueMemberExtensions.Contains(x.OwnerPeerName) && x.State == ChannelState.Up);
                        }
                    }
                

                var dashboardData = _context.GetDashboardData(fromDate, queueId);
                if (dashboardData != null)
                {
                    dashboardData.Actives = actives;
                    dashboardData.Waitings = waitings;
                    dashboardData.Total = actives + waitings + dashboardData.Abandoneds + dashboardData.Completeds;

                    return dashboardData;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in DashboardService GetQueueStatistics: " + ex.Message);
            }

            return new DashboardStatisticsModel();
        }
        public DashboardPagingResultEntry<QueueDashboardActiveCallEntry> GetQueueActiveCalls(string tsKey, int? queueId = null, int? datePeriodInDay = 0, ActiveCallsFilterModel filterModel = null)
        {
            var result = new DashboardPagingResultEntry<QueueDashboardActiveCallEntry>()
            {
                TotalRows = 0,
                Data = new List<QueueDashboardActiveCallEntry>()
            };

            try
            {
                var pageIndex = 0;
                var pageSize = 10;
                var sortExpression = string.Empty;
                var sortDirection = SortDirection.Descending;

                if (filterModel == null)
                    filterModel = new ActiveCallsFilterModel();

                if (filterModel.PageIndex < 0)
                    filterModel.PageIndex = 0;

                if (filterModel.PageSize == 0)
                    filterModel.PageSize = 10;

                if (string.IsNullOrEmpty(filterModel.SortExpression))
                    filterModel.SortExpression = nameof(ActiveCallsFilterModel.ConnectDate);

                if (string.IsNullOrEmpty(filterModel.SortDirection))
                    filterModel.SortDirection = "desc";

                pageIndex = filterModel.PageIndex;
                pageSize = filterModel.PageSize;
                sortExpression = filterModel.SortExpression;
                sortDirection = filterModel.SortDirection == "desc" ? SortDirection.Descending : SortDirection.Ascending;

                DateTime fromDate = DateTime.Today;
                if (datePeriodInDay.HasValue && datePeriodInDay.Value > 0)
                    fromDate = DateTime.Today.AddDays(-datePeriodInDay.Value);

           
                    var telephonyServerInfo = _aloVoipTelephonyService.GetTelephonyInfoByKey(tsKey);
                    if (telephonyServerInfo.Success)
                    {
                        IEnumerable<QueueEntry> queues = telephonyServerInfo.Queues;
                        if (queueId.HasValue)
                            queues = queues.Where(x => x.DbId == queueId.Value);

                        if (queues == null || !queues.Any())
                            return result;

                        var queueMemberExtensions = queues.SelectMany(x => x.Members).Select(x => x.Ext);

                        var activeChannels = telephonyServerInfo.Channels.Where(x => x.ConnectDate > fromDate && queueMemberExtensions.Contains(x.OwnerPeerName) && x.State == ChannelState.Up);
                        if (activeChannels == null || !activeChannels.Any())
                            return result;

                        // Filter
                        if (!string.IsNullOrEmpty(filterModel.Caller))
                            activeChannels = activeChannels.Where(x => x.ParentCall.PhoneNumber.Contains(filterModel.Caller));

                        //if (!string.IsNullOrEmpty(filterModel.Queue))
                        //activeChannels = activeChannels.Where(x => x.ParentCall.InitByChannel.CallerIdName.Contains(filterModel.Caller));

                        if (!string.IsNullOrEmpty(filterModel.Agent))
                            activeChannels = activeChannels.Where(x => x.CallerIdName.ToLower().Contains(filterModel.Caller.ToLower()));

                        if (!string.IsNullOrEmpty(filterModel.ConnectDate))
                            activeChannels = activeChannels.Where(x => x.ConnectDate.HasValue && ((FarsiToolbox.DateAndTime.PersianDateTime)x.ConnectDate.Value).ToString().Contains(filterModel.ConnectDate));

                        if (!string.IsNullOrEmpty(filterModel.Wait))
                            activeChannels = activeChannels.Where(x => new TimeSpan(Convert.ToInt64((x.CreateDate - x.ParentCall.StartDate).Ticks)).ToString("hh\\:mm\\:ss").Contains(filterModel.Wait));

                        if (!string.IsNullOrEmpty(filterModel.Talk))
                            activeChannels = activeChannels.Where(x => x.ConnectDate.HasValue && new TimeSpan(Convert.ToInt64((DateTime.Now - x.ConnectDate.Value).Ticks)).ToString("hh\\:mm\\:ss").Contains(filterModel.Talk));


                        var activeCallEntries = new List<QueueDashboardActiveCallEntry>();
                        activeChannels.ToList().ForEach(x =>
                        {
                            var queueName = "";
                            var queue = queues.FirstOrDefault(z => z.Members.Any(t => t.Ext == x.CallerIdNum));
                            if (queue != null)
                                queueName = queue.Name;

                            activeCallEntries.Add(new QueueDashboardActiveCallEntry()
                            {
                                Caller = x.ParentCall.PhoneNumber,
                                Agent = x.CallerIdName + " - " + x.CallerIdNum,
                                ConnectDate = (new TimeSpan(Convert.ToInt64((DateTime.Now - x.ConnectDate.Value).Ticks))).ToString().Substring(0, 8), //Infrastructure.Common.HumanizedString(x.ConnectDate.Value),
                                Queue = queueName,
                                Wait = (new TimeSpan(Convert.ToInt64((x.CreateDate - x.ParentCall.StartDate).Ticks))).ToString().Substring(0, 8), //Infrastructure.Common.HumanizedString(new TimeSpan(Convert.ToInt64((x.CreateDate - x.ParentCall.StartDate).Ticks))),
                                Talk = (new TimeSpan(Convert.ToInt64((DateTime.Now - x.ConnectDate.Value).Ticks))).ToString().Substring(0, 8) //Infrastructure.Common.HumanizedString(new TimeSpan(Convert.ToInt64((DateTime.Now - x.ConnectDate.Value).Ticks)))
                            });
                        });

                        var sortedData = activeCallEntries.AsQueryable().Order(sortExpression, sortDirection);

                        return new DashboardPagingResultEntry<QueueDashboardActiveCallEntry>()
                        {
                            TotalRows = sortedData.Count(),
                            Data = sortedData.Skip(pageIndex * pageSize).Take(pageSize)
                        };
                    }
                
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in DashboardService GetQueueActiveCalls: " + ex.Message);
            }

            return result;
        }
        public DashboardPagingResultEntry<QueueDashboardWaitCallEntry> GetQueueWaitingCalls(string tsKey, int? queueId = null, int? datePeriodInDay = 0, WaitingCallsFilterModel filterModel = null)
        {
            var result = new DashboardPagingResultEntry<QueueDashboardWaitCallEntry>()
            {
                TotalRows = 0,
                Data = new List<QueueDashboardWaitCallEntry>()
            };

            try
            {
                var pageIndex = 0;
                var pageSize = 10;
                var sortExpression = string.Empty;
                var sortDirection = SortDirection.Descending;

                if (filterModel == null)
                    filterModel = new WaitingCallsFilterModel();

                if (filterModel.PageIndex < 0)
                    filterModel.PageIndex = 0;

                if (filterModel.PageSize == 0)
                    filterModel.PageSize = 10;

                if (string.IsNullOrEmpty(filterModel.SortExpression))
                    filterModel.SortExpression = nameof(WaitingCallsFilterModel.JoinDate);

                if (string.IsNullOrEmpty(filterModel.SortDirection))
                    filterModel.SortDirection = "desc";

                pageIndex = filterModel.PageIndex;
                pageSize = filterModel.PageSize;
                sortExpression = filterModel.SortExpression;
                sortDirection = filterModel.SortDirection == "desc" ? SortDirection.Descending : SortDirection.Ascending;

                DateTime fromDate = DateTime.Today;
                if (datePeriodInDay.HasValue && datePeriodInDay.Value > 0)
                    fromDate = DateTime.Today.AddDays(-datePeriodInDay.Value);

             
                    var telephonyServerInfo = _aloVoipTelephonyService.GetTelephonyInfoByKey(tsKey);
                    if (telephonyServerInfo.Success)
                    {
                        IEnumerable<QueueEntry> queues = telephonyServerInfo.Queues.Where(x => x.Callers.Any(z => z.JoinDate > fromDate));
                        if (queueId.HasValue)
                            queues = queues.Where(x => x.DbId == queueId.Value);

                        if (queues == null || !queues.Any())
                            return result;

                        var waitCallsList = queues.Where(x => x.Callers.Any(z => z.Status == QueueCallerStatus.Waiting)).SelectMany(x => x.Callers);
                        if (waitCallsList == null || !waitCallsList.Any())
                            return result;

                        // Filter
                        if (!string.IsNullOrEmpty(filterModel.Caller))
                            waitCallsList = waitCallsList.Where(x => x.Name.ToLower().Contains(filterModel.Caller.ToLower()));

                        if (!string.IsNullOrEmpty(filterModel.Queue))
                            waitCallsList = waitCallsList.Where(x => x.ParentQueue.Name.ToLower().Contains(filterModel.Queue.ToLower()));

                        if (!string.IsNullOrEmpty(filterModel.JoinDate))
                            waitCallsList = waitCallsList.Where(x => ((FarsiToolbox.DateAndTime.PersianDateTime)x.JoinDate).ToString().Contains(filterModel.JoinDate));

                        if (!string.IsNullOrEmpty(filterModel.Wait))
                            waitCallsList = waitCallsList.Where(x => new TimeSpan(Convert.ToInt64((DateTime.Now - x.JoinDate).Ticks)).ToString("hh\\:mm\\:ss").Contains(filterModel.Wait));


                        var waitingCallEntries = new List<QueueDashboardWaitCallEntry>();
                        waitCallsList.ToList().ForEach(x =>
                        {
                            waitingCallEntries.Add(new QueueDashboardWaitCallEntry()
                            {
                                CallerId = 0,//x.DbId.Value,
                                Caller = x.Name,
                                Queue = x.ParentQueue.Name,
                                JoinDate = (new TimeSpan(Convert.ToInt64((DateTime.Now - x.JoinDate).Ticks))).ToString().Substring(0, 8), //Infrastructure.Common.HumanizedString(x.JoinDate),
                                Wait = (new TimeSpan(Convert.ToInt64((DateTime.Now - x.JoinDate).Ticks))).ToString().Substring(0, 8) //Infrastructure.Common.HumanizedString(new TimeSpan(Convert.ToInt64((DateTime.Now - x.JoinDate).Ticks)))
                            });
                        });

                        var sortedData = waitingCallEntries.AsQueryable().Order(sortExpression, sortDirection);

                        return new DashboardPagingResultEntry<QueueDashboardWaitCallEntry>()
                        {
                            TotalRows = sortedData.Count(),
                            Data = sortedData.Skip(pageIndex * pageSize).Take(pageSize)
                        };

                    }
                
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in DashboardService GetQueueWaitingCalls: " + ex.Message);
            }

            return result;
        }
        public IEnumerable<Queue> GetQueues(string tsKey)
        {
            var ts = _telephonySystemService.Find(x => x.Key == tsKey);
            if (ts == null)
                return null;

            return _queueService.List(x => x.TsId == ts.Id);
        }
        public object GetExtensions(string tsKey, int? queueId = null, int? datePeriodInDay = 0)
        {
            try
            {
                DateTime fromDate = DateTime.Today;
                if (datePeriodInDay.HasValue && datePeriodInDay.Value > 0)
                    fromDate = DateTime.Today.AddDays(-datePeriodInDay.Value);

                    var telephonyServerInfo = _aloVoipTelephonyService.GetTelephonyInfoByKey(tsKey);
                    if (telephonyServerInfo.Success)
                    {
                        IEnumerable<QueueEntry> queues = null;

                        if (queueId.HasValue && queueId.Value > 0)
                            queues = telephonyServerInfo.Queues.Where(x => x.DbId == queueId.Value);
                        else
                            queues = telephonyServerInfo.Queues;

                        var queueMembers = queues.SelectMany(x => x.Members);

                        var dbMemberStatistics = _context.GetMemberStatistics(fromDate, queueId);
                        if (dbMemberStatistics == null)
                            dbMemberStatistics = new List<MemberStatisticsModel>();

                        var operators = _operatorService.List();

                        // Add operators ths are not active or not in the list
                        queueMembers.Where(x => !dbMemberStatistics.Any(z => z.Ext == x.Ext)).ToList().ForEach(x =>
                        {
                            var operatorId = 0;
                            if (operators != null)
                            {
                                var oper = operators.FirstOrDefault(t => t.Peers.Any(s => s.Code == x.Ext));
                                if (oper != null)
                                {
                                    operatorId = oper.Id;
                                }
                            }
                            dbMemberStatistics.Add(new MemberStatisticsModel()
                            {
                                Ext = x.Ext,
                                Name = x.Name,
                                AnsweredCount = 0,
                                OperatorId = operatorId
                            });
                        });

                        // Set Operators name
                        dbMemberStatistics.ToList().ForEach(x =>
                        {
                            if (operators != null)
                            {
                                var oper = operators.FirstOrDefault(t => t.Peers.Any(s => s.Code == x.Ext));
                                if (oper != null)
                                    x.OperatorId = oper.Id;
                            }

                            var queueMember = queueMembers.FirstOrDefault(z => z.Ext == x.Ext);
                            if (queueMember != null)
                            {
                                x.Name = queueMember.Name;
                            }
                            else
                            {
                                x.Name = x.Ext;
                            }
                        });

                        return dbMemberStatistics.OrderByDescending(x => x.AnsweredCount).Select(x => new
                        {
                            x.Ext,
                            x.Name,
                            x.OperatorId,
                            x.AnsweredCount
                        });

                        /*return queueMembers.Select(x => new
                        {
                            x.Ext,
                            x.Name,
                            AnsweredCount = dbMemberStatistics.FirstOrDefault(z => z.Ext == x.Ext) != null ? dbMemberStatistics.FirstOrDefault(z => z.Ext == x.Ext).AnsweredCount : 0
                        });*/

                        /*var activeCalls = telephonyServerInfo.Channels.Where(x => x.ConnectDate > fromDate && queueMembers.Any(z => z.Ext.Contains(x.OwnerPeerName)) && x.State == ChannelState.Up).ToList();
                        var idelMembers = queueMembers.Where(x => !activeCalls.Select(z => z.OwnerPeerName).Contains(x.Ext));

                        return idelMembers.Select(x => new
                        {
                            x.Ext,
                            x.Name
                        });*/
                    }
                
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in DashboardService GetExtensions: " + ex.Message);
            }

            return new List<object>
            {
                new
                {
                    Ext = "",
                    Name = "",
                    OperatorId = 0,
                    AnsweredCount = 0
                }
            };
        }
        public async void UpdateQueueDashboardStatsChanges(string tsKey)
        {
            try
            {
                // Calling webservice...
                var client = new HttpClient()
                {
                    BaseAddress = new Uri(_settingService.WebSiteAddress)
                };
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var requestUri = string.Format("api/telephonysystems/{0}/dashboard/queue/statschanges", tsKey);
                var response = await client.PostAsync(requestUri, new StringContent(string.Empty));
                if (response.IsSuccessStatusCode)
                {
                    // Do Nothing yet
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in DashboardService UpdateQueueDashboardStatsChanges:" + ex.Message);
            }
        }


        /* CallCenter Dashboard */
        public IEnumerable<Peer> GetCallCenterOperatorExtensions(string tsKey, int userId)
        {
            var peers = _peerService.List(x => x.TelephonySystem.Key == tsKey && x.Operator != null && x.Operator.UserId == userId);
            if (peers != null && peers.Count() > 0)
            {
                return peers;
            }
            return new List<Peer>();
        }
        public IEnumerable<CallCenterDashboardStatisticsEntry> GetCallCenterStatistics(string tsKey, int userId)
        {
            var result = new List<CallCenterDashboardStatisticsEntry>();


                var telephonyServerInfo = _aloVoipTelephonyService.GetTelephonyInfoByKey(tsKey);
                if (telephonyServerInfo.Success)
                {
                    IEnumerable<OperatorDepartmentPermission> operatorDepartmentPermissions = null;
                    var currentUser = _applicationUserService.Find(userId);
                    if (currentUser.UserName.ToLower().Equals(Common.AdminUsername))
                        operatorDepartmentPermissions = GetAllDepartmentPermissionsForAdmin();
                    else
                        operatorDepartmentPermissions = _operatorDepartmentPermissionService.List(x => x.Operator.UserId == userId);
                    
                    // UnDepartmented Extensions
                    var unDepartmentedExtensions = _peerService.List(x => x.PeerTypeIndex == (int)PeerType.Extension && x.PeerProtocolTypeIndex == (int)PeerProtocolType.SIP && !x.DepartmentId.HasValue).OrderBy(x => x.Code);
                    if (unDepartmentedExtensions != null && unDepartmentedExtensions.Count() > 0)
                    {
                        var allowAllActions = operatorDepartmentPermissions.Count() == 0;
                        var dashboardStatistics = new CallCenterDashboardStatisticsEntry()
                        {
                            DepartmentTitle = "بدون دپارتمان",
                            CanListen = allowAllActions,
                            CanWhisper = allowAllActions,
                            CanConference = allowAllActions,
                            CanHangup = allowAllActions
                        };
                        var extensions = new List<CallCenterDashboardExtensionEntry>();
                        foreach (var unDepartmentedExtension in unDepartmentedExtensions)
                        {
                            var dashboardExtension = CreateCallCenterDashboardExtensionEntry(telephonyServerInfo, unDepartmentedExtension);
                            extensions.Add(dashboardExtension);
                        }
                        dashboardStatistics.Extensions = extensions.OrderBy(exten => int.Parse(exten.Ext));
                        result.Add(dashboardStatistics);
                    }

                    // Departmented Extensions
                    if (operatorDepartmentPermissions != null)
                    {
                        operatorDepartmentPermissions.ToList().ForEach(x =>
                        {
                            var dashboardStatistics = new CallCenterDashboardStatisticsEntry()
                            {
                                DepartmentId = x.DepartmentId,
                                DepartmentTitle = x.Department.Title,
                                CanListen = x.CanListen,
                                CanWhisper = x.CanWhisper,
                                CanConference = x.CanConference,
                                CanHangup = x.CanHangup
                            };
                            var extensions = new List<CallCenterDashboardExtensionEntry>();
                            foreach (var ext in x.Department.Extensions)
                            {
                                var dashboardExtension = CreateCallCenterDashboardExtensionEntry(telephonyServerInfo, ext);
                                extensions.Add(dashboardExtension);
                            }
                            dashboardStatistics.Extensions = extensions.OrderBy(exten => int.Parse(exten.Ext));

                            result.Add(dashboardStatistics);
                        });
                    }
                }
            

            return result;
        }

        private IEnumerable<OperatorDepartmentPermission> GetAllDepartmentPermissionsForAdmin()
        {
            var lstOperatorDepartmentPermissions = new List<OperatorDepartmentPermission>();

            var departments = _departmentService.List();
            foreach (var department in departments)
            {
                lstOperatorDepartmentPermissions.Add(new OperatorDepartmentPermission()
                {
                    Id = department.Id,
                    DepartmentId = department.Id,
                    Department = department,                    
                    CanConference = true,
                    CanHangup = true,
                    CanListen = true,
                    CanWhisper = true
                });
            }

            return lstOperatorDepartmentPermissions;
        }

        public void UpdateCallCenterDashboardExtension(string tsKey, string peerName, PeerType peerType)
        {
			var data = new { peerName, peerType };
			var baseAdress = _settingService.WebSiteAddress;
			CallApiExtention.Request($"{baseAdress}/api/telephonysystems/{tsKey}/dashboard/callcenter/extensionchanges", data);
        }
        private CallCenterDashboardExtensionEntry CreateCallCenterDashboardExtensionEntry(TelephonyServerInfoResult telephonyServerInfo, Peer ext)
        {
            var dashboardExtension = new CallCenterDashboardExtensionEntry()
            {
                Id = ext.Id,
                Name = ext.OwnerOperatorId.HasValue ? ext.Operator.FirstName + " " + ext.Operator.LastName : "",
                Ext = ext.Code,
                OperatorId = ext.OwnerOperatorId ?? 0,
                Trunk = string.Empty,
                CallType = string.Empty
            };
            if (ext.Enabled)
            {
                var extensionEntry = telephonyServerInfo.Extensions.FirstOrDefault(x => x.DbId == ext.Id);
                if (extensionEntry != null && extensionEntry.Status == ExtensionStatus.Unavailable)
                    dashboardExtension.Status = OperatorStatus.NotAvailable.ToString();
                else
                    dashboardExtension.Status = OperatorStatus.Available.ToString();
            }
            else
                dashboardExtension.Status = OperatorStatus.Disabled.ToString();


            var channels = telephonyServerInfo.Channels;
            if (channels != null && channels.Count > 0)
            {
                var peerChannel = channels.FirstOrDefault(channel => channel.OwnerPeer?.Id == ext.Code && channel.ParentCall != null &&
                                                                     channel.ParentCall.Channels != null && channel.ParentCall.Channels.Count() > 0 &&
                                                                     (channel.State == ChannelState.Busy || channel.State == ChannelState.Up || channel.State == ChannelState.Ringing));
                if (peerChannel != null)
                {
                    if (string.IsNullOrEmpty(dashboardExtension.Name) && dashboardExtension.Ext != peerChannel.CallerIdName)
                        dashboardExtension.Name = peerChannel.CallerIdName;

                    var parentCallChannels = peerChannel.ParentCall.Channels.Where(pcc => pcc.OwnerPeer != null &&
                                                                                   pcc.OwnerPeer.Id != ext.Code &&
                                                                                   pcc.State != ChannelState.Hangedup &&
                                                                                   pcc.State != ChannelState.Ringing &&
                                                                                   pcc.State != ChannelState.PreRing &&
                                                                                   pcc.State != ChannelState.Ring);
                    if (parentCallChannels != null && parentCallChannels.Count() > 0)
                    {
                        var operatorCalls = new List<CallCenterDashboardOperatorCallEntry>();
                        foreach (var channel in parentCallChannels)
                        {
                            var operatorCall = operatorCalls.FirstOrDefault(oc => oc.CallerNum == channel.OwnerPeer?.Id);
                            if (operatorCall != null)
                            {
                                operatorCalls.Remove(operatorCall);
                            }

                            var callerIdName = channel.CallerIdName;
                            var callerOperator = _peerService.Find(x => x.TelephonySystem.Key == telephonyServerInfo.Key && x.Code == channel.CallerIdNum);
                            if (callerOperator != null && callerOperator.OwnerOperatorId.HasValue)
                            {
                                var op = _context.Operators.FirstOrDefault(x => x.Id == callerOperator.OwnerOperatorId.Value);
                                if (op != null)
                                    callerIdName = op.FirstName + " " + op.LastName;// + " - " + callerIdName;
                            }

                            if (channel.OwnerPeer?.Type == PeerType.Trunk)
                                callerIdName = channel.CallerIdNum;

                            operatorCalls.Add(new CallCenterDashboardOperatorCallEntry()
                            {
                                ChannelName = channel.Channel,
                                CallerName = string.IsNullOrEmpty(callerIdName) ? peerChannel.DialedExtension : callerIdName,
                                CallerNum = string.IsNullOrEmpty(channel.CallerIdNum) ? peerChannel.DialedExtension : channel.CallerIdNum,
                                CallDuration = channel.Duration.ToString("hh\\:mm\\:ss") //peerChannel.Duration.ToString("hh\\:mm\\:ss")
                            });
                        }
                        dashboardExtension.Calls = operatorCalls;
                        dashboardExtension.CallType = peerChannel.ParentCall.CallType.ToString();

                        var trunk = parentCallChannels.FirstOrDefault(x => x.OwnerPeer?.Type == PeerType.Trunk);
                        if (trunk != null)
                        {
                            if (trunk.OwnerPeerProtocolType == PeerProtocolType.DAHDI)
                                dashboardExtension.Trunk = nameof(PeerProtocolType.DAHDI);
                            else
                                dashboardExtension.Trunk = trunk.OwnerPeerName;
                        }
                    }

                    if (peerChannel.State == ChannelState.Busy || peerChannel.State == ChannelState.Up)
                        dashboardExtension.Status = OperatorStatus.Busy.ToString();
                    else if (peerChannel.State == ChannelState.PreRing || peerChannel.State == ChannelState.Ring || peerChannel.State == ChannelState.Ringing)
                        dashboardExtension.Status = OperatorStatus.Ringing.ToString();
                }
            }

            return dashboardExtension;
        }
        public IEnumerable<Peer> GetExtensions(string tsKey)
        {
            return GetPeers(tsKey, PeerType.Extension);
        }
        public IEnumerable<Peer> GetTrunks(string tsKey)
        {
            return GetPeers(tsKey, PeerType.Trunk);
        }
        public bool CallDial(string tsKey, string extn, string dialNo)
        {
           
                return _aloVoipTelephonyService.Dial(tsKey, extn, dialNo, true);
            
        }
        public bool CallListen(string tsKey, string extn, string spiedExtn)
        {
        
                return _aloVoipTelephonyService.ChannelSpy(tsKey, extn, spiedExtn, (int)SpyMode.Spy);
            
        }
        public bool CallWhisper(string tsKey, string extn, string spiedExtn)
        {

                return _aloVoipTelephonyService.ChannelSpy(tsKey, extn, spiedExtn, (int)SpyMode.Whisper);
            
        }
        public bool CallConference(string tsKey, string extn, string spiedExtn)
        {
               return _aloVoipTelephonyService.ChannelSpy(tsKey, extn, spiedExtn, (int)SpyMode.Barge);
            
        }
        public bool CallHangup(string tsKey, string channelName)
        {
            
                return _aloVoipTelephonyService.ChannelHangup(tsKey, channelName);
            
        }


        /* CallBackRequest Dashboard */
        public IEnumerable<CallbackRequestModel> GetCallbackRequests(int userId)
        {
            var result = new List<CallbackRequestModel>();

            var peers = _peerService.List(x => x.OwnerOperatorId.HasValue && x.Operator.UserId == userId);
            if (peers == null || peers.Count() == 0)
                return result;

            var opertorExtensions = peers.Select(x => x.Id).ToList();

            var date = DateTime.Now.AddMinutes(-10);
            _context.ModuleAdvanceFollowMeResult
                    .Where(x => x.ModuleAdvanceFollowMeInputTypeIndex == (int)ModuleAdvanceFollowMeInputType.CallBack &&
                                x.CallBackStatusIndex == (int)CallBackStatus.Pending &&
                                (x.LastCallBackRetryDate == null || x.LastCallBackRetryDate < date) &&
                                opertorExtensions.Contains(x.ExtensionId))
                    .OrderBy(x => x.InputDate)
                    .ToList()
                    .ForEach(x =>
                    {
                        result.Add(new CallbackRequestModel()
                        {
                            Id = x.Id,
                            InputDate = Common.HumanizedString(x.InputDate),
                            CallerNumber = string.IsNullOrEmpty(x.CallChannel.Call.ProfileName) ? x.CallChannel.Call.PhoneNumber : x.CallChannel.Call.ProfileName,
                            ExtensionCode = x.Extension.Code,
                            CallBackStatus = ((CallBackStatus)x.CallBackStatusIndex).ToString(),
                            CallBackRetryCount = x.CallBackRetryCount,
                            LastCallBackRetryDate = x.LastCallBackRetryDate.HasValue ? Common.HumanizedString(x.LastCallBackRetryDate.Value) : "",
                            AutoDial = x.ModuleAdvancedFollowMe.AutoDialOperator
                        });
                    });

            return result;
        }
        public bool MarkCallbackRequestAsDone(int callbackRequestId)
        {
            var result = false;

            var advanceFollowMeResult = _context.ModuleAdvanceFollowMeResult.FirstOrDefault(x => x.Id == callbackRequestId);
            if (advanceFollowMeResult != null)
            {
                advanceFollowMeResult.CallBackStatusIndex = (int)CallBackStatus.Success;
                advanceFollowMeResult.LastCallBackRetryDate = DateTime.Now;
                _context.SaveChanges();
                result = true;
            }

            return result;
        }


        /* AgentReport */
        public IEnumerable<Department> GetDepartments()
        {
            return _context.GetDepartment();
        }


        private IEnumerable<Peer> GetPeers(string tsKey, PeerType peerType)
        {
            var peers = _peerService.List(x => x.TelephonySystem.Key == tsKey && x.PeerTypeIndex == (int)peerType && x.Enabled == true).ToList();
            if (peers != null && peers.Count > 0)
            {
                if (peers.Any(x => x.PeerProtocolTypeIndex == (int)PeerProtocolType.DAHDI))
                {
                    peers.RemoveAll(x => x.PeerProtocolTypeIndex == (int)PeerProtocolType.DAHDI);
                    peers.Add(new Peer()
                    {
                        Code = nameof(PeerProtocolType.DAHDI),
                        PeerProtocolTypeIndex = (int)PeerProtocolType.DAHDI
                    });
                }
                return peers;
            }
            return new List<Peer>();
        }


        public void Dispose()
        {
        }
    }
}