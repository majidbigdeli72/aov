﻿using Agent.Application.Interfaces;
using Agent.Application.Models;
using Agent.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Septa.AloVoip.Domain.Services
{
    public class QueueStatisticsService : IQueueStatisticsService
    {
        private readonly IAloVoipContext _context;
        private readonly DbContextServiceBase<Queue> _queueService;
        private readonly DbContextServiceBase<ModuleQueueOperatorVotingResult> _queueOperatorVotingResultService;

        public QueueStatisticsService(IAloVoipContext context)
        {
            _context = context;
            _queueService = new DbContextServiceBase<Queue>(context, context.Queues);
            _queueOperatorVotingResultService = new DbContextServiceBase<ModuleQueueOperatorVotingResult>(context, context.ModuleQueueOperatorVotingResults);
        }

        public QueueStatisticsModel GetQueueStatistics(QueueStatisticsFilterModel filterModel)
        {
            var queueStatisticsModel = new QueueStatisticsModel();

            if (filterModel == null)
                return queueStatisticsModel;

            var queue = _queueService.Find(x => x.Id == filterModel.QueueId);
            if (queue == null)
                return queueStatisticsModel;

            queueStatisticsModel.QueueStatistics.Title = queue.Name;

            var queueVotingResult = _queueOperatorVotingResultService.List(x => x.ModuleQueueOperatorVoting.QueueId == filterModel.QueueId);

            if (filterModel.Period == "today")
            {
                queueVotingResult = queueVotingResult.Where(x => x.CreateDate >= DateTime.Today);
            }
            else if (filterModel.Period == "lastweek")
            {
                queueVotingResult = queueVotingResult.Where(x => x.CreateDate >= DateTime.Today.AddDays(-7));
            }
            else if (filterModel.Period == "lastmonth")
            {
                queueVotingResult = queueVotingResult.Where(x => x.CreateDate >= DateTime.Today.AddMonths(-1));
            }
            else if (filterModel.Period == "last3month")
            {
                queueVotingResult = queueVotingResult.Where(x => x.CreateDate >= DateTime.Today.AddMonths(-3));
            }
            else if (filterModel.Period == "lastyear")
            {
                queueVotingResult = queueVotingResult.Where(x => x.CreateDate >= DateTime.Today.AddYears(-1));
            }
            else if (filterModel.Period == "custom")
            {
                var fromDate = (DateTime)(new FarsiToolbox.DateAndTime.PersianDateTime(int.Parse(filterModel.From.Substring(0, 4)), int.Parse(filterModel.From.Substring(4, 2)), int.Parse(filterModel.From.Substring(6, 2))));
                var toDate = (DateTime)(new FarsiToolbox.DateAndTime.PersianDateTime(int.Parse(filterModel.To.Substring(0, 4)), int.Parse(filterModel.To.Substring(4, 2)), int.Parse(filterModel.To.Substring(6, 2))));
                queueVotingResult = queueVotingResult.Where(x => x.CreateDate >= fromDate && x.CreateDate <= toDate);
            }

            var queueVotingList = queueVotingResult.ToList();
            if (queueVotingList == null || queueVotingList.Count == 0)
                return queueStatisticsModel;

            queueStatisticsModel.QueueStatistics = new QueueStatistic()
            {
                Title = queueVotingList[0].ModuleQueueOperatorVoting.Queue.Name,
                Statistics = queueVotingList.OrderBy(x => x.Input)
                                            .GroupBy(x => x.Input)
                                            .Select(t => new { t.Key, Value = t.Count() })
            };

            var lstOperatorStatistic = new List<OperatorStatistic>();
            queueVotingList.GroupBy(x => x.PeerId).Select(t => new
            {
                OperatorId = ((Peer)t.FirstOrDefault().Peer).OwnerOperatorId ?? 0,
                Name = ((Peer)t.FirstOrDefault().Peer).OwnerOperatorId.HasValue ? ((Peer)t.FirstOrDefault().Peer).Operator.FirstName + " " + ((Peer)t.FirstOrDefault().Peer).Operator.LastName : t.FirstOrDefault().Peer.Code,
                Code = t.FirstOrDefault().Peer.Code,
                Statistics = t.OrderBy(x => x.Input)
                              .GroupBy(x => x.Input)
                              .Select(x => new { x.Key, Value = x.Count() })
            }).ToList().ForEach(x =>
            {
                lstOperatorStatistic.Add(new OperatorStatistic()
                {
                    OperatorId = x.OperatorId,
                    Name = x.Name,
                    Code = x.Code,
                    Statistics = x.Statistics
                });
            });
            queueStatisticsModel.OperatorStatistics = lstOperatorStatistic;

            return queueStatisticsModel;
        }

        public void Dispose()
        {
        }
    }
}