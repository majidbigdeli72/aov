﻿using Agent.Application.Interfaces;
using Agent.Application.Models;
using Agent.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Septa.AloVoip.Domain.Services
{
    public class VoteStatisticsService : IVoteStatisticsService
    {
        private readonly IAloVoipContext _context;
        private readonly DbContextServiceBase<ModuleVoting> _votingService;
        private readonly DbContextServiceBase<ModuleVotingQuestionResult> _votingResultService;

        public VoteStatisticsService(IAloVoipContext context)
        {
            _context = context;
            _votingService = new DbContextServiceBase<ModuleVoting>(context, context.ModuleVotings);
            _votingResultService = new DbContextServiceBase<ModuleVotingQuestionResult>(context, context.ModuleVotingQuestionResults);
        }

        public VoteStatisticsModel GetVoteStatistics(VoteStatisticsFilterModel filterModel)
        {
            var voteStatisticsModel = new VoteStatisticsModel();

            if (filterModel == null)
                return voteStatisticsModel;

            var voting = _votingService.Find(x => x.Id == filterModel.VotingId);
            if (voting == null)
                return voteStatisticsModel;

            voteStatisticsModel.VotingStatistics.Title = voting.Name;

            var votingResult = _votingResultService.List(x => x.ModuleVotingQuestionItem.ModuleVotingQuestion.ModuleVotingId == filterModel.VotingId);

            if (filterModel.Period == "today")
            {
                votingResult = votingResult.Where(x => x.CreateDate >= DateTime.Today);
            }
            else if (filterModel.Period == "lastweek")
            {
                votingResult = votingResult.Where(x => x.CreateDate >= DateTime.Today.AddDays(-7));
            }
            else if (filterModel.Period == "lastmonth")
            {
                votingResult = votingResult.Where(x => x.CreateDate >= DateTime.Today.AddMonths(-1));
            }
            else if (filterModel.Period == "last3month")
            {
                votingResult = votingResult.Where(x => x.CreateDate >= DateTime.Today.AddMonths(-3));
            }
            else if (filterModel.Period == "lastyear")
            {
                votingResult = votingResult.Where(x => x.CreateDate >= DateTime.Today.AddYears(-1));
            }
            else if (filterModel.Period == "custom")
            {
                var fromDate = (DateTime)(new FarsiToolbox.DateAndTime.PersianDateTime(int.Parse(filterModel.From.Substring(0, 4)), int.Parse(filterModel.From.Substring(4, 2)), int.Parse(filterModel.From.Substring(6, 2))));
                var toDate = (DateTime)(new FarsiToolbox.DateAndTime.PersianDateTime(int.Parse(filterModel.To.Substring(0, 4)), int.Parse(filterModel.To.Substring(4, 2)), int.Parse(filterModel.To.Substring(6, 2))));
                votingResult = votingResult.Where(x => x.CreateDate >= fromDate && x.CreateDate <= toDate);
            }

            if (filterModel.OperatorId.HasValue)
            {
                votingResult = votingResult.Where(x => x.PeerId.HasValue && x.PeerId.Value == filterModel.OperatorId.Value);
            }
            if (filterModel.QueueId.HasValue)
            {
                votingResult = votingResult.Where(x => x.QueueId.HasValue && x.QueueId.Value == filterModel.QueueId.Value);
            }

            var votingList = votingResult.ToList();
            if (votingList == null || votingList.Count == 0)
                return voteStatisticsModel;

            var lstVoteQuestionStatistics = new List<VoteQuestionStatistics>();

            var questions = votingList.OrderBy(x => x.ModuleVotingQuestionItem.ModuleVotingQuestionId)
                                      .GroupBy(x => x.ModuleVotingQuestionItem.ModuleVotingQuestion)
                                      .Select(t => new { t.Key });
            questions.ToList().ForEach(x =>
            {
                lstVoteQuestionStatistics.Add(new VoteQuestionStatistics()
                {
                    QuestionTitle = x.Key.Title,
                    QuestionResult = votingList.Where(t => t.ModuleVotingQuestionItem.ModuleVotingQuestionId == x.Key.Id)
                                               .OrderBy(t => t.ModuleVotingQuestionItemId)
                                               .GroupBy(t => t.ModuleVotingQuestionItemId)
                                               .Select(t => new
                                               {
                                                   Key = votingList.FirstOrDefault(z => z.ModuleVotingQuestionItemId == t.Key).ModuleVotingQuestionItem.Title,
                                                   Value = t.Count()
                                               })
                });
            });
            voteStatisticsModel.VotingStatistics.Statistics = lstVoteQuestionStatistics;

            // Queues
            var lstQueue = new List<VotingQueue>();
            if (votingResult.Any(x => x.QueueId.HasValue))
            {
                votingResult.Where(x => x.QueueId.HasValue).ToList().ForEach(x =>
                {
                    if(!lstQueue.Any(t => t.Id == x.QueueId.Value))
                    {
                        lstQueue.Add(new VotingQueue()
                        {
                            Id = x.QueueId.Value,
                            Name = x.Queue.Name
                        });
                    }
                });
            }
            voteStatisticsModel.Queues = lstQueue;

            // Operators
            var lstOperator = new List<VotingOperator>();
            if (votingResult.Any(x => x.PeerId.HasValue))
            {
                votingResult.Where(x => x.PeerId.HasValue).Distinct().ToList().ForEach(x =>
                {
                    if (!lstOperator.Any(t => t.Id == x.PeerId.Value))
                    {
                        lstOperator.Add(new VotingOperator()
                        {
                            Id = x.PeerId.Value,
                            Name = x.Peer.Code
                        });
                    }
                });
            }
            voteStatisticsModel.Operators = lstOperator;


            return voteStatisticsModel;
        }

        public void Dispose()
        {
        }
    }
}