﻿using Agent.Application.Interfaces;
using Agent.Application.ViewModels;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Description;

namespace Septa.AloVoip.Domain.Services
{
    public class TelephonyClient : ClientBase<IAloVoipTelephonyService>, IAloVoipTelephonyService
    {
        public TelephonyClient() : base(CreateServiceEndPoint())
        {
        }

        private static ServiceEndpoint CreateServiceEndPoint()
        {
            var desc = ContractDescription.GetContract(typeof(IAloVoipTelephonyService));
            var binding = new BasicHttpBinding();
            binding.Security.Mode = BasicHttpSecurityMode.None;
            binding.MaxReceivedMessageSize = int.MaxValue;
            binding.MaxBufferPoolSize = int.MaxValue;
            binding.MaxBufferSize = int.MaxValue;
            binding.ReaderQuotas = new System.Xml.XmlDictionaryReaderQuotas()
            {
                MaxArrayLength = int.MaxValue,
                MaxStringContentLength = int.MaxValue
            };

            return new ServiceEndpoint(desc, binding, new EndpointAddress(EndPointAddress));
        }

        public static string EndPointAddress
        {
            get
            {
#if DEBUG
                return "http://localhost:2020/Services/ITelephonyService";
                //return "http://192.168.2.1:2020/Services/ITelephonyService";
#else
                return "http://localhost:2020/Services/ITelephonyService";
#endif

            }
        }



        public TelephonyServerInfoResult GetTelephonyInfoById(int tsId)
        {
            return Channel.GetTelephonyInfoById(tsId);
        }

        public void InitModule(int moduleId)
        {
            Channel.InitModule(moduleId);
        }

        public void InitOperatorSos(int tsId)
        {
            Channel.InitOperatorSos(tsId);
        }

        public void DestroyModule(int deletedModuleId)
        {
            Channel.DestroyModule(deletedModuleId);
        }


        public Dictionary<string, string> GetBillableObjectTypeProps(int lookupSourceId, string billableObjectTypeKey)
        {
            return Channel.GetBillableObjectTypeProps(lookupSourceId, billableObjectTypeKey);
        }
        public Dictionary<string, string> GetBillableObjectTypes(int lookupSourceId)
        {
            return Channel.GetBillableObjectTypes(lookupSourceId);
        }

        public Dictionary<string, string> GetMoneyAccounts(int lookupSourceId)
        {
            return Channel.GetMoneyAccounts(lookupSourceId);
        }

        public void RegisterVoiceOnTelephonyServer(string voiceName, byte[] voice)
        {
            Channel.RegisterVoiceOnTelephonyServer(voiceName, voice);
        }

        public void UnRegisterVoiceFromTelephonyServer(string voiceName)
        {
            Channel.UnRegisterVoiceFromTelephonyServer(voiceName);
        }

        public void ReBoot()
        {
            Channel.ReBoot();
        }

        public TelephonyServerInfoResult GetTelephonyInfoByKey(string tsKey)
        {
            var toReturn = Channel.GetTelephonyInfoByKey(tsKey);
            if (toReturn.Success)
                toReturn.ReconnectGraph();
            return toReturn;
        }

        public byte[] GetRecordedFile(string tsKey, string fileName)
        {
            return Channel.GetRecordedFile(tsKey, fileName);
        }

        public bool Dial(string tsKey, string extn, string dialNo, bool addOutgoingPrefix)
        {
            return Channel.Dial(tsKey, extn, dialNo, addOutgoingPrefix);
        }

        public bool ChannelSpy(string tsKey, string extn, string spiedNo, int spyMode)
        {
            return Channel.ChannelSpy(tsKey, extn, spiedNo, spyMode);
        }
        
        public bool CanDial(string tsKey)
        {
            return Channel.CanDial(tsKey);
        }

        public bool ChannelHangup(string tsKey, string channelName)
        {
            return Channel.ChannelHangup(tsKey, channelName);
        }

        public GeneralResult CheckConnection(int telephonySystemTypeIndex,
                                             string serverAddress,
                                             int serverPort,
                                             string amiUserName,
                                             string amiPassword,
                                             string linuxUserName,
                                             string linuxPassword,
                                             string mysqlUserName,
                                             string mysqlPassword)
        {
            return Channel.CheckConnection(telephonySystemTypeIndex,
                                           serverAddress,
                                           serverPort,
                                           amiUserName,
                                           amiPassword,
                                           linuxUserName,
                                           linuxPassword,
                                           mysqlUserName,
                                           mysqlPassword);
        }

        public Dictionary<string, string> GetIndicators()
        {
            return Channel.GetIndicators();
        }
    }
}