﻿using Agent.Application.Interfaces;
using Agent.Application.ViewModels;
using Septa.TelephonyServer;
using Serilog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace Agent.Application.Services
{
    public class AloVoIPInfoService : IAloVoIPInfoService
    {

        private static Dictionary<string, string> _featureNameValue;
        //the _featureNameValue dictionary is read from the license every _refreshEvery minutes
        private static int _refreshEvery = 5;
        private static DateTime _lastReadTime;

        private string[] _iv = new string[] { "80", "101", "97", "99", "104", "101", "115", "69", "110", "82", "101", "103", "97", "108", "105", "97" };
        private string[] _key = new string[] { "82", "101", "115", "112", "101", "99", "116", "77", "121", "65", "117", "116", "104", "111", "114", "105", "116", "97", "104", "71", "108", "111", "98", "97", "108", "87", "97", "114", "109", "105", "110", "103" };
        //assuming license info format is "Serial@@@@feature1:val1,feature2:val2,...,featureN:valN" 
        private const string serialSeparator = "@@@@";

        static AloVoIPInfoService()
        {
            ReadFeatures();
        }

        internal bool IsFeatureAvailable(string featureName)
        {
            try
            {
                return _featureNameValue.ContainsKey(featureName.Trim().ToLower());
            }
            catch
            {
                return false;
            }
        }

        public InfoViewModel IsFeatureAvailable(byte[] nm)
        {
            Log.Debug("%%%%%%%%%%Entering IsFeaturesAvailable%%%%%%%%%%");
            byte[] result = null;
           var timesAreSynched = false;
            AesManaged aes = new AesManaged();
            string featureName;
            long ticks;
            string featureDate = Decrypt(nm, _key.Select(x => Convert.ToByte(x)).ToArray(), _iv.Select(x => Convert.ToByte(x)).ToArray());
            if (string.IsNullOrEmpty(featureDate))
                Log.Error("Error while decrypting");
            else
            {
                timesAreSynched = AreServerAndClientTimesSynched(featureDate, out ticks);
                if (timesAreSynched && GetFeatureName(featureDate, out featureName))
                {
                    string val = GetFeatureValue(featureName, ticks);
                    result = string.IsNullOrEmpty(val) ? null : Encrypt(val, _key.Select(x => Convert.ToByte(x)).ToArray(), _iv.Select(x => Convert.ToByte(x)).ToArray());
                }
            }
            Log.Debug("%%%%%%%%%%Leaving IsFeaturesAvailable%%%%%%%%%%");
            Log.Debug("Result is " + (result == null ? "NULL" : "NOT NULL"));
            return new InfoViewModel() { 
            Result = result,
            TimesAreSynched = timesAreSynched            
            };
        }

        private string GetFeatureValue(string featureName, long ticks)
        {
            try
            {
                Log.Debug("getting feature value for feature : " + featureName);
                featureName = featureName.Trim().ToLower();
                if (_featureNameValue.ContainsKey(featureName.Trim().ToLower()))
                {
                    Log.Debug("features dictionary contains requested feature. generating result.");
                    string gen = GenerateResult(_featureNameValue[featureName], ticks);
                    Log.Debug("successfully generated the result. value : " + gen);
                    return gen;
                }
                Log.Error("features dictionary does not contains requested feature: " + featureName);
                return null;
            }
            catch (Exception ex)
            {
                Log.Error("error while getting feature value", ex);
                return null;
            }
        }

        private string Decrypt(byte[] bytes, byte[] key, byte[] iv)
        {
            Log.Debug("Decrypting...");
            try
            {
                AesManaged aes = new AesManaged();
                aes.Key = key;
                aes.IV = iv;
                using (MemoryStream ms = new MemoryStream(bytes))
                using (CryptoStream cs = new CryptoStream(ms, aes.CreateDecryptor(), CryptoStreamMode.Read))
                using (StreamReader sr = new StreamReader(cs))
                    return sr.ReadToEnd();
            }
            catch (Exception ex)
            {
                Log.Error("Error in decrypting", ex);
                return null;
            }
        }

        private byte[] Encrypt(string val, byte[] key, byte[] iv)
        {
            Log.Debug("Encrypting result");
            try
            {
                AesManaged aes = new AesManaged();
                aes.Key = key;
                aes.IV = iv;
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    using (StreamWriter sw = new StreamWriter(cs))
                        sw.Write(val);
                    return ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                Log.Error("Error in encrypting", ex);
                return null;
            }
        }

        private bool AreServerAndClientTimesSynched(string featureDate, out long ticks)
        {
            ticks = -1;
            Log.Debug("Checking ticks");
            try
            {
                long clientTicks = GetTicks(featureDate);
                if (clientTicks <= 0)
                    return false;
                DateTime clientDateTime = new DateTime(clientTicks);
                ticks = clientTicks;
                bool result = Math.Abs(DateTime.Now.Subtract(clientDateTime).TotalMinutes) < 2;
                Log.Debug("result is " + result.ToString());
                if (!result)
                {
                    Log.Debug("client time : " + clientDateTime.ToString());
                    Log.Debug("server time : " + DateTime.Now.ToString());
                }
                return result;
            }
            catch (Exception ex)
            {
                Log.Error("Error in client server time.", ex);
                return false;
            }
        }

        private bool GetFeatureName(string featureDate, out string name)
        {
            name = "";
            Log.Debug("getting feature name from decrypted data");
            try
            {
                string result = GetName(featureDate);
                if (string.IsNullOrEmpty(result))
                    return false;
                name = result;
                return true;
            }
            catch (Exception ex)
            {
                Log.Error("Error in getting feature name", ex);
                return false;
            }
        }

        private string GetName(string featureDate)
        {
            Log.Debug("extracting feature name");
            int indexOfDash = featureDate.IndexOf('-');
            if (indexOfDash <= 0)
            {
                Log.Error("passed string is not formatted correctly. excpected : ticks-featurename, actual : " + featureDate);
                return null;
            }
            return featureDate.Substring(indexOfDash + 1);
        }

        //returns -1 if format is invalid
        private long GetTicks(string featureDate)
        {
            Log.Debug("extrancting ticks");
            long res;
            int indexOfDash = featureDate.IndexOf('-');
            if (indexOfDash <= 0)
            {
                Log.Error("passed string is not formatted correctly. Excpected : ticks-featurename, Actual : " + featureDate);
                return -1;
            }
            if (long.TryParse(featureDate.Substring(0, indexOfDash), out res))
            {
                return res;
            }
            Log.Error("ticks are invalid. value : " + featureDate.Substring(0, indexOfDash));
            return -1;
        }

        private string GenerateResult(string result, long ticks)
        {
            return ticks + "-" + result;
        }

        private static void ReadFeatures()
        {
            try
            {
                Log.Debug("entered ReadFeatures to read features dictionary if its null or outdated");
                if (DateTime.Now.Subtract(_lastReadTime).Minutes > _refreshEvery || _featureNameValue == null)
                {
                    Log.Debug("Reading features from license");
                    _featureNameValue = new Dictionary<string, string>();
                    string licenseInfo = WinLicenseWrapper.GetLicenseInfo();
                    Log.Debug("returned data from license info : " + Environment.NewLine + licenseInfo);
                    if (string.IsNullOrEmpty(licenseInfo))
                    {
                        Log.Error("license info is empty. returning");
                        return;
                    }
                    Log.Debug("Getting index of serial separator (@@@@)");
                    int indexOfserialSeparator = licenseInfo.IndexOf(serialSeparator);
                    Log.Debug("index = " + indexOfserialSeparator);
                    if (indexOfserialSeparator == -1)
                    {
                        Log.Error("index of serial separator == -1; returning");
                        return;
                    }
                    Log.Debug("throwing away the first part of license info to get features CSV");
                    licenseInfo = licenseInfo.Substring(indexOfserialSeparator + serialSeparator.Length);
                    Log.Debug("features CSV : " + licenseInfo);
                    foreach (string featureVal in licenseInfo.Split(','))
                    {
                        try
                        {
                            Log.Debug("adding feature : " + featureVal);
                            _featureNameValue.Add(featureVal.Substring(0, featureVal.IndexOf(':')).Trim().ToLower(), featureVal.Substring(featureVal.IndexOf(':') + 1));
                        }
                        catch (Exception ex)
                        {
                            Log.Error("Error in reading features.", ex);
                        }
                    }
                    Log.Debug("Updating _lastReadTime = " + DateTime.Now.ToString());
                    _lastReadTime = DateTime.Now;
                    StringBuilder sb = new StringBuilder();
                    foreach (KeyValuePair<string, string> kvp in _featureNameValue)
                        sb.AppendLine(kvp.Key + "=>" + kvp.Value);
                    Log.Debug("dictionary contents : " + Environment.NewLine + sb.ToString());
                }
                else
                    Log.Debug("features dictionary was up to date");
            }
            catch (Exception ex)
            {
                Log.Error("Error in reading features.", ex);
            }
        }

        public string GetSerial()
        {
            try
            {
                return new SettingService().Serial;
            }
            catch
            {
                return null;
            }
        }

        public bool IsTrial()
        {
            return WinLicenseWrapper.IsTrial();
        }


    }

    internal class WinLicenseWrapper
    {
        [Flags]
        public enum LicenseRestrictionType
        {
            RegRestrictionDays = 0x0001,
            RegRestrictionExec = 0x0002,
            RegRestrictionDate = 0x0004,
            RegRestrictionRuntime = 0x0008,
            RegRestrictionGlobalTime = 0x0010,
            RegRestrictionCountry = 0x0020,
            RegRestrictionHardwareId = 0x0040,
            RegRestrictionNetwork = 0x0080,
            RegRestrictionInstallDate = 0x0100,
            RegRestrictionCreationDate = 0x0200,
            RegRestrictionEmbedUserInfo = 0x0400
        }

        public static string GetLicenseInfo()
        {
            StringBuilder sb = new StringBuilder(10000);
            WinLicenseNative.GetEnvironmentVariableW("WLRegGetLicenseInfo", sb, 10000);
            return sb.ToString();
        }
        public static string GetHardwareId()
        {
            StringBuilder toReturn = new StringBuilder(500);
            WinLicenseNative.GetEnvironmentVariable("WLHardwareGetID", toReturn, 500);
            return toReturn.ToString();
        }
        //public static void LoadDll()
        //{
        //    WinLicenseNative.WLLoadWinlicenseDll();
        //}
        class WinLicenseNative
        {
            //    [DllImport("WinlicenseSDK.dll", EntryPoint = "WLLoadWinlicenseDll", CallingConvention = CallingConvention.StdCall)]
            //public static extern void WLLoadWinlicenseDll();
            [DllImport("Kernel32.dll", CharSet = CharSet.Unicode, EntryPoint = "GetEnvironmentVariableW", CallingConvention = CallingConvention.StdCall)]
            public static extern int GetEnvironmentVariableW(string lpName, StringBuilder lpBuffer, int nSize);
            [DllImport("Kernel32.dll", EntryPoint = "GetEnvironmentVariable", CallingConvention = CallingConvention.StdCall)]
            public static extern int GetEnvironmentVariable(string lpName, StringBuilder lpBuffer, int nSize);
        }

        public static bool IsTrial()
        {
            StringBuilder sb = new StringBuilder(100);

            int licenseRestriction = WinLicenseNative.GetEnvironmentVariable("WLRegGetLicenseRestrictions", sb, 100);

            try
            {
                Log.Debug(string.Format("License Restriction String is : '{0}'", sb.ToString()));
                licenseRestriction = int.Parse(sb.ToString());
                Log.Debug(string.Format("License Restriction Int is : '{0}'", licenseRestriction.ToString()));

                return ((licenseRestriction & (int)LicenseRestrictionType.RegRestrictionDays) == (int)LicenseRestrictionType.RegRestrictionDays); //||

            }
            catch (Exception ex)
            {
                Log.Error("An exception is occurred during reading license restriction status : ", ex);
                return true;
            }
        }

    }


}
