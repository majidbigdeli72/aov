﻿using Agent.Application.Interfaces;
using Agent.Application.Models;
using Agent.Domain.Interfaces;
using System.Linq;

namespace Septa.AloVoip.Domain.Services
{
    public class SampleService : ISampleService
    {
        private IAloVoipContext _context;

        public SampleService(IAloVoipContext context)
        {
            _context = context;
        }

        public Call GetFirstCallInDb()
        {
            return _context.Calls.FirstOrDefault();
        }
    }
}
