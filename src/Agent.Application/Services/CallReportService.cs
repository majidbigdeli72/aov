﻿using Agent.Application.Models;
using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using FarsiToolbox.DateAndTime;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Septa.AloVoip.Domain.Services
{
    public class CallReportService : ICallReportService
    {
        private readonly IAloVoipContext _context;
        private readonly ISettingService _settingService;
        private readonly DbContextServiceBase<TelephonySystem> _telephonySystemService;
        private readonly DbContextServiceBase<CallChannel> _callChannelService;

        public CallReportService(IAloVoipContext context, ISettingService settingService)
        {
            _context = context;
            _settingService = settingService;
            _telephonySystemService = new DbContextServiceBase<TelephonySystem>(context, context.TelephonySystems);
            _callChannelService = new DbContextServiceBase<CallChannel>(context, context.CallChannels);
        }

        public IEnumerable<CallReportModel> GetCallReport(string tsKey,                                                          
                                                          int operatorId,
                                                          int? callDirectionIndex = null,
                                                          int? callTypeIndex = null,
                                                          int? callStatus = null,
                                                          string callStartDateFrom = null,
                                                          string callStartDateTo = null,
                                                          string callEndDateFrom = null,
                                                          string callEndDateTo = null,
                                                          string callDurationFrom = null,
                                                          string callDurationTo = null,
                                                          string callWaitingFrom = null,
                                                          string callWaitingTo = null,
                                                          string phoneNumber = null,
                                                          string callerIdNum = null,
                                                          string callerIdName = null,
                                                          string dialedExtension = null,
                                                          int? callChannelState = null,
                                                          int? queueId = null,
                                                          int? extensionPeerId = null,
                                                          bool isExtensionStarter = false,
                                                          int? extensionResponseIndex = null,
                                                          int? trunkPeerId = null,
                                                          bool isTrunkStarter = false,
                                                          int? trunkResponseIndex = null,
                                                          int pageNumber = 1,
                                                          int pageSize = 30,
                                                          bool isExcel = false)
        {
            var tsId = _telephonySystemService.Find(x => x.Key == tsKey).Id;

            return _context.GetCallReport(tsId,
                                          operatorId,
                                          callDirectionIndex,
                                          callTypeIndex,
                                          callStatus,
                                          GetGregorianDate(callStartDateFrom),
                                          GetGregorianDate(callStartDateTo),
                                          GetGregorianDate(callEndDateFrom),
                                          GetGregorianDate(callEndDateTo),
                                          GetTime(callDurationFrom),
                                          GetTime(callDurationTo),
                                          GetTime(callWaitingFrom),
                                          GetTime(callWaitingTo),
                                          phoneNumber,
                                          callerIdNum,
                                          callerIdName,
                                          dialedExtension,
                                          callChannelState,
                                          queueId,
                                          extensionPeerId,
                                          isExtensionStarter,
                                          extensionResponseIndex,
                                          trunkPeerId,
                                          isTrunkStarter,
                                          trunkResponseIndex,
                                          pageNumber,
                                          pageSize,
                                          isExcel);
        }

        public IEnumerable<CallChannel> GetCallPath(long callId)
        {
            return _callChannelService.List(x => x.CallId == callId).OrderBy(x => x.Id);
        }

        public IEnumerable<AgentReportModel> GetAgentReport(string tsKey, string startDate, string endDate, List<ExtentionModel> extentions, int departmentId)
        {
            var tsId = _telephonySystemService.Find(x => x.Key == tsKey).Id;
            if (extentions == null)
            {
                extentions = new List<ExtentionModel>();
            }
            return _context.GetAgentReport(tsId, GetGregorianDate(startDate), GetGregorianDate(endDate), extentions.ToDataTable(), departmentId);
        }

        public List<ExtentionModel> GetExtentionByDepartmentId(string tsKey, int? departmentId)
        {
            var tsId = _telephonySystemService.Find(x => x.Key == tsKey).Id;
            return _context.GetExtentionByDepartmentId(tsId, departmentId);
        }


        private DateTime? GetGregorianDate(string persianDate)
        {
            if (string.IsNullOrEmpty(persianDate))
                return null;

            PersianDateTime pdate;

            if (PersianDateTime.TryParse(persianDate, out pdate))
                return (DateTime)pdate;

            return null;
        }
        private string GetTime(string time)
        {
            if (string.IsNullOrEmpty(time))
                return null;

            DateTime dt;
            if (DateTime.TryParse(time, out dt))
                return dt.ToString("HH:mm:ss");

            return null;
        }


        public void Dispose()
        {
        }
    }
}