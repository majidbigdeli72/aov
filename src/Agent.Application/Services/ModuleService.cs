﻿using Agent.Application.Interfaces;
using Agent.Domain.Enum;
using Agent.Domain.Interfaces;
using Agent.Domain.Models;
using System.Linq;

namespace Septa.AloVoip.Domain.Services
{
    public class ModuleService : IModuleService
    {
        IAloVoipContext _context;
        public ModuleService(IAloVoipContext context)
        {
            _context = context;
        }

        public Module GetInheritedModule(int moduleId)
        {
            var m = _context.Modules.FirstOrDefault(p => p.Id == moduleId);
            if (m == null)
                return null;

            switch ((ModuleType)m.ModuleTypeIndex)
            {
                //case ModuleType.QueueOperatorAnnouncement:
                //    return _context.ModuleQueueOperatorAnnouncements.FirstOrDefault(p => p.Id == moduleId);
                case ModuleType.QueueOperatorVoting:
                    return _context.ModuleQueueOperatorVotings.FirstOrDefault(p => p.Id == moduleId);
                case ModuleType.SmartReconnect:
                    return _context.ModuleReconnects.FirstOrDefault(p => p.Id == moduleId);
                case ModuleType.SendMessage:
                    break;
                case ModuleType.HelpRequest:
                    break;
                case ModuleType.NumberMatch:
                    break;
                case ModuleType.CustomerClub:
                    return _context.ModuleCustomerClubs.FirstOrDefault(p => p.Id == moduleId);
                case ModuleType.AdvanceFollowMe:
                    return _context.ModuleAdvanceFollowMe.FirstOrDefault(p => p.Id == moduleId);
                case ModuleType.CheckContract:
                    return _context.ModuleCheckContracts.FirstOrDefault(p => p.Id == moduleId);
                case ModuleType.Pay:
                    return _context.ModulePays.FirstOrDefault(p => p.Id == moduleId);
                case ModuleType.RoleConnect:
                    return _context.ModuleRoleConnects.FirstOrDefault(p => p.Id == moduleId);
                case ModuleType.CardtableConnect:
                    break;
                case ModuleType.SayProcessState:
                    break;
                case ModuleType.CallbackRequest:
                    break;
                case ModuleType.IdentityInfoDecision:
                    break;
                default:
                    break;
            }
            return null;
        }
    }
}
