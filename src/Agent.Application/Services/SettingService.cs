﻿using Agent.Domain.Interfaces;
using Microsoft.Web.Administration;
using Microsoft.Win32;
using Serilog;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Xml;

namespace Agent.Application.Services
{
    public class SettingService : ISettingService
    {
        public IPAddress GetSystemIp()
        {
            var xmlDoc = new XmlDocument();
            xmlDoc.Load(AgentAppConfigPath);
            var serverIpNode = xmlDoc.SelectSingleNode("//appSettings/add[@key='serverIp']");
            if (serverIpNode != null)
                return IPAddress.Parse(serverIpNode.Attributes["value"].Value);
            else
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());
                foreach (var ip in host.AddressList)
                {
                    if (ip.AddressFamily == AddressFamily.InterNetwork)
                    {
                        return ip;
                    }
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public void Dispose()
        {

        }

        public string InstallPath
        {
            get
            {
                try
                {
                    RegistryKey localKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                    localKey = localKey.OpenSubKey(@"SOFTWARE\1st\AloVoIP\AloVoipTelephonyServer v1.0");
                    if (localKey != null)
                    {
                        string installPath = localKey.GetValue("InstallPath").ToString();
                        return installPath.EndsWith("\\") ? installPath.Substring(0, installPath.Length - 1) : installPath;
                    }
                    else
                    {
                        string path = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\1st\AloVoIP\AloVoipTelephonyServer v1.0", "InstallPath", "").ToString();
                        return path.EndsWith("\\") ? path.Substring(0, path.Length - 1) : path;
                    }
                }
                catch (Exception ex)
                {
                   Log.Error(ex, "InstallPath");
                    throw;
                }
            }
        }

        public string Serial
        {
            get
            {
                try
                {
                    RegistryKey localKey = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry32);
                    localKey = localKey.OpenSubKey(@"SOFTWARE\1st\AloVoIP\AloVoipTelephonyServer v1.0");
                    if (localKey != null)
                    {
                        string installPath = localKey.GetValue("Serial").ToString();
                        return installPath.EndsWith("\\") ? installPath.Substring(0, installPath.Length - 1) : installPath;
                    }
                    else
                    {
                        string path = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\1st\AloVoIP\AloVoipTelephonyServer v1.0", "Serial", "").ToString();
                        return path.EndsWith("\\") ? path.Substring(0, path.Length - 1) : path;
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "InstallPath");
                    throw;
                }
            }
        }

        public string AgentPath
        {
            get
            {
#if DEBUG
                return InstallPath;
#else
                return Path.Combine(InstallPath, "Agent");
#endif
            }
        }

        public string WebConsolePath
        {
            get
            {
#if DEBUG
                return InstallPath;
#else
                return Path.Combine(InstallPath, "WebConsole");
#endif
            }
        }

        public string WebSiteAddress
        {
            get
            {
                var webSiteAddress = string.Empty;
                using (ServerManager serverManager = new ServerManager())
                {
                    var aloVoIPSite = serverManager.Sites.FirstOrDefault(x => string.Compare(x.Name, "AloVoIPWebConsole", true) == 0 && x.Bindings.Count > 0);
                    if (aloVoIPSite != null)
                    {
                        var protocol = string.IsNullOrEmpty(aloVoIPSite.Bindings[0].Protocol) ? "http" : aloVoIPSite.Bindings[0].Protocol;
                        var host = string.IsNullOrEmpty(aloVoIPSite.Bindings[0].Host) ? "localhost" : aloVoIPSite.Bindings[0].Host;
                        var port = aloVoIPSite.Bindings[0].EndPoint.Port;
                        webSiteAddress = string.Format("{0}://{1}:{2}", protocol, host, port);
                    }
                }
                return webSiteAddress;
            }
        }

        public string AgentVoiceArchivePath
        {
            get
            {
                return Path.Combine(AgentPath, "VoiceArchive");
            }
        }

        public string AgentCommonVoiceArchivePath
        {
            get
            {
                return Path.Combine(AgentPath, "VoiceArchive", "Common");
            }
        }

        public string AgentAppConfigPath
        {
            get
            {
#if DEBUG
                return Path.Combine(InstallPath, "Config\\appSettings.config");
#else
                return Path.Combine(InstallPath, "Agent\\Config\\appSettings.config");
#endif
            }
        }

    }
}
