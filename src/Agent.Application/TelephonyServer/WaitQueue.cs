﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.AloVoip.Agent.TelephonyServer
{
    public static class WaitQueue
    {
        private static Dictionary<int, List<string>> CallQueue = new Dictionary<int, List<string>>();
        
        public static void Enqueue(int moduleId, string value)
        {
            lock (CallQueue)
            {
                if (CallQueue.ContainsKey(moduleId))
                {
                    CallQueue[moduleId].Add(value);
                }
                else
                {
                    List<string> Listvalue = new List<string> { value };
                    CallQueue.Add(moduleId, Listvalue);
                }
            }
        }

        public static int CheckTurn(int moduleId, string value)
        {
            lock (CallQueue)
            {
                return CallQueue[moduleId].IndexOf(value);
            }
        }
        public static string Dequeue(int moduleId)
        {
            lock (CallQueue)
            {
                string result = CallQueue[moduleId].First();
                CallQueue[moduleId].RemoveAt(0);
                if (CallQueue[moduleId].Count == 0)
                {
                    CallQueue.Remove(moduleId);
                }
                return result;
            }
        }

    }
}