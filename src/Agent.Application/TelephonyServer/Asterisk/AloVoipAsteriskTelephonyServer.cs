﻿using Agent.Application.Interfaces;
using Agent.Application.Models;
using Agent.Domain.Enum;
using Agent.Domain.Gateway.Asterisk;
using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using AsterNET.NetStandard.Manager.Action;
using AsterNET.NetStandard.Manager.Response;
using Septa.AloVoip.Agent.Gateway.Asterisk;
using Septa.TelephonyServer.Asterisk;
using Serilog;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using WinSCP;

namespace Septa.AloVoip.Agent.TelephonyServer.Asterisk
{
    public partial class AloVoipAsteriskTelephonyServer : AsteriskTelephonyServer, IAloVoipTelephonyServer
    {
        public Dictionary<TelephonySystemSettingType, string> Settings { get; set; }

        public OperatorSos OperatorSos { get; set; }

        protected string _mysqlUserName;
        protected string _mysqlPassword;

        public AloVoipAsteriskTelephonyServer(TelephonySystem ts, string winscpPath)
            : base(ts.Key, ts.ServerAddress, ts.ServerPort, ts.AmiUsername, ts.AmiPassword, 22, 22, ts.LinuxUsername, ts.LinuxPassword, ts.CountryAreaCode, ts.CityAreaCode,
                 ts.OutgoingPrefix, "from-internal", ts.RecordingsRootPath, winscpPath)
        {
            Id = ts.Id;
            Settings = GetSettings(ts.Settings);
            OperatorSos = ts.OperatorSos;

            _mysqlUserName = ts.MySqlUsername;
            _mysqlPassword = ts.MySqlPassword;

        }

        public void RegisterTeleMarketingPlayVoiceModule(string agiServerIP)
        {
            var commands = new[]
            {
                $"AGI(agi://{agiServerIP}:{AsteriskGateway.BindPort}/{Constants.Modules.TeleMarketingPlayVoice.AgiPrefix}-{Id})"
            };
            RegisterCodeCategory(Constants.Modules.TeleMarketingPlayVoice.CategoryName,
                Constants.Modules.TeleMarketingPlayVoice.AgiCode,
                commands);
        }

        public void RegisterSendFaxModule(string agiServerIP)
        {
            var commands = new[]
            {
                $"AGI(agi://{agiServerIP}:{AsteriskGateway.BindPort}/{Constants.Modules.SendFax.AgiPrefix}-{Id})"
            };
            RegisterCodeCategory(Constants.Modules.SendFax.CategoryName,
                Constants.Modules.SendFax.AgiCode,
                commands);
        }

        public void RegisterOperatorSosModule(string agiCode, string agiServerIP)
        {
            var commands = new[]
            {
                $"AGI(agi://{agiServerIP}:{AsteriskGateway.BindPort}/{Constants.Modules.OperatorSos.AgiPrefix}-{Id})"
            };
            RegisterCodeCategory(Constants.Modules.OperatorSos.CategoryName, agiCode, commands);
        }

        public void RegisterModule(int moduleId, string name, string code, ModuleType moduleType, string agiServerIP, string failedCode)
        {
            if (moduleType == ModuleType.QueueOperatorVoting)
                RegisterQueueModule(moduleId, name, agiServerIP);
            else
                RegisterOtherModule(moduleId, name, code, moduleType, agiServerIP, failedCode);
        }
        private void RegisterOtherModule(int moduleId, string name, string code, ModuleType moduleType, string agiServerIP, string failedCode)
        {
            for (int i = 0; i < DefaultRetryCount; i++)
            {
                var waitFor = 0;
                if (i > 0)
                    waitFor = i * DefaultRetryWait;

                System.Threading.Thread.SpinWait(waitFor);

                try
                {
                    string moduleCategoryName = moduleType + "_" + moduleId;

                    RegisterCodeCategory(moduleCategoryName,
                                         code,
                                         new string[] {
                                             string.Format("AGI(agi://{0}:{1}/{2})", agiServerIP, AsteriskGateway.BindPort, moduleId),
                                             string.Format("Goto({0})", failedCode)
                                         });

                    SaveMiscDest(name, code);

                    break;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error in Asterisk RegisterModule. Retry:{i}, {nameof(moduleId)}:{moduleId}, {nameof(name)}:{name}, {nameof(code)}:{code}, {nameof(moduleType)}:{moduleType}, {nameof(agiServerIP)}:{agiServerIP}");
                }
            }
        }
        private void RegisterQueueModule(int moduleId, string name, string agiServerIP)
        {
            var moduleInfo = GetQueueModuleInfo(moduleId);
            var queueId = moduleInfo.Item1;
            var operatorAnnouncement = moduleInfo.Item2;
            var voting = moduleInfo.Item3;

            for (int i = 0; i < DefaultRetryCount; i++)
            {
                try
                {
                    RegisterAsteriskCategory("queues_additional.conf", queueId, new string[] { "setinterfacevar" }, new string[] { "yes" }, reCreateCategory: false, reCreateVariable: false);

                    if (!_manager.IsConnected())
                        _manager.Login();

                    ManagerResponse response = SendAction(new GetConfigAction("extensions_override_freepbx.conf"));
                    UpdateConfigAction config = new UpdateConfigAction("extensions_override_freepbx.conf", "extensions_override_freepbx.conf", true);
                    if (!response.IsSuccess())
                        return;

                    GetConfigResponse responseConfig = (GetConfigResponse)response;

                    foreach (int key in responseConfig.Categories.Keys)
                    {
                        if (responseConfig.Categories[key] == "ext-queues")
                        {
                            if (!responseConfig.Lines(key).ContainsValue("include=ext-queues-custom"))
                            {
                                config.AddCommand(UpdateConfigAction.ACTION_APPEND, "ext-queues", "include", ">ext-queues-custom");
                            }
                            break;
                        }
                    }
                    if (config.Actions.Count > 0)
                    {
                        config.Reload = string.Empty;
                        SendAction(config);
                    }

                    GetDefaultQueueConfigsAndSaveToOverride(agiServerIP, moduleId, queueId, operatorAnnouncement, voting);
                    return;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error in Asterisk RegisterQueueModule. Retry:{i}, {nameof(moduleId)}:{moduleId}, {nameof(name)}:{name}, {nameof(queueId)}:{queueId}, {nameof(agiServerIP)}:{agiServerIP}");
                    Thread.Sleep(5000);

                    if (i == DefaultRetryCount - 1)
                        throw new Exception("Unable to regiter queue operator voting id:" + moduleId, ex);
                }
            }
        }

        private void GetDefaultQueueConfigsAndSaveToOverride(string agiServerIP, int moduleId, string queueId, bool operatorAnnouncement, bool voting)
        {
            ManagerResponse response = SendAction(new GetConfigAction("extensions_additional.conf"));
            UpdateConfigAction config = new UpdateConfigAction("extensions_additional.conf", "extensions_additional.conf", true);
            if (!response.IsSuccess())
                return;

            var agi = string.Format("agi://{0}:{1}/{2}", agiServerIP, AsteriskGateway.BindPort, moduleId);

            GetConfigResponse responseConfig = (GetConfigResponse)response;

            foreach (int key in responseConfig.Categories.Keys)
            {
                if (responseConfig.Categories[key] == "ext-queues")
                {
                    var queueLines = responseConfig.Lines(key);

                    List<string> values = new List<string>();

                    int remainLines = queueLines.Count;
                    var queueFound = false;
                    foreach (var queueLine in queueLines)
                    {
                        if (!queueLine.Value.StartsWith("exten"))
                            continue;

                        if (queueLine.Value == $"exten={queueId},1,Macro(user-callerid,)")
                            queueFound = true;
                        else if (queueLine.Value.Contains(",1,Macro(user-callerid,)"))
                            queueFound = false;

                        if (!queueFound)
                            continue;

                        var queueLineVal = queueLine.Value.Substring(6, queueLine.Value.Length - 6);

                        if (queueLineVal.Contains(",n(qcall),Queue("))
                        {
                            queueLineVal = queueLineVal.Replace("${QOPTIONS}", "c");
                            if (operatorAnnouncement)
                            {
                                queueLineVal = queueLineVal.Replace("${QAGI}", agi + "-1");
                            }
                            values.Add(">" + queueLineVal);

                            if (voting)
                                values.Add(string.Format(">{0},n,AGI({1}-2)", queueId, agi));
                        }
                        else
                            values.Add(">" + queueLineVal);
                    }

                    var splitedActions = values.Segmentize(20);
                    foreach (var actions in splitedActions)
                    {
                        var vars = new List<string>();
                        for (int i = 0; i < actions.Length; i++)
                        {
                            vars.Add("exten");
                        }
                        RegisterAsteriskCategory("extensions_override_freepbx.conf", "ext-queues", vars.ToArray(), actions.ToArray(), reCreateCategory: false);
                    }

                    break;
                }
            }
        }

        public void UnRegisterModule(int moduleId, ModuleType moduleType)
        {
            if (moduleType == ModuleType.QueueOperatorVoting)
                UnRegisterQueueModule(moduleId);
            else
                UnRegisterOtherModule(moduleId, moduleType);
        }
        private void UnRegisterOtherModule(int moduleId, ModuleType moduleType)
        {
            for (int i = 0; i < DefaultRetryCount; i++)
            {
                var waitFor = 0;
                if (i > 0)
                    waitFor = i * DefaultRetryWait;

                System.Threading.Thread.SpinWait(waitFor);

                try
                {
                    string moduleCategoryName = moduleType + "_" + moduleId;

                    if (!_manager.IsConnected())
                        _manager.Login();

                    ManagerResponse response = SendAction(new GetConfigAction("extensions_custom.conf"));
                    UpdateConfigAction config = new UpdateConfigAction("extensions_custom.conf", "extensions_custom.conf", true);
                    if (!response.IsSuccess())
                        return;

                    GetConfigResponse responseConfig = (GetConfigResponse)response;

                    if (responseConfig.Categories.Values.Contains(moduleCategoryName))
                        config.AddCommand(UpdateConfigAction.ACTION_DELCAT, moduleCategoryName);

                    if (config.Actions.Count > 0)
                    {
                        config.Reload = string.Empty;
                        SendAction(config);
                        Reload();
                    }

                    break;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error in Asterisk UnRegisterModule. Retry:{i}, {nameof(moduleId)}:{moduleId}, {nameof(moduleType)}:{moduleType}");
                }
            }
        }
        private void UnRegisterQueueModule(int moduleId)
        {
        }

        private void RegisterCodeCategory(string catName, string code, string[] cmds)
        {
            if (!_manager.IsConnected())
                _manager.Login();

            ManagerResponse response = SendAction(new GetConfigAction("extensions_custom.conf"));
            UpdateConfigAction config = new UpdateConfigAction("extensions_custom.conf", "extensions_custom.conf", true);
            if (!response.IsSuccess())
                return;

            GetConfigResponse responseConfig = (GetConfigResponse)response;
            foreach (int key in responseConfig.Categories.Keys)
            {
                if (responseConfig.Categories[key] == "from-internal-custom")
                {
                    if (!responseConfig.Lines(key).ContainsValue("include=" + catName))
                    {
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, "from-internal-custom", "include", ">" + catName);
                    }
                    break;
                }
            }

            if (responseConfig.Categories.Values.Contains(catName))
                config.AddCommand(UpdateConfigAction.ACTION_DELCAT, catName);

            config.AddCommand(UpdateConfigAction.ACTION_NEWCAT, catName);

            for (int i = 0; i < cmds.Length; i++)
            {
                config.AddCommand(UpdateConfigAction.ACTION_APPEND, catName, "exten", string.Format(">_{0},{1},{2}",
                    code,
                    i == 0 ? "1" : "n",
                    cmds[i]
                ));
            }

            if (config.Actions.Count > 0)
            {
                config.Reload = string.Empty;
                SendAction(config);
                //LoggingProvider.ServiceLogger.Debug("InitialConfigurator DoChannelSpy_SurveyConfig successfully");
            }
        }
        private void RegisterAsteriskCategory(string fileName, string catName, string[] vars, string[] values, bool reCreateCategory = true, bool reCreateVariable = true)
        {
            Log.Debug("Start Registering AsteriskCategory. fileName:" + fileName + ", catName:" + catName + ", vars:{@vars}, values:{@values}", vars, values);

            if (!_manager.IsConnected())
                _manager.Login();

            ManagerResponse response = SendAction(new GetConfigAction(fileName));
            UpdateConfigAction config = new UpdateConfigAction(fileName, fileName, true);
            if (!response.IsSuccess())
                return;

            GetConfigResponse responseConfig = (GetConfigResponse)response;

            if (reCreateCategory)
            {
                if (responseConfig.Categories.Values.Contains(catName))
                    config.AddCommand(UpdateConfigAction.ACTION_DELCAT, catName);

                config.AddCommand(UpdateConfigAction.ACTION_NEWCAT, catName);
            }

            for (int i = 0; i < vars.Length; i++)
            {
                if (reCreateVariable)
                {
                    config.AddCommand(UpdateConfigAction.ACTION_APPEND, catName, vars[i], values[i]);
                }
                else
                {
                    var categoryKey = responseConfig.Attributes.FirstOrDefault(x => x.Key.Contains("category-") && x.Value == catName).Key.Replace("category-", "");
                    if (!responseConfig.Attributes.Any(x => x.Key.Contains("line-" + categoryKey + "-") && x.Value.Contains("setinterfacevar")))
                    {
                        config.AddCommand(UpdateConfigAction.ACTION_APPEND, catName, vars[i], values[i]);
                    }
                }
            }
            if (config.Actions.Count > 0)
            {
                config.Reload = string.Empty;
                SendAction(config);
            }

            Log.Debug("Registering AsteriskCategory finished. fileName:" + fileName + ", catName:" + catName);
        }


        public void ReloadModules()
        {
            for (int i = 0; i < DefaultRetryCount; i++)
            {
                var waitFor = 0;
                if (i > 0)
                    waitFor = i * DefaultRetryWait;

                System.Threading.Thread.SpinWait(waitFor);

                try
                {
                    MiscDestinations = GetMiscDestinations();
                    Log.Debug("Asterisk ReloadModules. MiscDestinations: {@MiscDestinations}", MiscDestinations);

                    List<string> vars = new List<string>();
                    List<string> values = new List<string>();

                    vars.Add("include");
                    values.Add("> ext-miscdests-custom");

                    foreach (var desc in MiscDestinations)
                    {
                        vars.Add("exten");
                        values.Add($"> {desc.Id},1,Noop(MiscDest: {desc.Name})");

                        vars.Add("exten");
                        values.Add($"> {desc.Id},n,Goto(from-internal,{desc.Dial},1)");
                    }

                    RegisterAsteriskCategory("extensions_additional.conf", "ext-miscdests", vars.ToArray(), values.ToArray());

                    Reload();

                    break;
                }
                catch (Exception ex)
                {
                    Log.Error(ex, $"Error in Asterisk ReloadModules. Retry:{i}");
                }
            }
        }


        public void RegisterVoice(string voiceName, byte[] voice)
        {
            Log.Debug(string.Format("Start registering voice: {0}, Length: {1}", voiceName, voice == null ? 0 : voice.Length));

            if (string.IsNullOrEmpty(voiceName) || voice == null || voice.Length == 0)
                return;

            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = SupportSftp ? Protocol.Sftp : Protocol.Ftp,
                    HostName = _serverAddress,
                    PortNumber = _ftpPort,
                    UserName = _linuxUserName,
                    Password = _linuxPassword,
                    GiveUpSecurityAndAcceptAnySshHostKey = SupportSftp
                };

                using (Session session = new Session())
                {
                    session.ExecutablePath = WinscpPath;

                    // Connect
                    session.Open(sessionOptions);


                    var localPath = Path.Combine(Path.GetTempPath(), voiceName + ".wav");
                    File.WriteAllBytes(localPath, voice);

                    session.PutFiles(localPath, "/var/lib/asterisk/sounds/custom/").Check();

                    File.Delete(localPath);
                }

                Log.Debug(string.Format("Registering voice: {0} finished.", voiceName));
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in AsteriskTelephonyServer.RegisterVoice");
            }
        }

        public void UnRegisterVoice(string voiceName)
        {
            Log.Debug(string.Format("Start unRegistering voice: {0}", voiceName));

            if (string.IsNullOrEmpty(voiceName))
                return;

            try
            {
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = SupportSftp ? Protocol.Sftp : Protocol.Ftp,
                    HostName = _serverAddress,
                    PortNumber = _ftpPort,
                    UserName = _linuxUserName,
                    Password = _linuxPassword,
                    GiveUpSecurityAndAcceptAnySshHostKey = SupportSftp
                };

                using (Session session = new Session())
                {
                    session.ExecutablePath = WinscpPath;

                    // Connect
                    session.Open(sessionOptions);
                    session.RemoveFiles("/var/lib/asterisk/sounds/custom/" + voiceName + ".wav").Check();
                }

                Log.Debug(string.Format("UnRegistering voice: {0} finished.", voiceName));
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error in AsteriskTelephonyServer.UnRegisterVoice, {nameof(voiceName)}:{voiceName}");
            }
        }


        private Dictionary<TelephonySystemSettingType, string> GetSettings(ICollection<TelephonySystemSetting> settings)
        {
            return (settings != null && settings.Count > 0) ?
                    settings.ToDictionary(x => ((TelephonySystemSettingType)Enum.Parse(typeof(TelephonySystemSettingType), x.Key, true)), z => z.Value) :
                    new Dictionary<TelephonySystemSettingType, string>();
        }
    }
}