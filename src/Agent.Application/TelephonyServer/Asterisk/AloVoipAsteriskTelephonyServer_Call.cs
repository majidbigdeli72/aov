﻿using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Septa.TelephonyServer.Entries;
using Serilog;
using System.Linq;

namespace Septa.AloVoip.Agent.TelephonyServer.Asterisk
{
    public partial class AloVoipAsteriskTelephonyServer
    {

        protected override long CallCreatedPersist(CallEntry e)
        {
            Log.Debug("OnCallCreatedPersist...");

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var call = e.ToCall();
                context.Calls.Add(call);

                foreach (var channel in e.Channels)
                {
                    if (channel.IsLocal)
                        continue;

                    var callChannel = channel.ToCallChannel();
                    call.CallChannels.Add(callChannel);
                }
                context.SaveChanges();

                call.CallChannels.ToList().ForEach(x =>
                {
                    var callChannelEntry = e.Channels.FirstOrDefault(p => p.UniqueId == x.UniqueId);
                    if (callChannelEntry != null)
                        callChannelEntry.DbId = x.Id;
                });

                if (!e.InitByChannel.IsLocal)
                {
                    call.InitByChannelId = e.InitByChannel.DbId;
                    context.SaveChanges();
                }

                UpdateCallCenterDashboardExtension(e.InitByChannel.OwnerPeer.Name, e.InitByChannel.OwnerPeer.Type);

                return call.Id;
            }
        }
        protected override void CallUpdatedPersist(CallEntry e)
        {
            Log.Debug("OnCallUpdatedPersist...");

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var call = e.ToCall();
                context.Calls.Upsert(call);
                context.SaveChanges();
            }

            UpdateCallCenterDashboardExtension(e.InitByChannel.OwnerPeer.Name, e.InitByChannel.OwnerPeer.Type);
        }
        protected override long CallChannelCreatedPersist(CallChannelEntry e)
        {
            Log.Debug("OnCallChannelCreatedPersist...");

            long callChannelId = 0;

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var callChannel = e.ToCallChannel();
                context.CallChannels.Add(callChannel);

                context.SaveChanges();

                callChannelId = callChannel.Id;
            }

            UpdateCallCenterDashboardExtension(e.OwnerPeer.Name, e.OwnerPeer.Type);

            return callChannelId;
        }
        protected override void CallChannelUpdatedPersist(CallChannelEntry e, PeerEntry oldPeer)
        {
            Log.Debug("OnCallChannelUpdatedPersist...");

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var callChannel = e.ToCallChannel();
                context.CallChannels.Upsert(callChannel);

                context.SaveChanges();
            }

            UpdateQueueDashboardStatsChanges();
            UpdateCallCenterDashboardExtension(e.OwnerPeer.Name, e.OwnerPeer.Type);
			if (oldPeer != null)
				UpdateCallCenterDashboardExtension(oldPeer.Name, oldPeer.Type);
		}
        protected override void CallDestroyedPersist(CallEntry toDestroy, CallEntry main)
        {
            Log.Debug("OnCallDestroyedPersist...");

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var toDestroyCall = toDestroy.ToCall();
                var mainCall = main.ToCall();
                var toMoveChannels = context.CallChannels.Where(p => p.CallId == toDestroyCall.Id).ToList();

                foreach (var ch in toMoveChannels)
                    ch.CallId = mainCall.Id;
                context.SaveChanges();

                var toDeleteCall = context.Calls.FirstOrDefault(p => p.Id == toDestroyCall.Id);
                if (toDeleteCall != null)
                {
                    context.Calls.Remove(toDeleteCall);
                    context.SaveChanges();
                }

                UpdateCallCenterDashboardExtension(toDestroy.InitByChannel.OwnerPeer.Name, toDestroy.InitByChannel.OwnerPeer.Type);
            }
        }
    }
}
