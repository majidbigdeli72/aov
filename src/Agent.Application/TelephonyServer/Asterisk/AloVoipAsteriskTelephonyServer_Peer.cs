﻿using Agent.Application.Models;
using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Septa.TelephonyServer;
using Serilog;
using System.Linq;

namespace Septa.AloVoip.Agent.TelephonyServer.Asterisk
{
    public partial class AloVoipAsteriskTelephonyServer
    {
        protected override void PeersPersist()
        {
            base.PeersPersist();

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                // Fetch and sync peers from DB and Memory
                var dbPeers = context.Peers.Where(x => x.TsId == Id).ToList();
                var lmPeers = Peers.Where(x => dbPeers.All(z => z.Code != x.Name)).ToList();
                foreach (var peer in dbPeers)
                {
                    var peerSelected = Peers.FirstOrDefault(x => x.Name == peer.Code && x.Protocol == (PeerProtocolType)peer.PeerProtocolTypeIndex);
                    if (peerSelected == null)
                    {
                        peer.Enabled = false;
                    }
                    else
                    {
                        peer.Enabled = true;
                        peer.PeerTypeIndex = (int)peerSelected.Type;
                        peer.PeerProtocolTypeIndex = (int)peerSelected.Protocol;
                    }
                }
                if (dbPeers.Any())
                {
                    context.Peers.UpsertRange(dbPeers.ToArray());
                }
                foreach (var newPeer in lmPeers)
                {
                    context.Peers.Add(new Peer
                    {
                        TsId = Id,
                        Code = newPeer.Name,
                        PeerTypeIndex = (int)newPeer.Type,
                        PeerProtocolTypeIndex = (int)newPeer.Protocol,
                        Enabled = true
                    });
                }
                context.SaveChanges();
                foreach (var peer in context.Peers.Where(x => x.TsId == Id && x.Enabled).ToList())
                {
                    var find = Peers.FirstOrDefault(x => x.Name == peer.Code);
                    if (find != null)
                        find.DbId = peer.Id;
                }
            }

            Log.Debug("InitExtentionsAndTrunks for AsteriskTelephonyServer finished.");
        }
    }
}
