﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Septa.AloVoip.Agent.TelephonyServer.Asterisk.DataBaseHelper
{
    public static class ExtensionHelper
    {
        /// <summary>
        /// Converts datatable to list<T> dynamically
        /// </summary>
        /// <typeparam name="T">Class name</typeparam>
        /// <param name="dataTable">data table to convert</param>
        /// <returns>List<T></returns>
        public static List<T> ToList<T>(this DataTable dataTable) where T : new()
        {
            var dataList = new List<T>();

            //Define what attributes to be read from the class
            const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance;

            //Read Attribute Names and Types
            var objFieldNames = typeof(T).GetProperties(flags).Cast<PropertyInfo>().
                Select(item => new
                {
                    Name = item.Name,
                    Type = Nullable.GetUnderlyingType(item.PropertyType) ?? item.PropertyType
                }).ToList();

            //Read Datatable column names and types
            var dtlFieldNames = dataTable.Columns.Cast<DataColumn>().
                Select(item => new {
                    Name = item.ColumnName,
                    Type = item.DataType
                }).ToList();

            foreach (DataRow dataRow in dataTable.AsEnumerable().ToList())
            {
                var classObj = new T();

                foreach (var dtField in dtlFieldNames)
                {
                    PropertyInfo propertyInfos = classObj.GetType().GetProperty(dtField.Name);

                    var field = objFieldNames.Find(x => x.Name == dtField.Name);

                    if (field != null)
                    {

                        if (propertyInfos.PropertyType == typeof(DateTime))
                        {
                            propertyInfos.SetValue(classObj, ConvertToDateTime(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(int))
                        {
                            propertyInfos.SetValue(classObj, ConvertToInt(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(long))
                        {
                            propertyInfos.SetValue(classObj, ConvertToLong(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(decimal))
                        {
                            propertyInfos.SetValue(classObj, ConvertToDecimal(dataRow[dtField.Name]), null);
                        }
                        else if (propertyInfos.PropertyType == typeof(String))
                        {
                            propertyInfos.SetValue(classObj, ConvertToString(dataRow[dtField.Name]), null);
                        }
                    }


                }
                dataList.Add(classObj);
            }
            return dataList;
        }

        private static string ConvertToString(object value)
        {
            if (value == null)
                return string.Empty;
            else
                return Convert.ToString(value);
        }

        private static int ConvertToInt(object value)
        {
            if (value == null)
                return 0;
            else
                return Convert.ToInt32(value);
        }

        private static long ConvertToLong(object value)
        {
            if (value == null)
                return 0;
            else
                return Convert.ToInt64(value);
        }

        private static decimal ConvertToDecimal(object value)
        {
            if (value == null)
                return 0;
            else
                return Convert.ToDecimal(value);
        }

        private static DateTime ConvertToDateTime(object date)
        {
            if (date == null)
                return DateTime.MinValue;
            else
                return Convert.ToDateTime(date);
        }
    }
}
