﻿using MySql.Data.MySqlClient;
using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Generic;
using System.Data;

namespace Septa.AloVoip.Agent.TelephonyServer.Asterisk.DataBaseHelper
{
    public class MySqlConnectionHelper
    {
        private static object _myLock = new object();

        private static MySqlConnection GetConnection(string serverIp, string username, string password)
        {
            var toReturn = new MySqlConnection(GetMySqlConnectionString(serverIp, username, password));
            return toReturn;
        }

        public static List<T> GetDestination<T>(string serverIp, string username, string password)
        {
            var type = typeof(T);

            Log.Debug("Start GetDestination. Type:{@T}", type);
            lock (_myLock)
                using (MySqlConnection connection = GetConnection(serverIp, username, password))
                {
                    string cmdText = string.Empty;
                    DataSet ds = null;

                    if (type == typeof(AnnouncementEntry))
                        cmdText = "Select announcement_id as Id, CONVERT(CAST(description as BINARY) USING utf8) as Name From announcement";
                    else if (type == typeof(CallFlowControlEntry))
                        cmdText = "Select ext as Id, CONVERT(CAST(dest as BINARY) USING utf8) as Name From daynight";
                    else if (type == typeof(CallRecordingEntry))
                        cmdText = "Select callrecording_id as Id, CONVERT(CAST(description as BINARY) USING utf8) as Name From callrecording";
                    else if (type == typeof(CallBackEntry))
                        cmdText = "Select callback_id as Id, CONVERT(CAST(description as BINARY) USING utf8) as Name From callback";
                    else if (type == typeof(ConferenceEntry))
                        cmdText = "Select exten as Id, CONVERT(CAST(description as BINARY) USING utf8) as Name From meetme";
                    else if (type == typeof(CustomApplicationEntry))
                        cmdText = "Select custom_dest as Id, CONVERT(CAST(description as BINARY) USING utf8) as Name From custom_destinations";
                    else if (type == typeof(DisaEntry))
                        cmdText = "Select disa_id as Id, CONVERT(CAST(displayname as BINARY) USING utf8) as Name From disa";
                    else if (type == typeof(ExtensionEntry))
                        cmdText = "Select extension as Id, CONCAT(CONVERT(CAST(name as BINARY) USING utf8), ' ', extension) as Name From users";
                    else if (type == typeof(FeatureCodeAdminEntry))
                        cmdText = "Select defaultcode as Id, CONVERT(CAST(description as BINARY) USING utf8) as Name From featurecodes Where enabled=1 And providedest=1";
                    else if (type == typeof(IvrEntry))
                        cmdText = "Select id as Id, CONVERT(CAST(name as BINARY) USING utf8) as Name From ivr_details";
                    else if (type == typeof(LanguageEntry))
                        cmdText = "Select language_id as Id, CONVERT(CAST(description as BINARY) USING utf8) as Name From languages";
                    else if (type == typeof(MiscDestinationEntry))
                        cmdText = "Select id as Id, CONCAT(CONVERT(CAST(description as BINARY) USING utf8), ' ',destdial) as Name, destdial as Dial From miscdests";
                    else if (type == typeof(PagingAndIntercomEntry))
                        cmdText = "Select page_number as Id, page_number as Name From paging_groups";
                    else if (type == typeof(PhonebookDirectoryEntry))
                        return new List<PhonebookDirectoryEntry>() as List<T>;
                    else if (type == typeof(QueuePriorityEntry))
                        cmdText = "Select queueprio_id as Id, CONVERT(CAST(description as BINARY) USING utf8) as Name From queueprio";
                    else if (type == typeof(QueueEntry))
                        cmdText = "Select extension as Id, CONCAT(CONVERT(CAST(descr as BINARY) USING utf8), ' ', extension) as Name From queues_config";
                    else if (type == typeof(RingGroupEntry))
                        cmdText = "Select grpnum as Id, CONCAT(CONVERT(CAST(description as BINARY) USING utf8), ' ', grpnum) as Name From ringgroups";
                    else if (type == typeof(TerminateCallEntry))
                        return GetTerminateCallEntries() as List<T>;
                    else if (type == typeof(TimeConditionEntry))
                        cmdText = "Select timeconditions_id as Id, CONVERT(CAST(displayname as BINARY) USING utf8) as Name From timeconditions";
                    else if (type == typeof(TrunkEntry))
                        cmdText = "Select trunkid as Id, CONVERT(CAST(name as BINARY) USING utf8) as Name From trunks";
                    else if (type == typeof(VoicemailBlastingEntry))
                        cmdText = "Select grpnum as Id, CONVERT(CAST(description as BINARY) USING utf8) as Name From vmblast";


                    try
                    {
                        MySqlCommand cmd = connection.CreateCommand();
                        //cmd.CommandTimeout = int.MaxValue;
                        cmd.CommandText = cmdText;
                        var adapter = new MySqlDataAdapter(cmd);
                        ds = new DataSet();
                        adapter.Fill(ds);

                        if (ds.Tables == null || ds.Tables.Count == 0)
                            return null;

                        if (type == typeof(AnnouncementEntry))
                            return ds.Tables[0].ToList<AnnouncementEntry>() as List<T>;
                        else if (type == typeof(CallFlowControlEntry))
                            return ds.Tables[0].ToList<CallFlowControlEntry>() as List<T>;
                        else if (type == typeof(CallRecordingEntry))
                            return ds.Tables[0].ToList<CallRecordingEntry>() as List<T>;
                        else if (type == typeof(CallBackEntry))
                            return ds.Tables[0].ToList<CallBackEntry>() as List<T>;
                        else if (type == typeof(ConferenceEntry))
                            return ds.Tables[0].ToList<ConferenceEntry>() as List<T>;
                        else if (type == typeof(CustomApplicationEntry))
                            return ds.Tables[0].ToList<CustomApplicationEntry>() as List<T>;
                        else if (type == typeof(DisaEntry))
                            return ds.Tables[0].ToList<DisaEntry>() as List<T>;
                        else if (type == typeof(ExtensionEntry))
                            return ds.Tables[0].ToList<ExtensionEntry>() as List<T>;
                        else if (type == typeof(FeatureCodeAdminEntry))
                            return ds.Tables[0].ToList<FeatureCodeAdminEntry>() as List<T>;
                        else if (type == typeof(IvrEntry))
                            return ds.Tables[0].ToList<IvrEntry>() as List<T>;
                        else if (type == typeof(LanguageEntry))
                            return ds.Tables[0].ToList<LanguageEntry>() as List<T>;
                        else if (type == typeof(MiscDestinationEntry))
                            return ds.Tables[0].ToList<MiscDestinationEntry>() as List<T>;
                        else if (type == typeof(PagingAndIntercomEntry))
                            return ds.Tables[0].ToList<PagingAndIntercomEntry>() as List<T>;
                        else if (type == typeof(PhonebookDirectoryEntry))
                            return ds.Tables[0].ToList<PhonebookDirectoryEntry>() as List<T>;
                        else if (type == typeof(QueuePriorityEntry))
                            return ds.Tables[0].ToList<QueuePriorityEntry>() as List<T>;
                        else if (type == typeof(QueueEntry))
                            return ds.Tables[0].ToList<QueueEntry>() as List<T>;
                        else if (type == typeof(RingGroupEntry))
                            return ds.Tables[0].ToList<RingGroupEntry>() as List<T>;
                        else if (type == typeof(TerminateCallEntry))
                            return ds.Tables[0].ToList<TerminateCallEntry>() as List<T>;
                        else if (type == typeof(TimeConditionEntry))
                            return ds.Tables[0].ToList<TimeConditionEntry>() as List<T>;
                        else if (type == typeof(TrunkEntry))
                            return ds.Tables[0].ToList<TrunkEntry>() as List<T>;
                        else if (type == typeof(VoicemailBlastingEntry))
                            return ds.Tables[0].ToList<VoicemailBlastingEntry>() as List<T>;
                        else
                            return null;

                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Error in GetDestination.");
                        throw;
                    }
                }
        }

        public static List<TerminateCallEntry> GetTerminateCallEntries()
        {
            var lst = new List<TerminateCallEntry>()
                {
                    new TerminateCallEntry() { Id = "hangup", Name = "Hangup" },
                    new TerminateCallEntry() { Id = "congestion", Name = "Congestion"},
                    new TerminateCallEntry() { Id = "busy", Name = "Busy" },
                    new TerminateCallEntry() { Id = "zapateller", Name = "Play SIT Tone (Zapateller)" },
                    new TerminateCallEntry() { Id = "musiconhold", Name = "Put caller on hold forever" },
                    new TerminateCallEntry() { Id = "ring", Name = "Play ringtones to caller until they hangup" }
                };
            return lst;
        }

        public static void SaveMiscDest(string serverIp, string username, string password, string name, string code)
        {
            Log.Debug($"Start Saving MiscDest. name:{name}, code:{code}");
            lock (_myLock)
                using (MySqlConnection connection = GetConnection(serverIp, username, password))
                {
                    try
                    {
                        connection.Open();

                        MySqlCommand cmd = connection.CreateCommand();
                        //cmd.CommandTimeout = int.MaxValue;
                        cmd.CommandText = $"Select id From miscdests Where destdial='{code}'";
                        var result = cmd.ExecuteScalar();
                        if (result == null)
                        {
                            // Insert New
                            MySqlCommand insertCmd = connection.CreateCommand();
                            insertCmd.CommandText = $"Insert Into miscdests (description, destdial) Values (CAST('{name}' as BINARY), '{code}')";
                            insertCmd.ExecuteNonQuery();
                        }
                        else
                        {
                            // Update
                            MySqlCommand updateCmd = connection.CreateCommand();
                            updateCmd.CommandText = $"Update miscdests Set description=CAST('{name}' as BINARY) Where destdial='{code}'";
                            updateCmd.ExecuteNonQuery();
                        }

                        Log.Debug("Saving MiscDest finished.");
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Error in Saving MiscDest.");

                        throw;
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
        }

        public static string GetMySqlConnectionString(string serverIp, string username, string password)
        {
            return $"server={serverIp};user={username};database=asterisk;port=3306;password={password};charset=utf8;connection timeout=90";
        }
    }
}
