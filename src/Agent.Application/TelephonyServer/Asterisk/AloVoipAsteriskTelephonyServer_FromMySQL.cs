﻿using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Septa.AloVoip.Agent.TelephonyServer.Asterisk
{
    public partial class AloVoipAsteriskTelephonyServer
    {
        protected override void InitFromMySQL()
        {
            try
            {

                Announcements = DataBaseHelper.MySqlConnectionHelper.GetDestination<AnnouncementEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                CallFlowControls = DataBaseHelper.MySqlConnectionHelper.GetDestination<CallFlowControlEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                CallRecordings = DataBaseHelper.MySqlConnectionHelper.GetDestination<CallRecordingEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                CallBacks = DataBaseHelper.MySqlConnectionHelper.GetDestination<CallBackEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                Conferences = DataBaseHelper.MySqlConnectionHelper.GetDestination<ConferenceEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                CustomApplications = DataBaseHelper.MySqlConnectionHelper.GetDestination<CustomApplicationEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                Disas = DataBaseHelper.MySqlConnectionHelper.GetDestination<DisaEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                FeatureCodeAdmins = DataBaseHelper.MySqlConnectionHelper.GetDestination<FeatureCodeAdminEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                Ivrs = DataBaseHelper.MySqlConnectionHelper.GetDestination<IvrEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                Languages = DataBaseHelper.MySqlConnectionHelper.GetDestination<LanguageEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                MiscDestinations = DataBaseHelper.MySqlConnectionHelper.GetDestination<MiscDestinationEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                PagingAndIntercoms = DataBaseHelper.MySqlConnectionHelper.GetDestination<PagingAndIntercomEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                PhonebookDirectories = DataBaseHelper.MySqlConnectionHelper.GetDestination<PhonebookDirectoryEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                QueuePriorities = DataBaseHelper.MySqlConnectionHelper.GetDestination<QueuePriorityEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                RingGroups = DataBaseHelper.MySqlConnectionHelper.GetDestination<RingGroupEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                TerminateCalls = DataBaseHelper.MySqlConnectionHelper.GetDestination<TerminateCallEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                TimeConditions = DataBaseHelper.MySqlConnectionHelper.GetDestination<TimeConditionEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                VoicemailBlastings = DataBaseHelper.MySqlConnectionHelper.GetDestination<VoicemailBlastingEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);

                var trunks = DataBaseHelper.MySqlConnectionHelper.GetDestination<TrunkEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                foreach (var trunk in trunks)
                {
                    var t = Trunks.FirstOrDefault(p => p.Name == trunk.Name);
                    if (t != null)
                        t.Id = trunk.Id;
                }

                var exts = DataBaseHelper.MySqlConnectionHelper.GetDestination<ExtensionEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                foreach (var ext in exts)
                {
                    var e = Extensions.FirstOrDefault(p => p.Id == ext.Id);
                    if (e != null)
                        e.Desc = ext.Name;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in InitFromMySQL for AsteriskTelephonyServer.");
            }
        }
        protected override void InitFromMySQLQueue()
        {
            try
            {
                var queues = DataBaseHelper.MySqlConnectionHelper.GetDestination<QueueEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
                foreach (var q in queues)
                    if (_queues.ContainsKey(q.Id))
                        _queues[q.Id].Name = q.Name;

            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in InitFromMySQL for AsteriskTelephonyServer.");
            }
        }

        private void SaveMiscDest(string name, string code)
        {
            try
            {
                DataBaseHelper.MySqlConnectionHelper.SaveMiscDest(_serverAddress, _mysqlUserName, _mysqlPassword, name, code);
            }
            catch(Exception ex)
            {
                Log.Warning(ex, "Error in SaveMiscDest");
            }
        }

        protected override bool CheckMySqlConnection()
        {
            try
            {
                DataBaseHelper.MySqlConnectionHelper.GetDestination<IvrEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);

                return true;
            }
            catch
            {
                throw;
            }
        }
        private List<MiscDestinationEntry> GetMiscDestinations()
        {
            return DataBaseHelper.MySqlConnectionHelper.GetDestination<MiscDestinationEntry>(_serverAddress, _mysqlUserName, _mysqlPassword);
        }
    }
}
