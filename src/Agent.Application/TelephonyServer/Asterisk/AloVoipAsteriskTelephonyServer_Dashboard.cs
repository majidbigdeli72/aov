﻿using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using Septa.TelephonyServer;
using Serilog;
using System;

namespace Septa.AloVoip.Agent.TelephonyServer.Asterisk
{
    public partial class AloVoipAsteriskTelephonyServer
	{
		private void UpdateQueueDashboardStatsChanges()
		{
			try
			{           
               using (ApplicationConfigurator.ServiceProvider.Scoped<IDashboardService>(out var dashboardService))
				{
					dashboardService.UpdateQueueDashboardStatsChanges(this.Key);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Error in submit update queue to dashboard");
			}
		}
		private void UpdateCallCenterDashboardExtension(string ownerPeerName, PeerType peerType)
		{
			try
			{
				using (ApplicationConfigurator.ServiceProvider.Scoped<IDashboardService>(out var dashboardService))
				{
					dashboardService.UpdateCallCenterDashboardExtension(this.Key, ownerPeerName, peerType);
				}
			}
			catch (Exception ex)
			{
				Log.Error(ex, "Error in submit update peer {peer} to dashboard", ownerPeerName);
			}
		}
	}
}
