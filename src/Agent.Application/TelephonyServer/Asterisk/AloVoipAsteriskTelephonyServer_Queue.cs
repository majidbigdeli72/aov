﻿using Agent.Application.Models;
using Agent.Domain.Enum;
using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Septa.AloVoip.Agent.TelephonyServer.Asterisk
{
    public partial class AloVoipAsteriskTelephonyServer
    {
        protected override void InitQueues()
        {
            base.InitQueues();

            RegisterQueueVariables();
            Log.Debug("InitQueues for AsteriskTelephonyServer finished.");
        }

        public void RegisterQueueVariables()
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<ISettingService>(out var setting))
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                this.RegisterAsteriskCategory("extensions_override_freepbx.conf", "ext-queues", new string[] { }, new string[] { }, true);

                var qovs = context.ModuleQueueOperatorVotings.Where(p => p.TsId == Id).ToList();
                foreach (var qov in qovs)
                {
                    qov.Init(this, setting, false);
                    qov.LastSyncDate = DateTime.Now;

                    context.SaveChanges();
                }

                Reload();
            }
        }

        protected override long QueueCallerJoinedPersist(QueueCallerEntry e)
        {
            Log.Debug("QueueCallerJoinedPersist...");

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var caller = e.ToQueueCaller();
                if (caller != null)
                {
                    context.QueueCallers.Add(caller);
                    context.SaveChanges();
                }

                UpdateQueueDashboardStatsChanges();

                return caller == null ? 0 : caller.Id;
            }
        }

        protected override void QueueCallerLeavedPersist(QueueCallerEntry e)
        {
            Log.Debug("QueueCallerLeavedPersist...");

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var caller = e.ToQueueCaller();
                if (caller != null)
                {
                    context.QueueCallers.Upsert(caller);
                    context.SaveChanges();
                }

                UpdateQueueDashboardStatsChanges();
            }
        }
        protected override void QueueCallerAbandonedPersist(QueueCallerEntry e)
        {
            Log.Debug("QueueCallerAbandonedPersist...");

            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var caller = e.ToQueueCaller();
                if (caller != null)
                {
                    context.QueueCallers.Upsert(caller);
                    context.SaveChanges();
                }

                UpdateQueueDashboardStatsChanges();
            }
        }

        protected override void QueueMemberAddedPersist(QueueMemberEntry e)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var extension = context.Peers.FirstOrDefault(x => x.Code == e.Ext);
                if (extension != null)
                    LogQueueMemberLoginAction(context, extension.Id, e.Queue.DbId, ExtentionActionType.LogoffFromQueue, "وارد صف شد");
                else
                    Log.Warning($"Queue member with extension:'{e.Ext}' not found");
                UpdateQueueDashboardStatsChanges();
            }
        }

        protected override void QueueMemberRemovedPersist(QueueMemberEntry e)
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var extension = context.Peers.FirstOrDefault(x => x.Code == e.Ext);
                if (extension != null)
                    LogQueueMemberLoginAction(context, extension.Id, e.Queue.DbId, ExtentionActionType.LogoffFromQueue, "از صف خارج شده");
                else
                    Log.Warning($"Queue member with extension:'{e.Ext}' not found");
                UpdateQueueDashboardStatsChanges();
            }
        }

        protected override void QueuesPersist()
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                // Fetch and sync queue from DB and Memory
                var dbQueue = context.Queues.Where(x => x.TsId == Id).ToList();
                var lmQueue = _queues.Where(x => dbQueue.All(z => z.Code != x.Key)).ToList();
                foreach (var queue in dbQueue)
                {
                    var queueSelected = _queues.FirstOrDefault(x => x.Key == queue.Code);
                    if (queueSelected.Value == null)
                    {
                        queue.Enabled = false;
                    }
                    else
                    {
                        queue.Enabled = true;
                    }
                }
                if (dbQueue.Any())
                {
                    context.Queues.UpsertRange(dbQueue.ToArray());
                }
                foreach (var newQueue in lmQueue)
                {
                    context.Queues.Add(new Queue
                    {
                        TsId = Id,
                        Enabled = true,
                        Code = newQueue.Key,
                        Name = newQueue.Key,
                        WaitTimeMinThreshold = new TimeSpan(0, 0, 30),
                        WaitTimeCriticalThreshold = new TimeSpan(0, 5, 0),
                        WaitTimeMaxThreshold = new TimeSpan(0, 10, 0)
                    });
                }
                context.SaveChanges();
                foreach (var queue in context.Queues.Where(x => x.TsId == this.Id && x.Enabled).ToList())
                {
                    var find = _queues.First(x => x.Key == queue.Code);
                    {
                        find.Value.DbId = queue.Id;
                    }
                }

                UpdateQueueMembers(context);
            }

            InitFromMySQLQueue();
            UpdateQueueNames();
        }

        private void UpdateQueueMembers(IAloVoipContext context)
        {
            var queueIds = _queues.Select(p => p.Value.DbId).ToArray();
            var allDbQueueMembers = context.QueueMembers.Where(p => queueIds.Contains(p.QueueId)).ToList();
            if (allDbQueueMembers == null || allDbQueueMembers.Count == 0)
                allDbQueueMembers = new List<QueueMember>();

            foreach (var queue in _queues.Values)
            {
                foreach (var queueMemberEntry in queue.Members)
                {
                    var member = allDbQueueMembers.FirstOrDefault(x => x.QueueId == queue.DbId && x.Extension.Code == queueMemberEntry.Ext);
                    if (member == null)
                    {
                        var extension = context.Peers.FirstOrDefault(x => x.Code == queueMemberEntry.Ext);
                        if (extension != null)
                        {
                            var qm = new QueueMember
                            {
                                QueueId = queue.DbId,
                                ExtensionId = extension.Id,
                                IsLogined = true,
                                LastLoginDate = DateTime.Now
                            };
                            context.QueueMembers.Add(qm);
                            context.SaveChanges();
                            queueMemberEntry.DbId = qm.Id;

                            LogQueueMemberLoginAction(context, qm.ExtensionId, queue.DbId, ExtentionActionType.LoginToQueue, "در لیست صف ها اضافه شد");
                        }
                    }
                    else
                    {
                        queueMemberEntry.DbId = member.Id;
                        if (!member.IsLogined)
                        {
                            member.IsLogined = true;

                            LogQueueMemberLoginAction(context, queueMemberEntry.DbId, queue.DbId, ExtentionActionType.LoginToQueue, "ورود به صف");
                        }
                    }
                }
            }

            foreach (var queue in _queues.Values)
            {
                if (queue.Members != null && queue.Members.Count > 0)
                {
                    var allQueueMemberIds = queue.Members.Select(x => x.DbId).ToList();
                    var offlineQueueMembers = allDbQueueMembers.Where(x => x.QueueId == queue.DbId && !allQueueMemberIds.Contains(x.Id));
                    if (offlineQueueMembers != null)
                    {
                        var offlineQueueMemberList = offlineQueueMembers.ToList();
                        if (offlineQueueMembers != null && offlineQueueMemberList.Count > 0)
                        {
                            foreach (var offlineQueueMember in offlineQueueMemberList)
                            {
                                offlineQueueMember.IsLogined = false;
                                context.SaveChanges();

                                LogQueueMemberLoginAction(context, offlineQueueMember.Id, queue.DbId, ExtentionActionType.LogoffFromQueue, "از صف خارج شده");
                            }
                        }
                    }
                }
            }
        }

        private void LogQueueMemberLoginAction(IAloVoipContext context, long extentionId, int queueId, ExtentionActionType extentionActionType, string actionDescription)
        {
            context.ExtentionHistories.Add(new ExtentionHistory()
            {
                ExtentionId = extentionId,
                QueueId = queueId,
                Date = DateTime.Now,
                ExtentionActionTypeIndex = (int)extentionActionType,
                ActionDescription = actionDescription
            });
            context.SaveChanges();
        }

        private void UpdateQueueNames()
        {
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var dbQueues = context.Queues.Where(x => x.TsId == Id).ToList();
                foreach (var dbQueue in dbQueues)
                {
                    var queueEntry = _queues.FirstOrDefault(x => x.Key == dbQueue.Code);
                    if (queueEntry.Value != null && !string.IsNullOrEmpty(queueEntry.Value.Name))
                        dbQueue.Name = queueEntry.Value.Name;
                }
                context.SaveChanges();
            }
            Log.Debug("UpdateQueueNames for AsteriskTelephonyServer finished.");
        }

        private Tuple<string, bool, bool> GetQueueModuleInfo(int moduleId)
        {
            var queueId = string.Empty;
            var operatorAnnouncement = false;
            var voting = false;
            using (ApplicationConfigurator.ServiceProvider.Scoped<IAloVoipContext>(out var context))
            {
                var queue = context.ModuleQueueOperatorVotings.First(p => p.Id == moduleId);
                if (queue != null)
                {
                    queueId = queue.Queue.Code;
                    operatorAnnouncement = queue.OperatorAnnouncement;
                    voting = queue.Voting;
                }
            }

            return new Tuple<string, bool, bool>(queueId, operatorAnnouncement, voting);
        }
    }
}
