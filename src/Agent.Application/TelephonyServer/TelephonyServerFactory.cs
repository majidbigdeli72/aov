﻿using Agent.Application.Interfaces;
using Agent.Application.Models;
using Agent.Domain.Enum;
using Agent.Domain.Helper;
using Agent.Domain.Interfaces;
using Septa.AloVoip.Agent.TelephonyServer.Asterisk;
using System;
using System.IO;

namespace Septa.AloVoip.Agent.TelephonyServer
{
    public class TelephonyServerFactory
    {
        public static IAloVoipTelephonyServer Create(TelephonySystem ts)
        {
            switch ((TelephonySystemType)ts.TelephonySystemTypeIndex)
            {
                case TelephonySystemType.Astrisk:
                    using (ApplicationConfigurator.ServiceProvider.Scoped<ISettingService>(out var settingService))
                    {
                        var agentPath = settingService.AgentPath;
                        var winscpPath = Path.Combine(agentPath, "WinSCP.exe");
                        return new AloVoipAsteriskTelephonyServer(ts, winscpPath);
                    }
                default:
                    throw new Exception("Invalid TelephonySystemType");
            }
        }
    }
}
