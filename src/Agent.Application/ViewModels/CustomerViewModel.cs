﻿using Septa.TelephonyServer;
using Septa.TelephonyServer.Entries;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Agent.Application.ViewModels
{
    public class CustomerViewModel
    {
        [Key]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "The Name is Required")]
        [MinLength(2)]
        [MaxLength(100)]
        [System.ComponentModel.DisplayName("Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "The E-mail is Required")]
        [EmailAddress]
        [System.ComponentModel.DisplayName("E-mail")]
        public string Email { get; set; }

        [Required(ErrorMessage = "The BirthDate is Required")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:yyyy-MM-dd}")]
        [DataType(DataType.Date, ErrorMessage = "Data em formato inválido")]
        [System.ComponentModel.DisplayName("Birth Date")]
        public DateTime BirthDate { get; set; }
    }

    public class GeneralResult
    {
        public bool Success { get; set; }

        public string Message { get; set; }
    }

    public class TelephonyServerInfoResult : GeneralResult
    {
        public int Id { get; set; }
        public string Key { get; set; }
        public string Type { get; set; }
        public string OutgoingPrefix { get; set; }


        public virtual List<PeerEntry> Peers { get; set; }
        public virtual List<TrunkEntry> Trunks { get; set; }
        public virtual List<ExtensionEntry> Extensions { get; set; }
        public virtual List<QueueEntry> Queues { get; set; }
        public virtual List<CallEntry> Calls { get; set; }
        public virtual List<AnnouncementEntry> Announcements { get; set; }
        public virtual List<CallFlowControlEntry> CallFlowControls { get; set; }
        public virtual List<CallRecordingEntry> CallRecordings { get; set; }
        public virtual List<CallBackEntry> CallBacks { get; set; }
        public virtual List<ConferenceEntry> Conferences { get; set; }
        
        public virtual List<CustomApplicationEntry> CustomApplications { get; set; }
        
        public virtual List<DisaEntry> Disas { get; set; }
        
        public virtual List<FeatureCodeAdminEntry> FeatureCodeAdmins { get; set; }
        
        public virtual List<IvrEntry> Ivrs { get; set; }
        
        public virtual List<LanguageEntry> Languages { get; set; }
        
        public virtual List<MiscDestinationEntry> MiscDestinations { get; set; }
        
        public virtual List<PagingAndIntercomEntry> PagingAndIntercoms { get; set; }
        
        public virtual List<PhonebookDirectoryEntry> PhonebookDirectories { get; set; }
        
        public virtual List<QueuePriorityEntry> QueuePriorities { get; set; }
        
        public virtual List<RingGroupEntry> RingGroups { get; set; }
        
        public virtual List<TerminateCallEntry> TerminateCalls { get; set; }
        
        public virtual List<TimeConditionEntry> TimeConditions { get; set; }
        
        public virtual List<VoicemailBlastingEntry> VoicemailBlastings { get; set; }

        public TelephonyServerInfoResult(ITelephonyServer src)
        {
            this.Id = src.Id;
            this.Key = src.Key;
            this.Type = src.Type;
            this.OutgoingPrefix = src.OutgoingPrefix;
            if (src.Initialized)
            {
                this.Peers = src.Peers;
                this.Trunks = src.Trunks;
                this.Extensions = src.Extensions;
                this.Queues = src.Queues;
                this.Calls = src.Calls;
                this.Announcements = src.Announcements;
                this.CallFlowControls = src.CallFlowControls;
                this.CallRecordings = src.CallRecordings;
                this.CallBacks = src.CallBacks;
                this.Conferences = src.Conferences;
                this.CustomApplications = src.CustomApplications;
                this.Disas = src.Disas;
                this.FeatureCodeAdmins = src.FeatureCodeAdmins;
                this.Ivrs = src.Ivrs;
                this.Languages = src.Languages;
                this.MiscDestinations = src.MiscDestinations;
                this.PagingAndIntercoms = src.PagingAndIntercoms;
                this.PhonebookDirectories = src.PhonebookDirectories;
                this.QueuePriorities = src.QueuePriorities;
                this.RingGroups = src.RingGroups;
                this.TerminateCalls = src.TerminateCalls;
                this.TimeConditions = src.TimeConditions;
                this.VoicemailBlastings = src.VoicemailBlastings;
                this.Success = true;
            }
            else
            {
                Success = false;
                Message = "Telephony Server did not initialized yet!!!";
            }
        }

        public TelephonyServerInfoResult(string msg)
        {
            Success = false;
            Message = msg;
        }

        public void ReconnectGraph()
        {
            if (Calls != null && Calls.Count > 0)
            {
                foreach (var call in Calls)
                {
                    if (call.Channels != null && call.Channels.Count > 0)
                    {
                        foreach (var channel in call.Channels)
                            if (channel.ParentCall == null)
                                channel.ParentCall = call;
                    }
                }
            }

            if (Queues != null && Queues.Count > 0)
            {
                foreach (var queue in Queues)
                {
                    if (queue.Callers != null && queue.Callers.Count > 0)
                    {
                        foreach (var caller in queue.Callers)
                            if (caller.ParentQueue == null)
                                caller.ParentQueue = queue;
                    }

                    if (queue.AbandonedCallers != null && queue.AbandonedCallers.Count > 0)
                    {
                        foreach (var caller in queue.AbandonedCallers)
                            if (caller.ParentQueue == null)
                                caller.ParentQueue = queue;
                    }
                }
            }
        }

        public List<CallChannelEntry> Channels
        {
            get
            {
                var toReturn = new List<CallChannelEntry>();
                if (Calls != null && Calls.Count > 0)
                {
                    foreach (var call in Calls)
                    {
                        if (call.Channels != null && call.Channels.Count > 0)
                            toReturn.AddRange(call.Channels);
                    }
                }

                return toReturn;
            }
        }
    }


}
