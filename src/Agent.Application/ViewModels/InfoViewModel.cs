﻿namespace Agent.Application.ViewModels
{
    public class InfoViewModel
    {
        public bool TimesAreSynched { get; set; }

        public byte[] Result { get; set; }
    }
}
