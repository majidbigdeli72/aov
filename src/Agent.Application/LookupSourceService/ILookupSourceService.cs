﻿using Septa.AloVoip.Agent.PgCrmObjectTypeService;
using Septa.AloVoip.Agent.PgIdentityService;
using Septa.AloVoip.Agent.PgUserService;
using Septa.TelephonyServer;
using System;
using System.Collections.Generic;

namespace Septa.AloVoip.Agent.LookupSourceService
{
    public interface ILookupSourceService : ICallStoreService
    {
        IdentityInfo GetIdentityByPhoneNumber(string phoneNumber);
        IdentityInfo GetIdentityByCustomerNumber(string customerNumber);
        IdentityInfo GetIdentityByCustomerInfo(CustomerInfo customerInfo);
        UserInfo GetUserInfoByIdentityInfo(IdentityInfo identityInfo);
        bool IdentityHasValidContract(CustomerInfo customerInfo, string contractKey);
        UserTelephonySystemInfo GetUserExtenstions(string username);
        string GetUserExtenstion(string username, string telephonySystemKey);
        CardtableResultInfo GetCardtable(string crmObjectTypeKey, Guid identityId);

        string GetUserManagerExtenstionBy(int tsId, string userExtenstion);

        decimal? GetCustomerBalance(CustomerInfo customerInfo);
        PaymentInfo GetPaymentInfo(CustomerInfo customerInfo, string billableObjectTypeKey, string billableObjectNumber, string lookupNumberFieldKey, string valueFieldKey);
        bool SendPaymentLinkToUser(PaymentInfo paymentInfo, string mobileNumber, string moneyAccountUserKey, out string resultMessate);

        Dictionary<string, string> GetBillableObjectTypes();
        Dictionary<string, string> GetBillableObjectTypeProps(string billableObjectTypeKey);

        Dictionary<string, string> GetMoneyAccounts();
    }
}