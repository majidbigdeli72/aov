﻿using Agent.Domain.Enum;
using Agent.Domain.Interfaces;
using Septa.AloVoip.Agent.LookupSourceService.Payamgostar;
using System;
using System.Linq;

namespace Septa.AloVoip.Agent.LookupSourceService
{
    public class LookupSourceServiceFactory : ILookupSourceServiceFactory
    {
        private ILookupSourceService _lookupSourceService;
        private readonly IAloVoipContext _dbContext;

        public LookupSourceServiceFactory(IAloVoipContext dbContext)
        {
            if (dbContext == null)
                throw new ArgumentNullException(nameof(dbContext));

            _dbContext = dbContext;
        }

        public ILookupSourceService Create(int? lookupSourceId)
        {
            if (!lookupSourceId.HasValue)
                return null;

            if (_lookupSourceService == null)
            {
                var lookupSource = _dbContext.LookupSources.First(p => p.Id == lookupSourceId.Value);
                switch ((LookupSourceType)lookupSource.LookupSourceTypeIndex)
                {
                    case LookupSourceType.Payamgostar:
                        var payamGostarLookupSource = _dbContext.PayamGostarLookupSources.First(p => p.Id == lookupSource.Id);
                        _lookupSourceService = new PayamgostarLookupSourceService(payamGostarLookupSource);
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return _lookupSourceService;
        }
    }
}
