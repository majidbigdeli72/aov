﻿namespace Septa.AloVoip.Agent.LookupSourceService
{
    public interface ILookupSourceServiceFactory
    {
        ILookupSourceService Create(int? lookupSourceId);
    }
}