﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.AloVoip.Agent.LookupSourceService
{
    public class PaymentInfo
    {
        public Guid IdentityId { get; set; }
        public decimal Amount { get; set; }
    }
}
