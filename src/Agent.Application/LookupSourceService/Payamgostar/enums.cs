﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.AloVoip.Agent.LookupSourceService.Payamgostar
{
    public enum PhoneCallType
    {
        Missed = 1,
        Incoming = 2,
        Outgoing = 3,
        Internal = 4,
    }

    public enum CallChannelState
    {
        Down = 1,
        Ringing = 2,
        Up = 3
    }

    public enum ChannelOwnerType
    {
        Extension = 1,
        Trunk = 2
    }
}
