﻿

using Septa.AloVoip.Agent.PgContractService;
using Septa.AloVoip.Agent.PgCrmObjectService;
using Septa.AloVoip.Agent.PgCrmObjectTypeService;
using Septa.AloVoip.Agent.PgEPayService;
using Septa.AloVoip.Agent.PgIdentityService;
using Septa.AloVoip.Agent.PgInvoiceService;
using Septa.AloVoip.Agent.PgMoneyAccountService;
using Septa.AloVoip.Agent.PgUserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;

namespace Septa.AloVoip.Agent.LookupSourceService.Payamgostar
{
    public class PayamgostarBaseServiceClientFactory<TChannel> : ChannelFactory<TChannel>
    {
        public PayamgostarBaseServiceClientFactory(Binding binding, EndpointAddress remoteAddress) : base(binding, remoteAddress)
        {
        }
    }

    public class PayamgostarServiceClientFactory<TChannel>
    {
        private string ServicePathLocation
        {
            get
            {
                return new Dictionary<Type, string>
                {
                    { typeof(IIdentityChannel), "/services/api/IIdentity.svc"},
                    { typeof(Septa.AloVoip.Agent.PgUserService.IUser), "/services/api/IUser.svc"},
                    { typeof(IUserChannel), "/services/api/IUser.svc"},
                    { typeof(IContractChannel), "/services/api/IContract.svc"},
                    { typeof(ICrmObjectChannel), "/services/api/ICrmObject.svc"},
                    { typeof(ICrmObjectTypeChannel), "/services/api/ICrmObjectType.svc"},
                    { typeof(IEpay), "/services/api/IEpay.svc"},
                    { typeof(IInvoice), "/services/api/IInvoice.svc"},
                    { typeof(IMoneyAccount), "/services/api/IMoneyAccount.svc"},
                }.First(x => x.Key == typeof(TChannel)).Value;
            }
        }

        public TChannel Create(string endPointAddress)
        {
            string uri = $"{endPointAddress}{ServicePathLocation}";

            return new PayamgostarBaseServiceClientFactory<TChannel>(new BasicHttpBinding
            {
                Security = new BasicHttpSecurity(),
                MaxBufferPoolSize = int.MaxValue,
                MaxBufferSize = int.MaxValue,
                MaxReceivedMessageSize = int.MaxValue,
                ReaderQuotas = new System.Xml.XmlDictionaryReaderQuotas
                {
                    MaxArrayLength = int.MaxValue,
                    MaxBytesPerRead = int.MaxValue,
                    MaxDepth = int.MaxValue,
                    MaxNameTableCharCount = int.MaxValue,
                    MaxStringContentLength = int.MaxValue
                }
            }, new EndpointAddress(uri)).CreateChannel();
        }
    }
}
