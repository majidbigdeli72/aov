﻿using Agent.Application.Models;
using Septa.AloVoip.Agent.PgContractService;
using Septa.AloVoip.Agent.PgCrmObjectService;
using Septa.AloVoip.Agent.PgCrmObjectTypeService;
using Septa.AloVoip.Agent.PgEPayService;
using Septa.AloVoip.Agent.PgIdentityService;
using Septa.AloVoip.Agent.PgInvoiceService;
using Septa.AloVoip.Agent.PgMoneyAccountService;
using Septa.AloVoip.Agent.PgUserService;
using Septa.TelephonyServer;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Septa.AloVoip.Agent.LookupSourceService.Payamgostar
{
    public class PayamgostarLookupSourceService : PayamGostarCallStoreService, ILookupSourceService
    {
        public PayamgostarLookupSourceService(PayamGostarLookupSource lookupSource)
            : base(lookupSource.Id.ToString(), lookupSource.Host, lookupSource.Username, lookupSource.Password)
        {
        }

        public ICrmObjectTypeChannel CreateCrmObjectTypeClient()
        {
            return new PayamgostarServiceClientFactory<ICrmObjectTypeChannel>().Create(Host);
        }
        public ICrmObjectChannel CreateCrmObjectChannelClient()
        {
            return new PayamgostarServiceClientFactory<ICrmObjectChannel>().Create(Host);
        }
        public IIdentityChannel CreateIdentityChannelClient()
        {
            return new PayamgostarServiceClientFactory<IIdentityChannel>().Create(Host);
        }
        public IUserChannel CreateUserChannelClient()
        {
            return new PayamgostarServiceClientFactory<IUserChannel>().Create(Host);
        }
        public IContractChannel CreateContractChannelClient()
        {
            return new PayamgostarServiceClientFactory<IContractChannel>().Create(Host);
        }
        public IEpayChannel CreateEpayClient()
        {
            return new PayamgostarServiceClientFactory<IEpayChannel>().Create(Host);
        }
        public IInvoiceChannel CreateInvoiceClient()
        {
            return new PayamgostarServiceClientFactory<IInvoiceChannel>().Create(Host);
        }
        public IMoneyAccountChannel CreateMoneyAccountClient()
        {
            return new PayamgostarServiceClientFactory<IMoneyAccountChannel>().Create(Host);
        }

        public IdentityInfo GetIdentityByPhoneNumber(string phoneNumber)
        {
            try
            {
                using (var identityChannel = CreateIdentityChannelClient())
                {
                    return identityChannel.FindIdentityByPhoneNumber(Username, Password, phoneNumber).IdentityInfo;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error in GetIdentityByPhoneNumber. phoneNumber:{phoneNumber}");
            }

            Log.Debug($"GetIdentityByPhoneNumber, FindIdentityByPhoneNumber result is null. {nameof(phoneNumber)}:{phoneNumber}");
            return null;
        }

        public IdentityInfo GetIdentityByCustomerNumber(string customerNumber)
        {
            try
            {
                using (var identityChannel = CreateIdentityChannelClient())
                {
                    var result = identityChannel.SearchIdentity(Username, Password, string.Empty, $"CustomerNumber==\"{customerNumber}\"");
                    if (result.Success)
                    {
                        if (result.IdentityInfoList.Length == 0)
                        {
                            Log.Debug($"GetIdentityByCustomerNumber, SearchIdentity result is success but identityInfoList is empty. {nameof(customerNumber)}:{customerNumber}");
                            return null;
                        }
                        else
                            return result.IdentityInfoList[0];
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error in GetIdentityByCustomerNumber. customerNumber:{customerNumber}");
            }

            Log.Debug($"GetIdentityByCustomerNumber, SearchIdentity result is null. {nameof(customerNumber)}:{customerNumber}");
            return null;
        }

        public IdentityInfo GetIdentityByCustomerInfo(CustomerInfo customerInfo)
        {
            try
            {
                Guid customerId;
                if (Guid.TryParse(customerInfo.CustomerId, out customerId))
                {
                    using (var identityChannel = CreateIdentityChannelClient())
                    {
                        return identityChannel.FindIdentityById(Username, Password, customerId).IdentityInfo;
                    }
                }
                if (string.IsNullOrEmpty(customerInfo.CustomerNo))
                {
                    return GetIdentityByCustomerNumber(customerInfo.CustomerNo);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in GetIdentityByCustomerInfo. customerInfo: {@customerInfo}", customerInfo);
            }

            Log.Debug("GetIdentityByCustomerInfo result is null. {@customerInfo}", customerInfo);
            return null;
        }

        public UserInfo GetUserInfoByIdentityInfo(IdentityInfo identityInfo)
        {
            try
            {
                using (var userChannel = CreateUserChannelClient())
                {
                    return userChannel.GetUserByIdentityId(Username, Password, identityInfo.CrmId.Value);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in GetUserInfoByIdentityInfo. identityInfo: {@identityInfo}", identityInfo);
            }

            Log.Debug("GetUserInfoByIdentityInfo, GetUserByIdentityId result is null. {@identityInfo}", identityInfo);
            return null;
        }

        public bool IdentityHasValidContract(CustomerInfo customerInfo, string contractKey)
        {
            try
            {
                using (var contractChannel = CreateContractChannelClient())
                {
                    var contracts = contractChannel.SearchContract(Username, Password, contractKey, $"IdentityId==\"{customerInfo.CustomerId}\"");
                    if (!contracts.Success)
                    {
                        Log.Debug("IdentityHasValidContract, SearchContract result is not success. customerInfo:{@customerInfo}, contractKey:{@contractKey}", customerInfo, contractKey);
                        return false;
                    }

                    foreach (var contract in contracts.ContractInfoList)
                    {
                        if (contract.EndDate >= DateTime.Now)
                        {
                            if (contract.BillableObjectState == "User.GeneralPropertyItem.BillableObjectState_2" || contract.BillableObjectState == "تایید و شماره گذاری شده")
                                return true;
                            else
                                Log.Debug("IdentityHasValidContract, SearchContract found but BillableObjectState is not 'User.GeneralPropertyItem.BillableObjectState_2' or 'تایید و شماره گذاری شده'. customerInfo:{@customerInfo}, contractKey:{@contractKey}", customerInfo, contractKey);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in IdentityHasValidContract. customerInfo: {@customerInfo}, contractKey: {@contractKey}", customerInfo, contractKey);
            }

            Log.Debug("IdentityHasValidContract result is false. customerInfo:{@customerInfo}, contractKey:{@contractKey}", customerInfo, contractKey);
            return false;
        }

        public UserTelephonySystemInfo GetUserExtenstions(string username)
        {
            Log.Debug($"GetUserExtenstions. username:{username}");

            try
            {
                using (var userChannel = CreateUserChannelClient())
                {
                    return userChannel.GetUserExtensions(Username, Password, username);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error in GetUserExtenstions. username:{username}");
            }

            Log.Debug($"GetUserExtenstions, GetUserExtensions result is null. {nameof(username)}:{username}");
            return null;
        }

        public string GetUserExtenstion(string username, string telephonySystemKey)
        {
            Log.Debug($"GetUserExtenstion. username:{username}, telephonySystemKey:{telephonySystemKey}");

            try
            {
                using (var userChannel = CreateUserChannelClient())
                {
                    var userTelephonySystemInfo = userChannel.GetUserExtensions(Username, Password, username);
                    if (userTelephonySystemInfo != null &&
                        userTelephonySystemInfo.TelephonySystems == null &&
                        userTelephonySystemInfo.TelephonySystems.Length > 0)
                    {
                        var telephonySystem = userTelephonySystemInfo.TelephonySystems.FirstOrDefault(x => x.Key == telephonySystemKey);
                        if (telephonySystem != null)
                        {
                            var extensions = telephonySystem.Extensions;
                            if (extensions != null && extensions.Length > 0)
                            {
                                return extensions.First().Name;
                            }
                            else
                            {
                                Log.Debug($"GetUserExtenstion, TelephonySystem extension is null or empty.");
                            }
                        }
                        else
                        {
                            Log.Debug($"GetUserExtenstion, TelephonySystem is not found with the given key.");
                        }
                    }
                    else
                    {
                        Log.Debug($"GetUserExtenstion, UserTelephonySystemInfo is null or empty.");
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error in GetUserExtenstion, username:{username}, telephonySystemKey:{telephonySystemKey}");
            }

            Log.Debug($"GetUserExtenstion, GetUserExtensions result is empty. {nameof(username)}:{username}, {nameof(telephonySystemKey)}:{telephonySystemKey}");
            return string.Empty;
        }

        public CardtableResultInfo GetCardtable(string crmObjectTypeKey, Guid identityId)
        {
            Log.Debug($"GetCardtable. crmObjectTypeKey:{crmObjectTypeKey}, identityId:{identityId}");

            try
            {
                using (var crmObjectTypeChannel = CreateCrmObjectTypeClient())
                {
                    return crmObjectTypeChannel.GetCardtable(Username,
                                                             Password,
                                                             null,
                                                             null,
                                                             crmObjectTypeKey,
                                                             identityId,
                                                             SortOperator.Desc,
                                                             0,
                                                             1);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"GetCardtable, crmObjectTypeKey:{crmObjectTypeKey}, identityId:{identityId}");
            }

            return null;
        }

        public string GetUserManagerExtenstionBy(int tsId, string userExtenstion)
        {
            Log.Debug($"GetUserManagerExtenstionBy. {nameof(tsId)}:{tsId}, {nameof(userExtenstion)}:{userExtenstion}");

            try
            {
                using (var userChannel = CreateUserChannelClient())
                {
                    var result = userChannel.GetUserHelperExtensionBy(Username,
                                                                      Password,
                                                                      userExtenstion);
                    if (result.Success)
                        return result.HelperExtension;
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"GetUserManagerExtenstionBy. {nameof(userExtenstion)}:{userExtenstion}");
            }

            Log.Debug($"GetUserManagerExtenstionBy result is null. {nameof(tsId)}:{tsId}, {nameof(userExtenstion)}:{userExtenstion}");
            return null;
        }


        public decimal? GetCustomerBalance(CustomerInfo customerInfo)
        {
            try
            {
                Guid customerId;
                if (Guid.TryParse(customerInfo.CustomerId, out customerId))
                {
                    using (var identityChannel = CreateIdentityChannelClient())
                    {
                        var identityInfoResult = identityChannel.FindIdentityById(Username, Password, new Guid(customerInfo.CustomerId));
                        if (identityInfoResult.Success &&
                            identityInfoResult.IdentityInfo != null &&
                            identityInfoResult.IdentityInfo.Balance.HasValue &&
                            identityInfoResult.IdentityInfo.Balance.Value < 0)
                        {
                            return Math.Abs(identityInfoResult.IdentityInfo.Balance.Value);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in GetCustomerBalance. customerInfo: {@customerInfo}", customerInfo);
            }

            Log.Debug("GetCustomerBalance result is null. customerInfo:{@customerInfo}", customerInfo);
            return null;
        }
        private PaymentInfo GetInvoiceInfo(CustomerInfo customerInfo, string billableObjectTypeKey, string billableObjectNumber, string lookupNumberFieldKey, string valueFieldKey)
        {
            PaymentInfo paymentInfo = null;

            var query = string.Empty;

            if (customerInfo != null)
            {
                Guid customerId;
                if (Guid.TryParse(customerInfo.CustomerId, out customerId))
                {
                    query = $"IdentityId==\"{customerId}\" & ";
                }
            }

            // LookupNumberFieldKey
            if (string.IsNullOrEmpty(lookupNumberFieldKey))
                query = $"Number==\"{billableObjectNumber}\"";
            else
                query = $"{lookupNumberFieldKey}==\"{billableObjectNumber}\"";

            using (var invoiceChannel = CreateInvoiceClient())
            {
                var invoiceInfoResult = invoiceChannel.SearchInvoice(Username,
                                                                     Password,
                                                                     billableObjectTypeKey,
                                                                     query);
                if (invoiceInfoResult.Success &&
                    invoiceInfoResult.InvoiceInfoList != null &&
                    invoiceInfoResult.InvoiceInfoList.Length > 0)
                {
                    var invoice = invoiceInfoResult.InvoiceInfoList[0];

                    paymentInfo = new PaymentInfo();
                    if (invoice.IdentityId.HasValue)
                        paymentInfo.IdentityId = invoice.IdentityId.Value;

                    if (string.IsNullOrEmpty(valueFieldKey))
                    {
                        paymentInfo.Amount = invoice.FinalValue;
                    }
                    else
                    {
                        decimal res = 0;
                        var contractInfoType = invoice.GetType();
                        if (decimal.TryParse(contractInfoType.GetProperty(valueFieldKey).GetValue(invoice).ToString(), out res))
                            paymentInfo.Amount = res;
                    }
                }

                return paymentInfo;
            }
        }
        private PaymentInfo GetContractInfo(CustomerInfo customerInfo, string billableObjectTypeKey, string billableObjectNumber, string lookupNumberFieldKey, string valueFieldKey)
        {
            PaymentInfo paymentInfo = null;

            var query = string.Empty;

            if (customerInfo != null)
            {
                Guid customerId;
                if (Guid.TryParse(customerInfo.CustomerId, out customerId))
                {
                    query = $"IdentityId==\"{customerId}\" & ";
                }
            }

            // LookupNumberFieldKey
            if (string.IsNullOrEmpty(lookupNumberFieldKey))
                query = $"Number==\"{billableObjectNumber}\"";
            else
                query = $"{lookupNumberFieldKey}==\"{billableObjectNumber}\"";

            using (var contractChannel = CreateContractChannelClient())
            {
                var contractInfoResult = contractChannel.SearchContract(Username,
                                                                        Password,
                                                                        billableObjectTypeKey,
                                                                        query);
                if (contractInfoResult.Success &&
                    contractInfoResult.ContractInfoList != null &&
                    contractInfoResult.ContractInfoList.Length > 0)
                {
                    paymentInfo = new PaymentInfo();

                    if (contractInfoResult.ContractInfoList[0].IdentityId.HasValue)
                        paymentInfo.IdentityId = contractInfoResult.ContractInfoList[0].IdentityId.Value;

                    if (string.IsNullOrEmpty(valueFieldKey))
                    {
                        paymentInfo.Amount = contractInfoResult.ContractInfoList[0].FinalValue;
                    }
                    else
                    {
                        decimal res = 0;
                        var contractInfoType = contractInfoResult.ContractInfoList[0].GetType();
                        if (decimal.TryParse(contractInfoType.GetProperty(valueFieldKey).GetValue(contractInfoResult.ContractInfoList[0]).ToString(), out res))
                            paymentInfo.Amount = res;
                    }
                }

                return paymentInfo;
            }
        }
        public PaymentInfo GetPaymentInfo(CustomerInfo customerInfo, string billableObjectTypeKey, string billableObjectNumber, string lookupNumberFieldKey, string valueFieldKey)
        {
            try
            {
                using (var crmObjectType = CreateCrmObjectTypeClient())
                {
                    var crmObjectTypeInfo = crmObjectType.GetCrmObjectTypeInfo(Username, Password, billableObjectTypeKey);
                    if (crmObjectTypeInfo != null)
                    {
                        Log.Debug("PayamgostarLookupSourceService GetPaymentInfo. customerInfo:{@customerInfo}, billableObjectTypeKey:{@billableObjectTypeKey}, billableObjectNumber:{@billableObjectNumber}, lookupNumberFieldKey:{@lookupNumberFieldKey}, valueFieldKey:{@valueFieldKey}, CrmObjectType:{@CrmObjectType}", customerInfo, billableObjectTypeKey, billableObjectNumber, lookupNumberFieldKey, valueFieldKey, crmObjectTypeInfo.CrmObjectType);

                        switch ((CrmObjectTypes)crmObjectTypeInfo.CrmObjectType)
                        {
                            case CrmObjectTypes.Invoice:
                                return GetInvoiceInfo(customerInfo, billableObjectTypeKey, billableObjectNumber, lookupNumberFieldKey, valueFieldKey);
                            case CrmObjectTypes.Quote:
                                break;
                            case CrmObjectTypes.Receipt:
                                break;
                            case CrmObjectTypes.Contract:
                                return GetContractInfo(customerInfo, billableObjectTypeKey, billableObjectNumber, lookupNumberFieldKey, valueFieldKey);
                            case CrmObjectTypes.PurchaseInvoice:
                                break;
                            case CrmObjectTypes.ReturnPurchaseInvoice:
                                break;
                            case CrmObjectTypes.ReturnSaleInvoice:
                                break;
                            case CrmObjectTypes.PurchaseQuote:
                                break;
                            case CrmObjectTypes.Payment:
                                break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error in GetPaymentInfo, billableObjectNumber:{billableObjectNumber}, billableObjectTypeKey:{billableObjectTypeKey}, lookupNumberFieldKey:{lookupNumberFieldKey}");
            }

            return null;
        }

        public bool SendPaymentLinkToUser(PaymentInfo paymentInfo, string mobileNumber, string moneyAccountUserKey, out string resultMessate)
        {
            resultMessate = string.Empty;

            try
            {
                var paymentLinkInfo = new PaymentLinkInfo()
                {
                    IdentityId = paymentInfo.IdentityId,
                    Amount = paymentInfo.Amount,
                    ExpireAfterDays = 7,
                    Description = string.Empty,
                    MoneyAccountUserKey = moneyAccountUserKey,
                    MobilePhoneNumber = mobileNumber
                };

                using (var epayChannel = CreateEpayClient())
                {
                    var paymentLinkInfoResult = epayChannel.CreatePaymentLink(Username, Password, paymentLinkInfo);
                    Log.Debug("SendPaymentLinkToUser, paymentLinkInfoResult:{@paymentLinkInfoResult}", paymentLinkInfoResult);
                    if (paymentLinkInfoResult.Success)
                    {
                        //var paymentLink = paymentLinkInfoResult.PaymentLink;
                        return true;
                    }
                    else
                        resultMessate = paymentLinkInfoResult.Message;
                }
            }
            catch (Exception ex)
            {
                resultMessate = ex.Message;
                Log.Error(ex, "Error in SendPaymentLinkToUser. paymentInfo:{@paymentInfo}, mobileNumber:{@mobileNumber}, moneyAccountUserKey{@moneyAccountUserKey}", paymentInfo, mobileNumber, moneyAccountUserKey);
            }

            return false;
        }


        public Dictionary<string, string> GetBillableObjectTypes()
        {
            try
            {
                using (var crmObjectType = CreateCrmObjectTypeClient())
                {
                    var lstCrmObjectType = crmObjectType.GetCrmObjectTypeList(Username, Password, null);
                    if (lstCrmObjectType != null && lstCrmObjectType.CrmObjectTypeList != null && lstCrmObjectType.CrmObjectTypeList.Length > 0)
                    {
                        var crmObjectTypes = new[]
                        {
                        CrmObjectTypes.Invoice,
                        CrmObjectTypes.Quote,
                        CrmObjectTypes.Receipt,
                        CrmObjectTypes.Contract,
                        CrmObjectTypes.PurchaseInvoice,
                        CrmObjectTypes.ReturnPurchaseInvoice,
                        CrmObjectTypes.ReturnSaleInvoice,
                        CrmObjectTypes.PurchaseQuote,
                        CrmObjectTypes.Payment
                    };

                        var lstBillableObjects = lstCrmObjectType.CrmObjectTypeList.Where(x => crmObjectTypes.Contains(x.CrmObjectType) && x.UserKey != "");
                        if (lstBillableObjects != null)
                        {
                            return lstBillableObjects.ToDictionary(x => x.Name, x => x.UserKey);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in GetBillableObjectTypes.");
            }

            return null;
        }
        public Dictionary<string, string> GetBillableObjectTypeProps(string billableObjectTypeKey)
        {
            try
            {
                using (var crmObjectType = CreateCrmObjectTypeClient())
                {
                    var crmObjectTypeInfo = crmObjectType.GetCrmObjectTypeInfo(Username, Password, billableObjectTypeKey);
                    if (crmObjectTypeInfo != null &&
                        crmObjectTypeInfo.PropertyGroups != null &&
                        crmObjectTypeInfo.PropertyGroups.Length > 0 &&
                        crmObjectTypeInfo.PropertyGroups[0].Properties != null &&
                        crmObjectTypeInfo.PropertyGroups[0].Properties.Length > 0)
                    {
                        var propertyDisplayTypes = new[]
                        {
                    PropertyDisplayType.Text,
                    PropertyDisplayType.Currency,
                    PropertyDisplayType.Number
                };

                        var props = crmObjectTypeInfo.PropertyGroups[0].Properties.Where(x => propertyDisplayTypes.Contains(x.PropertyDisplayType.Value));
                        if (props != null)
                            return props.ToDictionary(x => x.Name, x => x.Key);
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, $"Error in GetBillableObjectTypeProps. billableObjectTypeKey:{billableObjectTypeKey}");
            }

            return null;
        }

        public Dictionary<string, string> GetMoneyAccounts()
        {
            try
            {
                using (var moneyAccountChannel = CreateMoneyAccountClient())
                {
                    var moneyAccountListInfo = moneyAccountChannel.GetMoneyAccountList(Username, Password, 0, 50);
                    if (moneyAccountListInfo != null && moneyAccountListInfo.Items != null && moneyAccountListInfo.Items.Length > 0)
                    {
                        var lstmoneyAccounts = moneyAccountListInfo.Items.Where(x => x.UserKey != "");
                        if (lstmoneyAccounts != null)
                        {
                            return lstmoneyAccounts.ToDictionary(x => x.Name, x => x.UserKey);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in GetMoneyAccounts");
            }

            return null;
        }
    }
}