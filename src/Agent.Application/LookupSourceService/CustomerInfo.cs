﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.AloVoip.Agent.LookupSourceService
{
    public class CustomerInfo
    {
        public string CustomerNo { get; set; }
        public string CustomerId { get; set; }
    }
}
