﻿using Agent.Domain.Interfaces;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.Module;
using Septa.AloVoip.Agent.Module.Exceptions;
using Septa.AloVoip.Agent.PgIdentityService;
using Serilog;
using System;

namespace Agent.Domain.Module
{

    public abstract class BaseModuleManager<T> : BaseEmbededModuleManager, IModuleManager where T : Agent.Domain.Models.Module
    {
        public ILookupSourceService MyLookupSourceService { get; protected set; }
        public T MyModule { get; protected set; }

        public BaseModuleManager(IAloVoipContext context, IGateway gateway, IGatewayRequest request, IGatewayChannel channel, ILookupSourceService lookupSourceService, IAloVoipTelephonyServer telephonyServer)
            : base(context, gateway, request, channel, telephonyServer)
        {
            MyLookupSourceService = lookupSourceService;
        }

        public override void GoTo(string destination, bool endProcess = true)
        {
            Log.Debug($"Log  goTo {destination}");

            if (string.IsNullOrEmpty(destination))
                MyGateway.GoTo(MyModule.FailedCode);
            else
                MyGateway.GoTo(destination);

            if (endProcess)
                throw new GoToException();
        }

        public override void GoToExtension(string destination, bool endProcess = true)
        {
            if (string.IsNullOrEmpty(destination))
                MyGateway.GoTo(MyModule.FailedCode);
            else
                MyGateway.GoToExtension(destination);

            if (endProcess)
                throw new GoToException();
        }

        public override string GetInput(string voiceFileKey, int timeout, int? maxDigits = null, char breakDigit = ' ')
        {
            return base.GetInput(this.MyModule.GetVoiceFileName(voiceFileKey), timeout, maxDigits, breakDigit);
        }


        protected IdentityInfo GetIndentityInfo()
        {
            var customerInfo = MyGateway.CustomerHelper.GetCustomerInfo();
            return MyLookupSourceService.GetIdentityByCustomerInfo(customerInfo);
        }

        protected string GetUserExtenstion(string userName)
        {
            Log.Debug($"Getting UserExtenstion for userName:{userName}, MyModule.TelephonySystem.Key:{MyModule.TelephonySystem.Key}");

            return MyLookupSourceService.GetUserExtenstion(userName, MyModule.TelephonySystem.Key);
        }

        public override bool SilentMode
        {
            get { return MyModule.SilentMode; }
        }

        public override void Process()
        {
            try
            {
                InternalProcess();
            }
            catch (GoToException) { }
            catch (Exception)
            {
                GoTo(MyModule.FailedCode, false);
            }
        }
    }
}