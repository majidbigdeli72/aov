﻿
using Agent.Application.Models;
using Agent.Domain.Interfaces;
using Agent.Domain.Module;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.TelephonyServer;
using Serilog;
using System;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{
    public class QueueOperatorVoting : BaseModuleManager<ModuleQueueOperatorVoting>
    {
        private readonly int _moduleId;
        private readonly string _moduleParams;

        public QueueOperatorVoting(int moduleId,
                                   string moduleParams,
                                   IAloVoipContext context,
                                   IGateway gateway,
                                   IGatewayRequest request,
                                   IGatewayChannel channel,
                                   ILookupSourceService lookupSourceService,
                                   IAloVoipTelephonyServer telephonyServer) : base(context, gateway, request, channel, lookupSourceService, telephonyServer)
        {
            _moduleId = moduleId;
            _moduleParams = moduleParams;
            this.MyModule = context.ModuleQueueOperatorVotings.FirstOrDefault(p => p.Id == moduleId);
        }

        private void OperatorAnnouncement()
        {
            StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.PreAnnounceOperatorVoice)));

            var agentExtenstion = base.GetCurrentQueueMemberExtenstion();
            Log.Debug($"QueueOperatorVoting, OperatorAnnouncement. {nameof(agentExtenstion)}:{agentExtenstion}");

            if (!string.IsNullOrEmpty(agentExtenstion))
            {
                if (MyModule.PlayOperatorExtensionInsteadOfName)
                {
                    // Play Operator Extenstion
                    MyGateway.PlayNumber(agentExtenstion);
                }
                else
                {
                    // Play Operator Name
                    var extension = MyTelephonyServer.Peers.FirstOrDefault(x => x.Type == PeerType.Extension && x.Id == agentExtenstion);
                    if (extension != null)
                    {
                        var peer = DbContext.Peers.FirstOrDefault(x => x.Id == extension.DbId);
                        if (peer != null && peer.OwnerOperatorId.HasValue && peer.Operator != null && peer.Operator.NameVoice != null)
                        {
                            var operatorNameVoiceFile = "OperatorName_" + peer.OwnerOperatorId;
                            StreamFileIfNotSilent(operatorNameVoiceFile);
                        }
                        else
                        {
                            Log.Debug($"QueueOperatorVoting, operator voice name NotFound {nameof(agentExtenstion)}:{agentExtenstion}. playing operator extenstion.");
                            MyGateway.PlayNumber(agentExtenstion);
                        }
                    }
                    else
                    {
                        Log.Debug($"QueueOperatorVoting, operator name NotFound, playing operator extenstion. {nameof(agentExtenstion)}:{agentExtenstion}");
                        MyGateway.PlayNumber(agentExtenstion);
                    }
                }
            }

            StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.PostAnnounceOperatorVoice)));
        }
        private void Vote()
        {
            var currentPeerId = GetCurrentQueueMemberPeerDbId;
            var currentCallId = CurrentCallDbId;
            var currentCallChannelId = CurrentCallChannelDbId;
            
            Log.Debug($"QueueOperatorVoting Vote values. {nameof(currentPeerId)}:{currentPeerId.Value}, {nameof(currentCallId)}:{currentCallId.Value}, {nameof(currentCallChannelId)}:{currentCallChannelId.Value}");

            var voteResult = GetVoteInput();
            if (!voteResult.HasValue)
            {
                MyGateway.Hangup();
                return;
            }

            Log.Debug($"QueueOperatorVoting Vote. {nameof(voteResult)}:{voteResult.Value},  {nameof(currentPeerId)}:{currentPeerId.Value}, {nameof(currentCallId)}:{currentCallId.Value}, {nameof(currentCallChannelId)}:{currentCallChannelId.Value}");

            if (!currentPeerId.HasValue || !currentCallId.HasValue || !currentCallChannelId.HasValue)
            {
                MyGateway.Hangup();
                return;
            }

            // Save voting result
            var votingResult = new ModuleQueueOperatorVotingResult()
            {
                CreateDate = DateTime.Now,
                ModuleQueueOperatorVotingId = MyModule.Id,
                Input = voteResult.Value.ToString(),
                PeerId = currentPeerId.Value,
                CallId = currentCallId.Value,
                CallChannelId = currentCallChannelId.Value,
                IsValid = true,
            };
            DbContext.ModuleQueueOperatorVotingResults.Add(votingResult);
            DbContext.SaveChanges();

            Log.Debug($"QueueOperatorVoting OperatorAnnouncement. votingResult saved");
            StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.SucceedVoice)));

            MyGateway.Hangup();
        }
        private long? GetVoteInput()
        {
            long? voteResult = null;

            for (int i = 0; i <= 2; i++)
            {
                var userInput = GetInputLong(nameof(MyModule.Voice), DefaultGetDataTimeout, 1);
                if (userInput.HasValue && (userInput.Value >= MyModule.InputDigitFrom && userInput.Value <= MyModule.InputDigitTo))
                {
                    voteResult = userInput;
                    break;
                }

                StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.InvalidInputVoice)));
            }

            return voteResult;
        }

        public override void InternalProcess()
        {
            MyGateway.PlayHoldMusic();

            Log.Debug("QueueOperatorVoting InternalProcess. IsCustomerPresent:{@IsCustomerPresent}, CustomerInfo: {@CustomerInfo}, OperatorAnnouncement:{@OperatorAnnouncement}, , Voting:{@Voting}", IsCustomerPresent(), GetCustomerInfo(), MyModule.OperatorAnnouncement, MyModule.Voting);

            if (MyModule.OperatorAnnouncement && _moduleParams == "1")
                OperatorAnnouncement();

            if (MyModule.Voting && _moduleParams == "2")
                Vote();
        }
    }
}