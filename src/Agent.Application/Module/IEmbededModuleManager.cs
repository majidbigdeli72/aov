﻿using Agent.Domain.Interfaces;
using Septa.AloVoip.Agent.Gateway;

namespace Septa.AloVoip.Agent.Module
{
    public interface IEmbededModuleManager
    {
        IGateway MyGateway { get; }
        IGatewayChannel MyChannel { get; }
        IGatewayRequest MyRequest { get; }
        IAloVoipTelephonyServer MyTelephonyServer { get; }

        bool SilentMode { get; }

        void Process();
    }
}