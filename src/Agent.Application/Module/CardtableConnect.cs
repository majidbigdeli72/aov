﻿using Agent.Application.Models;
using Agent.Domain.Interfaces;
using Agent.Domain.Module;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Serilog;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{
    public class CardtableConnect : BaseModuleManager<ModuleCardtableConnect>
    {
        public CardtableConnect(int moduleId,
                                IAloVoipContext context,
                                IGateway gateway,
                                IGatewayRequest request,
                                IGatewayChannel channel,
                                ILookupSourceService lookupSourceService,
                                IAloVoipTelephonyServer telephonyServer) : base(context, gateway, request, channel, lookupSourceService, telephonyServer)
        {
            if (lookupSourceService == null)
                throw new System.ArgumentNullException(nameof(lookupSourceService));

            this.MyLookupSourceService = lookupSourceService;
            this.MyModule = context.ModuleCardtableConnects.FirstOrDefault(x => x.Id == moduleId);
        }

        public override void InternalProcess()
        {
            MyGateway.PlayHoldMusic();

            Log.Debug($"CardtableConnect InternalProcess. IsCustomerPresent:{IsCustomerPresent()}");

            if (!IsCustomerPresent())
                GoTo(MyModule.FailedCode);
            else
            {
                var crmObjectTypeKey = MyModule.CrmObjectTypeKey;
                var crmId = GetIndentityInfo().CrmId.Value;

                Log.Debug($"CardtableConnect InternalProcess. CrmObjectTypeKey: {crmObjectTypeKey}, CrmId: {crmId}");

                var cardtableResultInfo = MyLookupSourceService.GetCardtable(crmObjectTypeKey, crmId);
                if (cardtableResultInfo.Success)
                {
                    if (cardtableResultInfo.CardtableItems != null && cardtableResultInfo.CardtableItems.Length > 0)
                    {
                        var cardtableInfo = cardtableResultInfo.CardtableItems.FirstOrDefault();
                        if (!string.IsNullOrEmpty(cardtableInfo.HolderName))
                        {
                            Log.Debug($"CardtableConnect InternalProcess. CardtableInfo HolderName :{cardtableInfo.HolderName}, Key: {MyModule.TelephonySystem.Key}");

                            var userTelephonySystemResultInfo = MyLookupSourceService.GetUserExtenstions(cardtableInfo.HolderName);
                            if (userTelephonySystemResultInfo.Success)
                            {
                                if (userTelephonySystemResultInfo.TelephonySystems != null && userTelephonySystemResultInfo.TelephonySystems.Length > 0)
                                {
                                    var userTelephonySystem = userTelephonySystemResultInfo.TelephonySystems.FirstOrDefault(x => x.Key == MyModule.TelephonySystem.Key);
                                    if (userTelephonySystem != null &&
                                        userTelephonySystem.Extensions != null &&
                                        userTelephonySystem.Extensions.Length > 0)
                                    {
                                        var userTelephony = userTelephonySystem.Extensions.First();

                                        Log.Debug($"CardtableConnect InternalProcess. userTelephony.Name:{userTelephony.Name}");

                                        GoToExtension(userTelephony.Name);
                                    }
                                    else
                                    {
                                        Log.Debug($"CardtableConnect InternalProcess. UserTelephonySystem or Extensions not found.");
                                    }
                                }
                                else
                                {
                                    Log.Debug($"CardtableConnect InternalProcess. UserTelephonySystem result is success but TelephonySystems contains not element.");
                                }
                            }
                            else
                            {
                                Log.Debug($"CardtableConnect InternalProcess. GetUserExtenstions, UserTelephonySystem result is not success.");
                            }
                        }
                        else
                        {
                            Log.Debug($"CardtableConnect InternalProcess. HolderName is null or empty.");
                        }
                    }
                    else
                    {
                        Log.Debug($"CardtableConnect InternalProcess. Cardtable result is success but cardtableItems contains not element.");
                    }
                }
                else
                {
                    Log.Debug($"CardtableConnect InternalProcess. Cardtable result is not success.");
                }
            }
        }
    }
}