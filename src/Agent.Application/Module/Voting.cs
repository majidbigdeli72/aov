﻿using Agent.Application.Models;
using Agent.Domain.Interfaces;
using Agent.Domain.Module;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Serilog;
using System;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{
    public partial class Voting : BaseModuleManager<ModuleVoting>
    {
        public Voting(int moduleId,
                      IAloVoipContext context,
                      IGateway gateway,
                      IGatewayRequest request,
                      IGatewayChannel channel,
                      ILookupSourceService lookupSourceService,
                      IAloVoipTelephonyServer telephonyServer) : base(context, gateway, request, channel, lookupSourceService, telephonyServer)
        {
            this.MyModule = context.ModuleVotings.FirstOrDefault(p => p.Id == moduleId);
        }

        public override void InternalProcess()
        {
            Log.Debug($"Voting InternalProcess. IsCustomerPresent:{ IsCustomerPresent()}");

            var gotoCode = MyModule.NextCode;

            StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.PreAnnounceVoice)));

            var hasVoted = false;
            foreach (var question in MyModule.ModuleVotingQuestions)
            {
                int? questionItemId = GetQuestionItemIdFromInput(question);
                if (questionItemId.HasValue)
                {
                    hasVoted = true;
                    SaveQuestionVoteResult(questionItemId.Value);
                }
            }

            Log.Debug($"Voting InternalProcess. {nameof(hasVoted)}:{hasVoted}");
            if (!hasVoted)
                gotoCode = MyModule.FailedCode;
            else
                StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.PostAnnounceVoice)));

            GoTo(gotoCode);
        }

        private int? GetQuestionItemIdFromInput(ModuleVotingQuestion question)
        {
            int? questionItemId = null;

            for (int i = 0; i < 2; i++)
            {
                long? userInput = GetInputLong(this.MyModule.VoteQuestionPrefix + question.Id, DefaultGetDataTimeout, 1);
                if (userInput.HasValue && question.ModuleVotingQuestionItems.Any(x => x.InputDigit == userInput.Value))
                {
                    questionItemId = question.ModuleVotingQuestionItems.FirstOrDefault(x => x.InputDigit == userInput.Value).Id;
                    break;
                }

                StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.InvalidInputVoice)));
            }

            return questionItemId;
        }

        private void SaveQuestionVoteResult(int questionItemId)
        {
            var callChannelId = CurrentCallChannelDbId;
            if (!callChannelId.HasValue)
                return;

            var votingResult = new ModuleVotingQuestionResult()
            {
                CreateDate = DateTime.Now,
                ModuleVotingQuestionItemId = questionItemId,
                CallChannelId = callChannelId.Value,
                IsValid = true
            };

            votingResult.PeerId = GetCurrentQueueMemberPeerDbId;
            votingResult.QueueId = CurrentQueueDbId;

            try
            {
                DbContext.ModuleVotingQuestionResults.Add(votingResult);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in saving QuestionVoteResult. votingResult: {@votingResult}", votingResult);
            }

            Log.Debug($"SaveQuestionVoteResult. QuestionItemId:{questionItemId}");
        }
    }
}