﻿using System;
using System.Runtime.Serialization;

namespace Septa.AloVoip.Agent.Module.Exceptions
{
    [Serializable]
    internal class BreakDigitCharDetectedException : Exception
    {
        public BreakDigitCharDetectedException()
        {
        }

        public BreakDigitCharDetectedException(string message) : base(message)
        {
        }

        public BreakDigitCharDetectedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BreakDigitCharDetectedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}