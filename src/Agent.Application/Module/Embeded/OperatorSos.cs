﻿using Agent.Domain.Enum;
using Agent.Domain.Interfaces;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.TelephonyServer;
using Serilog;
using System;
using System.Linq;

namespace Septa.AloVoip.Agent.Module.Embeded
{
    public class OperatorSos : BaseEmbededModuleManager
    {
        private readonly IAloVoipContext _context;

        public OperatorSos(IAloVoipContext context,
                             IGateway gateway,
                             IGatewayRequest request,
                             IGatewayChannel channel,
                             IAloVoipTelephonyServer telephonyServer) : base(context, gateway, request, channel, telephonyServer)
        {
            _context = context;
        }

        public override bool SilentMode
        {
            get { return false; }
        }

        public override void InternalProcess()
        {
            Log.Debug("OperatorSos, InternalProcess.");

            MyGateway.PlayHoldMusic();

            var userExtn = MyRequest.CallerId;
            var helperExtn = string.Empty;

            var operatorSos = MyTelephonyServer.OperatorSos;
            if (operatorSos == null)
            {
                Log.Debug("Operator sos is not provided.");
                throw new Exception("Operator sos is not provided.");
            }

            var operatorManagerLookupMode = operatorSos.OperatorManagerLookupModeTypeIndex;
            if (operatorManagerLookupMode == (int)OperatorManagerLookupModeType.ByOrganizationDepartment)
            {
                helperExtn = GetHelperExtensionFromDepartmentBy(tsId: MyTelephonyServer.Id, userExtension: userExtn);
            }
            else if (operatorManagerLookupMode == (int)OperatorManagerLookupModeType.ByLookupSource)
            {
                if (operatorSos.OperatorManagerLookupSourceId.HasValue)
                    helperExtn = GetHelperExtensionFromLookupSourceBy(tsId: MyTelephonyServer.Id, userExtension: userExtn, lookupSourceId: operatorSos.OperatorManagerLookupSourceId.Value);
            }
            else
                throw new NotImplementedException("Operator helper lookup mode type is not supported.");

            if (string.IsNullOrEmpty(helperExtn) || MyTelephonyServer.Extensions.FirstOrDefault(x => x.Name == helperExtn).Status != ExtensionStatus.Idle)
            {
                Log.Debug("OperatorSos, helper extension not found or not avilable.");

                MyGateway.StreamVoice(operatorSos.ManagerNotFoundVoiceFileName);
                MyGateway.Hangup();
                return;
            }

            Log.Debug($"OperatorSos, {nameof(userExtn)}:{userExtn}, {nameof(helperExtn)}:{helperExtn}");

            MyGateway.StreamVoice(operatorSos.OperatorSosSentVoiceFileName);

            MyTelephonyServer.ChannelSpy(helperExtn, userExtn, SpyMode.Whisper);
        }


        private string GetHelperExtensionFromLookupSourceBy(int tsId, string userExtension, int lookupSourceId)
        {
            var loopupSourceService = new LookupSourceServiceFactory(_context).Create(lookupSourceId);

            Log.Debug($"OperatorSos, Lookup source: {lookupSourceId}, {loopupSourceService.GetType().ToString()}");

            return loopupSourceService.GetUserManagerExtenstionBy(tsId: tsId, userExtenstion: userExtension);
        }
        private string GetHelperExtensionFromDepartmentBy(int tsId, string userExtension)
        {
            var ext = _context.Peers.FirstOrDefault(x => x.TsId == tsId && x.Code == userExtension);
            if (ext == null || !ext.DepartmentId.HasValue)
                return string.Empty;

            var helperExtensionId = GetDepartmentHelperExtensionIdBy(departmentId: ext.DepartmentId.Value, tsId: tsId);
            if (!helperExtensionId.HasValue)
                return string.Empty;

            return _context.Peers.FirstOrDefault(x => x.Id == helperExtensionId.Value).Code;
        }
        private int? GetDepartmentHelperExtensionIdBy(int departmentId, int tsId)
        {
            Log.Debug($"OperatorSos, GetDepartmentHelperExtensionIdBy: {nameof(departmentId)}:{departmentId}, {nameof(tsId)}:{tsId}");

            var department = _context.Departments.FirstOrDefault(x => x.Id == departmentId);
            var departmentTelephonySystemHelperExtension = _context.DepartmentTelephonySystemHelperExtensions.FirstOrDefault(x => x.DepartmentId == departmentId && x.TsId == tsId);
            if(departmentTelephonySystemHelperExtension != null)
            {
                return departmentTelephonySystemHelperExtension.HelperExtensionId;
            }
            else if(department.ParentId.HasValue)
            {
                return GetDepartmentHelperExtensionIdBy(departmentId: department.ParentId.Value, tsId: tsId);
            }
            else
            {
                return null;
            }
        }
    }
}