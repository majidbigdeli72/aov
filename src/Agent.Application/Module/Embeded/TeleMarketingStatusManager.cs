﻿using Agent.Domain.Enum;
using System.Collections.Concurrent;

namespace Septa.AloVoip.Agent.Module.Embeded
{
    public static class TeleMarketingStatusManager
    {
        private static ConcurrentDictionary<int, ConcurrentDictionary<long, TeleMarketingTargetProcessStatus>> _teleMarketingTargetProcessDic = new ConcurrentDictionary<int, ConcurrentDictionary<long, TeleMarketingTargetProcessStatus>>();

        public static ConcurrentDictionary<long, TeleMarketingTargetProcessStatus> GetManager(int tsId)
        {
            return _teleMarketingTargetProcessDic.GetOrAdd(tsId, (tsid) => { return new ConcurrentDictionary<long, TeleMarketingTargetProcessStatus>(); });
        }

        //public static List<Domain.Models.TeleMarketing> TeleMarketings(int tsId)
        //{
        //}
    }
}