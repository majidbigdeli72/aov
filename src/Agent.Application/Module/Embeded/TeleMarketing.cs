﻿using Agent.Domain.Enum;
using Agent.Domain.Interfaces;
using Microsoft.EntityFrameworkCore;
using Septa.AloVoip.Agent.Gateway;
using Septa.TelephonyServer;
using Serilog;
using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace Septa.AloVoip.Agent.Module.Embeded
{
    public class TeleMarketing : BaseEmbededModuleManager
    {

        public TeleMarketing(IAloVoipContext context, IGateway gateway, IGatewayRequest request, IGatewayChannel channel, IAloVoipTelephonyServer telephonyServer)
            : base(context, gateway, request, channel, telephonyServer)
        {
        }

        public override bool SilentMode
        {
            get { return false; }
        }

        public override void InternalProcess()
        {
            Log.Debug("TeleMarketing, InternalProcess.");

            MyGateway.PlayHoldMusic();

            var teleMarketingTargetId = int.Parse(MyGateway.GetVar("teleMarketingTarget"));

            Log.Debug($"TeleMarketing, InternalProcess, Getting telemarketingTarget from var teleMarketingTarget:{teleMarketingTargetId}");

            var teleMarketingTarget = DbContext.TeleMarketingTargets.FirstOrDefault(x => x.Id == teleMarketingTargetId);
            if (teleMarketingTarget == null)
                return;

            Log.Debug($"TeleMarketing, InternalProcess, TelemarketingTarget From DB: {nameof(teleMarketingTarget.Number)}:{teleMarketingTarget.Number}");

            TeleMarketingTargetProcessStatus teleMarketingTargetProcessStatus;
            if (!TeleMarketingStatusManager.GetManager(MyTelephonyServer.Id).TryGetValue(teleMarketingTargetId, out teleMarketingTargetProcessStatus))
                return;

            Log.Debug($"TeleMarketing, InternalProcess. TelemarketingTarget from manager: {nameof(teleMarketingTargetProcessStatus)}:{teleMarketingTargetProcessStatus}");

            TeleMarketingStatusManager.GetManager(MyTelephonyServer.Id)[teleMarketingTargetId] = TeleMarketingTargetProcessStatus.Fetched;

            try
            {
                int c = 0;
                bool callFound = false;
                while (c < 60)
                {
                    try
                    {
                        var currentCall = MyTelephonyServer.GetChannel(MyRequest.UniqueId);
                        if (currentCall != null)
                        {
                            var currentParentCall = currentCall.ParentCall;
                            if (currentParentCall != null)
                            {
                                if (currentParentCall.ChainChannels != null &&
                                    currentParentCall.ChainChannels.Count > 0 &&
                                    currentParentCall.ChainChannels.Any(p => p.OwnerPeer?.Type == PeerType.Trunk && p.State == ChannelState.Up))
                                {
                                    callFound = true;
                                    break;
                                }
                                else
                                    Log.Debug($"TeleMarketing, InternalProcess. currentParentCall.ChainChannels is null or not up. {nameof(MyRequest.UniqueId)}:{MyRequest.UniqueId}");
                            }
                            else
                                Log.Debug($"TeleMarketing, InternalProcess. currentParentCall is null. {nameof(MyRequest.UniqueId)}:{MyRequest.UniqueId}");
                        }
                        else
                            Log.Debug($"TeleMarketing, InternalProcess. currentCall is null. {nameof(MyRequest.UniqueId)}:{MyRequest.UniqueId}");
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Error in TeleMarketing InternalProcess GetChannel.");
                    }
                    finally
                    {
                        Thread.Sleep(500);
                        c++;
                    }
                }

                Log.Debug($"TeleMarketing, InternalProcess. {nameof(callFound)}:{callFound} for {nameof(teleMarketingTarget)}:{teleMarketingTarget.Id}");

                if (callFound)
                {
                    var teleMarketing = DbContext.TeleMarketings.FirstOrDefault(x => x.Id == teleMarketingTarget.TeleMarketingId);
                    if (teleMarketing.WaitBeforePlayIntroInSeconds > 0)
                        Thread.Sleep((teleMarketing.WaitBeforePlayIntroInSeconds) * 1000);

                    var stopwatch = new Stopwatch();
                    stopwatch.Start();

                    var voiceKeyFile = teleMarketing.IntroVoiceFileName;

                    if (teleMarketing.RequestResponeTypeIndex != (int)RequestResponeType.Nothing)
                    {
                        var responseDigit = GetInputLong(voiceKeyFile, 30000, maxDigits: 1);
                        if (responseDigit.HasValue && responseDigit.Value != -1)
                        {
                            teleMarketingTarget.TeleMarketingStatusIndex = (int)TeleMarketingStatus.Succeed;
                            teleMarketingTarget.ResponsedDigit = int.Parse(responseDigit.Value.ToString());

                            if (teleMarketing.RequestResponeTypeIndex == (int)RequestResponeType.Transfer)
                            {
                                if (!string.IsNullOrEmpty(teleMarketing.NextCode))
                                {
                                    teleMarketingTarget.RequestResponeTypeIndex = (int)RequestResponeType.Transfer;
                                    GoTo(teleMarketing.NextCode, false);
                                }
                                else
                                {
                                    teleMarketingTarget.RequestResponeTypeIndex = (int)RequestResponeType.Nothing;
                                }
                            }
                            else if (teleMarketing.RequestResponeTypeIndex == (int)RequestResponeType.Callback)
                            {
                                teleMarketingTarget.RequestResponeTypeIndex = (int)RequestResponeType.Callback;
                            }
                        }
                        else
                        {
                            teleMarketingTarget.TeleMarketingStatusIndex = (int)TeleMarketingStatus.Failed;
                            teleMarketingTarget.RequestResponeTypeIndex = (int)RequestResponeType.Nothing;
                        }
                    }
                    else
                    {
                        StreamFileIfNotSilent(voiceKeyFile);

                        teleMarketingTarget.TeleMarketingStatusIndex = (int)TeleMarketingStatus.Succeed;
                        teleMarketingTarget.RequestResponeTypeIndex = (int)RequestResponeType.Nothing;
                    }

                    stopwatch.Stop();

                    teleMarketingTarget.Duration = stopwatch.Elapsed;

                    DbContext.TeleMarketingTargets.Upsert(teleMarketingTarget);

                    DbContext.SaveChanges();
                }
            }
            finally
            {
                TeleMarketingStatusManager.GetManager(MyTelephonyServer.Id)[teleMarketingTargetId] = TeleMarketingTargetProcessStatus.Processed;
            }
        }
    }
}
