﻿using Agent.Application.Models;
using Agent.Domain.Interfaces;
using Agent.Domain.Module;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.Module.PaymentHelper;
using Serilog;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{
    public class Pay : BaseModuleManager<ModulePay>
    {
        private readonly IPaymentFactory _paymentFactory;

        public Pay(int moduleId,
                   IAloVoipContext context,
                   IGateway gateway,
                   IGatewayRequest request,
                   IGatewayChannel channel,
                   ILookupSourceService lookupSourceService,
                   IAloVoipTelephonyServer telephonyServer, 
                   IPaymentFactory paymentFactory) : base(context, gateway, request, channel, lookupSourceService, telephonyServer)
        {
            if (lookupSourceService == null)
                throw new System.ArgumentNullException(nameof(lookupSourceService));

            if (paymentFactory == null)
                throw new System.ArgumentNullException(nameof(paymentFactory));

            this.MyLookupSourceService = lookupSourceService;
            this.MyModule = context.ModulePays.FirstOrDefault(p => p.Id == moduleId);
            _paymentFactory = paymentFactory;
        }

        private string GetMobileNumberToSendLink()
        {
            string mobileNumber = string.Empty;

            if (MyModule.ForceToGetNewMobile)
            {
                mobileNumber = GetMobileNumberFromUserInput(MyModule, MyGateway, DefaultRetryCount, DefaultGetDataTimeout);
            }
            else
            {
                var useCurrentMobileNumberToSendLink = UseCurrentMobileNumberToSendLink(MyModule, MyGateway, MyRequest.CallerId, DefaultRetryCount, DefaultGetDataTimeout);
                if (useCurrentMobileNumberToSendLink)
                {
                    mobileNumber = MyRequest.CallerId;
                }
                else
                {
                    mobileNumber = GetMobileNumberFromUserInput(MyModule, MyGateway, DefaultRetryCount, DefaultGetDataTimeout);
                }
            }

            Log.Debug($"Pay, GetMobileNumberToSendLink. {nameof(mobileNumber)}: {mobileNumber}");
            return mobileNumber;
        }

        private bool UserLikesToGetPaymentLink()
        {
            var userLikesToGetPaymentLink = UserLikesToGetPaymentLink(nameof(MyModule.SelectSendLinkVoice), DefaultRetryCount, DefaultGetDataTimeout);

            Log.Debug($"Pay, UserLikesToGetPaymentLink. {nameof(userLikesToGetPaymentLink)}:{userLikesToGetPaymentLink}");

            return userLikesToGetPaymentLink;
        }

        private PaymentInfo GetPaymentInfo()
        {
            PaymentInfo paymentInfo = _paymentFactory.Create(MyModule).GetPaymentInfo();

            Log.Debug("Pay, GetPaymentInfo. paymentInfo:{@paymentInfo}", paymentInfo);

            return paymentInfo;
        }

        public override void InternalProcess()
        {
            MyGateway.PlayHoldMusic();

            Log.Debug("Pay InternalProcess called");

            var paymentInfo = GetPaymentInfo();
            if (paymentInfo == null)
            {
                StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.NoBillFoundToPay)));
                GoTo(MyModule.FailedCode);
                return;
            }

            if (paymentInfo.Amount <= 0)
            {
                // بتسانکار میباشد
                StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.NoBillFoundToPay)));
                GoTo(MyModule.FailedCode);
                return;
            }

            StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.PreSayBalanceVoice)));
            MyGateway.PlayNumber(paymentInfo.Amount.ToString());
            StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.AfterSayBalanceVoice)));

            var userLikesToGetPaymentLink = UserLikesToGetPaymentLink();
            Log.Debug($"Pay InternalProcess, {nameof(MyModule.SendPaymentLink)}: {MyModule.SendPaymentLink}, {nameof(userLikesToGetPaymentLink)}: {userLikesToGetPaymentLink}");
            if (MyModule.SendPaymentLink && userLikesToGetPaymentLink)
            {
                var mobile = GetMobileNumberToSendLink();
                if (string.IsNullOrEmpty(mobile))
                {
                    StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.WrongMobileVoice)));
                    GoTo(MyModule.FailedCode);
                    return;
                }

                MyGateway.PlayHoldMusic();

                // Send Link
                var paymentLink = string.Empty;
                var resultMessate = string.Empty;
                if (MyLookupSourceService.SendPaymentLinkToUser(paymentInfo, mobile, MyModule.MoneyAccountUserKey, out resultMessate))
                {
                    StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.SendAndThanksVoice)));
                    GoTo(MyModule.NextCode);
                    return;
                }
                else
                {
                    StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.FailedToSendLink)));
                    GoTo(MyModule.FailedCode);
                    return;
                }
            }
            else
            {
                GoTo(MyModule.NextCode);
            }
        }
        
        #region Pay module

        private bool UserLikesToGetPaymentLink(string voiceFileKey, int defaultRetryCount, int defaultGetDataTimeout)
        {
            long? likeToSendPaymentLink = null;
            for (int i = 0; i < defaultRetryCount; i++)
            {
                likeToSendPaymentLink = GetInputLong(voiceFileKey, defaultGetDataTimeout);
                if (likeToSendPaymentLink.HasValue)
                    break;
            }

            if (likeToSendPaymentLink.HasValue && likeToSendPaymentLink.Value == 1)
                return true;

            return false;
        }
        private string GetMobileNumberFromUserInput(ModulePay modulePay, IGateway gateway, int defaultRetryCount, int defaultGetDataTimeout)
        {
            string mobileNumber = string.Empty;

            long? inputNumber = null;
            for (int i = 0; i < defaultRetryCount; i++)
            {
                inputNumber = GetInputLong( nameof(modulePay.EnterMobileVoice), defaultGetDataTimeout);
                if (inputNumber.HasValue)
                {
                    mobileNumber = inputNumber.Value.ToString();
                    break;
                }
                else
                    GetInputLong( nameof(modulePay.WrongMobileVoice), defaultGetDataTimeout);
            }

            if (!string.IsNullOrEmpty(mobileNumber) && mobileNumber.Length == 10)
            {
                GetInputLong( nameof(modulePay.PreSayNewMobileVoice), defaultGetDataTimeout);
                gateway.PlayDigits(mobileNumber);
                GetInputLong( nameof(modulePay.AfterSayNewMobileVoice), defaultGetDataTimeout);

                long? isCorrect = null;
                for (int i = 0; i < defaultRetryCount; i++)
                {
                    isCorrect = GetInputLong( nameof(modulePay.RecheckNewMobileVoice), defaultGetDataTimeout);
                    if (isCorrect.HasValue)
                        break;
                }

                if (isCorrect.HasValue && isCorrect.Value == 1)
                {
                    return mobileNumber;
                }
            }

            return null;
        }
        private bool UseCurrentMobileNumberToSendLink(ModulePay modulePay, IGateway gateway, string callerId, int defaultRetryCount, int defaultGetDataTimeout)
        {
            // Say caller number
            MyGateway.StreamVoice(nameof(modulePay.PreSayCurrentMobileVoice));
            gateway.PlayDigits(callerId);
            MyGateway.StreamVoice(nameof(modulePay.AfterSayCurrentMobileVoice));

            long? useCurrentMobileNumber = null;
            for (int i = 0; i < defaultRetryCount; i++)
            {
                useCurrentMobileNumber = GetInputLong(nameof(modulePay.SelectCurrentMobileVoice), defaultGetDataTimeout);
                if (useCurrentMobileNumber.HasValue)
                    break;
            }
            if (useCurrentMobileNumber.HasValue && useCurrentMobileNumber.Value == 1)
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}