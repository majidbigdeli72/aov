﻿using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using System;

namespace Septa.AloVoip.Agent.Module.PaymentHelper
{
    public class CustomerBalancePayment : Payment
    {
        private readonly IGateway _gateway;

        public CustomerBalancePayment(ILookupSourceService lookupSourceService, IGateway gateway) : base(lookupSourceService)
        {
            if (gateway == null)
                throw new ArgumentNullException(nameof(gateway));

            _gateway = gateway;
        }

        public override PaymentInfo GetPaymentInfo()
        {
            PaymentInfo result = null;

            var customerInfo = _gateway.CustomerHelper.GetCustomerInfo();

            var customerBill = _lookupSourceService.GetCustomerBalance(customerInfo);
            if (customerBill.HasValue)
            {
                result = new PaymentInfo()
                {
                    IdentityId = new Guid(customerInfo.CustomerId),
                    Amount = customerBill.Value
                };
            }

            return result;
        }
    }
}
