﻿using Septa.AloVoip.Agent.LookupSourceService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.AloVoip.Agent.Module.PaymentHelper
{
    public interface IPayment
    {
        PaymentInfo GetPaymentInfo();
    }
}
