﻿using Septa.AloVoip.Agent.LookupSourceService;
using System;

namespace Septa.AloVoip.Agent.Module.PaymentHelper
{
    public abstract class Payment : IPayment
    {
        protected readonly int _defaultGetDataTimeout = 30000;
        protected readonly int _defaultRetryCount = 3;

        protected readonly ILookupSourceService _lookupSourceService;

        public Payment(ILookupSourceService lookupSourceService)
        {
            if (lookupSourceService == null)
                throw new ArgumentNullException(nameof(lookupSourceService));

            _lookupSourceService = lookupSourceService;
        }

        public abstract PaymentInfo GetPaymentInfo();
    }
}
