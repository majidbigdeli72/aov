﻿using Agent.Application.Models;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using System;

namespace Septa.AloVoip.Agent.Module.PaymentHelper
{
    public class CustomerAndItemNumberPayment : Payment
    {
        private readonly IGateway _gateway;
        private readonly ModulePay _modulePay;

        public CustomerAndItemNumberPayment(ILookupSourceService lookupSourceService,
                                            IGateway gateway,
                                            ModulePay modulePay) : base(lookupSourceService)
        {
            if (gateway == null)
                throw new ArgumentNullException(nameof(gateway));

            if (modulePay == null)
                throw new ArgumentNullException(nameof(modulePay));

            _gateway = gateway;
            _modulePay = modulePay;
        }

        private long? GetInput(string voiceFileKey, int timeOut)
        {
            try
            {
                var voiceFileName = _modulePay.SilentMode ? "" : _modulePay.GetVoiceFileName(voiceFileKey);
                var input = _gateway.GetInput(voiceFileName, timeOut);

                if (!string.IsNullOrEmpty(input))
                {
                    return Convert.ToInt64(input);
                }
            }
            catch
            {
            }

            return null;
        }

        public override PaymentInfo GetPaymentInfo()
        {
            for (int f = 0; f < _defaultRetryCount; f++)
            {
                long? itemNumber = null;
                for (int i = 0; i < _defaultRetryCount; i++)
                {

                    itemNumber = GetInput(nameof(_modulePay.EnterNumberVoice), _defaultGetDataTimeout);
                    if (itemNumber.HasValue)
                        break;
                }

                if (itemNumber.HasValue)
                {
                    _gateway.PlayHoldMusic();

                    CustomerInfo customerInfo = null;
                    if (_gateway.CustomerHelper.IsCustomerPresent())
                        customerInfo = _gateway.CustomerHelper.GetCustomerInfo();

                    var paymentInfo = _lookupSourceService.GetPaymentInfo(customerInfo,
                                                                          _modulePay.BillableObjectTypeKey,
                                                                          itemNumber.Value.ToString(),
                                                                          _modulePay.LookupNumberFieldKey,
                                                                          _modulePay.ValueFieldKey);
                    if (paymentInfo != null)
                        return paymentInfo;
                }

                if (!_modulePay.SilentMode)
                    _gateway.StreamVoice(_modulePay.GetVoiceFileName(nameof(_modulePay.WrongNumberVoice)));
            }

            return null;
        }
    }
}
