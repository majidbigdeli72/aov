﻿using Agent.Application.Models;

namespace Septa.AloVoip.Agent.Module.PaymentHelper
{
    public interface IPaymentFactory
    {
        IPayment Create(ModulePay modulePay);
    }
}
