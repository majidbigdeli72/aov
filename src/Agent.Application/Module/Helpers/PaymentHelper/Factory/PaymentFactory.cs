﻿using Agent.Application.Models;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using System;

namespace Septa.AloVoip.Agent.Module.PaymentHelper
{
    public class PaymentFactory : IPaymentFactory
    {
        private readonly ILookupSourceService _lookupSourceService;
        private readonly IGateway _gateway;

        public PaymentFactory(ILookupSourceService lookupSourceService,
                              IGateway gateway)
        {
            if (lookupSourceService == null)
                throw new ArgumentNullException(nameof(lookupSourceService));
            
            if (gateway == null)
                throw new ArgumentNullException(nameof(gateway));

            _lookupSourceService = lookupSourceService;
            _gateway = gateway;
        }

        public IPayment Create(ModulePay modulePay)
        {
            if (modulePay.WorkInAnonymousMode && !string.IsNullOrEmpty(modulePay.BillableObjectTypeKey))
            {
                return new CustomerAndItemNumberPayment(_lookupSourceService, _gateway, modulePay);
            }
            else if(_gateway.CustomerHelper.IsCustomerPresent())
            {
                if (string.IsNullOrEmpty(modulePay.BillableObjectTypeKey))
                {
                    return new CustomerBalancePayment(_lookupSourceService, _gateway);
                }
                else
                {
                    return new CustomerAndItemNumberPayment(_lookupSourceService, _gateway, modulePay);
                }
            }

            throw new NotImplementedException();
        }
    }
}
