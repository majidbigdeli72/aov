﻿using Septa.AloVoip.Agent.Module.Helpers.AuthenticatorHelper;

namespace Septa.AloVoip.Agent.Module.AuthenticatorHelper
{
    public interface IAuthenticator
    {
        AuthenticationResult Authenticate(CustomerClub module);
    }
}