﻿using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.Module.Helpers.AuthenticatorHelper;
using System;

namespace Septa.AloVoip.Agent.Module.AuthenticatorHelper
{
    public abstract class Authenticator : IAuthenticator
    {
        protected readonly int _defaultTimeout = 30000;
        protected readonly int _defaultRetryCount = 3;
        protected readonly ILookupSourceService _lookupSourceService;
        protected readonly IGatewayRequest _gatewayRequest;
        protected readonly IGateway _gateway;

        public Authenticator(ILookupSourceService lookupSourceService, IGatewayRequest gatewayRequest, IGateway gateway)
        {
            if (lookupSourceService == null)
                throw new ArgumentNullException(nameof(lookupSourceService));

            if (gatewayRequest == null)
                throw new ArgumentNullException(nameof(gatewayRequest));

            if (gateway == null)
                throw new ArgumentNullException(nameof(gateway));

            _lookupSourceService = lookupSourceService;
            _gatewayRequest = gatewayRequest;
            _gateway = gateway;
        }

        public abstract AuthenticationResult Authenticate(CustomerClub module);
    }
}
