﻿using Agent.Domain.Enum;

namespace Septa.AloVoip.Agent.Module.AuthenticatorHelper
{
    public interface IAuthenticatorFactory
    {
        IAuthenticator Create(AuthenticationMode authenticationMode);
    }
}