﻿using Agent.Domain.Enum;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;

using System;

namespace Septa.AloVoip.Agent.Module.AuthenticatorHelper
{
    public class AuthenticatorFactory : IAuthenticatorFactory
    {
        private readonly ILookupSourceService _lookupSourceService;
        private readonly IGatewayRequest _gatewayRequest;
        private readonly IGateway _gateway;

        public AuthenticatorFactory(ILookupSourceService lookupSourceService,
                                    IGatewayRequest gatewayRequest,
                                    IGateway gateway)
        {
            _lookupSourceService = lookupSourceService;
            _gatewayRequest = gatewayRequest;
            _gateway = gateway;
        }

        public IAuthenticator Create(AuthenticationMode authenticationMode)
        {
            switch (authenticationMode)
            {
                case AuthenticationMode.PhoneNumber:
                    return new PhoneNumberAuthenticator(_lookupSourceService, _gatewayRequest, _gateway);
                case AuthenticationMode.CustomerNumber:
                    return new CustomerNumberAuthenticator(_lookupSourceService, _gateway, _gatewayRequest);
                case AuthenticationMode.CustomerNumberAndTelephonyPass:
                    return new CustomerNumberAndTelephonyPassAuthenticator(_lookupSourceService, _gateway, _gatewayRequest);
                default:
                    throw new ArgumentOutOfRangeException(nameof(authenticationMode), $"invalid {nameof(authenticationMode)}");
            }
        }
    }
}