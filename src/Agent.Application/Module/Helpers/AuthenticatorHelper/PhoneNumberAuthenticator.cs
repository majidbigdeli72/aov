﻿using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.Module.Helpers.AuthenticatorHelper;

namespace Septa.AloVoip.Agent.Module.AuthenticatorHelper
{
    public class PhoneNumberAuthenticator : Authenticator
    {
        public PhoneNumberAuthenticator(ILookupSourceService lookupSourceService, IGatewayRequest gatewayRequest, IGateway gateway) 
            : base(lookupSourceService, gatewayRequest, gateway)
        {
        }

        public override AuthenticationResult Authenticate(CustomerClub module)
        {
            module.StreamFileIfNotSilent(module.MyModule.GetVoiceFileName(nameof(module.MyModule.IntroVoice)));
            module.MyGateway.PlayHoldMusic();

            var identityByPhoneNumber = _lookupSourceService.GetIdentityByPhoneNumber(_gatewayRequest.CallerId);
            if (identityByPhoneNumber != null)
            {
                _gateway.CustomerHelper.SetCustomer(identityByPhoneNumber);
                return AuthenticationResult.Success;
            }
            else
            {
                return AuthenticationResult.Failed;
            }
        }
    }
}