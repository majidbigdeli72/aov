﻿using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.Module.Exceptions;
using Septa.AloVoip.Agent.Module.Helpers.AuthenticatorHelper;
using System;

namespace Septa.AloVoip.Agent.Module.AuthenticatorHelper
{
    public class CustomerNumberAndTelephonyPassAuthenticator : Authenticator
    {
        public CustomerNumberAndTelephonyPassAuthenticator(ILookupSourceService lookupSourceService, IGateway gateway, IGatewayRequest gatewayRequest)
            : base(lookupSourceService, gatewayRequest, gateway)
        {
            if (gateway == null)
                throw new ArgumentNullException(nameof(gateway));
        }

        public override AuthenticationResult Authenticate(CustomerClub module)
        {
            var result = AuthenticationResult.Failed;

            long? customerNo = null;
            long? telephonyPass = null;

            for (int f = 0; f < _defaultRetryCount; f++)
            {
                for (int i = 0; i < _defaultRetryCount; i++)
                {
                    var voiceName = (f == 0 && i == 0) ? module.MyModule.IntroAndCustomerNumberVoice : nameof(module.MyModule.EnterCustomerNoVoice);

                    if (module.MyModule.RecoverCredentialEnabled && f == 0 && i == 0)
                    {
                        try
                        {
                            customerNo = module.GetInputLong(voiceName, _defaultTimeout, null, module.RecoverPassDigit);
                        }
                        catch (BreakDigitCharDetectedException)
                        {
                            return AuthenticationResult.RequestForRecoverCredential;
                        }
                    }
                    else
                    {
                        customerNo = module.GetInputLong(voiceName, _defaultTimeout);
                    }
                    if (customerNo.HasValue)
                        break;
                }

                if (customerNo.HasValue)
                {
                    for (int i = 0; i < _defaultRetryCount; i++)
                    {
                        telephonyPass = module.GetInputLong(nameof(module.MyModule.EnterPassVoice), _defaultTimeout);
                        if (telephonyPass.HasValue)
                            break;
                    }
                    if (telephonyPass.HasValue)
                    {
                        _gateway.PlayHoldMusic();

                        var identityInfo = _lookupSourceService.GetIdentityByCustomerNumber(string.Format("{0}{1}", module.MyModule.CustomerNumberPrefix, customerNo.ToString()));
                        if (identityInfo != null)
                        {
                            var userInfo = _lookupSourceService.GetUserInfoByIdentityInfo(identityInfo);
                            if (userInfo != null)
                            {
                                var isAuthenticated = userInfo.TelephonyPassword == telephonyPass.ToString();
                                if (isAuthenticated)
                                {
                                    _gateway.CustomerHelper.SetCustomer(identityInfo);
                                    return AuthenticationResult.Success;
                                }
                            }
                        }
                    }
                }

                if (result == AuthenticationResult.Failed)
                    _gateway.StreamVoice(module.MyModule.GetVoiceFileName(nameof(module.MyModule.WrongCustomerNoOrPassVoice)));
            }

            return result;
        }
    }
}