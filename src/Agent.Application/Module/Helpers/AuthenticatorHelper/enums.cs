﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Septa.AloVoip.Agent.Module.Helpers.AuthenticatorHelper
{
    public enum AuthenticationResult
    {
        Success,
        Failed,
        RequestForRecoverCredential
    }
}
