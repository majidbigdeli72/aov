﻿using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.Module.Helpers.AuthenticatorHelper;
using System;

namespace Septa.AloVoip.Agent.Module.AuthenticatorHelper
{
    public class CustomerNumberAuthenticator : Authenticator
    {
        public CustomerNumberAuthenticator(ILookupSourceService lookupSourceService, IGateway gateway, IGatewayRequest gatewayRequest) 
            : base(lookupSourceService, gatewayRequest, gateway)
        {
            if (gateway == null)
                throw new ArgumentNullException(nameof(gateway));
        }

        public override AuthenticationResult Authenticate(CustomerClub module)
        {
            for (int i = 0; i < _defaultRetryCount; i++)
            {
                var voiceName = i == 0 ? module.MyModule.IntroAndCustomerNumberVoice : nameof(module.MyModule.EnterCustomerNoVoice);
                var enterd = module.GetInputLong(voiceName, _defaultTimeout);
                if (enterd.HasValue)
                {
                    _gateway.PlayHoldMusic();

                    var identityInfo = _lookupSourceService.GetIdentityByCustomerNumber(string.Format("{0}{1}", module.MyModule.CustomerNumberPrefix, enterd.ToString()));
                    if (identityInfo != null)
                    {
                        _gateway.CustomerHelper.SetCustomer(identityInfo);
                        return AuthenticationResult.Success;
                    }
                }
                else
                {
                    _gateway.StreamVoice(module.MyModule.GetVoiceFileName(nameof(module.MyModule.WrongCustomerNoVoice)));
                }
            }
            return AuthenticationResult.Failed;
        }
    }
}