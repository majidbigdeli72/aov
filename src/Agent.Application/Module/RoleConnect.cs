﻿using Agent.Application.Models;
using Agent.Domain.Enum;
using Agent.Domain.Interfaces;
using Agent.Domain.Module;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Serilog;
using System;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{
    public class RoleConnect : BaseModuleManager<ModuleRoleConnect>
    {
        public RoleConnect(int moduleId,
                           IAloVoipContext context,
                           IGateway gateway,
                           IGatewayRequest request,
                           IGatewayChannel channel,
                           ILookupSourceService lookupSourceService,
                           IAloVoipTelephonyServer telephonyServer) : base(context, gateway, request, channel, lookupSourceService, telephonyServer)
        {
            if (lookupSourceService == null)
                throw new System.ArgumentNullException(nameof(lookupSourceService));

            this.MyLookupSourceService = lookupSourceService;
            this.MyModule = context.ModuleRoleConnects.FirstOrDefault(p => p.Id == moduleId);
        }

        public override void InternalProcess()
        {
            Log.Debug($"RoleConnect InternalProcess called. IsCustomerPresent: {IsCustomerPresent()}");

            if (!IsCustomerPresent())
                GoTo(MyModule.FailedCode);
            else
            {
                var indentityInfo = GetIndentityInfo();
                string userName;

                switch ((TargetRoleType)MyModule.TargetRoleTypeIndex)
                {
                    case TargetRoleType.SalesPerson:
                        userName = indentityInfo.SaleUsername;
                        break;
                    case TargetRoleType.SupportPerson:
                        userName = indentityInfo.SupportUsername;
                        break;
                    case TargetRoleType.OtherPerson:
                        userName = indentityInfo.OtherUsername;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                if (string.IsNullOrEmpty(userName))
                    GoTo(MyModule.FailedCode);

                var userExtenstion = GetUserExtenstion(userName);

                Log.Debug("RoleConnect, TargetRoleType:{@TargetRoleType},indentityInfo:{@indentityInfo}, userName:{@userName}, userExtenstion:{@userExtenstion}", MyModule.TargetRoleTypeIndex, indentityInfo, userName, userExtenstion);

                if (string.IsNullOrEmpty(userExtenstion))
                    GoTo(MyModule.FailedCode);
                else
                    GoToExtension(userExtenstion);

                //var info = MyLookupSourceService.GetUserExtenstions(userName)
                //                                .TelephonySystems
                //                                .First(x => MyModule.TelephonySystem.Key == x.Key);

                //GoToExtension(info.Extensions.First().Name);
            }
        }
    }
}