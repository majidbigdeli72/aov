﻿using Agent.Domain.Interfaces;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.Module.Exceptions;
using Septa.TelephonyServer;
using System;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{

    public abstract class BaseEmbededModuleManager : IEmbededModuleManager
    {
        protected const int DefaultGetDataTimeout = 30000;
        protected const int DefaultRetryCount = 3;

        public IAloVoipContext DbContext { get; protected set; }
        public IGateway MyGateway { get; protected set; }
        public IGatewayRequest MyRequest { get; protected set; }
        public IGatewayChannel MyChannel { get; protected set; }
        public IAloVoipTelephonyServer MyTelephonyServer { get; protected set; }

        public BaseEmbededModuleManager(IAloVoipContext context, IGateway gateway, IGatewayRequest request, IGatewayChannel channel, IAloVoipTelephonyServer telephonyServer)
        {
            DbContext = context;
            MyGateway = gateway;
            MyRequest = request;
            MyChannel = channel;
            MyTelephonyServer = telephonyServer;
        }

        public virtual void GoTo(string destination, bool endProcess = true)
        {
            if (string.IsNullOrEmpty(destination))
                throw new ArgumentException("IsNullOrEmpty destination", "destination");
            else
                MyGateway.GoTo(destination);

            if (endProcess)
                throw new GoToException();
        }

        public virtual void GoToExtension(string destination, bool endProcess = true)
        {
            if (string.IsNullOrEmpty(destination))
                throw new ArgumentException("IsNullOrEmpty destination", "destination");
            else
                MyGateway.GoToExtension(destination);

            if (endProcess)
                throw new GoToException();
        }

        protected CustomerInfo GetCustomerInfo()
        {
            return MyGateway.CustomerHelper.GetCustomerInfo();
        }

        protected bool IsCustomerPresent()
        {
            return MyGateway.CustomerHelper.IsCustomerPresent();
        }

        public virtual string GetInput(string voiceFileKey, int timeout, int? maxDigits = null, char breakDigit = ' ')
        {
            try
            {
                string input = string.Empty;
                var fileName = SilentMode || string.IsNullOrWhiteSpace(voiceFileKey) 
                    ? "silent" 
                    : voiceFileKey;
                if (breakDigit == ' ')
                {
                    input = this.MyGateway.GetInput(fileName, timeout, maxDigits);
                    return input;
                }
                else
                {
                    var firstDigit = this.MyGateway.GetInput(fileName, timeout, 1);
                    if (!string.IsNullOrEmpty(firstDigit) && firstDigit == breakDigit.ToString())
                        return firstDigit;

                    if (int.TryParse(firstDigit, out int result))
                    {
                        var remindDigits = maxDigits == 1
                                            ? ""
                                            : this.MyGateway.GetInput("silent", timeout, maxDigits.HasValue ? maxDigits - 1 : null);

                        return firstDigit + remindDigits;
                    }
                }
            }
            catch
            {

            }

            return null;
        }

        public long? GetInputLong(string voiceFileKey, int timeout, int? maxDigits = null, char breakDigit = ' ')
        {
            var input = GetInput(voiceFileKey, timeout, maxDigits, breakDigit);
            if (input == breakDigit.ToString())
                throw new BreakDigitCharDetectedException();

            try
            {
                if (!string.IsNullOrEmpty(input))
                    return Convert.ToInt64(input);
            }
            catch { }

            return null;
        }

        public long? GetDataFromCaller(string voiceFileKey)
        {
            return this.GetInputLong(voiceFileKey, DefaultGetDataTimeout);
        }

        public long? GetDataFromCaller(string voiceFileName, int retryCount, string retryVoiceFileName)
        {
            for (int i = 0; i < retryCount; i++)
                try
                {
                    var file = SilentMode ? "" : i == 0 ? voiceFileName : retryVoiceFileName;
                    return Convert.ToInt64(MyGateway.GetInput(file, DefaultGetDataTimeout));
                }
                catch { }

            return null;
        }

        public void StreamFileIfNotSilent(string filename)
        {
            if (!SilentMode)
                MyGateway.StreamVoice(filename);
        }

        public long? CurrentCallDbId
        {
            get
            {
                var channel = MyTelephonyServer.GetChannel(MyRequest.UniqueId);
                if (channel != null)
                    return channel.ParentCall.DbId;
                return null;
            }
        }
        public long? CurrentCallChannelDbId
        {
            get
            {
                var channel = MyTelephonyServer.GetChannel(MyRequest.UniqueId);
                if (channel != null)
                    return channel.DbId;
                return null;
            }
        }

        public string GetCurrentQueueName()
        {
            var queueName = MyGateway.GetVar("QUEUENAME");
            if (!string.IsNullOrEmpty(queueName))
                return queueName;//.Substring(memberInterface.IndexOf('/') + 1).Substring(0, memberInterface.Substring(memberInterface.IndexOf('/') + 1).IndexOf('@'));

            return string.Empty;
        }

        public string GetCurrentQueueMemberExtenstion()
        {
            var memberInterface = MyGateway.GetVar("MEMBERINTERFACE");
            if (!string.IsNullOrEmpty(memberInterface))
                return memberInterface.Substring(memberInterface.IndexOf('/') + 1).Substring(0, memberInterface.Substring(memberInterface.IndexOf('/') + 1).IndexOf('@'));

            return string.Empty;
        }

        public int? GetCurrentQueueMemberPeerDbId
        {
            get
            {
                var agentExtenstion = GetCurrentQueueMemberExtenstion();
                if (!string.IsNullOrEmpty(agentExtenstion))
                {
                    var extension = MyTelephonyServer.Peers.FirstOrDefault(x => x.Type == PeerType.Extension && x.Id == agentExtenstion);
                    if (extension != null)
                        return extension.DbId;
                }
                return null;
            }
        }
        public int? CurrentQueueDbId
        {
            get
            {
                var queueName = this.GetCurrentQueueName();
                if (!string.IsNullOrEmpty(queueName))
                {
                    var queue = MyTelephonyServer.Queues.FirstOrDefault(x => x.Name == queueName);
                    if (queue != null)
                    {
                        return queue.DbId;
                    }
                }
                return null;
            }
        }

        public virtual void Process()
        {
            try
            {
                InternalProcess();
            }
            catch (GoToException) { }
        }


        public abstract bool SilentMode { get; }
        public abstract void InternalProcess();
    }
}