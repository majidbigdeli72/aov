﻿using Septa.AloVoip.Agent.LookupSourceService;

namespace Septa.AloVoip.Agent.Module
{
    public interface IModuleManager : IEmbededModuleManager
    {
        ILookupSourceService MyLookupSourceService { get; }
    }
}