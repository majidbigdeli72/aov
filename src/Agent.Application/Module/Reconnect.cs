﻿using Agent.Application.Models;
using Agent.Domain.Interfaces;
using Agent.Domain.Module;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{
    public partial class Reconnect : BaseModuleManager<ModuleReconnect>
    {
        public Reconnect(int moduleId,
                         IAloVoipContext context,
                         IGateway gateway,
                         IGatewayRequest request,
                         IGatewayChannel channel,
                         ILookupSourceService lookupSourceService,
                         IAloVoipTelephonyServer telephonyServer) : base(context, gateway, request, channel, lookupSourceService, telephonyServer)
        {
            this.MyModule = context.ModuleReconnects.FirstOrDefault(p => p.Id == moduleId);
        }

        public override void InternalProcess()
        {
            MyGateway.PlayHoldMusic();

            Log.Debug($"Reconnect InternalProcess. IsCustomerPresent:{ IsCustomerPresent()}");

            var extensionToConnect = "";
            if (ExtensionFound(out extensionToConnect))
                GoToExtension(extensionToConnect);
            else
                GoTo(MyModule.FailedCode);
        }

        private bool ExtensionFound(out string extensionToConnect)
        {
            var queueMembers = GetQueueMembers(MyModule.ModuleReconnectScopeItems.Where(x => x.QueueId.HasValue).Select(x => x.QueueId.Value).ToList());
            var extensions = MyModule.ModuleReconnectScopeItems.Where(x => x.PeerId.HasValue).Select(x => x.Peer).ToList();
            if (queueMembers != null && queueMembers.Count > 0)
                extensions.AddRange(queueMembers);

            var extensionIds = extensions.Distinct().Select(x => x.Id).ToList();

            var calls = DbContext.Calls.Where(x => x.StartDate >= DateTime.Today &&
                                                   extensionIds.Contains(x.InitByPeerId.Value) &&
                                                   x.PhoneNumber.Substring(x.PhoneNumber.Length - 10).Contains(MyRequest.CallerId))
                                       .OrderByDescending(x => x.EndDate)
                                       .ToList();
            if (calls != null && calls.Count > 0)
            {
                extensionToConnect = calls[0].InitByPeer.Code;
                Log.Debug($"Reconnect. {calls.Count} calls found for {MyRequest.CallerId}. Last connected extension: {extensionToConnect}");
                return true;
            }

            Log.Debug($"Reconnect. No calls found for {MyRequest.CallerId}.");
            extensionToConnect = "";
            return false;
        }

        private List<Peer> GetQueueMembers(List<int> queueIds)
        {
            var queueMembers = DbContext.QueueMembers.Where(x => queueIds.Contains(x.QueueId) && x.IsLogined).ToList();
            if (queueMembers != null && queueMembers.Count > 0)
                return queueMembers.Select(x => x.Extension).ToList();

            return null;
        }
    }
}