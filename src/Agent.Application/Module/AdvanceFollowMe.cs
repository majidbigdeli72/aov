﻿using Agent.Application.Models;
using Agent.Domain.Enum;
using Agent.Domain.Interfaces;
using Agent.Domain.Module;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.TelephonyServer;
using Septa.TelephonyServer;
using Septa.TelephonyServer.Entries;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{
    public class AdvanceFollowMe : BaseModuleManager<ModuleAdvanceFollowMe>
    {
        private Dictionary<string, CallChannel> WaitingCallQueue = new Dictionary<string, CallChannel>();
        const int MaxUserInputTimeout = 5000;
        const int CheckTurnTimeout = 5000;
        const int PlayAnnonCheckTurnCount = 10;

        public AdvanceFollowMe(int moduleId,
            IAloVoipContext context,
            IGateway gateway,
            IGatewayRequest request,
            IGatewayChannel channel,
            ILookupSourceService lookupSourceService,
            IAloVoipTelephonyServer telephonyServer) : base(context, gateway, request, channel, lookupSourceService, telephonyServer)
        {
            if (lookupSourceService == null)
                throw new System.ArgumentNullException(nameof(lookupSourceService));

            this.MyModule = context.ModuleAdvanceFollowMe.FirstOrDefault(p => p.Id == moduleId);
            this.MyLookupSourceService = lookupSourceService;
        }

        private ExtensionEntry MyExtensionEntry
        {
            get
            {
                return MyTelephonyServer.Extensions.FirstOrDefault(p => p.DbId == MyModule.OwnerPeerId);
            }
        }
        private void Wait()
        {
            var dstCode = string.Empty;
            MyGateway.PlayHoldMusic();
            string OwnerPeerCode = MyModule.OwnerPeer.Code;
            if (MyModule.WaitQueueId != null)
            {
                dstCode = MyTelephonyServer.Queues.FirstOrDefault(x => x.DbId == MyModule.WaitQueueId).DestinationCode;
            }
            else
            {
                WaitQueue.Enqueue(MyModule.Id, MyRequest.UniqueId);
                int CheckTurnCount = -1;
                while (true)
                {
                    int turn = WaitQueue.CheckTurn(MyModule.Id, MyRequest.UniqueId);
                    if (MyGateway.GetChannelStatus() == ChannelStatus.Dead)
                    {
                        WaitQueue.Dequeue(MyModule.Id);
                        dstCode = MyModule.FailedCode;
                        break;
                    }
                    System.Threading.Thread.Sleep(CheckTurnTimeout);
                    if (turn == 0)
                    {
                        if (MyExtensionEntry.Status == ExtensionStatus.Idle)
                        {
                            MyTelephonyServer.UpdateExtensionStatus(MyExtensionEntry);
                            if (MyExtensionEntry.Status == ExtensionStatus.Idle)
                            {
                                WaitQueue.Dequeue(MyModule.Id);
                                dstCode = MyExtensionEntry.DestinationCode;
                                break;
                            }
                        }
                    }
                    if (CheckTurnCount >= PlayAnnonCheckTurnCount || CheckTurnCount == -1)
                    {
                        CheckTurnCount = 0;
                        StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.PreTurnVoice)));
                        MyGateway.PlayNumber((turn + 1).ToString());
                        StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.PostTurnVoice)));
                        MyGateway.PlayHoldMusic();
                    }
                    CheckTurnCount++;
                }
            }
            GoTo(dstCode);
        }

        private void LeaveAMessage()
        {
            MyGateway.VoiceMail(MyModule.OwnerPeer.Code);
            MyGateway.Hangup();
        }
        private void CallMe()
        {
            StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.PostCallbackVoice)));
            MyGateway.Hangup();
        }

        public override void InternalProcess()
        {
            MyGateway.PlayHoldMusic();

            Log.Debug($"AdvanceFollowMe InternalProcess. {nameof(MyModule.AutoDialOperator)}:{MyModule.AutoDialOperator}, {nameof(MyModule.Code)}:{MyModule.Code}, IsCustomerPresent:{ IsCustomerPresent()}");

            string InputDTMF = MyGateway.GetInput(MyModule.GetVoiceFileName(nameof(MyModule.IntroVoice)), MaxUserInputTimeout, 1);
            ModuleAdvanceFollowMeInputType ModuleAdvanceFollowMeInputTypeIndex = (ModuleAdvanceFollowMeInputType)Enum.Parse(typeof(ModuleAdvanceFollowMeInputType), InputDTMF);
            switch (ModuleAdvanceFollowMeInputTypeIndex)
            {
                case ModuleAdvanceFollowMeInputType.Wait:
                    Wait();
                    break;
                case ModuleAdvanceFollowMeInputType.VoiceMail:
                    LeaveAMessage();
                    break;
                case ModuleAdvanceFollowMeInputType.CallBack:
                    CallMe();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            SaveResult(InputDTMF, ModuleAdvanceFollowMeInputTypeIndex);
        }

        private void SaveResult(string InputDTMF, ModuleAdvanceFollowMeInputType ModuleAdvanceFollowMeInputTypeIndex)
        {
            try
            {
                string CallerId = MyRequest.CallerId;
                ModuleAdvanceFollowMeResult aModuleAdvanceFollowMeResult = new ModuleAdvanceFollowMeResult();
                aModuleAdvanceFollowMeResult.ModuleAdvanceFollowMeId = MyModule.Id;
                aModuleAdvanceFollowMeResult.ExtensionId = MyModule.OwnerPeerId;
                aModuleAdvanceFollowMeResult.InputDTMF = InputDTMF;
                aModuleAdvanceFollowMeResult.ModuleAdvanceFollowMeInputTypeIndex = (int)ModuleAdvanceFollowMeInputTypeIndex;
                var aCallChannel = DbContext.CallChannels.FirstOrDefault(CallChannel => (CallChannel.UniqueId == MyRequest.UniqueId));
                aModuleAdvanceFollowMeResult.CallChannelId = aCallChannel.Id;
                aModuleAdvanceFollowMeResult.InputDate = DateTime.Now;
                aModuleAdvanceFollowMeResult.CallBackStatusIndex = (int)CallBackStatus.Pending;
                DbContext.ModuleAdvanceFollowMeResult.Add(aModuleAdvanceFollowMeResult);
                DbContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Log.Error(ex.Message);
            }
        }
    }
}
