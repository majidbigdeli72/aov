﻿using Agent.Application.Models;
using Agent.Domain.Enum;
using Agent.Domain.Interfaces;
using Agent.Domain.Module;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Septa.AloVoip.Agent.Module.AuthenticatorHelper;
using Septa.AloVoip.Agent.Module.Helpers.AuthenticatorHelper;
using Serilog;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{
    public partial class CustomerClub : BaseModuleManager<ModuleCustomerClub>
    {
        private readonly IAuthenticatorFactory _authenticatorFactory;
        public readonly char RecoverPassDigit = '*';

        public CustomerClub(int moduleId,
                            IAloVoipContext context,
                            IGateway gateway,
                            IGatewayRequest request,
                            IGatewayChannel channel,
                            ILookupSourceService lookupSourceService,
                            IAloVoipTelephonyServer telephonyServer,
                            IAuthenticatorFactory authenticatorFactory) : base(context, gateway, request, channel, lookupSourceService, telephonyServer)
        {
            if (lookupSourceService == null)
                throw new System.ArgumentNullException(nameof(lookupSourceService));

            if (authenticatorFactory == null)
                throw new System.ArgumentNullException(nameof(authenticatorFactory));

            this.MyModule = context.ModuleCustomerClubs.FirstOrDefault(p => p.Id == moduleId);
            _authenticatorFactory = authenticatorFactory;
        }

        public override void InternalProcess()
        {
            MyGateway.PlayHoldMusic();

            Log.Debug($"CustomerClub InternalProcess. {nameof(MyModule.ReAuthenticate)}:{MyModule.ReAuthenticate}, {nameof(MyModule.AuthenticationModeIndex)}:{MyModule.AuthenticationModeIndex}, IsCustomerPresent:{ IsCustomerPresent()}");

            var authenticationResult = Authenticate();
            var gotoCode = MyModule.FailedCode;

            switch (authenticationResult)
            {
                case AuthenticationResult.Success:
                    StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.WelcomeVoice)));
                    gotoCode = MyModule.NextCode;
                    break;
                case AuthenticationResult.Failed:
                    Log.Debug("Start Faild");
                    StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.AuthenticationFailedVoice)));
                    Log.Debug("start GetVoiceFileName");

                    if (MyModule.RecoverCredentialEnabled)
                    {
                        Log.Debug("RecoverCredentialEnabled");
                        gotoCode = GetUserResultForRecoverCredentials();
                        Log.Debug($"Log RecoverCredentialEnabled {gotoCode}");

                    }
                    else {
                        gotoCode = MyModule.FailedCode;
                        Log.Debug($"Log Not RecoverCredentialEnabled {gotoCode}");

                    }

                    break;
                case AuthenticationResult.RequestForRecoverCredential:
                    gotoCode = MyModule.RecoverCredentialCode;
                    Log.Debug($"Log  RequestForRecoverCredential {gotoCode}");
                    break;
                default:
                    Log.Debug($"Log  default {gotoCode}");

                    gotoCode = MyModule.FailedCode;
                    break;
            }
            Log.Debug($"CustomerClub InternalProcess. {nameof(authenticationResult)}:{authenticationResult}, {nameof(gotoCode)}:{gotoCode}");
            GoTo(gotoCode);
        }

        private AuthenticationResult Authenticate()
        {
            var authenticationResult = AuthenticationResult.Failed;
            if (MyModule.ReAuthenticate || !IsCustomerPresent())
            {
                MyGateway.PlayHoldMusic();

                authenticationResult = _authenticatorFactory.Create((AuthenticationMode)MyModule.AuthenticationModeIndex).Authenticate(this);
            }
            return authenticationResult;
        }

        private string GetUserResultForRecoverCredentials()
        {
            var recoverCredentials = UserLikesToRecoverCredentials(nameof(MyModule.RecoverCredentialVoice), 0, 5000);
            if (recoverCredentials)
                return MyModule.RecoverCredentialCode;
            else
                return MyModule.FailedCode;
        }
        private bool UserLikesToRecoverCredentials(string voiceFileKey, int defaultRetryCount, int defaultGetDataTimeout)
        {
            string likeToRecoverCredentials = GetInput(voiceFileKey, defaultGetDataTimeout, 1, ' ');
            if (!string.IsNullOrEmpty(likeToRecoverCredentials) && likeToRecoverCredentials == RecoverPassDigit.ToString())
                return true;

            return false;
        }

    }
}