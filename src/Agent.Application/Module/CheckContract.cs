﻿using Agent.Application.Models;
using Agent.Domain.Interfaces;
using Agent.Domain.Module;
using Septa.AloVoip.Agent.Gateway;
using Septa.AloVoip.Agent.LookupSourceService;
using Serilog;
using System.Linq;

namespace Septa.AloVoip.Agent.Module
{
    public class CheckContract : BaseModuleManager<ModuleCheckContract>
    {
        public CheckContract(int moduleId,
                             IAloVoipContext context,
                             IGateway gateway,
                             IGatewayRequest request,
                             IGatewayChannel channel,
                             ILookupSourceService lookupSourceService,
                             IAloVoipTelephonyServer telephonyServer) : base(context, gateway, request, channel, lookupSourceService, telephonyServer)
        {
            if (lookupSourceService == null)
                throw new System.ArgumentNullException(nameof(lookupSourceService));

            this.MyLookupSourceService = lookupSourceService;
            this.MyModule = context.ModuleCheckContracts.FirstOrDefault(p => p.Id == moduleId);
        }

        public override void InternalProcess()
        {
            MyGateway.PlayHoldMusic();

            Log.Debug("CheckContract InternalProcess. IsCustomerPresent:{@IsCustomerPresent}, CustomerInfo: {@CustomerInfo}, ContractTypeKey:{@ContractTypeKey}", IsCustomerPresent(), GetCustomerInfo(), MyModule.ContractTypeKey);

            if (!IsCustomerPresent())
                GoTo(MyModule.FailedCode);
            else
            {
                var hasValidContract = MyLookupSourceService.IdentityHasValidContract(GetCustomerInfo(), MyModule.ContractTypeKey);
                Log.Debug($"CheckContract. {nameof(hasValidContract)}:{hasValidContract}");

                if (hasValidContract)
                {
                    StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.ContractIsValidVoice)));
                    GoTo(MyModule.NextCode);
                }
                else
                {
                    StreamFileIfNotSilent(MyModule.GetVoiceFileName(nameof(MyModule.ContractIsNotValidVoice)));
                    GoTo(MyModule.FailedCode);
                }
            }
        }

    }
}