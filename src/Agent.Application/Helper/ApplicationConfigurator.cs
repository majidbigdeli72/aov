﻿using Agent.Application.Models;
using Agent.Domain.Interfaces;
using Agent.Domain.Manager;
using Agent.Domain.Models.Identity;
using Agent.Infra.CrossCutting.IoC;
using Agent.Infra.Data.Context;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Text;

namespace Agent.Domain.Helper
{
    public static class ApplicationConfigurator
    {

        public static void Configure()
        {
            AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException;

            SystemDateTime.Init(() => DateTime.Now);

            Log.Logger = CreateLogger();


            Log.Information("Starting...");

        }

        private static ILogger CreateLogger()
        {
            return new LoggerConfiguration()
                .ReadFrom.AppSettings()
                .Enrich.WithThreadId()
                .Enrich.FromLogContext()
                .CreateLogger();
        }


        private static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            var exception = e.ExceptionObject as Exception;
            if (exception == null)
            {
                Log.Logger.Fatal("An unhandled exception occurred but exception details are not available.");
            }
            else
            {
                Log.Logger.Fatal(exception, "An unhandled exception occurred. See exception details.");
            }
        }

        public static IServiceProvider ServiceProvider { get; }

        static ApplicationConfigurator()
        {   
            IServiceCollection sc = new ServiceCollection();

            NativeInjectorBootStrapper.RegisterServices(sc);


            ServiceProvider = sc.BuildServiceProvider();
        }


    }


    public static class IdentityServicesRegistry
    {
        /// <summary>
        /// Creates and seeds the database.
        /// </summary>
        public static void InitializeDb(this IServiceProvider serviceProvider)
        {
            var scopeFactory = serviceProvider.GetRequiredService<IServiceScopeFactory>();
            using (var scope = scopeFactory.CreateScope())
            {
                var identityDbInitialize = scope.ServiceProvider.GetService<IIdentityDbInitializer>();
                identityDbInitialize.Initialize();
                identityDbInitialize.SeedData();
            }
        }

        public static void Init(this IServiceProvider serviceProvider)
        {
            Log.Debug("AloVoipAgent init started.");

            using (serviceProvider.Scoped<SystemManager>(out var _sm))
            {
                _sm.Boot(false);
            }
        }

    }
}
