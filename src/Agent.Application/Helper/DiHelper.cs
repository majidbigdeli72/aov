﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Text;

namespace Agent.Domain.Helper
{
    public static class DiHelper
    {
        public static IDisposable Scoped<T>(this IServiceProvider scopeFactory, out T context)
        {
            var scope = scopeFactory.CreateScope();
            var services = scope.ServiceProvider;
            context = services.GetService<T>();
            return scope;
        }

    }

}
