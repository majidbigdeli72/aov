﻿using System.Threading.Tasks;
using Agent.Domain.Core.Commands;
using Agent.Domain.Core.Events;


namespace Agent.Domain.Core.Bus
{
    public interface IMediatorHandler
    {
        Task SendCommand<T>(T command) where T : Command;
        Task RaiseEvent<T>(T @event) where T : Event;
    }
}
