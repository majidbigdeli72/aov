﻿using Agent.Application.Models.MySqlModels;
using Microsoft.EntityFrameworkCore;
using System;

namespace Agent.Domain.Interfaces
{
    public interface IMySqlContext : IDisposable
    {
        DbSet<QueueMySql> Queues { get; set; }
        DbSet<QueueDetailMySql> QueueDetails { get; set; }
        DbSet<QueuePriorityMySql> QueuePriorities { get; set; }
        DbSet<RingGroupMySql> RingGroups { get; set; }
        DbSet<TimeConditionMySql> TimeConditions { get; set; }
        DbSet<TimeGroupMySql> TimeGroups { get; set; }
        DbSet<TimeGroupDetailMySql> TimeGroupDetails { get; set; }
        DbSet<ExtensionMySql> Extensions { get; set; }
        DbSet<IvrMySql> Ivrs { get; set; }
        DbSet<IvrEntryMySql> IvrEntries { get; set; }
        DbSet<RecordingMySql> Recordings { get; set; }
        DbSet<DeviceMySql> Devices { get; set; }
        DbSet<SipMySql> Sips { get; set; }
        DbSet<IaxMySql> Iaxs { get; set; }
        DbSet<DahdiMySql> Dahdis { get; set; }

        int SaveChanges();
    }

}
