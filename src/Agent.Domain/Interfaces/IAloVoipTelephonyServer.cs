﻿using Agent.Application.Models;
using Agent.Domain.Enum;
using Agent.Domain.Interfaces;
using Septa.TelephonyServer;
using System.Collections.Generic;

namespace Agent.Domain.Interfaces
{
    public interface IAloVoipTelephonyServer : ITelephonyServer
    {
        Dictionary<TelephonySystemSettingType, string> Settings { get; set; }

        OperatorSos OperatorSos { get; set; }


        void RegisterModule(int moduleId, string name, string code, ModuleType moduleType, string agiServerIP, string failedCode);
        void UnRegisterModule(int moduleId, ModuleType moduleType);
        void RegisterTeleMarketingPlayVoiceModule(string agiServerIP);
        void RegisterSendFaxModule(string agiServerIP);
        void RegisterOperatorSosModule(string agiCode, string agiServerIP);

        void ReloadModules();
        void RegisterVoice(string voiceName, byte[] voice);
        void UnRegisterVoice(string voiceName);
    }


}
