﻿using Agent.Application.Models;
using System;
using System.Collections.Generic;

namespace Agent.Domain.Interfaces
{
    public interface ICallReportService : IDisposable
    {
        IEnumerable<CallReportModel> GetCallReport(string tsKey,
                                                   int operatorId,
                                                   int? callDirectionIndex = null,
                                                   int? callTypeIndex = null,
                                                   int? callStatus = null,
                                                   string callStartDateFrom = null,
                                                   string callStartDateTo = null,
                                                   string callEndDateFrom = null,
                                                   string callEndDateTo = null,
                                                   string callDurationFrom = null,
                                                   string callDurationTo = null,
                                                   string callWaitingFrom = null,
                                                   string callWaitingTo = null,
                                                   string phoneNumber = null,
                                                   string callerIdNum = null,
                                                   string callerIdName = null,
                                                   string dialedExtension = null,
                                                   int? callChannelState = null,
                                                   int? queueId = null,
                                                   int? extensionPeerId = null,
                                                   bool isExtensionStarter = false,
                                                   int? extensionResponseIndex = null,
                                                   int? trunkPeerId = null,
                                                   bool isTrunkStarter = false,
                                                   int? trunkResponseIndex = null,
                                                   int pageNumber = 1,
                                                   int pageSize = 30,
                                                   bool isExcel = false);

        IEnumerable<CallChannel> GetCallPath(long callId);

        IEnumerable<AgentReportModel> GetAgentReport(string tsKey, string startDate, string endDate, List<ExtentionModel> extentions, int departmentId);
        List<ExtentionModel> GetExtentionByDepartmentId(string tsKey, int? departmentId);
    }


}
