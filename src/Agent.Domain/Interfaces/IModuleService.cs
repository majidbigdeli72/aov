﻿using Agent.Application.Models;
using Agent.Domain.Models;

namespace Agent.Application.Interfaces
{
    public interface IModuleService
    {
        Module GetInheritedModule(int moduleId);
    }

}
