﻿using System;
using System.Net;

namespace Agent.Domain.Interfaces
{
    public interface ISettingService : IDisposable
    {
        IPAddress GetSystemIp();
        string Serial { get; }
        string InstallPath { get; }
        string AgentPath { get; }
        string WebConsolePath { get; }
        string WebSiteAddress { get; }
        string AgentVoiceArchivePath { get; }
        string AgentCommonVoiceArchivePath { get; }
    }


}
