﻿namespace Agent.Domain.Interfaces
{
    public interface IUserContext
    {
        bool IsInRole(string role);

        string UserName { get; }

        bool IsAuthenticated { get; }

        int? UserId { get; }

        string Ip { get; }
    }
}
