﻿using Agent.Application.Models;
using Septa.AloVoip.Domain.Entries;
using Septa.TelephonyServer;
using System;
using System.Collections.Generic;

namespace Agent.Domain.Interfaces
{
    public interface IDashboardService : IDisposable
    {
        object GetTelephonySystems();

        /* Queue Dashboard */
        DashboardStatisticsModel GetQueueStatistics(string tsKey, int? queueId = null, int? datePeriodInDay = 0);
        DashboardPagingResultEntry<QueueDashboardWaitCallEntry> GetQueueWaitingCalls(string tsKey, int? queueId = null, int? datePeriodInDay = 0, WaitingCallsFilterModel filterModel = null);
        DashboardPagingResultEntry<QueueDashboardActiveCallEntry> GetQueueActiveCalls(string tsKey, int? queueId = null, int? datePeriodInDay = 0, ActiveCallsFilterModel filterModel = null);
        IEnumerable<Queue> GetQueues(string tsKey);
        object GetExtensions(string tsKey, int? queueId = null, int? datePeriodInDay = 0);
        void UpdateQueueDashboardStatsChanges(string tsKey);



        /* CallCenter Dashboard */
        IEnumerable<Peer> GetCallCenterOperatorExtensions(string tsKey, int userId);
        IEnumerable<CallCenterDashboardStatisticsEntry> GetCallCenterStatistics(string tsKey, int userId);
        void UpdateCallCenterDashboardExtension(string tsKey, string extName, PeerType peerType);
        IEnumerable<Peer> GetExtensions(string tsKey);
        IEnumerable<Department> GetDepartments();
        IEnumerable<Peer> GetTrunks(string tsKey);
        bool CallDial(string tsKey, string extn, string dialNo);
        bool CallListen(string tsKey, string extn, string spiedExtn);
        bool CallWhisper(string tsKey, string extn, string spiedExtn);
        bool CallConference(string tsKey, string extn, string spiedExtn);
        bool CallHangup(string tsKey, string channelName);


        /* CallBackRequest Dashboard */
        IEnumerable<CallbackRequestModel> GetCallbackRequests(int userId);
        bool MarkCallbackRequestAsDone(int callbackRequestId);
    }


}
