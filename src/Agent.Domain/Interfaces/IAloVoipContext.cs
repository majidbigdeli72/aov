﻿using Agent.Application.Models;
using Agent.Domain.Models;
using Agent.Domain.Models.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;

namespace Agent.Domain.Interfaces
{
    public interface IAloVoipContext : IDisposable
    {
        DbSet<Call> Calls { get; set; }
        DbSet<CallChannel> CallChannels { get; set; }
        DbSet<LookupSource> LookupSources { get; set; }
        DbSet<PayamGostarLookupSource> PayamGostarLookupSources { get; set; }

        DbSet<Agent.Domain.Models.Module> Modules { get; set; }
        DbSet<ModuleCustomerClub> ModuleCustomerClubs { get; set; }
        DbSet<ModuleAdvanceFollowMe> ModuleAdvanceFollowMe { get; set; }
        DbSet<ModuleAdvanceFollowMeResult> ModuleAdvanceFollowMeResult { get; set; }
        DbSet<ModuleCheckContract> ModuleCheckContracts { get; set; }
        DbSet<ModuleCardtableConnect> ModuleCardtableConnects { get; set; }
        DbSet<ModuleQueueOperatorAnnouncement> ModuleQueueOperatorAnnouncements { get; set; }
           // DbSet<ModuleQueueOperatorVotingItem> ModuleQueueOperatorVotingItems { get; set; }
        DbSet<ModuleQueueOperatorVotingResult> ModuleQueueOperatorVotingResults { get; set; }
        DbSet<ModuleQueueOperatorVoting> ModuleQueueOperatorVotings { get; set; }

        DbSet<ModulePay> ModulePays { get; set; }
        DbSet<Operator> Operators { get; set; }
        DbSet<Peer> Peers { get; set; }
        DbSet<ExtentionHistory> ExtentionHistories { get; set; }
        DbSet<QueueCaller> QueueCallers { get; set; }
        DbSet<Queue> Queues { get; set; }
        DbSet<QueueMember> QueueMembers { get; set; }
        DbSet<TelephonySystem> TelephonySystems { get; set; }
        DbSet<TelephonySystemSetting> TelephonySystemSettings { get; set; }
        DbSet<ApplicationUser> Users { get; set; }
        DbSet<ModuleReconnect> ModuleReconnects { get; set; }
        DbSet<ModuleReconnectScopeItem> ModuleReconnectScopeItems { get; set; }
        DbSet<ModuleRoleConnect> ModuleRoleConnects { get; set; }
        DbSet<TelephonySystemLookupSource> TelephonySystemLookupSources { get; set; }
        DbSet<Department> Departments { get; set; }
        DbSet<OperatorDepartmentPermission> OperatorDepartmentPermissions { get; set; }


        DbSet<ModuleVoting> ModuleVotings { get; set; }
        DbSet<ModuleVotingQuestion> ModuleVotingQuestions { get; set; }
        DbSet<ModuleVotingQuestionItem> ModuleVotingQuestionItems { get; set; }
        DbSet<ModuleVotingQuestionResult> ModuleVotingQuestionResults { get; set; }

        DbSet<TeleMarketing> TeleMarketings { get; set; }
        DbSet<TeleMarketingTarget> TeleMarketingTargets { get; set; }

        DbSet<GeneralProperty> GeneralPropertys { get; set; }
        DbSet<GeneralPropertyItem> GeneralPropertyItems { get; set; }

        DbSet<OperatorSos> OperatorSos { get; set; }

        DbSet<DepartmentTelephonySystemHelperExtension> DepartmentTelephonySystemHelperExtensions { get; set; }


        DashboardStatisticsModel GetDashboardData(DateTime fromDate, int? queueId = null);
        List<MemberStatisticsModel> GetMemberStatistics(DateTime fromDate, int? queueId = null);

        List<CallReportModel> GetCallReport(int tsId,
                                            int operatorId,
                                            int? callDirectionIndex = null,
                                            int? callTypeIndex = null,
                                            int? callStatus = null,
                                            DateTime? callStartDateFrom = null,
                                            DateTime? callStartDateTo = null,
                                            DateTime? callEndDateFrom = null,
                                            DateTime? callEndDateTo = null,
                                            string callDurationFrom = null,
                                            string callDurationTo = null,
                                            string callWaitingFrom = null,
                                            string callWaitingTo = null,
                                            string phoneNumber = null,
                                            string callerIdNum = null,
                                            string callerIdName = null,
                                            string dialedExtension = null,
                                            int? callChannelState = null,
                                            int? queueId = null,
                                            int? extensionPeerId = null,
                                            bool isExtensionStarter = false,
                                            int? extensionResponseIndex = null,
                                            int? trunkPeerId = null,
                                            bool isTrunkStarter = false,
                                            int? trunkResponseIndex = null,
                                            int pageNumber = 1,
                                            int pageSize = 30,
                                            bool isExcel = false);

        List<AgentReportModel> GetAgentReport(int tsId, DateTime? StartDate, DateTime? EndDate, DataTable extentions, int deparmentId);

        List<ExtentionModel> GetExtentionByDepartmentId(int tsId, int? departmentId);

        List<Department> GetDepartment();


        EntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;
        Microsoft.EntityFrameworkCore.DbSet<TEntity> Set<TEntity>() where TEntity : class;
        int SaveChanges();
    }

}
