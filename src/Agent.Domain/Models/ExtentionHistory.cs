﻿namespace Agent.Application.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ExtentionHistory")]
    public partial class ExtentionHistory
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }

        public long ExtentionId { get; set; }

        public int? QueueId { get; set; }

        public DateTime Date { get; set; }

        public int ExtentionActionTypeIndex { get; set; }

        public string ActionDescription { get; set; }



        public virtual Peer Extention { get; set; }
        public virtual Queue Queue { get; set; }
    }
}