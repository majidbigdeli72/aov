namespace Agent.Application.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.Json.Serialization;

    [Table("ModuleReconnectScopeItem")]
    public partial class ModuleReconnectScopeItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [NotMapped]
        public string ClientId { get; set; }

        public int ModuleReconnectId { get; set; }

        public int? PeerId { get; set; }

        public int? QueueId { get; set; }

        [JsonIgnore]
        public virtual ModuleReconnect ModuleReconnect { get; set; }

        [JsonIgnore]
        public virtual Peer Peer { get; set; }

        [JsonIgnore]
        public virtual Queue Queue { get; set; }
    }
}
