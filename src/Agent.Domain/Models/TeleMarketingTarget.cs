﻿namespace Agent.Application.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TeleMarketingTarget")]
    public partial class TeleMarketingTarget
    {
        public TeleMarketingTarget()
        {
            Duration = new TimeSpan();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }
        public int TeleMarketingId { get; set; }

        [StringLength(50)]
        public string Number { get; set; }

        public int TeleMarketingStatusIndex { get; set; }

        public TimeSpan? Duration { get; set; }

        public int RetryCount { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? NextRetryDate { get; set; }

        public int RequestResponeTypeIndex { get; set; }

        public int? ResponsedDigit { get; set; }

        public virtual TeleMarketing TeleMarketing { get; set; }
    }
}