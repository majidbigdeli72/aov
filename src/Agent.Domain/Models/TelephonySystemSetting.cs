namespace Agent.Application.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TelephonySystemSetting")]
    public partial class TelephonySystemSetting
    {
        public TelephonySystemSetting()
        {
        }

      //  [Key, Column(Order = 0)]
        public int TelephonySystemId { get; set; }

        [Required]
        [StringLength(200)]
        [Key, Column(Order = 1)]
        public string Key { get; set; }

        [Required]
        [StringLength(200)]
        public string Value { get; set; }


        public virtual TelephonySystem TelephonySystem { get; set; }
    }
}
