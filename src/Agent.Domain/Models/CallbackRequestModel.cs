﻿namespace Agent.Application.Models
{
    public class CallbackRequestModel
    {
        public CallbackRequestModel()
        {
        }

        public long Id { get; set; }
        public string InputDate { get; set; }
        public string CallerNumber { get; set; }        
        public string ExtensionCode { get; set; }
        public string CallBackStatus { get; set; }
        public int CallBackRetryCount { get; set; }
        public string LastCallBackRetryDate { get; set; }
        public bool AutoDial { get; set; }
    }
}