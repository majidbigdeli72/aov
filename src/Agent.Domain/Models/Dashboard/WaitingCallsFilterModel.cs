﻿namespace Agent.Application.Models
{
    public class WaitingCallsFilterModel
    {
        public long? CallerId { get; set; }
        public string Caller { get; set; }
        public string Queue { get; set; }
        public string JoinDate { get; set; }
        public string Wait { get; set; }


        public string SortExpression { get; set; }
        public string SortDirection { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}