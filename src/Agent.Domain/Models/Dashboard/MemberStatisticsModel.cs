﻿namespace Agent.Application.Models
{
    public class MemberStatisticsModel
    {
        public int? OperatorId { get; set; }
        public string Ext { get; set; }
        public string Name { get; set; }
        public int AnsweredCount { get; set; }
    }
}