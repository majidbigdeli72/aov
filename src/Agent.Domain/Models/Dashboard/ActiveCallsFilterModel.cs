﻿namespace Agent.Application.Models
{
    public class ActiveCallsFilterModel
    {
        public string Caller { get; set; }
        public string Queue { get; set; }
        public string Agent { get; set; }
        public string ConnectDate { get; set; }
        public string Wait { get; set; }
        public string Talk { get; set; }


        public string SortExpression { get; set; }
        public string SortDirection { get; set; }

        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}