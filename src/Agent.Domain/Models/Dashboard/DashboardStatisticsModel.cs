﻿namespace Agent.Application.Models
{
    public class DashboardStatisticsModel
    {
        public int Total { get; set; }
        public int Actives { get; set; }
        public int Waitings { get; set; }
        public int Completeds { get; set; }
        public int Abandoneds { get; set; }
        public int RecalledAbandoneds { get; set; }

        public string AVGTalkTime { get; set; }
        public string LongestTalkTime { get; set; }

        public string AVGWaitTime { get; set; }
        public string LongestWaitTime { get; set; }


        public int WaitTimeSeconds_1 { get; set; }
        public int WaitTimeSecondsVal_1 { get; set; }

        public int WaitTimeSeconds_2 { get; set; }
        public int WaitTimeSecondsVal_2 { get; set; }

        public int WaitTimeSeconds_3 { get; set; }
        public int WaitTimeSecondsVal_3 { get; set; }

        public int WaitTimeSeconds_4 { get; set; }
        public int WaitTimeSecondsVal_4 { get; set; }

        public int WaitTimeSeconds_5 { get; set; }
        public int WaitTimeSecondsVal_5 { get; set; }
    }
}