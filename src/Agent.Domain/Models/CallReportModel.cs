﻿using Septa.TelephonyServer;
using System;

namespace Agent.Application.Models
{
    public class CallReportModel
    {
        public int TotalRows { get; set; }
        public long RowNum { get; set; }
        public long Id { get; set; }
        public string ProfileId { get; set; }
        public string ProfileName { get; set; }

        public string StartDatePersian
        {
            get
            {
                var pDate = ((FarsiToolbox.DateAndTime.PersianDateTime)StartDate);
                return pDate.ToString("HH:mm") + " - " + pDate.ToString("yyyy/MM/dd");
            }
        }
        public DateTime StartDate { get; set; }
        public string EndDatePersian
        {
            get
            {
                if (EndDate.HasValue)
                {
                    var pDate = ((FarsiToolbox.DateAndTime.PersianDateTime)EndDate);
                    return pDate.ToString("HH:mm") + " - " + pDate.ToString("yyyy/MM/dd");
                }
                else
                    return "";
            }
        }
        public DateTime? EndDate { get; set; }

        public string CallDirection
        {
            get
            {
                return ((CallDirection)CallDirectionIndex).GetDisplayName();
            }
        }
        public int CallDirectionIndex { get; set; }


        public string CallType
        {
            get
            {
                if (CallStatusIndex == (int)Septa.TelephonyServer.CallStatus.Live)
                    return "";
                else
                    return ((CallType)CallTypeIndex).GetDisplayName();
            }
        }
        public int CallTypeIndex { get; set; }


        public string CallStatus
        {
            get
            {
                return ((CallStatus)CallStatusIndex).GetDisplayName();
            }
        }
        public int CallStatusIndex { get; set; }


        public TimeSpan CallDuration { get; set; }
        public TimeSpan RingDuration { get; set; }
        public string Caller { get; set; }
        public string PhoneNumber { get; set; }
        public string Peers { get; set; }
    }
}