﻿using Agent.Domain.Attributes;
using Agent.Domain.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models
{
    [Table("ModuleAdvanceFollowMe")]
    public class ModuleAdvanceFollowMe : Module
    {
        public ModuleAdvanceFollowMe()
        {
            ModuleAdvanceFollowMeResults = new HashSet<ModuleAdvanceFollowMeResult>();
        }


        [TelephonyVoice(Text = "متاسفانه فرد مورد نظر در دسترس نیست جهت انتظار تماس عدد یک گذاشتن پیغام عدد دو و یا تماس از سوی اپراتور به در دسترس بودن عدد سه را شماره گیری فرمایید")]
        public byte[] IntroVoice { get; set; }
        [TelephonyVoice(Text = "شما نفر ")]
        public byte[] PreTurnVoice { get; set; }
        [TelephonyVoice(Text = "صف انتظار هستید")]
        public byte[] PostTurnVoice { get; set; }
        [TelephonyVoice(Text = "شماره شما در صف پیگیری تماس ثبت شد و بزودی با شما تماس گرفته خواهد شد")]
        public byte[] PostCallbackVoice { get; set; }

        public bool AutoDialOperator { get; set; }

        public int OwnerPeerId { get; set; }

        public int? WaitQueueId { get; set; }

        public ICollection<ModuleAdvanceFollowMeResult> ModuleAdvanceFollowMeResults { get; set; }

        public virtual Peer OwnerPeer { get; set; }
        public virtual Queue WaitQueue { get; set; }
    }
}
