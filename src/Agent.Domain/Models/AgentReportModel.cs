﻿using System;

namespace Agent.Application.Models
{
    public class AgentReportModel
    {
        public int Code { get; set; }
        public int? OwnerOperatorId { get; set; }
        public int? InputCountCall { get; set; }
        public int? OutPutCountCall { get; set; }
        public TimeSpan? MaxCallDuration { get; set; }
        public TimeSpan? MinCallDuration { get; set; }
        public TimeSpan? AvgCallDuration { get; set; }
    }

    public class ExtentionModel
    {
        public int Code { get; set; }
    }
}