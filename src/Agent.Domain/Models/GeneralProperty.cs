using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace Agent.Application.Models
{
    [Table("GeneralProperty")]
	public class GeneralProperty
    {
		[SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public GeneralProperty()
		{
            GeneralPropertyItems = new HashSet<GeneralPropertyItem>();
		}

		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public int Id { get; set; }

        [StringLength(50)]
        public string Key { get; set; }

        [StringLength(50)]
		public string Mame { get; set; }




		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<GeneralPropertyItem> GeneralPropertyItems { get; set; }
	}
}