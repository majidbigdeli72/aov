﻿namespace Agent.Application.Models
{
    using Agent.Domain.Enum;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("LookupSource")]
	public partial class LookupSource
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public LookupSource()
		{ }

		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key]
		public int Id { get; set; }

		[Required]
		[StringLength(400)]
		public string Name { get; set; }

		public int LookupSourceTypeIndex { get; set; }

		public string LookupSourceTypeIndexName
		{
			get
			{
				switch ((LookupSourceType)LookupSourceTypeIndex)
				{
					case LookupSourceType.Payamgostar:
						return "پیام گستر";
					default:
						return string.Empty;
				}
			}
		}
	}
}
