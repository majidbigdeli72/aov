﻿namespace Agent.Application.Models
{
    public static class Constants
    {
        public static class Modules
        {
            public static class TeleMarketingPlayVoice
            {
                public const string CategoryName = "AloVoIP-TeleMarketingPlayVoice";
                public const string AgiPrefix = "TeleMarketingPlayVoice";
                public const string AgiCode = "**78788";
            }
            public static class SendFax
            {
                public const string CategoryName = "AloVoIP-SendFax";
                public const string AgiPrefix = "SendFax";
                public const string AgiCode = "**78789";
            }
            public static class OperatorSos
            {
                public const string CategoryName = "AloVoIP-OperatorSos";
                public const string AgiPrefix = "OperatorSos";
                public const string AgiCode = "*125";
            }
        }
    }
}
