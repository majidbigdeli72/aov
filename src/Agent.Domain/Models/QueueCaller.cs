namespace Agent.Application.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("QueueCaller")]
	public partial class QueueCaller
	{
		[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key]
		public long Id { get; set; }

		public int QueueId { get; set; }

		public long CallChannelId { get; set; }

		public int QueueCallerStatusIndex { get; set; }

		public DateTime JoinDate { get; set; }

		public DateTime? LeaveDate { get; set; }

        public int JoinPosition { get; set; }

        public int? AbandonPosition { get; set; }

        public long? AnsweredByCallChannelId { get; set; }

        public int? AnsweredByPeerId { get; set; }

		public virtual CallChannel AnsweredByCallChannel { get; set; }

		public virtual Peer AnsweredByPeer { get; set; }

		public virtual Queue Queue { get; set; }
	}
}