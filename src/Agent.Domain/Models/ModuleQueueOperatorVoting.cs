﻿namespace Agent.Application.Models
{
    using Agent.Domain.Attributes;
    using Agent.Domain.Models;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.Json.Serialization;

    [Table("ModuleQueueOperatorVoting")]
    public partial class ModuleQueueOperatorVoting : Module
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ModuleQueueOperatorVoting()
        {
            ModuleQueueOperatorVotingResults = new HashSet<ModuleQueueOperatorVotingResult>();
        }
        
        public int QueueId { get; set; }

        public bool OperatorAnnouncement { get; set; }

        public bool PlayOperatorExtensionInsteadOfName { get; set; }

        public bool Voting { get; set; }



        [TelephonyVoice(Text = "شما به اپراتور")]
        public byte[] PreAnnounceOperatorVoice { get; set; }

        [TelephonyVoice(Text = "متصل می شوید")]
        public byte[] PostAnnounceOperatorVoice { get; set; }

        [TelephonyVoice(Text = "ورودی نامعتبر می باشد")]
        public byte[] InvalidInputVoice { get; set; }

        [TelephonyVoice(Text = "نظر شما ثبت شد. باتشکر از شما")]
        public byte[] SucceedVoice { get; set; }

        

        public int InputDigitFrom { get; set; }
        public int InputDigitTo { get; set; }


        [TelephonyVoice]
        [JsonIgnore]
        public byte[] Voice { get; set; }



        public virtual Queue Queue { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModuleQueueOperatorVotingResult> ModuleQueueOperatorVotingResults { get; set; }
    }
}
