﻿namespace Agent.Application.Models
{
    using Agent.Domain.Attributes;
    using Agent.Domain.Models;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ModuleRoleConnect")]
    public partial class ModuleRoleConnect : Module
    {
        public int TargetRoleTypeIndex { get; set; }
        public bool SayOperatorName { get; set; }

        [TelephonyVoice(Text = "شما به")]
        public byte[] PreSayBalanceVoice { get; set; }
        [TelephonyVoice(Text = "متصل می شوید")]
        public byte[] AfterSayBalanceVoice { get; set; }

    }
}
