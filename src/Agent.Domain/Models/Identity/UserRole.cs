﻿using Microsoft.AspNetCore.Identity;

namespace Agent.Domain.Models.Identity
{
    public class UserRole : IdentityUserRole<int> {

        public virtual ApplicationUser User { get; set; }

        public virtual ApplicationRole Role { get; set; }

    }

}
