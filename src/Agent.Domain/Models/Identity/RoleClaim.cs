﻿using Microsoft.AspNetCore.Identity;

namespace Agent.Domain.Models.Identity
{
    public class RoleClaim : IdentityRoleClaim<int>
    {
        public virtual ApplicationRole Role { get; set; }
    }

}
