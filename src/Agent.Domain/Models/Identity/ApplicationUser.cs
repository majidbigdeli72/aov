﻿using Agent.Domain.Models.Identity;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Agent.Domain.Models.Identity
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser<int>
    {

        //public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        //{
        //    // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
        //    var userIdentity = await manager.CreateAsync(this, DefaultAuthenticationTypes.ApplicationCookie);

        //    var claimsToAdd = new List<Claim>();

        //    if (userIdentity.Name.ToLower().Equals(Common.AdminUsername))
        //    {
        //        claimsToAdd.AddRange(GetClaimsByRole(Domain.Enum.UserRole.Admin));
        //        claimsToAdd.AddRange(GetClaimsByRole(Domain.Enum.UserRole.VoipManager));
        //        claimsToAdd.AddRange(GetClaimsByRole(Domain.Enum.UserRole.Operator));
        //    }
        //    else
        //    {
        //        var userRoles = userIdentity.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value).ToList();
        //        foreach (var userRole in userRoles)
        //        {
        //            if (userRole == Domain.Enum.UserRole.Admin.ToString())
        //                claimsToAdd.AddRange(GetClaimsByRole(Domain.Enum.UserRole.Admin));
        //            else if (userRole == Domain.Enum.UserRole.VoipManager.ToString())
        //                claimsToAdd.AddRange(GetClaimsByRole(Domain.Enum.UserRole.VoipManager));
        //            else if (userRole == Domain.Enum.UserRole.Operator.ToString())
        //                claimsToAdd.AddRange(GetClaimsByRole(Domain.Enum.UserRole.Operator));
        //        }
        //    }
        //    if (claimsToAdd.Count > 0)
        //        userIdentity.AddClaims(claimsToAdd);

        //    return userIdentity;
        //}

        //private List<Claim> GetClaimsByRole(Agent.Domain.Enum.UserRole userRole)
        //{
        //    var toReturn = new List<Claim>();

        //    var claims = ClaimData.Claims.FirstOrDefault(x => x.Key == userRole).Value;
        //    foreach (var claim in claims)
        //        toReturn.Add(new Claim(claim.ToString(), claim.GetDisplayName()));

        //    return toReturn;
        //}

        //  public virtual ICollection<UserUsedPassword> UserUsedPasswords { get; set; }

        //    public virtual ICollection<UserToken> UserTokens { get; set; }

     //   public virtual ICollection<UserUsedPassword> UserUsedPasswords { get; set; }

        public virtual ICollection<UserToken> UserTokens { get; set; }

        public virtual ICollection<UserRole> Roles { get; set; }

        public virtual ICollection<UserLogin> Logins { get; set; }

        public virtual ICollection<UserClaim> Claims { get; set; }

    }

}
