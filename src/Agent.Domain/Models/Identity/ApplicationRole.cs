﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace Agent.Domain.Models.Identity
{
    public class ApplicationRole : IdentityRole<int> {

        public virtual ICollection<UserRole> Users { get; set; }

        public virtual ICollection<RoleClaim> Claims { get; set; }

    };

}
