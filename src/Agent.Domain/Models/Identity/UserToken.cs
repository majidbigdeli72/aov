﻿using Microsoft.AspNetCore.Identity;

namespace Agent.Domain.Models.Identity
{
    public class UserToken : IdentityUserToken<int>
    {
        public virtual ApplicationUser User { get; set; }
    }

}
