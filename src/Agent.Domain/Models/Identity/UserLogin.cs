﻿using Microsoft.AspNetCore.Identity;

namespace Agent.Domain.Models.Identity
{
    public class UserLogin : IdentityUserLogin<int> {
        public virtual ApplicationUser User { get; set; }
    }

}
