﻿using Microsoft.AspNetCore.Identity;

namespace Agent.Domain.Models.Identity
{
    public class UserClaim : IdentityUserClaim<int> {
        public virtual ApplicationUser User { get; set; }
    }

}
