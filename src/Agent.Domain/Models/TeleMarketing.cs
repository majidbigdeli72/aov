﻿namespace Agent.Application.Models
{
    using Agent.Domain.Attributes;
    using Septa.TelephonyServer;
    using Septa.TelephonyServer.Entries;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TeleMarketing")]
    public partial class TeleMarketing
    {
        public TeleMarketing()
        {
            CreateDate = DateTime.Now;
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int TsId { get; set; }

        [Required]
        [StringLength(300)]
        public string Name { get; set; }

        public DateTime CreateDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartDate { get; set; }

        [NotMapped]
        public string PersianStartDate { get; set; }

        [TelephonyVoice(Text = "تبلیغات تلفنی الو ویپ")]
        public byte[] IntroVoice { get; set; }

        public int MinimumSuccessDurationThreshold { get; set; }
        public int MaxConcurrentCall { get; set; }

        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }

        public int RequestResponeTypeIndex { get; set; }

        [StringLength(50)]
        public string NextCode { get; set; }
        [NotMapped]
        public string DestinationCodeForNextCode { get; set; }
        [NotMapped]
        public string CustomNextCode { get; set; }
        [NotMapped]
        public DestinationType DestinationTypeForNextCode
        {
            get
            {
                return BaseDestinationEntry.GetDestinationType(NextCode);
            }
        }

        public int WaitBeforePlayIntroInSeconds { get; set; }

        [NotMapped]
        public string IntroVoiceFileName
        {
            get
            {
                return $"TMV_{this.Id}_{nameof(this.IntroVoice)}";
            }
        }



        public virtual TelephonySystem TelephonySystem { get; set; }
        public virtual ICollection<TeleMarketingTarget> TeleMarketingTargets { get; set; }
    }
}