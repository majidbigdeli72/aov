﻿using Agent.Domain.Attributes;
using Agent.Domain.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models
{
    [Table("ModuleCheckContract")]
    public class ModuleCheckContract : Module
    {
        [Required]
        [StringLength(450)]
        public string ContractTypeKey { get; set; }

        [TelephonyVoice(Text = "قرارداد شما معتبر می باشد")]
        public byte[] ContractIsValidVoice { get; set; }

        [TelephonyVoice(Text = "قرارداد شما معتبر نمی باشد")]
        public byte[] ContractIsNotValidVoice { get; set; }
    }
}
