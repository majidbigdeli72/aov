﻿namespace Agent.Application.Models
{
    using Agent.Domain.Attributes;
    using Agent.Domain.Models;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ModuleQueueOperatorAnnouncement")]
    public partial class ModuleQueueOperatorAnnouncement : Module
    {
        public int QueueId { get; set; }

        public bool ForceToPlayOperatorExtention { get; set; }

        [TelephonyVoice(Text = "شما به اپراتور")]
        public byte[] PreAnnounceOperatorVoice { get; set; }

        [TelephonyVoice(Text = "متصل می شوید")]
        public byte[] PostAnnounceOperatorVoice { get; set; }
        
    }
}
