﻿namespace Agent.Application.Models
{
    using Agent.Application.Interfaces;
    using Agent.Domain.Attributes;
    using Agent.Domain.Enum;
    using Agent.Domain.Interfaces;
    using Agent.Domain.Models;
    using Septa.TelephonyServer;
    using Septa.TelephonyServer.Entries;
    using Serilog;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.IO;

    [Table("ModuleCustomerClub")]
    public partial class ModuleCustomerClub : Module
    {
        public readonly string IntroAndCustomerNumberVoice = "IntroAndCustomerNumberVoice";


        public string CustomerNumberPrefix { get; set; }

        public int AuthenticationModeIndex { get; set; }

        public bool ReAuthenticate { get; set; }

        public bool RecoverCredentialEnabled { get; set; }

        [StringLength(50)]
        public string RecoverCredentialCode { get; set; }
        [NotMapped]
        public string DestinationCodeForRecoverCredentialCode { get; set; }
        [NotMapped]
        public string CustomRecoverCredentialCode { get; set; }
        [NotMapped]
        public DestinationType DestinationTypeForRecoverCredentialCode
        {
            get
            {
                return BaseDestinationEntry.GetDestinationType(RecoverCredentialCode);
            }
        }

        // Step 1
        [TelephonyVoice(Text = "به باشگاه مشتریان تلفنی خوش آمدید")]
        public byte[] IntroVoice { get; set; }

        [TelephonyVoice(Text = "لطفا شماره مشتری را وارد کرده و در انتها کلید مربع را انتخاب نمایید")]
        public byte[] EnterCustomerNoVoice { get; set; }

        [TelephonyVoice(Text = "لطفا رمز تلفنی خود را وارد کرده و در انتها کلید مربع را انتخاب نمایید")]
        public byte[] EnterPassVoice { get; set; }

        [TelephonyVoice(Text = "ورود شما با موفقیت انجام شد")]
        public byte[] WelcomeVoice { get; set; }

        [TelephonyVoice(Text = "شماره مشتری وارد شده معتبر نمی باشد")]
        public byte[] WrongCustomerNoVoice { get; set; }

        [TelephonyVoice(Text = "شماره مشتری یا رمز تلفنی وارد شده معتبر نمی باشد")]
        public byte[] WrongCustomerNoOrPassVoice { get; set; }

        [TelephonyVoice(Text = "احراز هویت شما با موفقیت انجام نشد")]
        public byte[] AuthenticationFailedVoice { get; set; }

        [TelephonyVoice(Text = "در صورت فراموشی اطلاعات کاربری خود عدد صفر را وارد نمایید")]
        public byte[] RecoverCredentialVoice { get; set; }


        public override void Init(IAloVoipTelephonyServer server, ISettingService settingService, bool reloadAllModules)
        {
            base.Init(server, settingService, reloadAllModules);

            if (this.LastSyncDate < this.ModifyDate)
            {
                if (AuthenticationModeIndex == (int)AuthenticationMode.CustomerNumberAndTelephonyPass ||
                    AuthenticationModeIndex == (int)AuthenticationMode.CustomerNumber)
                {
                    var introVoicePath = Path.Combine(Path.GetTempPath(), nameof(IntroVoice) + VoiceFileExtention);
                    var customerNoVoicePath = Path.Combine(Path.GetTempPath(), nameof(EnterCustomerNoVoice) + VoiceFileExtention);
                    List<string> voiceFiles = new List<string> { introVoicePath, customerNoVoicePath };
                    var mergedVoicePath = Path.Combine(Path.GetTempPath(), IntroAndCustomerNumberVoice + VoiceFileExtention);

                    try
                    {
                        File.WriteAllBytes(introVoicePath, IntroVoice);
                        File.WriteAllBytes(customerNoVoicePath, EnterCustomerNoVoice);

                        new WaveIO().Merge(voiceFiles.ToArray(), mergedVoicePath);

                        server.RegisterVoice(GetVoiceFileName(IntroAndCustomerNumberVoice), File.ReadAllBytes(mergedVoicePath));
                    }
                    catch
                    {
                    }
                    finally
                    {
                        try
                        {
                            if (File.Exists(introVoicePath))
                                File.Delete(introVoicePath);

                            if (File.Exists(customerNoVoicePath))
                                File.Delete(customerNoVoicePath);

                            if (File.Exists(mergedVoicePath))
                                File.Delete(mergedVoicePath);
                        }
                        catch { }
                    }
                }
            }

            Log.Debug($"Initing ModuleCustomerClub Id:{this.Id}, Name:{this.Name} finished.");
        }
    }
}