﻿namespace Agent.Application.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.Json.Serialization;

    [Table("ModuleVotingQuestionItem")]
    public partial class ModuleVotingQuestionItem
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int ModuleVotingQuestionId { get; set; }

        public string Title { get; set; }

        public int InputDigit { get; set; }

        public decimal Points { get; set; }


        [JsonIgnore]
        public virtual ModuleVotingQuestion ModuleVotingQuestion { get; set; }
    }
}