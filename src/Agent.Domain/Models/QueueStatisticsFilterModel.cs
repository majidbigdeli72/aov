﻿namespace Agent.Application.Models
{
    public class QueueStatisticsFilterModel
    {
        public int QueueId { get; set; }
        public string Period { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}