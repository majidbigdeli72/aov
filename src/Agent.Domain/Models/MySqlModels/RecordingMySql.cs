﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("recordings")]
    public class RecordingMySql
    {
        [Key]
        public int id { get; set; }
        public string displayname { get; set; }
        public byte[] filename { get; set; }
        public string description { get; set; }
        public byte fcode { get; set; }
        public string fcode_pass { get; set; }
    }
}