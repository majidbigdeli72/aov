﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("queueprio")]
    public class QueuePriorityMySql
    {
        [Key]
        public int queueprio_id { get; set; }
        public string queue_priority { get; set; }
        public string description { get; set; }
        public string dest { get; set; }
    }
}