﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("ivr_entries")]
    public class IvrEntryMySql
    {
        [Key, Column(Order = 0)]
        public int ivr_id { get; set; }
        [Key, Column(Order = 1)]
        public string selection { get; set; }
        public string dest { get; set; }
        public byte ivr_ret { get; set; }
    }
}