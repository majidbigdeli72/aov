﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("timegroups_groups")]
    public class TimeGroupMySql
    {
        [Key]
        public int id { get; set; }
        public string description { get; set; }
    }
}