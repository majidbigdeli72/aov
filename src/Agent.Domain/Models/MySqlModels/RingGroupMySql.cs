﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("ringgroups")]
    public class RingGroupMySql
    {
        [Key]
        public string grpnum { get; set; }
        public string strategy { get; set; }
        public Int16 grptime { get; set; }
        public string grppre { get; set; }
        public string grplist { get; set; }
        public int annmsg_id { get; set; }
        public string postdest { get; set; }
        public string description { get; set; }
        public string alertinfo { get; set; }
        public int remotealert_id { get; set; }
        public string needsconf { get; set; }
        public int toolate_id { get; set; }
        public string ringing { get; set; }
        public string cwignore { get; set; }
        public string cfignore { get; set; }
        public string cpickup { get; set; }
        public string recording { get; set; }
    }
}