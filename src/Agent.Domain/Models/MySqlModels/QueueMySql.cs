﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("queues_config")]
    public class QueueMySql
    {
        [Key]
        public string extension { get; set; }
        public string descr { get; set; }
        public string grppre { get; set; }
        public string alertinfo { get; set; }
        public byte ringing { get; set; }
        public string maxwait { get; set; }
        public string password { get; set; }
        public string ivr_id { get; set; }
        public string dest { get; set; }
        public byte cwignore { get; set; }
        public string qregex { get; set; }
        public int agentannounce_id { get; set; }
        public int joinannounce_id { get; set; }
        public byte queuewait { get; set; }
        public byte use_queue_context { get; set; }
        public byte togglehint { get; set; }
        public byte qnoanswer { get; set; }
        public byte callconfirm { get; set; }
        public int callconfirm_id { get; set; }
        public string monitor_type { get; set; }
        public int monitor_heard { get; set; }
        public int monitor_spoken { get; set; }
        public string callback_id { get; set; }
    }
}
