﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("devices")]
    public class DeviceMySql
    {
        public string id { get; set; }
        public string tech { get; set; }
        public string dial { get; set; }
        public string devicetype { get; set; }
        public string user { get; set; }
        public string description { get; set; }
        public string emergency_cid { get; set; }
    }
}