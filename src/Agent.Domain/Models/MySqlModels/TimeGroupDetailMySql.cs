﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("timegroups_details")]
    public class TimeGroupDetailMySql
    {
        [Key]
        public int id { get; set; }
        public int timegroupid { get; set; }        
        public string time { get; set; }
    }
}