﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("timeconditions")]
    public class TimeConditionMySql
    {
        [Key]
        public int timeconditions_id { get; set; }
        public string displayname { get; set; }
        public int time { get; set; }
        public string truegoto { get; set; }
        public string falsegoto { get; set; }
        public string deptname { get; set; }
        public byte generate_hint { get; set; }
        public string priority { get; set; }
    }
}