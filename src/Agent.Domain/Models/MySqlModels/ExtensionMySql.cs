﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agent.Application.Models.MySqlModels
{
    [Table("users")]
    public class ExtensionMySql
    {
        [Key]
        public string extension { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public string voicemail { get; set; }
        public int ringtimer { get; set; }
        public string noanswer { get; set; }
        public string recording { get; set; }
        public string outboundcid { get; set; }
        public string sipname { get; set; }
        public string mohclass { get; set; }
        public string noanswer_cid { get; set; }
        public string busy_cid { get; set; }
        public string chanunavail_cid { get; set; }
        public string noanswer_dest { get; set; }
        public string busy_dest { get; set; }
        public string chanunavail_dest { get; set; }
    }
}