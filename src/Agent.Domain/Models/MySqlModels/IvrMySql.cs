﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("ivr_details")]
    public class IvrMySql
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string announcement { get; set; }
        public string directdial { get; set; }
        public string invalid_loops { get; set; }
        public string invalid_retry_recording { get; set; }
        public string invalid_destination { get; set; }
        public string invalid_recording { get; set; }
        public string retvm { get; set; }
        public int timeout_time { get; set; }
        public string timeout_recording { get; set; }
        public string timeout_retry_recording { get; set; }
        public string timeout_destination { get; set; }
        public string timeout_loops { get; set; }
        public byte timeout_append_announce { get; set; }
        public byte invalid_append_announce { get; set; }
    }
}