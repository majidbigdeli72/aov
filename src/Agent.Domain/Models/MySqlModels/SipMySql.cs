﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models.MySqlModels
{
    [Table("sip")]
    public class SipMySql
    {
        [Key, Column(Order = 0)]
        public string id { get; set; }
        [Key, Column(Order = 1)]
        public string keyword { get; set; }
        public string data { get; set; }
        public int flags { get; set; }
    }
}