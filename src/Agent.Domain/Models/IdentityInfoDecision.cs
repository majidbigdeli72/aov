﻿using Agent.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace Agent.Application.Models
{
    public class IdentityInfoDecision : Module
    {
        [Required]
        public string ConditionExpression { get; set; }
    }
}
