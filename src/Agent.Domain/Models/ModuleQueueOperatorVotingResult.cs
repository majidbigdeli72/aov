namespace Agent.Application.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ModuleQueueOperatorVotingResult")]
    public partial class ModuleQueueOperatorVotingResult
    {
	    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
	    [Key]
        public long Id { get; set; }
        public DateTime CreateDate { get; set; }
        public int ModuleQueueOperatorVotingId { get; set; }
        public int PeerId { get; set; }
        public long CallId { get; set; }
        public long CallChannelId { get; set; }

        [Required]
        [StringLength(10)]
        public string Input { get; set; }

        public bool IsValid { get; set; }



        public virtual CallChannel CallChannel { get; set; }
        public virtual Call Call { get; set; }
        public virtual Peer Peer { get; set; }
        public virtual ModuleQueueOperatorVoting ModuleQueueOperatorVoting { get; set; }
    }
}