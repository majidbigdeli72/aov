namespace Agent.Application.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("OperatorDepartmentPermission")]
	public partial class OperatorDepartmentPermission
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int OperatorId { get; set; }
        public int DepartmentId { get; set; }
        public bool CanListen { get; set; }
        public bool CanWhisper { get; set; }
        public bool CanConference { get; set; }
        public bool CanHangup { get; set; }


        public virtual Operator Operator { get; set; }
        public virtual Department Department { get; set; }
    }
}