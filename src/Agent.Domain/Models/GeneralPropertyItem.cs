namespace Agent.Application.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("GeneralPropertyItem")]
    public partial class GeneralPropertyItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GeneralPropertyItem()
        {
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key]
        public int Id { get; set; }

        public int GeneralPropertyId { get; set; }
        
        public int Index { get; set; }

        public string Key { get; set; }

        public string Name { get; set; }

        public string Attribute { get; set; }

        
        public virtual GeneralProperty GeneralProperty { get; set; }
    }
}
