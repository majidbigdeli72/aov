namespace Agent.Application.Models
{
    using Agent.Domain.Models;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TelephonySystem")]
    public partial class TelephonySystem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TelephonySystem()
        {
            Calls = new HashSet<Call>();
            Modules = new HashSet<Module>();
            Peers = new HashSet<Peer>();
            Queues = new HashSet<Queue>();
            TeleMarketings = new HashSet<TeleMarketing>();
            Settings = new HashSet<TelephonySystemSetting>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Key { get; set; }

        [StringLength(50)]
        public string BrevityName { get; set; }

        public bool UpdateSettingsIfNotExists { get; set; }

        [Required]
        [StringLength(400)]
        public string ServerAddress { get; set; }

        public int ServerPort { get; set; }

        [Required]
        [StringLength(100)]
        public string AmiUsername { get; set; }

        [Required]
        [StringLength(100)]
        public string AmiPassword { get; set; }

        [Required]
        [StringLength(100)]
        public string LinuxUsername { get; set; }

        [Required]
        [StringLength(100)]
        public string LinuxPassword { get; set; }

        [StringLength(10)]
        public string OutgoingPrefix { get; set; }

        public string RecordingsRootPath { get; set; }

        [StringLength(5)]
        public string CountryAreaCode { get; set; }

        [StringLength(5)]
        public string CityAreaCode { get; set; }

        [Required]
        [StringLength(100)]
        public string MySqlUsername { get; set; }

        [Required]
        [StringLength(100)]
        public string MySqlPassword { get; set; }



        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Call> Calls { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Module> Modules { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Peer> Peers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Queue> Queues { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TeleMarketing> TeleMarketings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TelephonySystemSetting> Settings { get; set; }

        public virtual OperatorSos OperatorSos { get; set; }

        public int TelephonySystemTypeIndex { get; set; }
    }
}
