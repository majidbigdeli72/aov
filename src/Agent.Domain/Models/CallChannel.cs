namespace Agent.Application.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("CallChannel")]
    public partial class CallChannel
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CallChannel()
        {
            Calls = new HashSet<Call>();
            QueueEntries = new HashSet<QueueCaller>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		[Key]
        public long Id { get; set; }

        public long CallId { get; set; }

        [Required]
        [StringLength(50)]
        public string UniqueId { get; set; }

        public int PeerId { get; set; }

        public int State { get; set; }

        public int Response { get; set; }

        public DateTime CreateDate { get; set; }
        [NotMapped]
        public string CreateDatePersian
        {
            get
            {
                var pDate = ((FarsiToolbox.DateAndTime.PersianDateTime)CreateDate);
                return pDate.ToString("HH:mm:ss") + " - " + pDate.ToString("yyyy/MM/dd");
            }
        }
        public int CreateDatePersianInt { get; set; }

        public DateTime? ConnectDate { get; set; }

        [NotMapped]
        public string ConnectDatePersian
        {
            get
            {
                if (ConnectDate.HasValue)
                {
                    var pDate = ((FarsiToolbox.DateAndTime.PersianDateTime)ConnectDate.Value);
                    return pDate.ToString("HH:mm:ss") + " - " + pDate.ToString("yyyy/MM/dd");
                }
                else
                    return "";
            }
        }
        public int? ConnectDatePersianInt { get; set; }


        public DateTime? HangupDate { get; set; }
        [NotMapped]
        public string HangupDatePersian
        {
            get
            {
                if (HangupDate.HasValue)
                {
                    var pDate = ((FarsiToolbox.DateAndTime.PersianDateTime)HangupDate.Value);
                    return pDate.ToString("HH:mm:ss") + " - " + pDate.ToString("yyyy/MM/dd");
                }
                else
                    return "";
            }
        }
        public int? HangupDatePersianInt { get; set; }

        [StringLength(450)]
        public string DialedExtension { get; set; }
        [StringLength(450)]
        public string CallerIdName { get; set; }
        [StringLength(450)]
        public string CallerIdNum { get; set; }

        [StringLength(450)]
        public string RecordFileName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Call> Calls { get; set; }

        public virtual Call Call { get; set; }

        public virtual Peer Peer { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QueueCaller> QueueEntries { get; set; }
    }
}
