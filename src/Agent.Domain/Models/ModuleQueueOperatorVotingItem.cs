
/*namespace Agent.Application.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ModuleQueueOperatorVotingItem")]
    public partial class ModuleQueueOperatorVotingItem
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ModuleQueueOperatorVotingItem()
        {
            ModuleQueueOperatorVotingResults = new HashSet<ModuleQueueOperatorVotingResult>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [NotMapped]
        public string ClientId { get; set; }


        public int ModuleQueueOperatorVotingId { get; set; }

        [Required]
        [StringLength(400)]
        public string Title { get; set; }

        public int InputDigit { get; set; }

        public double Point { get; set; }

        [TelephonyVoice]
        [JsonIgnore]
        public byte[] Voice { get; set; }

        [JsonIgnore]
        public virtual ModuleQueueOperatorVoting ModuleQueueOperatorVoting { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]

        [JsonIgnore]
        public virtual ICollection<ModuleQueueOperatorVotingResult> ModuleQueueOperatorVotingResults { get; set; }
    }
}
*/