﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agent.Application.Models
{
    [Table("DepartmentTelephonySystemHelperExtension")]
    public class DepartmentTelephonySystemHelperExtension
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DepartmentTelephonySystemHelperExtension()
        {
        }


        [Column(Order = 0)]
        public int DepartmentId { get; set; }

        [Column(Order = 1)]
        public int TsId { get; set; }

        public int HelperExtensionId { get; set; }


        [ForeignKey("DepartmentId")]
        public virtual Department Department { get; set; }

        [ForeignKey("TsId")]
        public virtual TelephonySystem TelephonySystem { get; set; }

        [ForeignKey("HelperExtensionId")]
        public virtual Peer Peer { get; set; }
    }
}