﻿namespace Agent.Application.Models
{
    using Agent.Application.Interfaces;
    using Agent.Domain.Attributes;
    using Agent.Domain.Interfaces;
    using Agent.Domain.Models;
    using Serilog;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ModuleVoting")]
    public partial class ModuleVoting : Module
    {
        public readonly string VoteQuestionPrefix = "VoteQuestion_";

        public ModuleVoting()
        {
            ModuleVotingQuestions = new HashSet<ModuleVotingQuestion>();
        }

        [TelephonyVoice(Text = "به نظرسنجی خوش آمدید")]
        public byte[] PreAnnounceVoice { get; set; }

        [TelephonyVoice(Text = "نظر شما ثبت شد. باتشکر از شما")]
        public byte[] PostAnnounceVoice { get; set; }

        [TelephonyVoice(Text = "ورودی نامعتبر می باشد")]
        public byte[] InvalidInputVoice { get; set; }


        public virtual ICollection<ModuleVotingQuestion> ModuleVotingQuestions { get; set; }

        public override void Init(IAloVoipTelephonyServer server, ISettingService settingService, bool reloadAllModules)
        {
            base.Init(server, settingService, reloadAllModules);

            if (this.LastSyncDate < this.ModifyDate)
            {
                foreach (var question in this.ModuleVotingQuestions)
                    server.RegisterVoice(GetVoiceFileName(VoteQuestionPrefix + question.Id), question.QuestionVoice);
            }
            Log.Debug($"Initing ModuleVoting Id:{this.Id}, Name:{this.Name} finished.");
        }
    }
}