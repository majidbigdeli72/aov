using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;

namespace Agent.Application.Models
{
    [Table("Call")]
	public class Call
	{
		[SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public Call()
		{
			CallChannels = new HashSet<CallChannel>();
		}

		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
		public long Id { get; set; }

        public int TsId { get; set; }

        [StringLength(50)]
		public string ProfileId { get; set; }

		[StringLength(400)]
		public string ProfileName { get; set; }

		public DateTime StartDate { get; set; }

        public int StartDatePersianInt { get; set; }

        public DateTime? EndDate { get; set; }
        public int? EndDatePersianInt { get; set; }

        public int CallDirectionIndex { get; set; }
        public int CallTypeIndex { get; set; }

        public int Status { get; set; }

        public TimeSpan? CallDuration { get; set; }

		public TimeSpan? RingDuration { get; set; }

		[StringLength(50)]
		public string PhoneNumber { get; set; }

		public long? InitByChannelId { get; set; }

		public virtual CallChannel InitByChannel { get; set; }

		public int? InitByPeerId { get; set; }
		public virtual Peer InitByPeer { get; set; }

		[SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public virtual ICollection<CallChannel> CallChannels { get; set; }

		public virtual TelephonySystem TelephonySystem { get; set; }
	}
}