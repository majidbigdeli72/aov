﻿namespace Agent.Application.Models
{
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("TelephonySystemLookupSource")]
	public partial class TelephonySystemLookupSource
    {
		public TelephonySystemLookupSource()
		{
        }

        [Column(Order = 0)]
        public int TelephonySystemId { get; set; }

        [ Column(Order = 1)]
        public int LookupSourceId { get; set; }


        public virtual TelephonySystem TelephonySystem { get; set; }
        public virtual LookupSource LookupSource { get; set; }
    }
}
