﻿using System.Collections.Generic;

namespace Agent.Application.Models
{
    public class VoteStatisticsModel
    {
        public VoteStatisticsModel()
        {
            VotingStatistics = new VotingStatistic();
            Queues = new HashSet<VotingQueue>();
            Operators = new HashSet<VotingOperator>();
        }

        public VotingStatistic VotingStatistics { get; set; }
        public IEnumerable<VotingQueue> Queues { get; set; }
        public IEnumerable<VotingOperator> Operators { get; set; }
    }


    public class VotingStatistic
    {
        public VotingStatistic()
        {
            Statistics = new HashSet<VoteQuestionStatistics>();
        }

        public string Title { get; set; }
        public IEnumerable<VoteQuestionStatistics> Statistics { get; set; }
    }
    public class VoteQuestionStatistics
    {
        public VoteQuestionStatistics()
        {
            QuestionResult = new HashSet<object>();
        }

        public string QuestionTitle { get; set; }
        public IEnumerable<object> QuestionResult { get; set; }
    }
    public class VotingQueue
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
    public class VotingOperator
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}