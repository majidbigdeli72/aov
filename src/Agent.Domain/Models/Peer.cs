namespace Agent.Application.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Peer")]
    public partial class Peer
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Peer()
        {
            Calls = new HashSet<Call>();
            CallChannels = new HashSet<CallChannel>();
            QueueEntries = new HashSet<QueueCaller>();
        }

		[Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int TsId { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        public int PeerProtocolTypeIndex { get; set; }

        public int PeerTypeIndex { get; set; }

        public int? OwnerOperatorId { get; set; }

        public bool Enabled { get; set; }

        public int? DepartmentId { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Call> Calls { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CallChannel> CallChannels { get; set; }

        public virtual Operator Operator { get; set; }

        public virtual TelephonySystem TelephonySystem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QueueCaller> QueueEntries { get; set; }

        public virtual Department Department { get; set; }
    }
}
