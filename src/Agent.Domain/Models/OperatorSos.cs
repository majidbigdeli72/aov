﻿namespace Agent.Application.Models
{
    using Agent.Domain.Attributes;
    using Agent.Domain.Interfaces;
    using Serilog;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("OperatorSos")]
    public partial class OperatorSos
    {
        [Key]
        public int TsId { get; set; }

        [Required]
        [StringLength(20)]
        public string RequestCode { get; set; }


        [Required]
        public int OperatorManagerLookupModeTypeIndex { get; set; }

        public int? OperatorManagerLookupSourceId { get; set; }


        [NotMapped]
        public int Id
        {
            get
            {
                return TsId;
            }
        }


        [TelephonyVoice(Text = "درخواست کمک به اپراتور ارسال شد")]
        public byte[] OperatorSosSentVoice { get; set; }

        [TelephonyVoice(Text = "مدیر مربوطه در دسترس نمی باشد")]
        public byte[] ManagerNotFoundVoice { get; set; }


        [NotMapped]
        public string OperatorSosSentVoiceFileName
        {
            get
            {
                return $"OSV_{this.TsId}_{nameof(this.OperatorSosSentVoice)}";
            }
        }

        [NotMapped]
        public string ManagerNotFoundVoiceFileName
        {
            get
            {
                return $"OSV_{this.TsId}_{nameof(this.ManagerNotFoundVoice)}";
            }
        }


        public virtual TelephonySystem TelephonySystem { get; set; }



        public void Init(IAloVoipTelephonyServer server)
        {
            if (OperatorSosSentVoice != null)
                server.RegisterVoice(OperatorSosSentVoiceFileName, OperatorSosSentVoice);

            if (ManagerNotFoundVoice != null)
                server.RegisterVoice(ManagerNotFoundVoiceFileName, ManagerNotFoundVoice);

            Log.Debug($"Initing ModuleOperatorSos TsId:{this.TsId} finished.");
        }
    }
}