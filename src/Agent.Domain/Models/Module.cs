﻿namespace Agent.Domain.Models
{
    using Agent.Application.Interfaces;
    using Agent.Application.Models;
    using Agent.Domain.Attributes;
    using Agent.Domain.Enum;
    using Agent.Domain.Interfaces;
    using Septa.TelephonyServer;
    using Septa.TelephonyServer.Entries;
    using Serilog;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.IO;
    using System.Linq;
    using System.Reflection;

    [Table("Module")]
    public partial class Module
    {
        public const string SilentVoiceFileName = "Silent";
        public const string VoiceFileExtention = ".wav";

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public virtual int Id { get; set; }

        public int TsId { get; set; }

        public int? LookupSourceId { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        [Required]
        [StringLength(300)]
        public string Name { get; set; }

        public int ModuleTypeIndex { get; set; }

        [NotMapped]
        public ModuleType ModuleType
        {
            get { return (ModuleType)ModuleTypeIndex; }
        }

        public string ModuleTypeName
        {
            get
            {
                switch (ModuleType)
                {
                    case ModuleType.CustomerClub:
                        return "باشگاه مشتریان";
                    case ModuleType.QueueOperatorVoting:
                        return "نظرسنجی اپراتور";
                    case ModuleType.SmartReconnect:
                        return "تماس مجدد";
                    case ModuleType.RoleConnect:
                        return "انتقال هوشنمد تماس بر اساس تخصص";
                    //case ModuleType.QueueOperatorAnnouncement:
                    //    return "اعلام اپراتور پاسخگو";
                    case ModuleType.CardtableConnect:
                        return "اتصال به کارتابل";
                    case ModuleType.CheckContract:
                        return "بررسی قرارداد";
                    case ModuleType.Pay:
                        return "پرداخت واسط";
                    case ModuleType.AdvanceFollowMe:
                        return "پیگیری تماس";
                    case ModuleType.Voting:
                        return "نظرسنجی";

                }

                return "";
            }
        }

        public bool ReInitOnStartup { get; set; }

        public bool Enabled { get; set; }

        public bool SilentMode { get; set; }


        [StringLength(50)]
        public string NextCode { get; set; }
        [NotMapped]
        public string DestinationCodeForNextCode { get; set; }
        [NotMapped]
        public string CustomNextCode { get; set; }
        [NotMapped]
        public DestinationType DestinationTypeForNextCode
        {
            get
            {
                return BaseDestinationEntry.GetDestinationType(NextCode);
            }
        }


        [StringLength(50)]
        public string FailedCode { get; set; }
        [NotMapped]
        public string DestinationCodeForFailedCode { get; set; }
        [NotMapped]
        public string CustomFailedCode { get; set; }
        [NotMapped]
        public DestinationType DestinationTypeForFailedCode
        {
            get
            {
                return BaseDestinationEntry.GetDestinationType(FailedCode);
            }
        }

        public DateTime? LastSyncDate { get; set; }
        public DateTime ModifyDate { get; set; }


        public virtual TelephonySystem TelephonySystem { get; set; }

        public virtual LookupSource LookupSource { get; set; }


        public virtual void Init(IAloVoipTelephonyServer server, ISettingService settingService, bool reloadAllModules)
        {
            Log.Debug($"Start Init module Id:{this.Id}, Name:{this.Name}.");

            server.RegisterModule(this.Id, this.Name, this.Code, this.ModuleType, settingService.GetSystemIp().ToString(), this.FailedCode);

            if (reloadAllModules)
                server.ReloadModules();

            if (this.LastSyncDate < this.ModifyDate)
            {
                var voiceProps = this.GetType().GetProperties().Where(p => p.IsDefined(typeof(TelephonyVoiceAttribute), true));
                foreach (var voiceProp in voiceProps)
                    server.RegisterVoice(GetVoiceFileName(voiceProp.Name), GetVoice(voiceProp, settingService));
            }
            Log.Debug($"Initing module Id:{this.Id}, Name:{this.Name} finished.");
        }

        public string GetVoiceFileName(string propertyName)
        {
            return $"MV_{this.Id}_{propertyName}";
        }

        protected byte[] GetVoice(PropertyInfo prop, ISettingService settingService)
        {
            var voice = prop.GetValue(this) as byte[];

            if (voice != null && voice.Length > 0)
            {
                return voice;
            }
            else if (voice == null)
            {
                return File.ReadAllBytes(Path.Combine(settingService.AgentVoiceArchivePath, ModuleType.ToString(), prop.Name + VoiceFileExtention));
            }
            else
            {
                return GetSilentVoice(settingService);
            }
        }

        public static byte[] GetSilentVoice(ISettingService settingService)
        {
            return File.ReadAllBytes(Path.Combine(settingService.AgentCommonVoiceArchivePath, SilentVoiceFileName + VoiceFileExtention));
        }
    }
}
