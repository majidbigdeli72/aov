﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agent.Application.Models
{
    public class QueueStatisticsModel
    {
        public QueueStatisticsModel()
        {
            QueueStatistics = new QueueStatistic();
            OperatorStatistics = new HashSet<OperatorStatistic>();
        }

        public QueueStatistic QueueStatistics { get; set; }
        public IEnumerable<OperatorStatistic> OperatorStatistics { get; set; }
    }


    public class QueueStatistic
    {
        public QueueStatistic()
        {
            Statistics = new HashSet<object>();
        }

        public string Title { get; set; }
        public IEnumerable<object> Statistics { get; set; }
    }
    public class OperatorStatistic
    {
        public int OperatorId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public IEnumerable<object> Statistics { get; set; }
    }
}