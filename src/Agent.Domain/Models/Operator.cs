namespace Agent.Application.Models
{
    using Agent.Domain.Models.Identity;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Operator")]
	public partial class Operator
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
		public Operator()
		{
			Peers = new HashSet<Peer>();
            DepartmentPermissions = new HashSet<OperatorDepartmentPermission>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int GenderIndex { get; set; }

        [Required]
		[StringLength(200)]
		public string FirstName { get; set; }

		[Required]
		[StringLength(200)]
		public string LastName { get; set; }

        public byte[] NameVoice { get; set; }

        public byte[] Avatar { get; set; }

        public int? UserId { get; set; }
        public virtual ApplicationUser User { get; set; }

        [StringLength(11)]
        public string Mobile { get; set; }

        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Peer> Peers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OperatorDepartmentPermission> DepartmentPermissions { get; set; }
    }
}