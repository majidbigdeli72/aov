﻿using Agent.Domain.Attributes;
using Agent.Domain.Models;
using System.ComponentModel.DataAnnotations.Schema;


namespace Agent.Application.Models
{
    [Table("ModulePay")]
    public partial class ModulePay : Module
    {
        public string MoneyAccountUserKey { get; set; }
        public bool WorkInAnonymousMode { get; set; }
        public string BillableObjectTypeKey { get; set; }
        public string LookupNumberFieldKey { get; set; }
        public string ValueFieldKey { get; set; }
        public bool SendPaymentLink { get; set; }
        public bool ForceToGetNewMobile { get; set; }

        [TelephonyVoice(Text ="لطفا شماره آیتم مورد نظر را وارد کرده و در انتها کلید مربع را انتخاب نمایید")]
        public byte[] EnterNumberVoice { get; set; }

        [TelephonyVoice(Text = "شماره وارد شده صحیح نیست. لطفا مجددا سعی نمایید")]
        public byte[] WrongNumberVoice { get; set; }

        [TelephonyVoice(Text = "صورتحساب شما مبلغ")]
        public byte[] PreSayBalanceVoice { get; set; }

        [TelephonyVoice(Text = "ریال می باشد")]
        public byte[] AfterSayBalanceVoice { get; set; }

        [TelephonyVoice(Text = "در صورت تمایل به دریافت لینک پرداخت عدد 1 و در غیر این صورت عدد 0 را وارد نمایید")]
        public byte[] SelectSendLinkVoice { get; set; }

        [TelephonyVoice(Text = "شماره شما")]
        public byte[] PreSayCurrentMobileVoice { get; set; }

        [TelephonyVoice(Text = "می باشد")]
        public byte[] AfterSayCurrentMobileVoice { get; set; }

        [TelephonyVoice(Text = "در صورت تمایل به ادامه با همین شماره عدد 1 و در غیر این صورت عدد 0 را وارد نمایید")]
        public byte[] SelectCurrentMobileVoice { get; set; }

        [TelephonyVoice(Text = "لطفا شماره موبایل خود را وارد کرده و در انتها کلید مربع را انتخاب نمایید")]
        public byte[] EnterMobileVoice { get; set; }

        [TelephonyVoice(Text = "شماره موبایل وارد شده صحیح نیست. لطفا مجددا سعی نمایید")]
        public byte[] WrongMobileVoice { get; set; }

        [TelephonyVoice(Text = "شماره وارد شده")]
        public byte[] PreSayNewMobileVoice { get; set; }

        [TelephonyVoice(Text = "می باشد")]
        public byte[] AfterSayNewMobileVoice { get; set; }

        [TelephonyVoice(Text = "اگر صحیح است عدد 1 و در غیر این صورت عدد 0 را وارد نمایید")]
        public byte[] RecheckNewMobileVoice { get; set; }

        [TelephonyVoice(Text = "لینک پرداخت به شماره شما ارسال شد. از شما جهت استفاده از خدمات غیرحضوری صمیمانه متشکریم.")]
        public byte[] SendAndThanksVoice { get; set; }

        [TelephonyVoice(Text = "صورت حسابی جهت پرداخت یافت نشد")]
        public byte[] NoBillFoundToPay { get; set; }

        [TelephonyVoice(Text = "ارسال لینک پرداخت با موفقیت انجام نشد")]
        public byte[] FailedToSendLink { get; set; }
    }
}