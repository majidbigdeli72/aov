﻿namespace Agent.Application.Models
{
    using Agent.Domain.Attributes;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.Json.Serialization;

    [Table("ModuleVotingQuestion")]
    public partial class ModuleVotingQuestion
    {
        public ModuleVotingQuestion()
        {
            ModuleVotingQuestionItems = new HashSet<ModuleVotingQuestionItem>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [NotMapped]
        public Guid ClientId
        {
            get
            {
                return Guid.NewGuid();
            }
        }

        public int ModuleVotingId { get; set; }

        public string Title { get; set; }

        public decimal Weight { get; set; }

        [TelephonyVoice(Text = "سوال")]
        public byte[] QuestionVoice { get; set; }

        [NotMapped]
        public string QuestionVoiceString { get; set; }


        [JsonIgnore]
        public virtual ModuleVoting ModuleVoting { get; set; }

        public virtual ICollection<ModuleVotingQuestionItem> ModuleVotingQuestionItems { get; set; }
    }
}