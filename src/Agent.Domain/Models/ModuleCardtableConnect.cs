﻿using Agent.Domain.Models;
using System.ComponentModel.DataAnnotations;

namespace Agent.Application.Models
{
    public class ModuleCardtableConnect : Module
    {
        [Required]
        [StringLength(450)]
        public string CrmObjectTypeKey { get; set; }
    }
}
