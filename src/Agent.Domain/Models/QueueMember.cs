﻿namespace Agent.Application.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("QueueMember")]
    public partial class QueueMember
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }

        public int QueueId { get; set; }

        public int ExtensionId { get; set; }

        public bool IsLogined { get; set; }

        public DateTime LastLoginDate { get; set; }

        public DateTime? LastLogoffDate { get; set; }



        public virtual Peer Extension { get; set; }
        public virtual Queue Queue { get; set; }
    }
}