namespace Agent.Application.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Queue")]
    public partial class Queue
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Queue()
        {
            QueueEntries = new HashSet<QueueCaller>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public int TsId { get; set; }

        [Required]
        [StringLength(50)]
        public string Code { get; set; }

        [Required]
        [StringLength(200)]
        public string Name { get; set; }

        public bool Enabled { get; set; }

        public bool JoinPlayAlert { get; set; }

        public bool LeavePlayAlert { get; set; }

        public TimeSpan WaitTimeMinThreshold { get; set; }
        public TimeSpan WaitTimeCriticalThreshold { get; set; }
        public bool WaitTimeCriticalThresholdPlayAlert { get; set; }
        public TimeSpan WaitTimeMaxThreshold { get; set; }
        public bool WaitTimeMaxThresholdPlayAlert { get; set; }


        public bool RemoveFromAbandonedAfterCallback { get; set; }

        public virtual TelephonySystem TelephonySystem { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<QueueCaller> QueueEntries { get; set; }
    }
}
