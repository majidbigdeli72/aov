﻿namespace Agent.Application.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ModuleVotingQuestionResult")]
    public partial class ModuleVotingQuestionResult
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        public DateTime CreateDate { get; set; }

        public int ModuleVotingQuestionItemId { get; set; }
        public long CallChannelId { get; set; }
        public int? QueueId { get; set; }        
        public int? PeerId { get; set; }

        public string Input { get; set; }
        public bool IsValid { get; set; }


        public virtual ModuleVotingQuestionItem ModuleVotingQuestionItem { get; set; }
        public virtual CallChannel CallChannel { get; set; }
        public virtual Queue Queue { get; set; }        
        public virtual Peer Peer { get; set; }
    }
}