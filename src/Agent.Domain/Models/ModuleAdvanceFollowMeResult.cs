﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Agent.Application.Models
{
    [Table("ModuleAdvanceFollowMeResult")]
    public class ModuleAdvanceFollowMeResult
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public long Id { get; set; }
        public int ModuleAdvanceFollowMeId { get; set; }
        public DateTime InputDate { get; set; }
        public string InputDTMF { get; set; }
        public int ModuleAdvanceFollowMeInputTypeIndex { get; set; }
        public int ExtensionId { get; set; }
        public long CallChannelId { get; set; }
        public int? CallBackStatusIndex { get; set; }
        public int CallBackRetryCount { get; set; }
        public DateTime? LastCallBackRetryDate { get; set; }
        public virtual ModuleAdvanceFollowMe ModuleAdvancedFollowMe { get; set; }
        public virtual Peer Extension { get; set; }
        public virtual CallChannel CallChannel { get; set; }
    }
}
