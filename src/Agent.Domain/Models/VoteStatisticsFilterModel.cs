﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agent.Application.Models
{
    public class VoteStatisticsFilterModel
    {
        public int VotingId { get; set; }
        public string Period { get; set; }
        public string From { get; set; }
        public string To { get; set; }

        public int? QueueId { get; set; }
        public int? OperatorId { get; set; }
    }
}