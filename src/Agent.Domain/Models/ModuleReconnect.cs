namespace Agent.Application.Models
{
    using Agent.Domain.Models;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("ModuleReconnect")]
    public partial class ModuleReconnect : Module
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ModuleReconnect()
        {
            ModuleReconnectScopeItems = new HashSet<ModuleReconnectScopeItem>();
        }
        
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ModuleReconnectScopeItem> ModuleReconnectScopeItems { get; set; }
    }
}
