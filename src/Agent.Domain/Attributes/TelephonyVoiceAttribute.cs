﻿using System;

namespace Agent.Domain.Attributes
{
    class TelephonyVoiceAttribute : Attribute
    {
        public string Text { get; set; }
    }
}
