﻿using FarsiToolbox.DateAndTime;
using Humanizer;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Agent.Domain.Helper
{
    public static class Common
    {
        public const string AdminUsername = "admin";


        public static string HumanizedString(DateTime dateTime)
        {
            return dateTime.Humanize(utcDate: false, culture: new CultureInfo("fa-IR"));
        }
        public static string HumanizedString(TimeSpan timeSpan)
        {
            return timeSpan.Humanize(precision: 1, culture: new CultureInfo("fa-IR"));
        }

        public static DateTime? ConvertPersianDateToGeorgianDate(string persianStringDateTime)
        {
            PersianDateTime persianDate;
            if (PersianDateTime.TryParse(persianStringDateTime, out persianDate))
                return (DateTime)persianDate;

            return null;
        }

        public static List<T[]> Segmentize<T>(this IEnumerable<T> list, int segmentSize = 2000)
        {
            List<T[]> toReturn = new List<T[]>();

            for (int i = 0; i * segmentSize < list.Count(); i++)
            {
                T[] toget = list.Skip(i * segmentSize).Take((list.Count() - (i * segmentSize)) > segmentSize
                                                                ? segmentSize
                                                                : list.Count() - (i * segmentSize)).ToArray();
                toReturn.Add(toget);
            }
            return toReturn;
        }
    }
}
