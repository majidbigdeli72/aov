﻿using System;

namespace Agent.Domain.Helper
{
    public static class SystemDateTime
    {
        private static Func<DateTime> _now;

        public static DateTime Now
        {
            get
            {
                return _now();
            }
        }

        public static void Init(Func<DateTime> now)
        {
            _now = now;
        }
    }


}
