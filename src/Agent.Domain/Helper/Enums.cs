﻿using System.Collections.Generic;
using System.Linq;

namespace Agent.Domain.Helper
{
    public static class Enums
    {
        public static IEnumerable<T> GetEnumMembers<T>()
        {
            return System.Enum.GetValues(typeof(T)).Cast<T>();
        }
    }
}
