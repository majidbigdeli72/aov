﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace Agent.Domain.Helper
{
    public static class CallApiExtention
    {
        public static string Request(string url, object model, string method = "POST")
        {

            var http = (HttpWebRequest)WebRequest.Create(new Uri(url));

            http.Accept = "application/json";
            http.ContentType = "application/json";
            http.Method = method;
            string parsedContent = System.Text.Json.JsonSerializer.Serialize(model);
            ASCIIEncoding encoding = new ASCIIEncoding();
            Byte[] bytes = encoding.GetBytes(parsedContent);

            using (var newStream = http.GetRequestStream())
            {
                newStream.Write(bytes, 0, bytes.Length);

                using (var response = http.GetResponse())
                using (var stream = response.GetResponseStream())
                using (var sr = new StreamReader(stream))
                    return sr.ReadToEnd();
            }
        }
    }

}
