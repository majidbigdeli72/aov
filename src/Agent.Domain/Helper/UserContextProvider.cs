﻿using Agent.Domain.Interfaces;

namespace Agent.Domain.Helper
{
    public static class UserContextProvider
    {
        private static IUserContext _userContext;
        public static void Init(IUserContext userContext)
        {
            _userContext = userContext;
        }

        public static IUserContext CurrentUserContext
        {
            get
            {
                return _userContext;
            }
        }
    }
}
