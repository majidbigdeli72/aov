﻿using Agent.Application.Models;
using Septa.TelephonyServer.Entries;

namespace Agent.Domain.Helper
{
    public static class Extensions
    {
        public static Call ToCall(this CallEntry callEntry)
        {
            var toReturn = new Call()
            {
                TsId = callEntry.TsId,
                ProfileId = callEntry.ProfileId,
                ProfileName = callEntry.ProfileName,
                StartDate = callEntry.StartDate,
                EndDate = callEntry.EndDate,

                //CreateDatePersianInt = e.CreateDate. //TODO
                CallDirectionIndex = (int)callEntry.CallDirection,
                CallTypeIndex = (int)callEntry.CallType,
                Status = (int)callEntry.Status,
                CallDuration = callEntry.CallDuration,
                RingDuration = callEntry.RingDuration,
                PhoneNumber = callEntry.PhoneNumber,
                InitByPeerId = callEntry.InitByChannel.OwnerPeer?.DbId
            };

            if (callEntry.DbId.HasValue)
            {
                toReturn.Id = callEntry.DbId.Value;
                toReturn.InitByChannelId = callEntry.InitByChannel.DbId;
            }

            return toReturn;
        }

        public static CallChannel ToCallChannel(this CallChannelEntry callChannelEntry)
        {
            var toReturn = new CallChannel()
            {
                //CallId = callChannelEntry.CallId,
                UniqueId = callChannelEntry.UniqueId,
                PeerId = callChannelEntry.OwnerPeer.DbId,
                CreateDate = callChannelEntry.CreateDate,
                ConnectDate = callChannelEntry.ConnectDate,
                HangupDate = callChannelEntry.HangupDate,
                State = (int)callChannelEntry.State,
                Response = (int)callChannelEntry.Response,
                DialedExtension = callChannelEntry.DialedExtension,
                CallerIdName = callChannelEntry.CallerIdName,
                CallerIdNum = callChannelEntry.CallerIdNum,
                RecordFileName = callChannelEntry.RecordFileName
            };

            if (callChannelEntry.DbId.HasValue)
                toReturn.Id = callChannelEntry.DbId.Value;

            if (callChannelEntry.ParentCall != null && callChannelEntry.ParentCall.DbId.HasValue)
                toReturn.CallId = callChannelEntry.ParentCall.DbId.Value;

            return toReturn;
        }

        public static QueueCaller ToQueueCaller(this QueueCallerEntry queueCallerEntry)
        {
            if (queueCallerEntry.ParentQueue == null ||
                queueCallerEntry.CallChannel == null ||
                queueCallerEntry.CallChannel.DbId == null)
            {
                return null;
            }

            QueueCaller toReturn = new QueueCaller()
            {
                QueueId = queueCallerEntry.ParentQueue.DbId,
                CallChannelId = queueCallerEntry.CallChannel.DbId.Value,
                QueueCallerStatusIndex = (int)queueCallerEntry.Status,
                JoinDate = queueCallerEntry.JoinDate,
                LeaveDate = queueCallerEntry.LeaveDate,
                JoinPosition = queueCallerEntry.JoinPosition,
                AbandonPosition = queueCallerEntry.AbandonPosition
            };

            if (queueCallerEntry.AnsweredBy != null)
            {
                toReturn.AnsweredByCallChannelId = queueCallerEntry.AnsweredBy.DbId;
                toReturn.AnsweredByPeerId = queueCallerEntry.AnsweredBy.OwnerPeer.DbId;
            }

            if (queueCallerEntry.DbId.HasValue)
                toReturn.Id = queueCallerEntry.DbId.Value;

            return toReturn;
        }
    }

}
