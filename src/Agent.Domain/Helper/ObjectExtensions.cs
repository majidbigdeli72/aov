﻿using System.ComponentModel;

namespace Agent.Domain.Helper
{
    public static class ObjectExtensions
    {
        public static T? To<T>(this object obj) where T : struct
        {
            if (obj == null)
            {
                return null;
            }

            try
            {
                var converter = TypeDescriptor.GetConverter(typeof(T));
                return (T?)converter.ConvertFromString(obj.ToString());
            }
            catch
            {
                return default(T?);
            }
        }
    }

}
