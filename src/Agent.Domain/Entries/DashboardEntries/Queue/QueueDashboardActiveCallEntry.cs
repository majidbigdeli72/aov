﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Septa.AloVoip.Domain.Entries
{
    public class QueueDashboardActiveCallEntry
    {
        public string Caller { get; set; }

        public string Queue { get; set; }

        public string Agent { get; set; }

        public string ConnectDate { get; set; }

        public string Wait { get; set; }

        public string Talk { get; set; }
    }
}