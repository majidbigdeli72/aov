﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Septa.AloVoip.Domain.Entries
{
    public class QueueDashboardWaitCallEntry
    {
        public long CallerId { get; set; }
        public string Caller { get; set; }
        public string Queue { get; set; }
        public string JoinDate { get; set; }
        public string Wait { get; set; }
    }
}