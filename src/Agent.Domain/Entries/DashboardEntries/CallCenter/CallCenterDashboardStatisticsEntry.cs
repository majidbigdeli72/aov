﻿using System.Collections.Generic;

namespace Septa.AloVoip.Domain.Entries
{
    public class CallCenterDashboardStatisticsEntry
    {
        public CallCenterDashboardStatisticsEntry()
        {
            Extensions = new HashSet<CallCenterDashboardExtensionEntry>();
        }

        public int DepartmentId { get; set; }
        public string DepartmentTitle { get; set; }

        public bool CanListen { get; set; }
        public bool CanWhisper { get; set; }
        public bool CanConference { get; set; }
        public bool CanHangup { get; set; }

        public IEnumerable<CallCenterDashboardExtensionEntry> Extensions { get; set; }
    }
}