﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Septa.AloVoip.Domain.Entries
{
    [Serializable]
    [DataContract]
    public class CallCenterDashboardOperatorCallEntry
    {
        [DataMember]
        public string ChannelName { get; set; }

        [DataMember]
        public string CallerName { get; set; }

        [DataMember]
        public string CallerNum { get; set; }

        [DataMember]
        public string CallDuration { get; set; }
    }
}