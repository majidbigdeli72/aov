﻿namespace Septa.AloVoip.Domain.Entries
{
    public enum OperatorStatus
    {
        Available = 1,
        Busy = 2,
        NotAvailable = 3,
        Ringing = 4,
        Disabled = 5
    }
}