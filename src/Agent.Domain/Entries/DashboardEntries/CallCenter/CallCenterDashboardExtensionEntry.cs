﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Septa.AloVoip.Domain.Entries
{
    [Serializable]
    [DataContract]
    public class CallCenterDashboardExtensionEntry
    {
        public CallCenterDashboardExtensionEntry()
        {
            Calls = new HashSet<CallCenterDashboardOperatorCallEntry>();
        }

        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Ext { get; set; }

        [DataMember]
        public string Status { get; set; }

        [DataMember]
        public string CallType { get; set; }

        [DataMember]
        public int? OperatorId { get; set; }

        [DataMember]
        public string Trunk { get; set; }

        [DataMember]
        public IEnumerable<CallCenterDashboardOperatorCallEntry> Calls { get; set; }
    }
}