﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace Septa.AloVoip.Domain.Entries
{
    public class DashboardPagingResultEntry<T> where T : class
    {
        public int TotalRows { get; set; }

        public IEnumerable<T> Data { get; set; }
    }
}