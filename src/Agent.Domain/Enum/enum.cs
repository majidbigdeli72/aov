﻿using Septa.TelephonyServer;
using System;
using System.Collections.Generic;
using System.Text;

namespace Agent.Domain.Enum
{
    public enum RequestResponeType
    {
        [DisplayName("---")]
        Nothing = 1,

        [DisplayName("درخواست تماس")]
        Callback = 2,

        [DisplayName("درخواست انتقال")]
        Transfer = 3,
    }

    public enum SortDirection
    {
        Ascending = 0,
        Descending = 1
    }

    public enum TeleMarketingStatus
    {
        Queued = 1,
        Retrying = 2,
        Succeed = 3,
        Failed = 4,
    }

    public enum TeleMarketingTargetProcessStatus
    {
        Queued = 1,
        Fetched = 2,
        Processed = 3
    }

    public enum ModuleType
    {
        // Has no lookup source        
        //QueueOperatorAnnouncement = 1, //OK
        QueueOperatorVoting = 2, //OK
        SmartReconnect = 3, //OK
        SendMessage = 4,
        HelpRequest = 5,
        NumberMatch = 6,

        CustomerClub = 11, //OK
        CheckContract = 12, //OK
        Pay = 13,
        RoleConnect = 14, //OK
        CardtableConnect = 15,
        SayProcessState = 16,
        CallbackRequest = 17,
        IdentityInfoDecision = 18,
        AdvanceFollowMe = 19,
        Voting = 20,
        OperatorSos = 21
    }
    public enum Gender
    {
        Male = 1,
        Female = 2,
    }

    public enum LookupSourceType
    {
        Payamgostar = 1,
    }

    public enum AuthenticationMode
    {
        PhoneNumber = 1,
        CustomerNumber,
        // شماره مشتری و رمز تلفنی
        CustomerNumberAndTelephonyPass
    }

    public enum TargetRoleType
    {
        SalesPerson = 1,
        SupportPerson,
        OtherPerson
    }

    [Flags]
    public enum UserRole
    {
        Admin = 1,
        Operator,
        VoipManager
    }

    public enum TelephonySystemType
    {
        Astrisk = 1,
        Dialexia = 2,
        BicomSystems = 3,
        Trixbox = 4,
        ThreeCXPhoneSystem = 5
    }

    public enum ModuleAdvanceFollowMeInputType
    {
        Wait = 1,
        VoiceMail = 2,
        CallBack = 3
    }
    public enum ChannelStatus
    {
        Dead = -1,
        DownAndAvailable = 0,
        DownButReserved = 1,
        OffHook = 2,
        DigitsOrEquivalentHaveBeenDialed = 3,
        Ringing = 4,
        RemoteEndIsRinging = 5,
        Up = 6,
        Busy = 7

    }


    public enum CallBackStatus
    {
        Pending = 0,
        Success = 1,
        Failed = 2
    }

    public enum ExtentionActionType
    {
        [DisplayName("وارد شده به صف")]
        LoginToQueue = 1,

        [DisplayName("خارج شده از صف")]
        LogoffFromQueue
    }

    public enum AloVoIPFeature
    {
        UserLimit,

        Module_CustomerClub,
        Module_Voting,
        Module_QueueOperatorVoting,
        Module_Reconnect,
        Module_RoleConnect,
        Module_CheckContract,
        Module_CardtableConnect,
        Module_Pay,
        Module_AdvanceFollowMe,
        Module_TeleMarketing,
        Module_OperatorSos,

        Dashboard_Queue,
        Dashboard_CallCenter,
        Dashboard_CallbackRequest,
        Dashboard_CallReport,
        Dashboard_AgentReport,

        Manage_ServerSettings
    }

    public enum AloVoIPClaims
    {
        [DisplayName("مشاهده داشبورد صف")]
        Show_Dashboard_Queue,

        [DisplayName("مشاهده داشبورد مرکز تماس")]
        Show_Dashboard_CallCenter,

        [DisplayName("مشاهده داشبورد درخواست تماس")]
        Show_Dashboard_CallbackRequest,

        [DisplayName("مشاهده گزارش تماس")]
        Show_Dashboard_CallReport,

        [DisplayName("مدیریت ماژول ها")]
        Manage_Modules,

        [DisplayName("مدیریت سیستم های تلفنی")]
        Manage_TelephonySystems,

        [DisplayName("مدیریت منابع جستجو")]
        Manage_LookupSources,

        [DisplayName("مدیریت صف ها")]
        Manage_Queues,

        [DisplayName("مدیریت اپراتورها")]
        Manage_Operators,

        [DisplayName("مدیریت داخلی ها")]
        Manage_Extensions,

        [DisplayName("مدیریت دپارتمان ها")]
        Manage_Departments,

        [DisplayName("ایجاد تماس")]
        Create_Call,

        [DisplayName("تغییر رمز عبور")]
        Account_ChangePassword,

        [DisplayName("مدیریت تنظیمات سرور")]
        Manage_ServerSettings,

        [DisplayName("مشاهده گزارش داخلی ها")]
        Show_Dashboard_AgentReport,
    }

    public enum DownloadVoiceType
    {
        [DisplayName("همه تماس")]
        ByCall = 1,

        [DisplayName("کانال خاص")]
        ByChannel
    }

    public enum TelephonySystemSettingType
    {
        [DisplayName("کد درخواست کمک اپراتور")]
        OperatorSos = 1,

        [DisplayName("نوع تشخیص مدیر")]
        OperatorManagerLookupMode = 2,

        [DisplayName("کد منبع جستجو جهت تشخیص مدیر")]
        OperatorManagerLookupSourceId = 3,
    }

    public enum OperatorManagerLookupModeType
    {
        [DisplayName("تشخیص مدیر با توجه به دپارتمان")]
        ByOrganizationDepartment = 1,

        [DisplayName("تشخیص مدیر از طریق منبع جستجو")]
        ByLookupSource = 2
    }
}
