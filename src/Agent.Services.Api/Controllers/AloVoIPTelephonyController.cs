﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agent.Application.Interfaces;
using Agent.Application.ViewModels;
using Agent.Domain.Core.Bus;
using Agent.Domain.Core.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Agent.Services.Api.Controllers
{
    public class AloVoIPTelephonyController : ApiController
    {
        private readonly IAloVoipTelephonyService _aloVoipTelephonyService;

        public AloVoIPTelephonyController(
            IAloVoipTelephonyService aloVoipTelephonyService,
            INotificationHandler<DomainNotification> notifications,
            IMediatorHandler mediator) : base(notifications, mediator)
        {
            _aloVoipTelephonyService = aloVoipTelephonyService;
        }

        [HttpPost]
        [Route("CanDial")]
        public IActionResult CanDial(string tsKey)
        {
            return Response(_aloVoipTelephonyService.CanDial(tsKey));
        }

        [HttpPost]
        [Route("ChannelHangup")]
        public IActionResult ChannelHangup(string tsKey, string extn)
        {
            return Response(_aloVoipTelephonyService.ChannelHangup(tsKey,extn));
        }

        [HttpPost]
        [Route("ChannelSpy")]
        public IActionResult ChannelSpy(string tsKey, string extn, string spiedExtn, int spyMode)
        {
           return Response(_aloVoipTelephonyService.ChannelSpy(tsKey, extn, spiedExtn, spyMode));
        }

        [HttpPost]
        [Route("CheckConnection")]
        public IActionResult CheckConnection(int telephonySystemTypeIndex, string serverAddress, int serverPort, string amiUserName, string amiPassword, string linuxUserName, string linuxPassword, string mysqlUserName, string mysqlPassword)
        {
            return Response(_aloVoipTelephonyService.CheckConnection(telephonySystemTypeIndex, serverAddress, serverPort, amiUserName, amiPassword, linuxUserName, linuxPassword, mysqlUserName, mysqlPassword));
        }

        [HttpPost]
        [Route("DestroyModule")]
        public IActionResult DestroyModule(int deletedModuleId)
        {
            _aloVoipTelephonyService.DestroyModule(deletedModuleId);
            return Response();
        }

        [HttpPost]
        [Route("Dial")]
        public IActionResult Dial(string tsKey, string extn, string dialNo, bool applyOutgoingRules)
        {
            return Response(_aloVoipTelephonyService.Dial(tsKey, extn, dialNo, applyOutgoingRules));
        }

        [HttpPost]
        [Route("GetBillableObjectTypeProps")]
        public IActionResult GetBillableObjectTypeProps(int lookupSourceId, string billableObjectTypeKey)
        {
            return Response(_aloVoipTelephonyService.GetBillableObjectTypeProps(lookupSourceId, billableObjectTypeKey));
        }

        [HttpPost]
        [Route("GetBillableObjectTypes")]
        public IActionResult GetBillableObjectTypes(int lookupSourceId)
        {
            return Response(_aloVoipTelephonyService.GetBillableObjectTypes(lookupSourceId));
        }

        [HttpPost]
        [Route("GetIndicators")]
        public IActionResult GetIndicators()
        {
            return Response(_aloVoipTelephonyService.GetIndicators());
        }

        [HttpPost]
        [Route("GetMoneyAccounts")]
        public IActionResult GetMoneyAccounts(int lookupSourceId)
        {
            return Response(_aloVoipTelephonyService.GetMoneyAccounts(lookupSourceId));
        }

        [HttpPost]
        [Route("GetRecordedFile")]
        public IActionResult GetRecordedFile(string tsKey, string fileName)
        {
            return Response(_aloVoipTelephonyService.GetRecordedFile(tsKey, fileName));

        }

        [HttpPost]
        [Route("GetTelephonyInfoById")]
        public IActionResult GetTelephonyInfoById(int tsId)
        {
            return Response(_aloVoipTelephonyService.GetTelephonyInfoById(tsId));
        }

        [HttpPost]
        [Route("GetTelephonyInfoByKey")]
        public IActionResult GetTelephonyInfoByKey(string tsKey)
        {
            return Response(_aloVoipTelephonyService.GetTelephonyInfoByKey(tsKey));
        }

        [HttpPost]
        [Route("InitModule")]
        public IActionResult InitModule(int moduleId)
        {
            _aloVoipTelephonyService.InitModule(moduleId);
            return Response();
        }

        [HttpPost]
        [Route("InitOperatorSos")]
        public IActionResult InitOperatorSos(int tsId)
        {
            _aloVoipTelephonyService.InitOperatorSos(tsId);
            return Response();
        }

        [HttpPost]
        [Route("ReBoot")]
        public IActionResult ReBoot()
        {
            _aloVoipTelephonyService.ReBoot();
            return Response();
        }

        [HttpPost]
        [Route("RegisterVoiceOnTelephonyServer")]
        public IActionResult RegisterVoiceOnTelephonyServer(string voiceName, byte[] voice)
        {
            _aloVoipTelephonyService.RegisterVoiceOnTelephonyServer(voiceName,voice);
            return Response();
        }

        [HttpPost]
        [Route("UnRegisterVoiceFromTelephonyServer")]
        public IActionResult UnRegisterVoiceFromTelephonyServer(string voiceName)
        {
            _aloVoipTelephonyService.UnRegisterVoiceFromTelephonyServer(voiceName);
            return Response();
        }
    }
}