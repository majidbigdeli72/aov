﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Agent.Application.Interfaces;
using Agent.Domain.Core.Bus;
using Agent.Domain.Core.Notifications;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Agent.Services.Api.Controllers
{
    public class AloVoIPInfoController : ApiController
    {
        private readonly IAloVoIPInfoService _aloVoIPInfoService;

        public AloVoIPInfoController(
            IAloVoIPInfoService aloVoIPInfoService, 
            INotificationHandler<DomainNotification> notifications,
            IMediatorHandler mediator) : base(notifications, mediator)
        {
            _aloVoIPInfoService = aloVoIPInfoService;
        }

        [HttpPost]
        [Route("IsFeatureAvailable")]
        public IActionResult IsFeatureAvailable(byte[] nm)
        {
           return Response(_aloVoIPInfoService.IsFeatureAvailable(nm));
        }

        [HttpPost]
        [Route("GetSerial")]
        public IActionResult GetSerial()
        {
            return Response(_aloVoIPInfoService.GetSerial());
        }

        [HttpPost]
        [Route("IsTrial")]
        public IActionResult IsTrial()
        {
            return Response(_aloVoIPInfoService.IsTrial());
        }

    }
}