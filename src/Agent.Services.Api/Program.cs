﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Agent.Domain.Classes;
using Agent.Domain.Helper;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Agent.Services.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
          //  new AloVoipAgent().Init();
            var webHost = CreateWebHostBuilder(args).Build();
            webHost.Services.Init();
            webHost.Services.InitializeDb();
            webHost.Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}
